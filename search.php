<div style="width:100%;" align="center">
<div style="width:960px; padding:90px 0; min-height:600px;" align="left">
<h4 style="text-align:left">Поиск по сайту:</h4>
<form enctype='multipart/form-data' name="search" id="search" method="POST" action="<?="http://".$_SERVER['SERVER_NAME'];?>/search/" style="margin-bottom:15px;">
<input value="<?=htmlspecialchars($arr_url[1])?>" id="search_str" name="search_str" placeholder="Введите строку для поиска" style="font-size:14pt; background:none; border:1px solid #666; padding:10px 15px; display:block; float:left; width:600px;" autocomplete="off">
<input type="submit" value="Найти" style="background:none; padding:10px; border:1px solid #666; height:44px; line-height:22px; cursor:pointer; float:left; display:block; border-left:none">
<div style="float:none; clear:both;"></div>
</form>
<?
require_once('./code/Stemming.php');
$kol = 0;
function returnme ($s,$a) {
	$s = preg_replace('({code}(.*){code})','',$s);
	$s = trim(strip_tags($s));
	$arr_separator = array('&nbsp;','-','<br>');
	$s = str_replace($arr_separator,' ',$s);
	$s = explode(' ',$s);
	$ss = '';
	$main = -1;
	for ($h=0;$h<count($s);$h++) { //Перебьор слов
		$est = 0;
		for ($l=0;$l<count($a);$l++) { //Перебьор искомых слов
			if (stristr(mb_strtolower($s[$h]), mb_strtolower($a[$l])) === FALSE) {
			} else $est = 1;			
		}
		if ($est==1) {$main = $h; break;}
	}
	
	if ($main<20) $start = 0; else $start = $main-20;
	$end = min($main+21,count($s));
	
	for ($h=$start;$h<$end;$h++) {
		if ($h==$main) {
			$ss = $ss."<b>".$s[$h]."</b> ";
		} else {
			$ss = $ss.$s[$h]." ";						
		}
	}
	
	if ($end != count($s)) $ss = $ss." ...";
	if ($start !=0) $ss = '... '.$ss;
	return $ss;
}

if (trim($arr_url[1])!="") {
	//Проверим орфографию
	$str = trim($arr_url[1]);
	$checker=json_decode(file_get_contents("http://speller.yandex.net/services/spellservice.json/checkText?text=".urlencode($str)));
	$checked_str=$str;
	if (!is_array($checker)) {
		$checker = array();
	}
	foreach($checker as $word) {
		$checked_str=str_replace($word->word,$word->s[0],$checked_str);
	}
	if(mb_strtolower($checked_str,'utf8')!=mb_strtolower($str,'utf8') && !empty($checked_str)) {
		echo "<b>Возможно вы имели ввиду: <a href=\"/search/$checked_str\">$checked_str</a></b><br><br>";
	}
	$stemming= new Stemming();
	$stem_str=$stemming->stem_string($str);
	
	$search_req = " AND ( ";
	$search_array = explode(" ", $stem_str);
	
	$search_str=preg_replace("/[^а-яА-Яa-zA-z0-9\-]/ui","%",$stem_str); //заменяем все знаки кроме цифр и букв на % (любое кол-во любых символов)
	
	// массив колонок по которым ищем и их коэффицент релевантности
	$search_columns=array(
	  'qf_title'=>'20',
	  'qf_url'=>'15',
	  'qf_znach'=>'10',
	  'qf_desc'=>'5',
	  'qf_key'=>'5'
	);
	
	$select =", ( 0";
	$search_req_arr=array();
	
 
foreach($search_columns as $col=>$coeff) {
  // полнотекстовый
  $select.= " + IF ($col LIKE '".$search_str."', $coeff*3, 0)";
  $search_req_arr[]= " $col LIKE '%".$search_str."%'";
 
  // для отдельного слова
  $word_coeff=round(($coeff/count($search_array)),2);
 
  foreach($search_array as $word) {
    $select .= "+ IF ($col LIKE '%".$word."%', ".$word_coeff.", 0)";
    $search_req_arr[] = "$col LIKE '%".$word."%'";
  }
}
$select.=") AS `relevant`";
$search_req .=implode(" OR ",$search_req_arr);
$search_req .= ")";
 
$query="SELECT qf_url, qf_title, qf_znach, qf_sort $select
FROM qf_pages
WHERE qf_sort>-1 $search_req
 UNION SELECT qf_url, qf_title, qf_znach, qf_sort $select FROM qf_news WHERE 1=1 $search_req ORDER BY relevant DESC LIMIT 0,50";

$kol = 0;
$q = $pdo->query($query);
while ($res=$q->fetch()) { $kol++;?>
<a href="http://<?=$_SERVER['HTTP_HOST']?>/<? if ($res['qf_sort']<0) echo 'blog/';?><?=$res['qf_url']?>" style="font-size:14pt;"><?=returnme($res['qf_title'],$search_array)?></a>
<p style="margin-bottom:30px; margin-top:5px;"><?=returnme($res['qf_znach'],$search_array)?><br><a href="http://<?=$_SERVER['HTTP_HOST']?>/<? if ($res['qf_sort']<0) echo 'blog/';?><?=$res['qf_url']?>" style="font-size:10pt;">http://<?=$_SERVER['HTTP_HOST']?>/<? if ($res['qf_sort']<0) echo 'blog/';?><?=$res['qf_url']?></a></p>

<? }
}

if ($kol==0 && $arr_url[1]!="") {?>
<p>По вашему запросу ничего не найдено</p>
<? } ?>

</div>
<? include 'footer.php';?>
</div>