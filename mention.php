<!-- <div class="bg" style="width:100%; padding-top:69px; background-image:url(/img/top_mention.jpg)" align="center"><div style="padding:100px; width:960px; color:#FFF; font-size:18pt;">
<? $q = $pdo->query("SELECT * FROM qf_mentions WHERE qf_type=1 ORDER BY RAND() LIMIT 1");
while ($res = $q->fetch()) {
?>
<p style="margin:15px 0 30px 0">«<?=$res['qf_txt']?>»</p>
<p style="margin:0px; font-size:14pt"><?=$res['qf_footer']?></p>
<? } ?>
</div></div> -->

<div align="center"><div style="width:960px; margin-top:75px">
<h1 style="font-size:24pt; padding:30px 0; margin:0px;">Отзывы наших клиентов</h1>

<a href="/mention/" class="ah1<? if ($arr_url[1]!="video") echo '_a';?>">Текст</a>
<a href="/mention/video/" class="ah1<? if ($arr_url[1]=='video') echo '_a';?>">Видео</a>

<div id="container">
<?
$t = 0;
if ($arr_url[1]=="video") $t = 1;
$total_rows = ceil($pdo->query('SELECT qf_id FROM qf_mentions')->rowCount()/4);
$total_rows_video = ceil($pdo->query('SELECT qf_id FROM qf_mentions WHERE qf_type = 2')->rowCount()/4);
$total_rows_text = ceil($pdo->query('SELECT qf_id FROM qf_mentions WHERE qf_type = 1')->rowCount()/4);

if ($r_page!="") $r_page=$r_page; else $r_page = 1;
$our_page=$r_page*4-4;

if ($t==1) {
  $q = $pdo->query("SELECT * FROM qf_mentions WHERE qf_type = 2 ORDER BY qf_sort DESC LIMIT ".$our_page.",4");
} else {
  $q = $pdo->query("SELECT * FROM qf_mentions WHERE qf_type = 1 ORDER BY qf_sort DESC LIMIT ".$our_page.",4");
}

while ($res = $q->fetch()) {
	if ($arr_url[1]=='video' && $res['qf_type'] != 1) {
?>
<div class="otziv">
	<div class="video"><?=$res['qf_video']?></div>
    <div class="podpis">
    	<?=$res['qf_footer']?><br>
        <a href="http://<?=$res['qf_link']?>" target="_blank"><?=$res['qf_link']?></a>    	        
    </div>
    <!-- <a href="#m<?=$res['qf_id']?>" rel="mention" class="showmen">Читать полностью</a> -->
</div>
<? } else {?>
	<div class="otziv">  
    <div class="mt">
    	<div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/mentions/<?=$res['qf_id']?>.jpg"></div>
        <div class="word"><?=$res['qf_txt']?></div>
        <a href="#m<?=$res['qf_id']?>" rel="mention" class="showmen">Читать полностью</a>
    </div>
	<div class="podpis">
    	<?=$res['qf_footer']?><br>
        <a href="http://<?=$res['qf_link']?>" target="_blank"><?=$res['qf_link']?></a>    	
    </div>
</div>
<? } 
}?>
<div style="clear:both; float:none;"></div>
</div>

<noindex>
<div id="forZomm" style="display:none;">
  <h1><? $res ?></h1>
<? $q = $pdo->query("SELECT * FROM qf_mentions WHERE qf_type = 1 ORDER BY qf_sort DESC LIMIT ".$our_page.",4");
while ($res = $q->fetch()) {
  if ($res['qf_type']==2) {
?>
<div class="otziv2" id="m<?=$res['qf_id']?>">
  <div class="video"><?=$res['qf_video']?></div>
    <div class="podpis">
      <?=$res['qf_footer']?><br>
        <a href="http://<?=$res['qf_link']?>" target="_blank"><?=$res['qf_link']?></a>      
    </div>
</div>
<? } else {?>
  <div class="otziv2" id="m<?=$res['qf_id']?>">  
    <div class="mt">
      <div class="logo"><img src="http://<?=$_SERVER['HTTP_HOST']?>/mentions/<?=$res['qf_id']?>.jpg"></div>
        <div class="word"><?=$res['qf_txt']?></div>
    </div>
  <div class="podpis">
      <?=$res['qf_footer']?><br>
        <a href="http://<?=$res['qf_link']?>" target="_blank"><?=$res['qf_link']?></a>      
    </div>
</div>
<? } 
}?>
<div style="clear:both; float:none;"></div>
</div>
</div></div>
</noindex>

<? if ($arr_url[1]=='video') {?>
<div style="padding:30px;" align="center">
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="150" align="left"><? if ($r_page>1) {?><a class="arrow_l" href="/mention/video/<?=$r_page-1?>"><< Страница <?=$r_page-1?></a><? } ?>&nbsp;</td>
    <td align="center">
    <?for ($i = 0; $i < $total_rows_video; $i++) {?>
        <a href="/mention/video/<?=$i+1?>"?> 
          <?if (($i+1 == $arr_url[2]) || ($i==0 && $arr_url[2]=="")) {?>
            <u><?=$i+1?></u>
          <?} else {?>
            <?=$i+1?>
          <?}?>
        </a>
        <?if(($i+1)!=$total_rows_video) { ?>, <?}?>
      <?}?>
    </td>
    <td width="150" align="right"><? if ($r_page<$total_rows_video) {?><a class="arrow_r" href="/mention/video/<?=$r_page+1?>">Страница <?=$r_page+1?> >></a><? } ?></td>
  </tr>
</table>
</div>
<? } else { ?>
<div style="padding:30px;" align="center">
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="150" align="left"><? if ($r_page>1) {?><a class="arrow_l" href="/mention/<?=$r_page-1?>"><< Страница <?=$r_page-1?></a><? } ?>&nbsp;</td>
    <td align="center">
      <?for ($i = 0; $i < $total_rows_text; $i++) {?>
        <a href="/mention/<?=$i+1?>"?> 
          <?if (($i+1 == $arr_url[1]) || ($i==0 && $arr_url[1]=="")) {?>
            <u><?=$i+1?></u>
          <?} else {?>
            <?=$i+1?>
          <?}?>
        </a>
        <?if(($i+1)!=$total_rows_text) { ?>, <?}?>
      <?}?>
    </td>
    <td width="150" align="right"><? if ($r_page<$total_rows_text) {?><a class="arrow_r" href="/mention/<?=$r_page+1?>">Страница <?=$r_page+1?> >></a><? } ?></td>
  </tr>
</table>
</div>
<? } ?>

<div style="padding:30px; margin-top:30px; background:#f4f4f4;" align="center">
<? include './code/logos.php';?>
</div>

<div class="bg" style="padding:30px 0 0px 0; background-image:url(/img/mention.jpg); color:#fff; font-size:24pt;" align="center">
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" align="center" valign="middle"><b>НАШИ ПАРТНЕРЫ</b><br>по России и СНГ</td>
    </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">Дальневосточный округ<div class="count"><span>66</span><br>городов</div></td>
    <td align="center" valign="middle">Приволжский округ
      <div class="count"><span>198</span><br>
      городов</div></td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">Северо-Западный округ
      <div class="count"><span>146</span><br>городов</div></td>
    <td align="center" valign="middle">Северо-Кавказский округ
      <div class="count"><span>56</span><br>городов</div></td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">Сибирский округ
      <div class="count"><span>130</span><br>городов</div></td>
    <td align="center" valign="middle">Уральский округ
      <div class="count"><span>115</span><br>городов</div></td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">Центральный округ
      <div class="count"><span>310</span><br>городов</div></td>
    <td align="center" valign="middle">Южный округ
      <div class="count"><span>79</span><br>городов</div></td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="middle">СНГ
      <div class="count"><span>80</span><br>городов</div></td>
    </tr>
</table>
<div style="margin-top:30px">
<? include 'footer.php';?>
</div>
</div>
<script>
$(document).ready(function(e) {
    $("a[rel=mention]").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200
	});
	
	arr = $('.otziv .mt');
  console.log(arr, 'array');
	for (i=0;i<arr.length;i++) {
		ob = $(arr[i]).children('.logo');
		ob2 = $(arr[i]).children('.word');
		h = $(ob).height() + $(ob2).height();
		if (h+20>275) {
			oba = $(arr[i]).children('.showmen');
			$(oba).css({'display':'block'});
		}
		
	}
});
</script>