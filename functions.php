<?

include __DIR__."/mail/libmail.php";

function sendmail($sub,$text,$to,$f = false, $save = true) {
	global $arr_g;
	global $pdo;
	
	$m= new Mail();  // можно сразу указать кодировку, можно ничего не указывать ($m= new Mail;)
	$m->From( "Ceramic 3D;".$arr_g['smtp']['login'] ); // от кого Можно использовать имя, отделяется точкой с запятой
	$m->ReplyTo( 'Ceramic 3D;'.$arr_g['smtp']['login'] ); // куда ответить, тоже можно указать имя
	if($f) {
		for($k = 0; $k < count($f); $k++){
			$m->Attach($f[$k], "", "", "attachment");
		}
	}
	$m->To($to);   // кому, в этом поле так же разрешено указывать имя
	$m->Subject($sub);
	$m->Body($text,"html");
	$m->Priority(3) ;	// установка приоритета
	if ($arr_g['smtp']['pass']!="") {
		$m->smtp_on($arr_g['smtp']['server'],$arr_g['smtp']['login'],$arr_g['smtp']['pass'], $arr_g['smtp']['port'], 10); // используя эу команду отправка пойдет через smtp
	}
	$m->log_on(false); // включаем лог, чтобы посмотреть служебную информацию
	$m->Send();	// отправка
	
	if ($save) {
		$today = getdate();
		$q = $pdo->prepare("INSERT INTO qf_mail (qf_time, qf_sub, qf_txt, qf_to) VALUES (?,?,?,?)");
		$q->execute(array($today[0],$sub,$text,$to));
	}
}

function numberFormat($value, $z = 2, $simvol = '&#8381;'){
	if($value != 0)
		return number_format($value, $z, ',', " ")." ".$simvol;
	else
		return '—';
}

function rus2translit($string){
    $converter = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => '', 'ы' => 'y', 'ъ' => '',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

function str2url($str){
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = str_replace('-', ' ', $str);
    $str = preg_replace('~[^-a-z0-9_\.]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    $str = trim($str, ".");
    return $str;
}

function p_($t) {
	echo '<pre>';
	print_r($t);
	echo '</pre>';
}
?>