<?php
// Version: 1.1; ManagePermissions

$txt['permissions_title'] = '���������� ������� ������� �������������';
$txt['permissions_modify'] = '��������';
$txt['permissions_access'] = '������';
$txt['permissions_allowed'] = '���������';
$txt['permissions_denied'] = '���������';

$txt['permissions_switch'] = '����������� ��';
$txt['permissions_global'] = '����������';
$txt['permissions_local'] = '���������';

$txt['permissions_groups'] = '����� ������� �� �������';
$txt['permissions_all'] = '���';
$txt['permissions_none'] = '���';
$txt['permissions_set_permissions'] = '����������';

$txt['permissions_with_selection'] = '� ����������';
$txt['permissions_apply_pre_defined'] = '��������� ����������������� ������� ���� �������';
$txt['permissions_select_pre_defined'] = '�������� �������';
$txt['permissions_copy_from_board'] = '���������� ����� ������� � ����� �������';
$txt['permissions_select_board'] = '�������� ������';
$txt['permissions_like_group'] = '���������� ����� ������� ��� � ���� ������';
$txt['permissions_select_membergroup'] = '������� ������';
$txt['permissions_add'] = '���������';
$txt['permissions_remove'] = '�������� ����� �������';
$txt['permissions_deny'] = '���������';
$txt['permissions_select_permission'] = '�������� ����� �������';

// All of the following block of strings should not use entities, instead use \\" for &quot; etc.
$txt['permissions_only_one_option'] = '�� ������ ������� ������ ���� �������� ��� ��������� ���� �������';
$txt['permissions_no_action'] = '�������� �� �������';
$txt['permissions_deny_dangerous'] = '�� ����������� ��������� ���� ��� ��������� ��������.\\n��� ��� ��� ���������, ���� �� �������� �� ������� ����-�� �����.\\n\\n�� ������������� ������ ����������?';

$txt['permissions_boards'] = '����� ������� �� ��������';

$txt['permissions_modify_group'] = '��������� ���� �������������';
$txt['permissions_general'] = '�������� �����';
$txt['permissions_board'] = '����� ��� �������� � ����������� ������������';
$txt['permissions_commit'] = '���������';
$txt['permissions_modify_local'] = '�������� ��������� ����� �������';
$txt['permissions_on'] = '� �������';
$txt['permissions_local_for'] = '��������� ����� ������� ��� �������';
$txt['permissions_option_on'] = '+';
$txt['permissions_option_off'] = '-';
$txt['permissions_option_deny'] = 'X';
$txt['permissions_option_desc'] = '� �������� ���� ������� �� ������ ���������� ��������� ����� \'���������\' (+), \'���������\' (-), ��� <span style="color: red;">\'���������\' (X)</span>.<br /><br />����������� �������� ���� ������� � �������������.';

$txt['permissiongroup_general'] = '�����';
$txt['permissionname_view_stats'] = '�������� ���������� ������';
$txt['permissionhelp_view_stats'] = '���������� ������������� ���������� ����� ���������� � ������, ����� ���, ����� ���������� �������������, ���������� ����������� ���������, ��������� ��� � �.�.';
$txt['permissionname_view_mlist'] = '�������� ������ �������������';
$txt['permissionhelp_view_mlist'] = '������ ������������� ���������� ���� �������������, ������������������ �� ������. ������ ����� ���� ������������ �� ������ �������. ������ ������������� �������� � ������� �������� � �� �������� �� ����������� ������.';
$txt['permissionname_who_view'] = '�������� ��� Online';
$txt['permissionhelp_who_view'] = '������ ��� Online ���������� �������������, ������� � ������ ������ ��������� �� ������, � ����� �� ������� �������� ��� ��������������. ��� ����� ������� ����� �������� ������ ���� �� �������� �� � \'��������������� � ����������\'. ����� ����������� ���� ������, ������� �� ������ ��� Online, �� ������� ��������. ���� ���������, ������������ ������ ������������� ������ ��� Online, �� �� ����� ������, ��� ��������� � ��� ������ ��������� ������������.';
$txt['permissionname_search_posts'] = '����� ��������� � ���';
$txt['permissionhelp_search_posts'] = '������ ����� ��������� ������������� ������������ ������� ������ �� ������. ���� ��������� ��� �������, �� ������� �������� ������������ ������ ������ �����.';
$txt['permissionname_karma_edit'] = '��������� ����� �������������';
$txt['permissionhelp_karma_edit'] = '����� ��� �������������� ����������� ������, ������� ���������� ������������ ������������ �� ������. ������ ����� ������� ����� ��������, ������ ���� �� �������� ������� �����, � ���������� ������ ������. ��� ������, ������ ����� ������� ���������� ����������.';

$txt['permissiongroup_pm'] = '������ ���������';
$txt['permissionname_pm_read'] = '������ ������ ���������';
$txt['permissionhelp_pm_read'] = '������ ����� ������� ���� ������������� ������ � ������ ������ ���������. ��� ���� ����, ������������ �� ������ ������ ������ ���������.';
$txt['permissionname_pm_send'] = '�������� ������ ���������';
$txt['permissionhelp_pm_send'] = '������ ����� ������� ���� ������������� ����������� ���������� ������ ���������. ���������� ����� ��� ������ ������ ���������.';

$txt['permissiongroup_calendar'] = '���������';
$txt['permissionname_calendar_view'] = '�������� ���������';
$txt['permissionhelp_calendar_view'] = '��������� ���������� ��� ��������, ��������� ��� ������� � ������ ������ ����. ��� ����� ������� ���� ����������� ������������� ���������. ����� ���� ��� �� �������� ��� �����, ������������ ������ ������ ��������� � ������ ������ ������. �� �������� �������� ������� ��������� � ���������� ������.';
$txt['permissionname_calendar_post'] = '�������� ������� � ���������';
$txt['permissionhelp_calendar_post'] = '������� ��� ��������� ����, ������� ��������� � ������������� ��� ���������. ������� ����� ���� �������, ���� ������������ ����� ����� �������� ����� ��� �� ������.';
$txt['permissionname_calendar_edit'] = '�������������� ������� � ���������';
$txt['permissionhelp_calendar_edit'] = '������� ����� ���� ���������������, �������� �� ������� ��������� (*). ��� ����������� �������������� �������, ������������ ������ ����� ����� ������������� ������ ��������� � ����.';
$txt['permissionname_calendar_edit_own'] = '����������� �������';
$txt['permissionname_calendar_edit_any'] = '����� �������';

$txt['permissiongroup_maintenance'] = '����������������� ������';
$txt['permissionname_admin_forum'] = '����������������� ������ � ���� ������';
$txt['permissionhelp_admin_forum'] = '��� ����� ��������� ������������:<ul><li>�������� ��������� ������, ���� ������ � ��� ���������� </li><li>��������� �������� �����������</li><li> ������������ ������� ������������ ������</li><li> ������������� ������ ������ � ��� ������������� ��������</li></ul> ����������� ��� ����� ������� � �������������. ��� ���� ����� ������� ���������� �� ������.';
$txt['permissionname_manage_boards'] = '���������� ��������� � �����������';
$txt['permissionhelp_manage_boards'] = '��� ����� ������� ��������� ������������� ���������, ������������� � ������� ������� � ��������� �� ������.';
$txt['permissionname_manage_attachments'] = '���������� ���������� � ���������';
$txt['permissionhelp_manage_attachments'] = '��� ����� ������� ��������� ������������� ��������� ���������� � ��������� �� ������.';
$txt['permissionname_manage_smileys'] = '���������� ��������';
$txt['permissionhelp_manage_smileys'] = '��� ����� ������� ��������� ������������� ��������� �������� ������. �������, ���������, �������������, � ��� �� ��������� ����� ������ �������';
$txt['permissionname_edit_news'] = '�������������� ��������';
$txt['permissionhelp_edit_news'] = '��� ����� ��������� ������������� ��������� ��������� ������. ������� �������� ������ ���� �������� � ���������� ������.';

$txt['permissiongroup_member_admin'] = '����������������� �������������';
$txt['permissionname_moderate_forum'] = '���������� �������������� ������';
$txt['permissionhelp_moderate_forum'] = '��� ����� ������� �������� ��� ������ ������� �������������, ����� ���:<ul><li>������ � ��������� �����������</li><li>�������� � �������� �������������</li><li>�������� ���������� �������������, ���������� �������� IP ������</li><li>��������� ������� ������</li><li>��������� ����������� �� ��������� ������� ������ � ����������� �������� ������� ������</li><li>�������� ������ ��������� �������������, ������� ���������� �� ��������� ������ ���������</li><li>������ �������</li></ul>';
$txt['permissionname_manage_membergroups'] = '���������� �������� �������������';
$txt['permissionhelp_manage_membergroups'] = '��������� ������������� ������������� ������ �������������, � ��� �� �������� ������������� � ��� ������.';
$txt['permissionname_manage_permissions'] = '���������� ������� �������';
$txt['permissionhelp_manage_permissions'] = '��� ����� ��������� ������������� ������ ����� ������� � ����� �������������.';
$txt['permissionname_manage_bans'] = '�������������� ��� �����';
$txt['permissionhelp_manage_bans'] = '��� ����� ��������� ������������� ������������� ��� ����. ���� ����������� ������� ��� ������� ��������� � ������ �� ���������� �������������.';
$txt['permissionname_send_mail'] = '�������� email �������������';
$txt['permissionhelp_send_mail'] = '��� ����� ��������� ������ �������� �������� ��������� �������������, ���� ��������� ������� �������������. ����� ���������� email ��� ������ ���������.';

$txt['permissiongroup_profile'] = '������� �������������';
$txt['permissionname_profile_view'] = '�������� �������� �������������';
$txt['permissionhelp_profile_view'] = '��� ����� ��������� ������������� ������� ������������������ �� ������ �������������. �������� ����� ����������, ���������� � ��� ��������� ������������.';
$txt['permissionname_profile_view_own'] = '����������� �������';
$txt['permissionname_profile_view_any'] = '����� �������';
$txt['permissionname_profile_identity'] = '��������� �������� ������� ������';
$txt['permissionhelp_profile_identity'] = '��������� ������� ������ �������� � ���� �������� ���������, ����� ��� ��������� ������, email ������, ����� � �.�.';
$txt['permissionname_profile_identity_own'] = '����������� �������';
$txt['permissionname_profile_identity_any'] = '����� �������';
$txt['permissionname_profile_extra'] = '�������������� �������������� �������� �������';
$txt['permissionhelp_profile_extra'] = '�������������� ��������� ������� ������ �������� � ���� ��������� �������, ��� ����������, ����������� � ������ ���������.';
$txt['permissionname_profile_extra_own'] = '����������� �������';
$txt['permissionname_profile_extra_any'] = '����� �������';
$txt['permissionname_profile_title'] = '��������� ������� ��� ��������';
$txt['permissionhelp_profile_title'] = '������ ������� ������������ � ������ ����, ��� �������� ������� ������������, �����������, ���� ��� ���� �� ���� ��������� ������.';
$txt['permissionname_profile_title_own'] = '����������� �������';
$txt['permissionname_profile_title_any'] = '����� �������';
$txt['permissionname_profile_remove'] = '�������� ������� ������';
$txt['permissionhelp_profile_remove'] = '��� ����� ��������� ������������� ������� �� ����������� ������� ������ � ������.';
$txt['permissionname_profile_remove_own'] = '�����������';
$txt['permissionname_profile_remove_any'] = '�����';
$txt['permissionname_profile_server_avatar'] = '������������� �������� ������';
$txt['permissionhelp_profile_server_avatar'] = '��� ����� ��������� ������������� ������������ �������, ������� ����������� �� ����� ������.';
$txt['permissionname_profile_upload_avatar'] = '�������� �������� �� ������';
$txt['permissionhelp_profile_upload_avatar'] = '��� ����� �������� ������������� ��������� ���� ����������� ������� �� ������.';
$txt['permissionname_profile_remote_avatar'] = '��������� ��������� ������';
$txt['permissionhelp_profile_remote_avatar'] = '��� ����� �������� ������������� ��������� ������ �� �������, ������������� �� ������ �������. � ����� ������������, �� ����� ��������� ������������ ������ ������� ������������� �������������.';

$txt['permissiongroup_general_board'] = '�����';
$txt['permissionname_moderate_board'] = '������������� �������';
$txt['permissionhelp_moderate_board'] = '��� ����� ��������� ��������� ��������� ������� ������������� � ��������. ��������, ����� � �������� ����, ��������� ������� ��������� ����������� � �������� ����������� �����������.';

$txt['permissiongroup_topic'] = '����';
$txt['permissionname_post_new'] = '�������� ����� ���';
$txt['permissionhelp_post_new'] = '��� ����� ��������� ������������� ��������� ����� ����. �� ���������, ��� �� ��������� �������� � ����. �� ���� ���� � ������������ ��� ���� �������� � ����, �� ������ ������ �� �������.';
$txt['permissionname_merge_any'] = '����������� ���';
$txt['permissionhelp_merge_any'] = '��� ����� ��������� ������������� ���������� ��� ���� � ����. ������� ����� ��������� ��, � ������� ������ ��������� ������� ������ �� �������.';
$txt['permissionname_split_any'] = '���������� ���';
$txt['permissionhelp_split_any'] = '��� ����� ��������� ������������� ��������� ����';
$txt['permissionname_send_topic'] = '�������� ��� �������';
$txt['permissionhelp_send_topic'] = '��� ����� ��������� ������������� ���������� ������ �� ���� ����� �������.';
$txt['permissionname_make_sticky'] = '������������ ���';
$txt['permissionhelp_make_sticky'] = '��� ����� ��������� ������������� ����������� ����.';
$txt['permissionname_move'] = '����������� ���';
$txt['permissionhelp_move'] = '��� ����� ��������� ���������� ���� �� ������ ������� � ������.';
$txt['permissionname_move_own'] = '����������� ����';
$txt['permissionname_move_any'] = '����� ����';
$txt['permissionname_lock'] = '�������� ���';
$txt['permissionhelp_lock'] = '��� ����� ��������� ������������� ��������� ����. ����� ����� � ��� ����� �������� ������ ��������� ��� �������������.';
$txt['permissionname_lock_own'] = '����������� ����';
$txt['permissionname_lock_any'] = '����� ����';
$txt['permissionname_remove'] = '�������� ���';
$txt['permissionhelp_remove'] = '��� ����� ��������� ������������� ������� ����.';
$txt['permissionname_remove_own'] = '����������� ����';
$txt['permissionname_remove_any'] = '����� ����';
$txt['permissionname_post_reply'] = '�������� ��������� � ����';
$txt['permissionhelp_post_reply'] = '��� ����� ��������� ������������� �������� � ����';
$txt['permissionname_post_reply_own'] = '����������� ����';
$txt['permissionname_post_reply_any'] = '����� ����';
$txt['permissionname_modify_replies'] = '�������������� ����� ������� � ����������� ����';
$txt['permissionhelp_modify_replies'] = '��� ����� ��������� ������ ���� �������� ������ � ����������� ����.';
$txt['permissionname_delete_replies'] = '�������� ����� ������� � ����������� ����';
$txt['permissionhelp_delete_replies'] = '��� ����� ��������� ������ ���� ������� ������ � ����������� ����.';
$txt['permissionname_announce_topic'] = '���������� ������������� � ����';
$txt['permissionhelp_announce_topic'] = '��� ����� ��������� ���������� ����������� � ���� �� email ������������������ ������������� ��� ������ ��������� ������� �������������.';

$txt['permissiongroup_post'] = '���������';
$txt['permissionname_delete'] = '�������� ���������';
$txt['permissionhelp_delete'] = '��� ����� ��������� ������������� ������� ��������� � �����, ����� ������ ������� ���������.';
$txt['permissionname_delete_own'] = '����������� ���������';
$txt['permissionname_delete_any'] = '����� ���������';
$txt['permissionname_modify'] = '�������������� ���������';
$txt['permissionhelp_modify'] = '�������������� ���������';
$txt['permissionname_modify_own'] = '����������� ���������';
$txt['permissionname_modify_any'] = '����� ���������';
$txt['permissionname_report_any'] = '���������� �����������';
$txt['permissionhelp_report_any'] = '��� ����� �������� � ������ ������ ������ ��� ���������� �����������. ����� ����������, ��� ���������� ������� �������� ����������� �� email �� ������� � ������������.';

$txt['permissiongroup_poll'] = '�����������';
$txt['permissionname_poll_view'] = '�������� �����������';
$txt['permissionhelp_poll_view'] = '��� ����� ��������� ������������� ������������� �����������. ��� ����� ����� ��� ������ ���� ���� (��� �����������).';
$txt['permissionname_poll_vote'] = '����������� ����������';
$txt['permissionhelp_poll_vote'] = '��� ����� ��������� ������������������ ������������� ���������� � ������������.';
$txt['permissionname_poll_post'] = '�������� �����������';
$txt['permissionhelp_poll_post'] = '��� ����� ��������� ������������� ��������� �����������.';
$txt['permissionname_poll_add'] = '���������� ����������� � ����';
$txt['permissionhelp_poll_add'] = '��� ����� ��������� ��������� ����������� � ����, ������� ��� ���� �������. ��� ����� ������� ����� �������������� ������� ��������� � ����.';
$txt['permissionname_poll_add_own'] = '����������� ����';
$txt['permissionname_poll_add_any'] = '����� ����';
$txt['permissionname_poll_edit'] = '�������������� �����������';
$txt['permissionhelp_poll_edit'] = '��� ����� ��������� ������������� �������� ������� � ����������� � ���������� ������� �������. ��� ������� ����� ���������� ������������ ��������� � ����� �����������, ������������ ������ ����� �����  \'������������� �������\'.';
$txt['permissionname_poll_edit_own'] = '����������� �����������';
$txt['permissionname_poll_edit_any'] = '����� �����������';
$txt['permissionname_poll_lock'] = '�������� �����������';
$txt['permissionhelp_poll_lock'] = '��� ����� ��������� ������������� ��������� �����������.';
$txt['permissionname_poll_lock_own'] = '����������� �����������';
$txt['permissionname_poll_lock_any'] = '����� �����������';
$txt['permissionname_poll_remove'] = '�������� �����������';
$txt['permissionhelp_poll_remove'] = '��� ����� ��������� ������������� ������� �����������.';
$txt['permissionname_poll_remove_own'] = '����������� �����������';
$txt['permissionname_poll_remove_any'] = '����� �����������';

$txt['permissiongroup_notification'] = '�����������';
$txt['permissionname_mark_any_notify'] = '��������� ����������� � ����� �������';
$txt['permissionhelp_mark_any_notify'] = '��� ����� ��������� ������������� �������� ����������� � ����� ���������� � ����.';
$txt['permissionname_mark_notify'] = '��������� ����������� � ����� �����';
$txt['permissionhelp_mark_notify'] = '��� ����� ��������� ������������� �������� ����������� � ����� �����.';

$txt['permissiongroup_attachment'] = '��������';
$txt['permissionname_view_attachments'] = '�������� ��������';
$txt['permissionhelp_view_attachments'] = '�������� - ��� �����, ������� ������������ ��������� � ������ ���������. ��� ����� ��������� ������������� ��������, ��������� ��������������.';
$txt['permissionname_post_attachment'] = '�������� ��������';
$txt['permissionhelp_post_attachment'] = '�������� - ��� �����, ������� ������������ ��������� � ������ ���������. ��� ����� �������� ����������� �������� � ���������. �� ���� ��������� ����� ���� ��������� ��������.';

$txt['permissionicon'] = '';

$txt['permission_settings_title'] = '��������� ���� �������';
$txt['groups_manage_permissions'] = '������ �������������, ������� ����� �������� ����� �������';
$txt['permission_settings_submit'] = '���������';
$txt['permission_settings_enable_deny'] = '�������� ������������� ����������� ���� ��� �����';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['permission_disable_deny_warning'] = '���������� ���� ������� ������� ��� ����������� ����� �������������.';
$txt['permission_by_membergroup_desc'] = '����� �� ������ ���������� ���������� ����� ��� ������ ������. ��� ����� ����� ����������� � ����� �������, ����� ��� �� ������� ���������������� ��������� ����� �������.';
$txt['permission_by_board_desc'] = '����� �� ������ ���������� ����� ����� ����� ������������ ������, ���������� ��� ���������. ������������� ��������� ���������� �������������, ��� ��� ����� ������������� � ���� ������� ��������� ���� ������������ �����, �������� ������������ �� ����������.';
$txt['permission_settings_desc'] = '����� �� ������ �������, ��� ����� ����� �������� ����� � ��������.';
$txt['permission_settings_enable_postgroups'] = '�������� ������������� ���� ��� ����� ���������� �� ���������� ���������';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['permission_disable_postgroups_warning'] = '���������� ���� ������� ������� ��� ������������ ����� ������� ��� ����� ���������� �� ���������� ���������.';
$txt['permission_settings_enable_by_board'] = '�������� ����������� ��������� ���� ������� ��� ������� �������';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['permission_disable_by_board_warning'] = '���������� ���� ����� ������� ��� ������������ ����� ������� ��� �������.';

?>