<?php
// Version: 1.1.8; Packages

$txt['smf154'] = '����������';
$txt['smf160'] = '����� ������ ������� ���������, �� ��� ��������� ���������� ������ ���������� ��������� PHP ���, ������� ������������ ������ � ������� �����������';
$txt['smf161'] = '���������';
$txt['smf163'] = '���������';
$txt['smf173'] = '�����:';
$txt['smf174'] = '�������������';
$txt['smf175'] = '����������/�������������� �������';
$txt['smf180'] = '������ ������ � ������';
$txt['smf181'] = '����� � ������';
$txt['smf182'] = '�������� �����';
$txt['smf183'] = '������� ������� �����������';
$txt['smf184'] = '�����';
$txt['smf185'] = '�������� ������';
$txt['smf186'] = '�������� �������';
$txt['smf187'] = 'URL';
$txt['smf189'] = '������ ����������� �����������.';
$txt['smf190'] = '�������';
$txt['smf192'] = '����� ������� ��������';
$txt['smf193'] = '����� ��� ������� ��������';
$txt['smf198'] = '�������� ������� �����������';
$txt['smf159b'] = '���������� �����';
$txt['smf162b'] = '����� ������ ������� ���������, �� ��� ���������� ������ ���������� ��������� SQL ������, ������� ������ ��������� � ���� ���� ������.';
$txt['smf163b'] = '���������';
$txt['smf174b'] = 'SQL �������';
$txt['smf189b'] = '��� ������������� ������� �����������';
$txt['smf188b'] = '����� ������������� �������';
$txt['smf198b'] = '�������';
$txt['smf198d'] = '������� ������ �������';
$txt['smf198h'] = '��������, PHP �� ����� ������� ���������� � SAFE MODE. ��� ������� ������������ � SAFE MODE. ��������.';
$txt['smf198i'] = '��� ����� �����������.';

$txt['download_packages'] = '��������� �������';
$txt['installed_packages'] = '������������� ������';
$txt['package_settings'] = '���������';
$txt['package_manager_desc'] = '� ������� ��������� �������, �� ������ ��������� � ���������� ��������� ������ ����������� ��� ������ ������.';
$txt['installed_packages_desc'] = '� ���� ������� �� ������ ����������� ������������� �� ����� ������ ������ �����������, ���� ������� ��, ������� ��� ������ �� �����.';
$txt['download_packages_desc'] = '� ���� ������� �� ������ ������� ����� ��� �������� � ���������� ���.';

$txt['package1'] = '�������� ������� �����������';
$txt['package3'] = '����� �������';
$txt['package4'] = '������� ����� �����';
$txt['package5'] = '��������� ����� �������';
$txt['package6'] = '�������� � �������� ������������� �������';
$txt['package7'] = '������ �����������';
$txt['package8'] = '�������';
$txt['package9'] = '�������� �����';
$txt['package10'] = '����������� �����';
$txt['package11'] = '����������';
$txt['package12'] = '�������� �������';
$txt['package13'] = '�������� ����';
$txt['package14'] = '����������� �����';
$txt['package15'] = '�������';
$txt['package24'] = '��� ������';
$txt['package34'] = '�������������';
$txt['package37'] = '����������';
$txt['package39'] = '������� ���� ���������, ������ �� ������ �� ������������.';
$txt['package41'] = '�������� ����� ��� ����������, ������ �� ������ ��� ������������.';

$txt['pacman2'] = '�������� ������';
$txt['pacman3'] = '������';
$txt['pacman4'] = '�����';
$txt['pacman6'] = '�������� ������';
$txt['pacman8'] = '��� ��������';
$txt['pacman9'] = '��������';
$txt['pacman10'] = '������������ �����';

$txt['package_installed_key'] = '������������� ������';
$txt['package_installed_current'] = '��������� ������';
$txt['package_installed_old'] = '���������� ������';
$txt['package_installed_warning1'] = '���� ����� ��� ���������� �� ����� ������!';
$txt['package_installed_warning2'] = '�� ������ ������� ������ ������ ������ ��� ��������� ������ ������� ���������� ��� ����� ������.';
$txt['package_installed_warning3'] = '����������, �� ��������� ������ ��������� ����� ����� ������ � ���� ������ ����� ��� ��� ������������� ������ �����������.';
$txt['package_installed_extract'] = '���������� ������';
$txt['package_installed_done'] = '����� ��� ������� ����������. ������ �� ������ ������������ ��������������� �� ������� ��� ���������.';
$txt['package_installed_redirecting'] = '���������������...';
$txt['package_installed_redirect_go_now'] = '�������';
$txt['package_installed_redirect_cancel'] = '��������� � �������� �������';

$txt['packages_latest'] = '��������� ������ � �������';
$txt['packages_latest_fetch'] = '���������� �������� ���������� � ��������� � ����� ���������� ������� � www.simplemachines.org...';

$txt['package_upgrade'] = '��������';
$txt['package_install_readme'] = '���������� � ������';
$txt['package_install_type'] = '���';
$txt['package_install_action'] = '����';
$txt['package_install_desc'] = '��������';
$txt['package42'] = '������������ ��������';
$txt['package44'] = '��������� ����� ������ ���������� ��������� ��������:';
$txt['package45'] = '�����, ������� �� ��������� ����������, ���������, ���� �� ��������� � ���� ������� SMF.';
$txt['package50'] = '�������';
$txt['package51'] = '�����������';
$txt['package52'] = '������� ����';
$txt['package53'] = '����������';
$txt['package54'] = '�����';
$txt['package55'] = '����������';
$txt['package56'] = '��������� �����';
$txt['package57'] = '���������� ����';

$txt['package_install_actions'] = '������������ ��������� �������';
$txt['package_will_fail_title'] = '������ � ��������� ������';
$txt['package_will_fail_warning'] = '�� ������� ����, ���� ������ �������� ��� �������� ��������� ������.
        <b>������������</b> �� ������������� ���������� ���������� ���������, ���� �� �� ������ ��� �������, ��� �� ��������� ��������� ����������� ������.
        ��� ������ ����� ���������� ��� ��������������� ��� �������������� ������, � �������, ������� �� ��������� ���������, ��-�� ������ � ����� ������, ��-�� ����, ��� ����� ������� ������� ������� ������, ������� �� ���������� �� ����� ������, ��� ����� ���������� ��� ������ ������ ������ SMF.';
// Don't use entities in the below string.
$txt['package_will_fail_popup'] = '�� �������, ��� ������ ���������� ��������� ����� ������?';
$txt['package_install_now'] = '����������';
$txt['package_uninstall_now'] = '�������';

$txt['package_bytes'] = '����';

$txt['package_action_missing'] = '<b style="color: red;">���� �� ������</b>';
$txt['package_action_error'] = '<b style="color: red;">������</b>';
$txt['package_action_failure'] = '<b style="color: red;">��������</b>';
$txt['package_action_success'] = '<b>�������</b>';
$txt['package_action_skipping'] = '<b>���������� ����</b>';

$txt['package_uninstall_actions'] = '�������� ��������';
$txt['package_uninstall_done'] = '����� ��� ������.';
$txt['package_uninstall_cannot'] = '���� ����� �� ����� ���� ������, ������ ��� � ��� ��� ��������� ��������!<br /><br />����������, ���������� � ������ ������, ��� ����� ������ ����������.';

$txt['package_install_options'] = '��������� ���������';
$txt['package_install_options_ftp_why'] = '��������� FTP ��������� ��������� �������, �� ������ ������� ��������� ����� �� ������ ����������� ������.<br />����� �� ������ ��������� �������� �� ��������� ��� ������ �������.';
$txt['package_install_options_ftp_server'] = 'FTP ������';
$txt['package_install_options_ftp_port'] = '����';
$txt['package_install_options_ftp_user'] = '������������';
$txt['package_install_options_make_backups'] = '��������� ��������� ����� ������ � ������� (~) � ����� ����� �����.';

$txt['package_ftp_necessary'] = '����������� ���������� FTP';
$txt['package_ftp_why'] = '��������� �����, ������� �������� ������� ������ ��������, �� ����� ���� �� ������. �������� ��� �����, ��������� FTP.';
$txt['package_ftp_why_download'] = '��� �������� �������, ���������� ��� ����������� ������� ������ ����� ����� �� ������. �������� ������� ����� ������������ ���� ��������� FTP ��� ��������� ���� ����.';
$txt['package_ftp_server'] = 'FTP ������';
$txt['package_ftp_port'] = '����';
$txt['package_ftp_username'] = '������������';
$txt['package_ftp_password'] = '������';
$txt['package_ftp_path'] = '��������� ���� � SMF';

// For a break, use \\n instead of <br />... and don't use entities.
$txt['package_delete_bad'] = '�����, ������� �� ��������� �������, ���������� �� ������. ���� �� ��� �������, � ���������� �� �� ������� ��� �������.\\n\\n�� �������?';

$txt['package_examine_file'] = '����������� ����� � ������';
$txt['package_file_contents'] = '����������� ����';

$txt['package_upload_title'] = '��������� �����';
$txt['package_upload_select'] = '����� ��� ��������';
$txt['package_upload'] = '���������';
$txt['package_upload_error_supports'] = '�������� ������� ������������ ������ ��� ���� ������: ';
$txt['package_upload_error_broken'] = '�����, ������� �� ��������� ���������, �� �������� ������� ����������� ��� ���������.';
$txt['package_uploaded_success'] = '�������� ������';
$txt['package_uploaded_successfully'] = '����� ������� ��������';

$txt['package_modification_malformed'] = '���� ����������� ����������� �������.';
$txt['package_modification_missing'] = '���� �� ������.';
$txt['package_no_zlib'] = '��������, ���� ������������ PHP �� ������������ <b>zlib</b>. ��� ��������� zlib, �������� ������� �������� �� �����. ����������, ���������� � ������ �������.';

// Untranslated!
$txt['package_cleanperms_title'] = '��������� ����';
$txt['package_cleanperms_desc'] = '����� �� ������ �������� ����� ������, ������������ ����� �������.';
$txt['package_cleanperms_type'] = '�������� ����� ������ �� ������';
$txt['package_cleanperms_standard'] = '������ ����������� ����� ����� ����� �� ������.';
$txt['package_cleanperms_free'] = '��� ����� ����� ����� �� ������.';
$txt['package_cleanperms_restrictive'] = '������� ������ ����� ����� �� ������.';
$txt['package_cleanperms_go'] = '��������';

$txt['package_confirm_view_package_content'] = '�� �������, ��� ������ ������� ���������� ������ �������������� �� ������:<br /><br />%1$s';
$txt['package_confirm_proceed'] = '����������';
$txt['package_confirm_go_back'] = '���������';

?>