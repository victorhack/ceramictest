<?php
// Version: 1.1; Search

$txt[183] = '��������� ������';
$txt[189] = '�������� ������, � ������� ����� ������������� �����, ��� ������ �����';
$txt[343] = '�� ����� �������';
$txt[344] = '����� �� ����';
$txt[583] = '�� ������������';

$txt['search_post_age'] = '������� ���������';
$txt['search_between'] = '�����';
$txt['search_and'] = '�';
$txt['search_options'] = '��������';
$txt['search_show_complete_messages'] = '���������� ���������� � ���� ���������';
$txt['search_subject_only'] = '������ �������� ����';
$txt['search_relevance'] = '����������';
$txt['search_date_posted'] = '���� ���������';
$txt['search_order'] = '������� ����������';
$txt['search_orderby_relevant_first'] = '�������� ���������� ���������� �������';
$txt['search_orderby_large_first'] = '���������� ���� �������';
$txt['search_orderby_small_first'] = '���������� ���� �������';
$txt['search_orderby_recent_first'] = '��������� ���� �������';
$txt['search_orderby_old_first'] = '������ ���� �������';

$txt['search_specific_topic'] = '������ ��������� ������ � ����';

$txt['mods_cat_search'] = '�����';
$txt['groups_search_posts'] = '������ ������������� � �������� � ������� ������';
$txt['simpleSearch'] = '��������� ������� �����';
$txt['search_results_per_page'] = '�������� ����������� �� ��������';
$txt['search_weight_frequency'] = '������������� ������ �� ���������� ��������� � ����';
$txt['search_weight_age'] = '������������� ������ �� �������� ��������� ���������';
$txt['search_weight_length'] = '������������� ������ �� �������� ����';
$txt['search_weight_subject'] = '������������� ������ �� �������� ���� ���������';
$txt['search_weight_first_message'] = '������������� ������ �� ������������ ������ ����������';
$txt['search_weight_sticky'] = '������������� ������ �� ������������� �����';

$txt['search_settings_desc'] = '����� �� ������ �������� ������� ��������� ������.';
$txt['search_settings_title'] = '��������� ������';
$txt['search_settings_save'] = '���������';

$txt['search_weights'] = '��������� ������';
$txt['search_weights_desc'] = '����� �� ������ �������� �������������� ���������� ������ ���������� �� ������.';
$txt['search_weights_title'] = '������������� ������';
$txt['search_weights_total'] = '�����';
$txt['search_weights_save'] = '���������';

$txt['search_method'] = '��������������';
$txt['search_method_desc'] = '����� �� ������ ���������� ��������� ������.';
$txt['search_method_title'] = '����� ������';
$txt['search_method_save'] = '���������';
$txt['search_method_messages_table_space'] = '������ ��������� � ���� ������';
$txt['search_method_messages_index_space'] = '������ �������� � ���� ������';
$txt['search_method_kilobytes'] = '��';
$txt['search_method_fulltext_index'] = '�������������� ��������������';
$txt['search_method_no_index_exists'] = '�� ������';
$txt['search_method_fulltext_create'] = '�������';
$txt['search_method_fulltext_cannot_create'] = '���������� ������� ��������������, ������������ ������ ��������� - 65,535, ���� ��� ������� ���������� �� ���� MyISAM';
$txt['search_method_index_already_exsits'] = '������';
$txt['search_method_fulltext_remove'] = '�������';
$txt['search_method_index_partial'] = '��� ������';
$txt['search_index_custom_resume'] = '����������';
// This string is used in a javascript confirmation popup; don't use entities.
$txt['search_method_fulltext_warning'] = '��� ������������� ��������������� ������, �� ������ ������� �������������� ��������������!';

$txt['search_index'] = '��������� ��������������';
$txt['search_index_none'] = '�� ������������ ��������������';
$txt['search_index_custom'] = '���������� ��������������';
$txt['search_index_label'] = '��������������';
$txt['search_index_size'] = '������';
$txt['search_index_create_custom'] = '�������';
$txt['search_index_custom_remove'] = '�������';
// This string is used in a javascript confirmation popup; don't use entities.
$txt['search_index_custom_warning'] = '��� ������������� ����������� ������, �� ������ ������� ���������� ��������������!';

$txt['search_force_index'] = '������������ ������ ��������� ������';
$txt['search_match_words'] = '������ ����� �������';
$txt['search_max_results'] = '�������� ����������� ��� �����������';
$txt['search_max_results_disable'] = '(0 ��� �����������)';

$txt['search_create_index'] = '�������� ��������������';
$txt['search_create_index_why'] = '��� ���� ����� ��������� ��������������?';
$txt['search_create_index_start'] = '�������';
$txt['search_predefined'] = '����������������� �������';
$txt['search_predefined_small'] = '������������� ��������������';
$txt['search_predefined_moderate'] = '��������������� ��������������';
$txt['search_predefined_large'] = '��������������� ��������������';
$txt['search_create_index_continue'] = '����������';
$txt['search_create_index_not_ready'] = 'SMF ������� ��������� �������������� ���������. ����� ������������� ������� �������� �������, ������� �������� �������������� ��� �������������. ������� ������������� ����������� ����� ��������� ������. � ������ ������ ���������, ������� �� ������ ����.';
$txt['search_create_index_progress'] = '���������';
$txt['search_create_index_done'] = '�������������� �������!';
$txt['search_create_index_done_link'] = '����������';
$txt['search_double_index'] = '���� ������� ��������� ���������� ��� ��������� ��������������. ��� ��������� ������������������, ������������� ������� ���� �� ��������� ��������������.';

$txt['search_error_indexed_chars'] = '�������� ����� �������� ����������. ��� ������� 3 ������� ���������� ��� ����������.';
$txt['search_error_max_percentage'] = '�������� ������� ���������� ����. ����������� �������� ����� 5%.';

$txt['search_adjust_query'] = '�������� ��������� ������';
$txt['search_adjust_submit'] = '��������� �����';
$txt['search_did_you_mean'] = '�������� �� �����';

$txt['search_example'] = '<i>������:</i> ��� ���� "�������� �����" -�����';

?>