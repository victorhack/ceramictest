<?php
// Version: 1.1; PersonalMessage

$txt[143] = '������ ���������';
$txt[148] = '��������� ���������';
$txt[150] = '����';
$txt[1502] = '�����';
$txt[316] = '��������';
$txt[320] = '���������';
$txt[321] = '����� ���������';
$txt[411] = '������� ���������';
// Don't translate "PMBOX" in this string.
$txt[412] = '������� ��� ������ ���������';
$txt[413] = '�� �������, ��� ������ ������� ��� ���������?';
$txt[535] = '����������';
// Don't translate the word "SUBJECT" here, as it is used to format the message - use numeric entities as well.
$txt[561] = '����� ������ ���������: SUBJECT';
// Don't translate SENDER or MESSAGE in this language string; they are replaced with the corresponding text - use numeric entities too.
$txt[562] = '������ ��� �� �������� ������ ��������� �� SENDER �� ' . $context['forum_name'] . '.' . "\n\n" . '�������� �������� ��� ����� ���� �����������. ���������� �� ��������� �� ���� email.' . "\n\n" . '������������ ��� ���������:' . "\n\n" . 'MESSAGE';
$txt[748] = '(����������: \'���1, ���2\')';
// Use numeric entities in the below string.
$txt['instant_reply'] = '�������� �� ��� ������ ��������� �����:';

$txt['smf249'] = '�� �������, ��� ������ ������� ��� ���������� ������ ���������?';

$txt['sent_to'] = '����������';
$txt['reply_to_all'] = '�������� ����';

$txt['pm_capacity'] = '����������';
$txt['pm_currently_using'] = '%s ���������, %s%% �����.';

$txt['pm_error_user_not_found'] = '�� ���� ����� ������������ \'%s\'.';
$txt['pm_error_ignored_by_user'] = '������������ \'%s\' ������������ ���� ������ ���������.';
$txt['pm_error_data_limit_reached'] = '��������� �� ����� ���� ���������� \'%s\' ���� ����������!';
$txt['pm_successfully_sent'] = '��������� ������ ���������� \'%s\'.';
$txt['pm_too_many_recipients'] = '�� �� ������ ���������� ������ ��������� ����� %d ����������� ������������.';
$txt['pm_too_many_per_hour'] = '�� ��������� ���������� %d ���������� ������ ��������� �� ���� ���.';
$txt['pm_send_report'] = '��������� �����';
$txt['pm_save_outbox'] = '��������� ����� � ���������';
$txt['pm_undisclosed_recipients'] = '������� ����������';

$txt['pm_read'] = '���������';
$txt['pm_replied'] = '��������';

// Message Pruning.
$txt['pm_prune'] = '������� ���������';
$txt['pm_prune_desc1'] = '������� ��� ������ ��������� ������ ���';
$txt['pm_prune_desc2'] = '����.';
$txt['pm_prune_warning'] = '�� �������, ��� ������ �������� ���� ������ ���������?';

// Actions Drop Down.
$txt['pm_actions_title'] = '���������� ��������';
$txt['pm_actions_delete_selected'] = '������� ����������';
$txt['pm_actions_filter_by_label'] = '����������� �� �������';
$txt['pm_actions_go'] = '���������';

// Manage Labels Screen.
$txt['pm_apply'] = '�������';
$txt['pm_manage_labels'] = '���������� ��������';
$txt['pm_labels_delete'] = '�� �������, ��� ������ ������� ���������� ������?';
$txt['pm_labels_desc'] = '����� �� ������ ���������, ������������� � ������� ������ ��� ����� ������ ���������.';
$txt['pm_label_add_new'] = '�������� ����� �����';
$txt['pm_label_name'] = '��� ������';
$txt['pm_labels_no_exist'] = '� ��� ��� ��������� �������!';

// Labeling Drop Down.
$txt['pm_current_label'] = '�����';
$txt['pm_msg_label_title'] = '�������� ������';
$txt['pm_msg_label_apply'] = '�������� �����';
$txt['pm_msg_label_remove'] = '������� �����';
$txt['pm_msg_label_inbox'] = '��������';
$txt['pm_sel_label_title'] = '������ ��� ����������';
$txt['labels_too_many'] = '��������, %s ��������� ����� ����������� ����������� ���������� �������!';

// Sidebar Headings.
$txt['pm_labels'] = '������';
$txt['pm_messages'] = '���������';
$txt['pm_preferences'] = '������������';

$txt['pm_is_replied_to'] = '�� �������� �� ��� ���������.';

// Reporting messages.
$txt['pm_report_to_admin'] = '�������� ��������������';
$txt['pm_report_title'] = '�������� � ������ ���������';
$txt['pm_report_desc'] = '� ���� �������� �� ������ �������� � ���������� ���� ������ ��������� ������������� ������.';
$txt['pm_report_admins'] = '��������� ��������������';
$txt['pm_report_all_admins'] = '��������� ���� ��������������� ������';
$txt['pm_report_reason'] = '�������';
$txt['pm_report_message'] = '���������';

// Important - The following strings should use numeric entities.
$txt['pm_report_pm_subject'] = '[�����] ';
// In the below string, do not translate "{REPORTER}" or "{SENDER}".
$txt['pm_report_pm_user_sent'] = '{REPORTER} �������� ����� � ������ ���������, ������������ {SENDER}, �� ��������� ��������:';
$txt['pm_report_pm_other_recipients'] = '������ ���������� ���������:';
$txt['pm_report_pm_hidden'] = '%d ������� ����������(�)';
$txt['pm_report_pm_unedited_below'] = '���� ���������� ������� ��������� � ������� �������� �����:';
$txt['pm_report_pm_sent'] = '�����������:';

$txt['pm_report_done'] = '������� �� ������������ �����. ����� �� �������� ��������� �� �������������';
$txt['pm_report_return'] = '�������� �� ��������';

$txt['pm_search_title'] = '����� ������ ���������';
$txt['pm_search_bar_title'] = '����� ���������';
$txt['pm_search_text'] = '�����';
$txt['pm_search_go'] = '�����';
$txt['pm_search_advanced'] = '����������� �����';
$txt['pm_search_user'] = '�� ������������';
$txt['pm_search_match_all'] = '���������� ��� �����';
$txt['pm_search_match_any'] = '���������� ����� �����';
$txt['pm_search_options'] = '��������';
$txt['pm_search_post_age'] = '�� ��������';
$txt['pm_search_show_complete'] = '���������� � ����������� ��������� �������.';
$txt['pm_search_subject_only'] = '����� ������ �� ���� � ������.';
$txt['pm_search_between'] = '�����';
$txt['pm_search_between_and'] = '�';
$txt['pm_search_between_days'] = '����';
$txt['pm_search_order'] = '���������� ������';
$txt['pm_search_choose_label'] = '�������� ��������� ������ ��� ����� �����';

$txt['pm_search_results'] = '���������� ������';
$txt['pm_search_none_found'] = '��������� �� �������';

$txt['pm_search_orderby_relevant_first'] = '�������� ������';
$txt['pm_search_orderby_recent_first'] = '��������� �������';
$txt['pm_search_orderby_old_first'] = '������ �������';

$txt['pm_visual_verification_label'] = '���������� ��������';
$txt['pm_visual_verification_desc'] = '���������� ������� ��� �� ����������� ������ ��� ��������� ������ ���������.';
$txt['pm_visual_verification_listen'] = '����������';

?>