<?php
// Version: 1.1; ManageMembers

$txt['membergroups_title'] = '���������� �������� �������������';
$txt['membergroups_description'] = '������ ������������� ����� ������������ ��������� ��������� � ���� ����� �������. ��������� ������ ������������� ������� �� ���������� ���������. �� ������ ��������� ������ ������������ ����� � ��� ������� � ������� ��������� ������� ������.';
$txt['membergroups_modify'] = '��������';

$txt['membergroups_add_group'] = '�������� ������';
$txt['membergroups_regular'] = '�������� ������';
$txt['membergroups_post'] = '������ ���������� �� ���������� ���������';

$txt['membergroups_new_group'] = '����� ������ �������������';
$txt['membergroups_group_name'] = '�������� ����� ������';
$txt['membergroups_new_board'] = '��������� �������';
$txt['membergroups_new_board_desc'] = '������� ��������� ��� ������ �������������';
$txt['membergroups_new_board_post_groups'] = '<em>����������: ������, ��� ���� ����� �������� ��������� �� ��������� ��������� ����, ������ ��� ������ �� ��������� ������������� ��� �����.</em>';
$txt['membergroups_new_as_type'] = '�� ����';
$txt['membergroups_new_as_copy'] = '���������� ��';
$txt['membergroups_new_copy_none'] = '(���)';
$txt['membergroups_can_edit_later'] = '�� ������ ��������������� ��� �������.';

$txt['membergroups_edit_group'] = '������������� ������ �������������';
$txt['membergroups_edit_name'] = '�������� ������';
$txt['membergroups_edit_post_group'] = '��� ������ ������� �� ���������� ���������';
$txt['membergroups_min_posts'] = '��������� ���������� ���������';
$txt['membergroups_online_color'] = '���� � ������ online';
$txt['membergroups_star_count'] = '���������� �����������(�����)';
$txt['membergroups_star_image'] = '��� ����� �������';
$txt['membergroups_star_image_note'] = '�� ������ ������������ �������� $language ��� �������� �����, ���� ���������';
$txt['membergroups_max_messages'] = '������������ ���������� ������ ���������';
$txt['membergroups_max_messages_note'] = '0 - �������������';
$txt['membergroups_edit_save'] = '���������';
$txt['membergroups_delete'] = '�������';
$txt['membergroups_confirm_delete'] = '�� ������� ��� ������ ������� ��� ������?!';

$txt['membergroups_members_title'] = '�������� ���� ������������� ������';
$txt['membergroups_members_no_members'] = '��� ������ ������ �����';
$txt['membergroups_members_add_title'] = '�������� ������������ � ��� ������';
$txt['membergroups_members_add_desc'] = '������ ����������� �������������';
$txt['membergroups_members_add'] = '�������� �������������';
$txt['membergroups_members_remove'] = '������� �� ������';

$txt['membergroups_postgroups'] = '������ �������������, ������� ����� �� ������� ���������';

$txt['membergroups_edit_groups'] = '������������� ������ �������������';
$txt['membergroups_settings'] = '�������� ������ �������������';
$txt['groups_manage_membergroups'] = '������, ������� ��������� �������� ������ �������������';
$txt['membergroups_settings_submit'] = '���������';
$txt['membergroups_select_permission_type'] = '���������� �����';
$txt['membergroups_images_url'] = '{theme URL}/images/';
$txt['membergroups_select_visible_boards'] = '������� �������';

$txt['admin_browse_approve'] = '������������, ������� ������ �������, ������� ���������';
$txt['admin_browse_approve_desc'] = '����� �� ������ ��������� ����� ��������������, ������� ������ ������� ������� ���������.';
$txt['admin_browse_activate'] = '������� ������ �������������, ��������� ���������';
$txt['admin_browse_activate_desc'] = '������ �������������, ��������� ���������.';
$txt['admin_browse_awaiting_approval'] = '������� ��������� <span style="font-weight: normal">(%d)</span>';
$txt['admin_browse_awaiting_activate'] = '������� ��������� <span style="font-weight: normal">(%d)</span>';

$txt['admin_browse_username'] = '��� ������������';
$txt['admin_browse_email'] = 'Email ����� ';
$txt['admin_browse_ip'] = 'IP �����';
$txt['admin_browse_registered'] = '���������������';
$txt['admin_browse_id'] = 'ID';
$txt['admin_browse_with_selected'] = '� �����������';
$txt['admin_browse_no_members_approval'] = '��� �������������, ��������� ���������.';
$txt['admin_browse_no_members_activate'] = '��� �������������, ��������� ���������.';

// Don't use entities in the below strings, except the main ones. (lt, gt, quot.)
$txt['admin_browse_warn'] = '���� ���������� �������������?';
$txt['admin_browse_outstanding_warn'] = '���� �������������?';
$txt['admin_browse_w_approve'] = '�����������';
$txt['admin_browse_w_activate'] = '������������';
$txt['admin_browse_w_delete'] = '�������';
$txt['admin_browse_w_reject'] = '���������';
$txt['admin_browse_w_remind'] = '���������';
$txt['admin_browse_w_approve_deletion'] = '����������� (�������� ������� ������)';
$txt['admin_browse_w_email'] = '� ��������� email';
$txt['admin_browse_w_approve_require_activate'] = '�������� � ����������� ���������';

$txt['admin_browse_filter_by'] = '����������� ��';
$txt['admin_browse_filter_show'] = '�����������';
$txt['admin_browse_filter_type_0'] = '���������������� ������� ������';
$txt['admin_browse_filter_type_2'] = '���������������� Email ���������';
$txt['admin_browse_filter_type_3'] = '������������ ����� ������� ������';
$txt['admin_browse_filter_type_4'] = '����������� �� �������� ������� ������';
$txt['admin_browse_filter_type_5'] = '����������� ������� ������, �� ���������� ��� ���������� ����';

$txt['admin_browse_outstanding'] = '���������������� ������������';
$txt['admin_browse_outstanding_days_1'] = '�� ����� �������������� ������������������� �����';
$txt['admin_browse_outstanding_days_2'] = '���� �����';
$txt['admin_browse_outstanding_perform'] = '��������� ��������� ��������';
$txt['admin_browse_outstanding_go'] = '��������� ��������';

// Use numeric entities in the below nine strings.
$txt['admin_approve_reject'] = '����������� ���������';
$txt['admin_approve_reject_desc'] = '� ��������� ���� ������� ������������� � ' . $context['forum_name'] . ' ���� ���������.';
$txt['admin_approve_delete'] = '������� ������ �������';
$txt['admin_approve_delete_desc'] = '���� ������� ������ ��  ' . $context['forum_name'] . ' ���� �������.  ��� ��������� ��-�� ����, ��� �� �� ������������ ���� ������� ������. ����������� ����� ����������� � ��� �����������.';
$txt['admin_approve_remind'] = '��������� � �����������';
$txt['admin_approve_remind_desc'] = '��, ��-��������, �� ������������ ���� ������� ������ ��  ';
$txt['admin_approve_remind_desc2'] = '����������, ������� �� ������, ����� ������������ ���� ������� ������:';
$txt['admin_approve_accept_desc'] = '���� ������� ������ ���� ������������ ������� ��������������� ������. �� ������ ����� � ��������� ���� ��������� �� ������.';
$txt['admin_approve_require_activation'] = '���� ������� ������ ��  ' . $context['forum_name'] . ' ���� �������� ��������������� ������, ������ ��� ���������� �� ������������, ����� ��� ��� �� ������� ��������� ���������.';

?>