<?php
// Version: 1.1; Admin

$txt[4] = '������� ������';
$txt[5] = '������������';
$txt[6] = '��������';
$txt[7] = '�������';
$txt[8] = '������ �������������';
$txt[9] = '���������� ��������������';
$txt[11] = '���� �������� ������ ���� ������������������ ������������� �� ����� ������.';
$txt[65] = '����������';
$txt[135] = '����������� �����';
$txt[136] = '�������� ����������� ����� �����, � ������ ��� ������.';
$txt[141] = '� ���� ����, ��� ����� �������� ������������, �������� ���� ��������� �����, �������, �� ������ �� ��������� ������������� ������ ������. ���� �� ������ ������ ����� �����, ������� ������� ����������� ������� ������ ���, ����� �� ���������� ������������.<br />������ ���� ������ ������ �����.';
$txt[207] = '����������������� �����';
$txt[216] = '������������� ���� ���������� ������ ������';
$txt[222] = '��������� �������';
$txt[338] = '�������� email, ���� ��� ���� � ������ �������� �������';
$txt[341] = '����������������� �����';
$txt[342] = '���� ����������������� ��� � ������.';
$txt[347] = '�� ���� �������� �� ������ �������� �������� ��������� ������ ������. ������ ����� ���������, ��������� ������������ ��������� ����� �������� ����� � ��������� ���������.  �����, �������� ��������, ��� ��������� �� ���� �������� (����� ��� ������ �������) ��������������� �� ��������� ��� ���� ������������� ��� ������ ��� ������.';
$txt[348] = '��������� ����� �� ����������� ������������?';
$txt[350] = '�������� ������';
$txt[351] = '�����(URL) ������';
$txt[352] = '��� Cookie';
$txt[355] = 'Email ����� ����������';
$txt[356] = '���������� ���� � ���������� � �������';
$txt[360] = '���������� ���� � ���������� Sources';
$txt[365] = '���� ���� ���������';
$txt[366] = '���� ������ ���������';
$txt[367] = '���� ���� ����';
$txt[368] = '���� ���� ���� #2';
$txt[369] = '���� ���� ���������';
$txt[370] = '���� ���������� �������';
$txt[379] = '���������� �������?';
$txt[380] = '��������� ������ ��������� ���������?';
$txt[388] = '���� ��������� ��������� �����';
$txt[389] = '���� ������ � ��������� �����';
$txt[424] = '�����������������';
$txt[426] = '������������';
$txt[427] = '�������';
$txt[428] = '������������';
$txt[429] = '��������� �������� ������';
$txt[495] = 'SMF ����';
$txt[496] = 'SMF �����';
$txt[501] = '������������';
$txt[521] = '���������� ������ ������ ������';
$txt[571] = '�������������';
$txt[584] = '���������� � ��������� �������� ���������������� ����������';
$txt[608] = '������� ��������� �������������';
$txt[610] = '������������ ��� ������� � ����';
$txt[644] = '��� ��� &quot;' . $txt[208] . '&quot;.  ����� �� ������ ��������� ����������� �������, ����������� �����, ������������� ����, ������������� ������ �����������, ��������� ������ ���������� � ������ ������.<div style="margin-top: 1ex;">���� � ��� �������� ��������, ����������, �������� �������� &quot;��������� � �������������&quot; . ���� ��� ���������� ��� �� �������, ���������� ���������� �� <a href="http://www.simplemachines.org/community/index.php" target="_blank">����� �� �������</a> � ����� ���������.</div> ��������, �� ������� ������ �� ���� ������� ��� ��������, ����� ��  <img src="' . $settings['images_url'] . '/helptopics.gif" alt="' . $txt[119] . '" title="' . $txt[119] . '" border="0" />  ���� ��� ��������� ��������� ����������.
<br /><br />������� �� ������� ���� ����������� �������� <a href="http://www.simplemachines.ru/" target="_blank">SimpleMachines Russian Community</a>: Grek_Kamchatka � Mavn. �� ����� ��������� � ����������� �� �������� ����������� �� ����� <a href="http://www.simplemachines.ru/index.php?action=forum" target="_blank">������� ��������� SMF</a>.';
$txt[670] = '����������, ����������� �� ����� ������� � ������. ��������� BBC ����, ����� ��� <span title="������!!">[b]</span>, <span title="���������!!">[i]</span> � <span title="������������!!">[u]</span> ��������� � ��������, � ����� ������ � HTML. ��� ���� ����� ������ �������, ������ �������� ������ �� ������.';
$txt[684] = '�������������� ������';
$txt[699] = '����������������� ����� ����� �������������� ����������� �������������, ���� ��� ������ ������������� �����. �������� ���������, ������� �� ������ ������������, ����� ��� ��� ������� ���������.';
$txt[702] = '�������� email � ����� ��������� ����� ������������� ����� �����������?';
$txt[726] = '������ ���������� �����.';
$txt[727] = 'C ������ ��������.';
$txt[728] = '��������� ��� ������������.';
$txt[729] = '��������� ������������ ���.';
$txt[735] = '�� ������ �������� email ������ ������������ � ���� ��������. Email ������ �� ��������� ����� ������������� ����� �������� ����, �� �� ������ ��������� ��� ������� �� �� ������ �������. ��������� � ���, ��� ������ ����� �������� ��� ��� �������� �� �������: \'�����1; �����2\'.';
$txt[739] = '�������� ����� �������� �������� � ��������� �����';
$txt[740] = '���������� ������ BBC �� ��������� ������� ��������� � ������� ������ ���������?';

$txt['smf1'] = '���������� ������� ��������� ����� Settings.php - ���������, ��� ���� Settings_bak.php ���������� � �������� ������������.';
$txt['modSettings_title'] = '�������������� � ���������';
$txt['modSettings_info'] = '��������� ��� ��������� �������� ���������� �������.';
$txt['smf5'] = '������ ���� ������ MySQL:';
$txt['smf6'] = '��� ������������ ���� ������:';
$txt['smf7'] = '������ ���� ������:';
$txt['smf8'] = '��� ���� ������:';
$txt['smf11'] = '��������������� ����������';
$txt['smf12'] = '��� ���������� ������������, ����� ������������ �������������� �� ������ � �� ������ ������� ��� ������ ��� ������ �����������.';
$txt['smf54'] = '������� ������ ���� ������:';
$txt['smf55'] = '������� �� ctrl ��� ������ ���������� ���������.  ������� �� ��� ' . $txt[17] . ' ������, ����� ������ ������.';
$txt['smf73'] = '�������� ������ ������';
$txt['smf74'] = '���������� ��������� ������';
$txt['smf85'] = '�� ������ ��������� ��� ������?';
$txt['smf86'] = '����������� ������ ������';
$txt['smf92'] = '��� ������ ����������! ����������, ��������� ����� ��������� ���������, ������ ��� ����.';
$txt['smf201'] = '�������� � �������';
$txt['smf202'] = '����� �� ������ ���������������� ��������� �����. �� ������ ������� �������� �� �������, �� ���� ����������. ���������� �� ��������� ������������� ����.';
$txt['smf203'] = '���������� ��������� ������';
$txt['smf204'] = '����� ��������';
$txt['smf205'] = '����� ������ ����� ��������';
$txt['smf206'] = '����� ���������� ����� � ����� ��������';
$txt['smf207'] = '���������� ���������� �������';
$txt['smf208'] = '���� ��������';
$txt['smf209'] = '������� �������� ������ ���';
$txt['smf210'] = '������� �������� ������ ���';
$txt['smf213'] = '��� �����';
$txt['smf214'] = '������ �����';
$txt['smf215'] = '������������ ������ ����� �� ����������';
$txt['smf216'] = '[�������� ������� ���������������]';
$txt['smf217'] = '������� � Simple Machines...';
$txt['smf219'] = '������� ���';
$txt['smf250'] = '����������, �������� ������ �������������, ������� �� ������ �� ��������� email.';
$txt['smf281'] = '�������������� ���� ������';
$txt['smf282'] = '���� ���� ������ ��������  %d ������.';
$txt['smf283'] = '������� �������������� ���� ������...';
$txt['smf284'] = '����������� %1$s... %2$f �� ��������������.';
$txt['smf285'] = '��� ������� ���� ��������������.';
$txt['smf285b'] = '��� ������������� �������������� �������.';
$txt['smf286'] = ' ������ ��������������.';
$txt['smf310'] = '������ ID ������������ �� ����������';
$txt['smf319'] = '������������� ������ �������� ���� ����� �������������';
$txt['smf320'] = '�������� - agreement.txt �� ������������, ����� ��������� ������� �� �����������, �� ����� ���������.';

$txt['dvc1'] = '����� ������������ ������ ������������� ������, � �������� ��������� ������. ���� ����� �� ���� ������ �������, �� ������ ��������� � ��������� �� ��������� ������ � <a href="http://www.simplemachines.org/" target="_blank">www.simplemachines.org</a>.';
$txt['dvc_more'] = '(���������)';

$txt['lfyi'] = '���������� ����������� � simplemachines.org ��� ��������� ��������� ��������.';

$txt['manage_calendar'] = '���������';
$txt['manage_search'] = '�����';

$txt['smileys_manage'] = '������ � ������';
$txt['smileys_manage_info'] = '��������� � ���������� ����� �������, � ����� ���������� �������� ���������.';
$txt['package1'] = '������ �����������';
$txt['package_info'] = '��������� ����� ������������ ��� ��������� ������������ �������������� � ���� �������.';
$txt['theme_admin'] = '���� ����������';
$txt['theme_admin_info'] = '���������, ��������� � ���������� ������ ����������.';
$txt['registration_center'] = '�����������';
$txt['member_center_info'] = '�������� ������ �������������, ����� � ���������� ��������������, ������� �� ������������ ���� ������� ������.';

$txt['viewmembers_name'] = '��� ������������ (������������)';
$txt['viewmembers_online'] = '��������� �����';
$txt['viewmembers_today'] = '�������';
$txt['viewmembers_day_ago'] = '���� �����';
$txt['viewmembers_days_ago'] = '���� �����';

$txt['display_name'] = '������������ ���';
$txt['email_address'] = 'Email �����';
$txt['ip_address'] = 'IP �����';
$txt['member_id'] = 'ID';

$txt['security_wrong'] = '������� ����� ��� ���������������!' . "\n" .
        'Referer: ' . (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'unknown') . "\n" .
        'User agent: ' . $_SERVER['HTTP_USER_AGENT'] . "\n" .
        'IP: ' . $GLOBALS['user_info']['ip'];

$txt['email_as_html'] = '��������� � HTML �������.  (������� ��� ����� �� ������ ���������� email ��� ������� HTML.)';
$txt['email_parsed_html'] = '�������� &lt;br /&gt; � &amp;nbsp ���� � ��� ���������.';
$txt['email_variables'] = '� ���� ��������� �� ������ ������������ ��������� &quot;���������&quot;.  ������� <a href="' . $scripturl . '?action=helpadmin;help=emailmembers" onclick="return reqWin(this.href);" class="help">�����</a> ��� ��������� ��������� ����������.';
$txt['email_force'] = '��������� ��������� ���� ��� �������������, ������� ���������� �� ��������� ��������.';
$txt['email_as_pms'] = '��������� ���������, ���� ������� ��������� ������ ���������.';
$txt['email_continue'] = '����������';
$txt['email_done'] = '������.';

$txt['ban_title'] = '������ �����';
$txt[724] = '��� �� IP: (������ 192.168.12.213 ��� 128.0.*.*) - ���� �������� �� ������';
$txt[725] = '��� �� Email: (������ badguy@somewhere.com) - ���� �������� �� ������';
$txt[7252] = '��� �� ����� ������������: (������ l33tuser) - ���� �������� �� ������';

$txt['ban_description'] = '����� �� ������ ����������� ������������� �� IP ������, ����� �����, ����� ������������ ��� �� email.';
$txt['ban_add_new'] = '�������� ���';
$txt['ban_banned_entity'] = '���������� �������';
$txt['ban_on_ip'] = '��� �� IP ������ (������ 192.168.10-20.*)';
$txt['ban_on_hostname'] = '��� �� ����� ����� (������ *.mil)';
$txt['ban_on_email'] = '��� �� Email ������ (������ *@badsite.com)';
$txt['ban_on_username'] = '��� �� ����� ������������';
$txt['ban_notes'] = '����������';
$txt['ban_restriction'] = '��� ����';
$txt['ban_full_ban'] = '������ ���';
$txt['ban_partial_ban'] = '��������� ���';
$txt['ban_cannot_post'] = '������ ��������� ���������';
$txt['ban_cannot_register'] = '������ ����������������';
$txt['ban_cannot_login'] = '������ �������';
$txt['ban_add'] = '��������';
$txt['ban_edit_list'] = '������ �����';
$txt['ban_type'] = '��� �����';
$txt['ban_days'] = '����(����)';
$txt['ban_will_expire_within'] = '��� �������� �����';
$txt['ban_added'] = '���������';
$txt['ban_expires'] = '�������������';
$txt['ban_hits'] = '�����';
$txt['ban_actions'] = '��������';
$txt['ban_expiration'] = '�������������';
$txt['ban_reason_desc'] = '�������� ����, ������������ ���������������� ������������.';
$txt['ban_notes_desc'] = '���������� ����� ������ ������ �������������, ������� ������ � ���������� ������.';
$txt['ban_remove_selected'] = '������� ���������';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_remove_selected_confirm'] = '�� ������������� ������ ������� ���������� ����?';
$txt['ban_modify'] = '��������';
$txt['ban_name'] = '���';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_name_empty'] = '��� ���� ��������� �������';
$txt['ban_name_exists'] = '����� ��� ��� ����������. ����������, �������� ������ ���.';
$txt['ban_edit'] = '������������� ���';
$txt['ban_add_notes'] = '<b>����������</b>: ����� �������� ���� �� ������ �������� � ���� �������������� ���������, ����� ��� IP �����, ��� ����� � email �����.';
$txt['ban_expired'] = '���� �����/�������';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_restriction_empty'] = '����������� �� �������.';

$txt['ban_triggers'] = '���������';
$txt['ban_add_trigger'] = '�������� ��������� ����';
$txt['ban_add_trigger_submit'] = '��������';
$txt['ban_edit_trigger'] = '��������';
$txt['ban_edit_trigger_title'] = '������������� ���';
$txt['ban_edit_trigger_submit'] = '��������';
$txt['ban_remove_selected_triggers'] = '������� ���������� ��������� �����';
// Escape any single quotes in here twice.. 'it\'s' -> 'it\\\'s'.
$txt['ban_remove_selected_triggers_confirm'] = '�� �������, ��� ������ ������� ��������� ����?';
$txt['ban_trigger_browse'] = '��������� ����';
$txt['ban_trigger_browse_description'] = '����� �������� ��� �������� ����, ��������������� �� IP ������, ����� �����, Email ������ � �� ����� ������������.';

$txt['ban_log'] = '��� �����';
$txt['ban_log_description'] = '� ����� ���� �������� ��� ��������, ����� ��������������� ������������ ������� �� �����  (\'������ ���\' � \'��� ����������� ������������������\' ������ ��� ����).';
$txt['ban_log_no_entries'] = '� ����� ���� ������ ���';
$txt['ban_log_ip'] = 'IP';
$txt['ban_log_email'] = 'Email �����';
$txt['ban_log_member'] = '������������';
$txt['ban_log_date'] = '����';
$txt['ban_log_remove_all'] = '������� ���';
$txt['ban_log_remove_all_confirm'] = '�� �������, ��� ������ ������� ��� ���� ����?';
$txt['ban_log_remove_selected'] = '������� ����������';
$txt['ban_log_remove_selected_confirm'] = '�� �������, ��� ������ ������� ���������� ���� ����?';
$txt['ban_no_triggers'] = '��� ���������� ����';

$txt['apply_filter'] = '��������� ������';
$txt['applying_filter'] = '����������� ������';
$txt['filter_only_member'] = '�������� ��������� �� ������� ����� ������������';
$txt['filter_only_ip'] = '�������� ��������� �� ������� ����� IP ������';
$txt['filter_only_session'] = '�������� ��������� �� ������� ���� ������';
$txt['filter_only_url'] = '�������� ��������� �� ������� ����� ������(URL)';
$txt['filter_only_message'] = '�������� ������ ������ � ���� �����������';
$txt['session'] = '������';
$txt['error_url'] = 'URL ������ �� ��������';
$txt['error_message'] = '��������� �� ������';
$txt['clear_filter'] = '�������� ������';
$txt['remove_selection'] = '������� ����������';
$txt['remove_filtered_results'] = '������� ��� ���������� ����������';
$txt['sure_about_errorlog_remove'] = '�� �������, ��� ������ ������� ��� ��������� �� �������?';
$txt['reverse_direction'] = '�������� � �������� ��������������� �������';

$txt['settings_not_writable'] = '��� ��������� �� ����� ���� ��������, ������ ��� Settings.php �������� ������ ��� ������.';

$txt['maintain_title'] = '������������ ������';
$txt['maintain_info'] = '��������������� ������, �������� ��������� �����, �������� �� ������ � ������� ������ �������������� ��� ������ ���� �������.';
$txt['maintain_done'] = '������������ ���������.';
$txt['maintain_no_errors'] = '������ �� �������!';

$txt['maintain_general'] = '����� ������������';
$txt['maintain_recount'] = '����������� ��� ������ � ����������.';
$txt['maintain_errors'] = '����� � ��������� ����� ������.';
$txt['maintain_logs'] = '������� �������������� ����.';
$txt['maintain_optimize'] = '�������������� ��� ������� ��� ���������� ��������������.';
$txt['maintain_version'] = '��������� ������ ������������ ������.';
$txt['maintain_return'] = '�������� � ������������ ������';

$txt['maintain_backup'] = '��������� ����������� ���� ������';
$txt['maintain_backup_struct'] = '��������� ��������� ������.';
$txt['maintain_backup_data'] = '��������� ��� ������ �� ������ (������ ����� ������).';
$txt['maintain_backup_gz'] = '����� ���� gzip.';
$txt['maintain_backup_save'] = '���������';

$txt['maintain_old'] = '������� ������ ���������';
$txt['maintain_old_since_days1'] = '������� ��� ����, � ������� �� ��������� ��������� �����  ';
$txt['maintain_old_since_days2'] = ' ����.';
$txt['maintain_old_nothing_else'] = '����� ��� ���� (������� �����������).';
$txt['maintain_old_are_moved'] = '����������� � ����������� ���.';
$txt['maintain_old_are_locked'] = '�������� ����.';
$txt['maintain_old_are_not_stickied'] = '�� ������� ������������� ����.';
$txt['maintain_old_all'] = '��� �������';
$txt['maintain_old_choose'] = '������� ������������ �������';
$txt['maintain_old_remove'] = '�������';
$txt['maintain_old_confirm'] = '�� �������, ��� ������ ������� ������ ���������?\\n\\n��� �� ����� ���� �������!';

$txt['db_error_send'] = '��������� email �� ������ ���������� � MySQL';
$txt['db_persist'] = '������������ ���������� ���������� � MySQL';

$txt['default_language'] = '���� ������ �� ���������';

$txt['maintenance1'] = '���������:';
$txt['maintenance2'] = '���������:';

$txt['errlog1'] = '���� ������ ������';
$txt['errlog2'] = '������ ����� ������ ����������� ��� ������ �� ����� ������. ��� �������� ����� ������ �� ���� ������, �������� ����� � ������� ������ ' . $txt[31] . '.';

$txt['theme4'] = '�������� ���� ����������';
$txt['theme_current_settings'] = '������� ���� ����������';

$txt['repair_zero_ids'] = '����� ���� �(���) ��������� ID ������� ����� 0.';
$txt['repair_missing_topics'] = '��������� #%d � ���� ���� �� ���������� #%d.';
$txt['repair_missing_messages'] = '���� #%d �� ��������  ����������� ���������.';
$txt['repair_stats_topics_1'] = '���� #%d �������� ������ ��������� � ������������ ID %d.';
$txt['repair_stats_topics_2'] = '���� #%d �������� ��������� ��������� � ������������ ID %d.';
$txt['repair_stats_topics_3'] = '���� #%d �������� ������������ ���������� �������, %d.';
$txt['repair_missing_boards'] = '���� #%d  �� ������ #%d, �� ����������.';
$txt['repair_missing_categories'] = '������ #%d  � ��������� #%d, �� ����������.';
$txt['repair_missing_posters'] = '��������� #%d ������������ ������������� #%d, �� ����������.';
$txt['repair_missing_parents'] = '� ������� #%d ���������� #%d, �� ����������.';
$txt['repair_missing_polls'] = '� ���� #%d ����������� �� ������� #%d.';
$txt['repair_missing_calendar_topics'] = '������� #%d � ���� #%d, �� ����������.';
$txt['repair_missing_log_topics'] = '���� #%d ���������� ��� ����������� ����� ��� ����� �������������, �����������.';
$txt['repair_missing_log_topics_members'] = '������������ #%d ������� ���� ��� ����� ���� ��� �����������, �� ��� �����������.';
$txt['repair_missing_log_boards'] = '������ #%d,  ���������� �����������, ���� ��� ����� �������������, �� ����������.';
$txt['repair_missing_log_boards_members'] = '������������ #%d, ���������� ����������� ���� ��� ����� �������, �� ����������.';
$txt['repair_missing_log_mark_read'] = '������ #%d, ���������� �����������, ����� ��� ����� ��������������, �� ����������.';
$txt['repair_missing_log_mark_read_members'] = '������������ #%d, ���������� ���� ��� ����� ������� ������������, �� ����������.';
$txt['repair_missing_pms'] = '������ ��������� #%d, ���������� ������ ��� ����� ������������� �� ����������.';
$txt['repair_missing_recipients'] = '������������ #%d, ���������� ���� ��� ����� ������ ��������� �� ����������.';
$txt['repair_missing_senders'] = '������ ��������� #%d, ���������� ������������� #%d �� ����������.';
$txt['repair_missing_notify_members'] = '�����������, ��������� ������������� #%d �� ����������.';
$txt['repair_missing_cached_subject'] = '�������� ���� #%d �� ���� ���������.';
$txt['repair_missing_topic_for_cache'] = '����� \'%s\' ��������� �� �������������� ����.';
$txt['repair_missing_log_poll_member'] = '����������� #%1$d, ���� ������ �������������  #%2$d , ������� ������ �� ����������.';
$txt['repair_missing_log_poll_vote'] = '������������� ��� ����� ����� #%1$d � �������������� ����������� #%2$d.';
$txt['repair_missing_thumbnail_parent'] = '���������� ����� �������� � ������ %s, �� ��� ����� ��������.';

$txt['dvc_your'] = '���� ������';
$txt['dvc_current'] = '��������� ������';
$txt['dvc_sources'] = 'Sources';
$txt['dvc_default'] = '������ ���� ���������� �� ���������';
$txt['dvc_templates'] = '������� ������ ���� ����������';
$txt['dvc_languages'] = '�������� �����';

$txt['modlog_view'] = '���� ���������';
$txt['modlog_date'] = '����';
$txt['modlog_member'] = '������������';
$txt['modlog_position'] = '������';
$txt['modlog_action'] = '��������';
$txt['modlog_ip'] = 'IP';
$txt['modlog_search_result'] = '���������� ������';
$txt['modlog_total_entries'] = '����� �����������';
$txt['modlog_ac_banned'] = '������������';
$txt['modlog_ac_locked'] = '������ ����';
$txt['modlog_ac_stickied'] = '����������� ����';
$txt['modlog_ac_deleted'] = '�������';
$txt['modlog_ac_deleted_member'] = '������ ������������';
$txt['modlog_ac_removed'] = '���������� ����';
$txt['modlog_ac_modified'] = '�������� ���������';
$txt['modlog_ac_merged'] = '���������� ����';
$txt['modlog_ac_split'] = '����������� ����';
$txt['modlog_ac_moved'] = '���������� ����';
$txt['modlog_ac_profile'] = '�������������� �������';
$txt['modlog_ac_pruned'] = '������ �����';
$txt['modlog_ac_news'] = '��������������� �������';
$txt['modlog_enter_comment'] = '��������� ����������� �����������';
$txt['modlog_moderation_log'] = '���� �������������';
$txt['modlog_moderation_log_desc'] = '���� �������� ������ ���� �������� �����������.<br /><b>����������:</b> �������� �� ����� ���� ������� �� ����� ������ ���� �� ������� 24 ���� � ������� �� ���������.';
$txt['modlog_no_entries_found'] = '������ �� �������';
$txt['modlog_remove'] = '�������';
$txt['modlog_removeall'] = '������� ���';
$txt['modlog_go'] = '�����';
$txt['modlog_add'] = '��������';
$txt['modlog_search'] = '������� �����';
$txt['modlog_by'] = '';

$txt['smileys_default_set_for_theme'] = '������� ������ �� ��������� ��� ���� ����';
$txt['smileys_no_default'] = '(������������ ������ �� ���������)';

$txt['censor_test'] = '����������� ����������� �����';
$txt['censor_test_save'] = '�����������';
$txt['censor_case'] = '�� ��������� ������� ����';
$txt['smf231'] = '������ ����� �������';

$txt['admin_confirm_password'] = '(��������� ������)';

$txt['date_format'] = '(����-��-��)';
$txt['undefined_gender'] = '�/�';
$txt['age'] = '������� ������������';
$txt['activation_status'] = '������ ���������';
$txt['activated'] = '�����������';
$txt['not_activated'] = '�� �����������';
$txt['primary'] = '��������';
$txt['additional'] = '����������';
$txt['messenger_address'] = '�����';
$txt['wild_cards_allowed'] = '�������  * � ? ���������';
$txt['search_for'] = '�����';
$txt['member_part_of_these_membergroups'] = '������������ ������ � ������';
$txt['membergroups'] = '������ �������������';
$txt['confirm_delete_members'] = '�� �������, ��� ������ ������� ��������� �������������?';

$txt['support_credits_title'] = '��������� � �������������';
$txt['support_credits_info'] = '���������� � ���������, �������� ���������� �� �������� � ������� ������, ���� � ��� �������� ��������.';
$txt['support_title'] = '�������������� ���������';
$txt['support_versions_current'] = '��������� ������ SMF';
$txt['support_versions_forum'] = '������ ������';
$txt['support_versions_php'] = '������ PHP';
$txt['support_versions_mysql'] = '������ MySQL';
$txt['support_versions_server'] = '������ Web �������';
$txt['support_versions_gd'] = '������ GD';
$txt['support_versions'] = '���������� � �������';
$txt['support_latest'] = '��������� � ������� ������';
$txt['support_latest_fetch'] = '��������� ���������� � ���������...';

$txt['edit_permissions'] = '����� �������';
$txt['edit_permissions_info'] = '��������� ����������� � ��������� ������������ ��������� ��� ��� ������� �������.';
$txt['membergroups_members'] = '������� ������������';
$txt['membergroups_guests'] = '�����';
$txt['membergroups_guests_na'] = '���';
$txt['membergroups_name'] = '�������� ������';
$txt['membergroups_stars'] = '�������';
$txt['membergroups_members_top'] = '�������������';
$txt['membergroups_add_group'] = '�������� ������';
$txt['membergroups_permissions'] = '����� �������';

$txt['permitgroups_restrict'] = '������������';
$txt['permitgroups_standard'] = '�����������';
$txt['permitgroups_moderator'] = '���������';
$txt['permitgroups_maintenance'] = '�����������';
$txt['permitgroups_inherit'] = '�����������';

$txt['confirm_delete_attachments_all'] = '�� �������, ��� ������ ������� ��� ��������?';
$txt['confirm_delete_attachments'] = '�� �������, ��� ������ ������� ��� ���������� ��������?';
$txt['attachment_manager_browse_files'] = '�������� ������';
$txt['attachment_manager_repair'] = '������������';
$txt['attachment_manager_avatars'] = '�������';
$txt['attachment_manager_attachments'] = '��������';
$txt['attachment_manager_thumbs'] = '������';
$txt['attachment_manager_last_active'] = '��������� ����������';
$txt['attachment_manager_member'] = '������������';
$txt['attachment_manager_avatars_older'] = '������� ������� �������������, ���������� ����� ���';
$txt['attachment_manager_total_avatars'] = '����� ��������';

$txt['attachment_manager_settings'] = '�������� ��������';
$txt['attachment_manager_avatar_settings'] = '�������� ��������';
$txt['attachment_manager_browse'] = '�������� ������';
$txt['attachment_manager_maintenance'] = '������������ ������';
$txt['attachment_manager_save'] = '���������';

$txt['attachment_mode'] = '������ ��������';
$txt['attachment_mode_deactivate'] = '��������� ��������';
$txt['attachment_mode_enable_all'] = '��������� ��� ��������';
$txt['attachment_mode_disable_new'] = '��������� ����� ��������';
$txt['attachmentCheckExtensions'] = '��������� ���������� ��������';
$txt['attachmentExtensions'] = '��������� ���������� ��������';
$txt['attachmentShowImages'] = '���������� ������������� �������� ��� ����������';
$txt['attachmentEncryptFilenames'] = '��������� ����� ���������� ������';
$txt['attachmentUploadDir'] = '����� ��������';
$txt['attachmentDirSizeLimit'] = '������������ ������ ����� ��������';
$txt['attachmentPostLimit'] = '������������ ������ �������� � ����� ���������';
$txt['attachmentSizeLimit'] = '������������ ������ ��������';
$txt['attachmentNumPerPostLimit'] = '������������ ���������� �������� � ���������';
$txt['attachmentThumbnails'] = '�������� ������ �����������, ��� ����������� ��� ����������';
$txt['attachmentThumbWidth'] = '������������ ������ ������';
$txt['attachmentThumbHeight'] = '������������ ������ ������';

$txt['mods_cat_avatars'] = '�������';
$txt['avatar_directory'] = '����� ��������';
$txt['avatar_url'] = '�����(URL) ��������';
$txt['avatar_dimension_note'] = '(0 = ��� ������)';
$txt['avatar_max_width_external'] = '������������ ������ �������� �������';
$txt['avatar_max_height_external'] = '������������ ������ �������� �������';
$txt['avatar_action_too_large'] = '���� ������ ������ �������...';
$txt['option_refuse'] = '��������� �����������';
$txt['option_html_resize'] = '��������� HTML �������� �������';
$txt['option_js_resize'] = '�������� ������ ��� ������ JavaScript';
$txt['option_download_and_resize'] = '��������� � �������� ������ (��������� GD ������)';
$txt['avatar_max_width_upload'] = '������������ ������ ������������ �������';
$txt['avatar_max_height_upload'] = '������������ ������ ������������ �������';
$txt['avatar_resize_upload'] = '�������� ������� ������� ��������';
$txt['avatar_resize_upload_note'] = '(��������� GD ������)';
$txt['avatar_download_png'] = '������������ ������ PNG ��� �������� � ����������� ���������?';
$txt['avatar_gd_warning'] = 'GD ������ �� ����������. ��������� ������� ���������.';
$txt['avatar_external'] = '������� �������';
$txt['avatar_upload'] = '����������� �������';
$txt['avatar_server_stored'] = '������� ������������� �� �������';
$txt['avatar_server_stored_groups'] = '������ �������������, ������� ��������� ������������� ������� � �������';
$txt['avatar_upload_groups'] = '������ �������������, ������� ��������� ���������� ������� �� ������';
$txt['avatar_external_url_groups'] = '������ �������������, ������� ��������� �������� ������� ������� �� ������(URL)';
$txt['avatar_select_permission'] = '����� �����';
$txt['avatar_download_external'] = '�������� ������ � ������(URL)';
$txt['custom_avatar_enabled'] = '��������� ������� �:';
$txt['option_attachment_dir'] = '����� ��������';
$txt['option_specified_dir'] = '������ �����';
$txt['custom_avatar_dir'] = '����� ��������';
$txt['custom_avatar_dir_desc'] = '��� �� ������ ���� ��� ��, ��� � ����� �� �������.';
$txt['custom_avatar_url'] = '�����(URL) ��������';

$txt['repair_attachments'] = '������������ ��������';
$txt['repair_attachments_complete'] = '������������ ���������';
$txt['repair_attachments_complete_desc'] = '��� ���������� ������ ���� ����������';
$txt['repair_attachments_no_errors'] = '������ �� �������!';
$txt['repair_attachments_error_desc'] = '� ���� ������������ ���� ������� ������. �������� �� ������ ������� �� ������ ��������� � ������� ����������.';
$txt['repair_attachments_continue'] = '����������';
$txt['repair_attachments_cancel'] = '������';
$txt['attach_repair_missing_thumbnail_parent'] = '%d ������ �� ������������� �� ������ ��������������� �������������';
$txt['attach_repair_parent_missing_thumbnail'] = '%d ����������� �� ����� �������, ���� ������� ��������';
$txt['attach_repair_file_missing_on_disk'] = '%d ��������/������� ����� ������������� ������, �� ����������� �� �������';
$txt['attach_repair_file_wrong_size'] = '%d ��������/������� ����� ������������ ������';
$txt['attach_repair_file_size_of_zero'] = '%d ��������/������� ����� ������� ������. (�������� ��������)';
$txt['attach_repair_attachment_no_msg'] = '%d �������� ������ �� ������������� � ����������.';
$txt['attach_repair_avatar_no_member'] = '%d ������ ������ �� ������������ � �������������';

$txt['news_title'] = '������� � ��������';
$txt['news_settings_desc'] = '����� �� ������ �������� ��������� � ����� ������� ��� �������� � ��������.';
$txt['news_settings_submit'] = '���������';
$txt['news_mailing_desc'] = '�� ����� ����, �� ������ ���������� ��������� ���� ������������������ �������������. �� ������ ������������� ���� �������� ��� ��������� ��������� ����. ������� ��� �������� ������ ���������� � ��������.';
$txt['groups_edit_news'] = '������, ������� ��������� ������������� �������';
$txt['groups_send_mail'] = '������, ������� ��������� ��������� ��������� ��������� � ������';
$txt['xmlnews_enable'] = '��������� XML/RSS �������';
$txt['xmlnews_maxlen'] = '������������ ����� ���������:<div class="smalltext">(0 ��� �����������, �� ��� ������ ����.)</div>';
$txt['editnews_clickadd'] = '������� ���� ��� ���������� ������� ������.';
$txt['editnews_remove_selected'] = '������� ����������';
$txt['editnews_remove_confirm'] = '�� �������, ��� ������ ������� ���������� �������?';
$txt['censor_clickadd'] = '������� ���� ��� ���������� ������� �����.';

$txt['layout_controls'] = '�����';

$txt['generate_reports'] = '��������� �������';

$txt['permission_mode_normal'] = '����������';
$txt['permission_mode_no_polls'] = '��� �����������';
$txt['permission_mode_reply_only'] = '������ ������';
$txt['permission_mode_read_only'] = '������ ��� ������';

$txt['update_available'] = '�������� ����������!';
$txt['update_message'] = '�� ����������� ���������� ������ SMF, ���������� ��������� ������, ��� ������ ���������� � ����� ������.
�� ����������� ���<a href="" id="update-link">�������� ��� �����</a> �� ��������� ������. ��� ������ ����� ��������� �����!';

$txt['salvaged_category_name'] = '������� ����������';
$txt['salvaged_category_error'] = '���������� ������� ������� ����������!';
$txt['salvaged_board_name'] = '���� � ������� ����������';
$txt['salvaged_board_description'] = '���� ��������� ��� ��������� � ��������������� ������';
$txt['salvaged_board_error'] = '���������� ������� � ������� ����������, ������ ��� ��������������� ��� !';

$txt['manageposts'] = '��������� � ����';
$txt['manageposts_title'] = '���������� ����������� � ������';
$txt['manageposts_description'] = '����� �� ������ ��������� ����� �����������, ���������� � ������ � �����������.';

$txt['manageposts_seconds'] = '������';
$txt['manageposts_minutes'] = '�����';
$txt['manageposts_characters'] = '������';
$txt['manageposts_days'] = '����';
$txt['manageposts_posts'] = '���������';
$txt['manageposts_topics'] = '���';

$txt['manageposts_settings'] = '��������� ���������';
$txt['manageposts_settings_description'] = '����� �� ������ ���������� ���, ��� ������� � ����������� � �� ���������.';
$txt['manageposts_settings_submit'] = '���������';

$txt['manageposts_bbc_settings'] = 'BB ���';
$txt['manageposts_bbc_settings_description'] = 'BB ��� ����� �������������� ��� ���������� ���������. ������, ������� ��������� �����  \'������\' �� ������ �������� ��� [b]������[/b]. ��� BB ��� ���� ��������� ����������� �������� (\'[\' � \']\').';
$txt['manageposts_bbc_settings_title'] = '��������� BB ����';
$txt['manageposts_bbc_settings_submit'] = '���������';

$txt['manageposts_topic_settings'] = '��������� ���';
$txt['manageposts_topic_settings_description'] = '����� �� ������ ���������� ��� ��������� ���������� ���.';
$txt['manageposts_topic_settings_submit'] = '���������';

$txt['removeNestedQuotes'] = '������� ��������� ������ ��� �������� ���������';
$txt['enableEmbeddedFlash'] = '�������� Flash � ����������';
$txt['enableEmbeddedFlash_warning'] = '����� ���� ���� � ������������!';
$txt['enableSpellChecking'] = '��������� �������� ����������';
$txt['enableSpellChecking_warning'] = '��� ����� �������� �� �� ���� ��������!';
$txt['max_messageLength'] = '������������ ������ ���������';
$txt['max_messageLength_zero'] = '0 ������������.';
$txt['fixLongWords'] = '������� ����� ������� ���';
$txt['fixLongWords_zero'] = '0 ���������';
$txt['topicSummaryPosts'] = '���������� ������������ ��������� � ����';
$txt['spamWaitTime'] = '����� ����� ��������� ��������� � ������ IP';
$txt['edit_wait_time'] = '����� �������������� ���������';
$txt['edit_disable_time'] = '�����, � ������� �������� ��������� ������������� ���������';
$txt['edit_disable_time_zero'] = '0 ���������';

$txt['enableBBC'] = '��������� BB ���';
$txt['enablePostHTML'] = '��������� <i>��������</i> HTML � ����������';
$txt['autoLinkUrls'] = '������������� ��������� ������ URL';
$txt['bbcTagsToUse'] = '��������� BB ����';
$txt['bbcTagsToUse_select'] = '������� ����, ����������� ��� �������������';
$txt['bbcTagsToUse_select_all'] = '������� ��� ����';

$txt['enableStickyTopics'] = '��������� ������������� ����';
$txt['enableParticipation'] = '��������� ������';
$txt['oldTopicDays'] = '����� ����� �������� ���� ��������� ����������';
$txt['oldTopicDays_zero'] = '0 ���������';
$txt['defaultMaxTopics'] = '���������� ��� �� ��������';
$txt['defaultMaxMessages'] = '���������� ��������� �� �������� ����';
$txt['hotTopicPosts'] = '���������� ��������� ��� ���������� ����';
$txt['hotTopicVeryPosts'] = '���������� ��������� ��� ����� ���������� ����';
$txt['enableAllMessages'] = '������������ ������ ���� ��� ����������� &quot;����&quot; ���������';
$txt['enableAllMessages_zero'] = '0 ������� �� ���������� &quot;���&quot;';
$txt['enablePreviousNext'] = '��������� ������ ���������� ����/��������� ����';

$txt['not_done_title'] = '�� ������!';
$txt['not_done_reason'] = '����� �������� ���������� �������, ������� �������� �������������. ��� ���������� ����� ��������� ������. ���� ��� �� ��������� ������� �� ������ ����������.';
$txt['not_done_continue'] = '����������';

$txt['core_configuration'] = '����� ���������';
$txt['other_configuration'] = '������ ���������';
$txt['caching_settings'] = '�����������';

$txt['utf8_title'] = '������������� ���� ������ � ������ � UTF-8';
$txt['utf8_introduction'] = 'UTF-8 �������� ������������� ���������� ������������ ����������� ��� ����� ����. �������������� ����� ���� ������ � ������ � UTF-8 ����� ��������� ��������� �������������� ������ ������. ��� ����� ����� �������� ����� � ����������� ���������� ��� ������ � ������������ �������.';
$txt['utf8_warning'] = '���� �� ������ ������������� ������ � ���� ������ � UTF-8, ������ ���������:
<ul>
	<li>�������������� � ��������� UTF-8 ����� ���� <em>�������</em> ��� ����� ������! ��������� � ���, ��� � ��� ���� ��������� ����� ����� ���� ������, <i>�� ����</i> ��� ������� ��������������.</li>
	<li>��������� ��������� UTF-8 �� ��������� ������ ��� ����������� ���������, ���� ����� �� �����, ���� �� ������ ������������ ���� �� ���� ��� ��������� ��������������.</li>
	<li>����� ���� ��� �� ������������� ���� ������ � ���� ������ � UTF-8, ��� ����� ����� �������� ����� � ��������� UTF-8.</li>
</ul>';
$txt['utf8_charset_not_supported'] = '�������������� �� %s � UTF-8 �� ��������������.';
$txt['utf8_detected_charset'] = '��������� �������� ������ �� ��������� (\'%1$s\'), ��������� ����� ������ \'%2$s\'.';
$txt['utf8_already_utf8'] = '���� ���� ������ � ������ ��� ������������� � UTF-8. �������������� �� ���������.';
$txt['utf8_source_charset'] = '��������� ������';
$txt['utf8_proceed'] = '����������';
$txt['utf8_database_charset'] = '��������� ���� ������';
$txt['utf8_target_charset'] = '������������� ������ � ���� ������ �';
$txt['utf8_utf8'] = 'UTF-8';
$txt['utf8_db_version_too_low'] = '������ MySQL ������������ �� ����� ������� ���� ������ �� ������������ ��������� UTF-8. ����������� ������������� ������ 4.1.2.';

$txt['entity_convert_title'] = '������������� ��������-HTML � UTF-8';
$txt['entity_convert_only_utf8'] = '���� ������, ������ ���� � ������� UTF-8 ������ ��� ��������-HTML ����� ������������� � UTF-8';
$txt['entity_convert_introduction'] = '��� ������� ����������� ��� ������� ����������� � ���� ������ ��� ��������-HTML � ��������� UTF-8. ��� �������� �������, ����� �� ������ ��� �������������, ��� ����� �� ��������� ISO-8859-1 ���� �������������� �� ��������� ������� �� ������. ����� ������� �������� ��� ������� ��� ��������-HTML. ��� �������, �������-HTML &amp;#945; ������������ ��������� ������ &#945; (�����). �������������� � ��������� UTF-8 ������� ����� � ���������� ������ � �������� ������ ��������.';
$txt['entity_convert_proceed'] = '���������';

$txt['copyright_ends_in'] = '���� �������� �� �������� ��������� SMF �������� ����� <b>%1$s</b> ����.';
$txt['copyright_click_renew'] = '������� <a href="http://www.simplemachines.org/copyright/renew.php?key=%1$s" style="color: red;">����</a> ��� ����������';
$txt['copyright_removal'] = '�������� ���������';
$txt['copyright_removal_desc'] = '����� ������� �������� � ������ SMF ������ �� ������ ������ ��� �� ����� Simple Machines. ���� �� ��� �� ��������� ���, ����������, ������� <a href="http://www.simplemachines.org/copyright/index.php">�����</a>. ��� ������ ��� ����� ������ � ��������, �������� �� ����� ������ ����� �����, ���� ���� �������� �� ����������. ���������� ����� ����� ���������� � ������ �����������������.';
$txt['copyright_code'] = '��� ���������';
$txt['copyright_failed'] = '�������� - �� ����� �� ���������� ���. ����������, ���������, ��� �� ����� ���������� �����(URL), ��� ��������.';
$txt['copyright_proceed'] = '�������';

?>