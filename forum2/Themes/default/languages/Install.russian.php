<?php
// Version: 1.1; Install

// These should be the same as those in index.language.php.
$txt['lang_character_set'] = 'windows-1251';
$txt['lang_rtl'] = false;

$txt['smf_installer'] = '��������� ������ SMF';
$txt['installer_language'] = '����';
$txt['installer_language_set'] = '����������';
$txt['congratulations'] = '�����������, ������� ��������� ��������!';
$txt['congratulations_help'] = '���� � ��� ��������� �������� � ������� ������, �� ��������� ��� <a href="http://www.simplemachines.org/community/index.php" target="_blank">���� ����������� ���������</a>.';
$txt['still_writable'] = '���������� ��������� ����� ����� �� ������. � ����� ������������ ������������� ������� �� ��������� ������ ��� ������.';
$txt['delete_installer'] = '������� ����, ����� ������� install.php.';
$txt['delete_installer_maybe'] = '<i>(�������� �� �� ���� ��������.)</i>';
$txt['go_to_your_forum'] = '������ �� ������ ������� �� ������������� <a href="%s">�����</a>. ����� �����, ��� ����� �������� ������ �����������������.';
$txt['good_luck'] = '�����!<br />SimpleMachines';

$txt['user_refresh_install'] = '���������� ������';
$txt['user_refresh_install_desc'] = '�� ����� ���������, ����������� ���������, ��� ���� ��� ��������� ������, ������� ������ ���� ������� � ���� ������, ��� ����������.<br />��� ������� ���� ��������� ������� �� ���������, ���� ������, ������������ � ��������, ���������.';

$txt['default_topic_subject'] = '����� ���������� � SMF!';
$txt['default_topic_message'] = '����� ���������� �� ����� SimpleMachines!<br /><br />���� � ��� ��������� �������� � �������, ����������, ����������� �� [url=http://www.simplemachines.org/community/index.php]����������� ���� ���������[/url].<br /><br />������� �������!<br />SimpleMachines';
$txt['default_board_name'] = '����� ������';
$txt['default_board_description'] = '� ���� ������� ����� ����� ��������� �� ����� ����.';
$txt['default_category_name'] = '������� ���������';
$txt['default_time_format'] = '%d %B %Y, %H:%M:%S';
$txt['default_news'] = 'SMF ����� ������ ��� ����������!';
$txt['default_karmaLabel'] = '�����:';
$txt['default_karmaSmiteLabel'] = '[������]';
$txt['default_karmaApplaudLabel'] = '[���������]';
$txt['default_reserved_names'] = 'Admin\\nWebmaster\\nGuest\\nroot';
$txt['default_smileyset_name'] = '�� ���������';
$txt['default_classic_smileyset_name'] = 'Classic';
$txt['default_theme_name'] = 'SMF Default Theme - Core';
$txt['default_classic_theme_name'] = 'Classic YaBB SE Theme';
$txt['default_babylon_theme_name'] = 'Babylon Theme';

$txt['default_administrator_group'] = '�������������';
$txt['default_global_moderator_group'] = '���������� ���������';
$txt['default_moderator_group'] = '���������';
$txt['default_newbie_group'] = '�������';
$txt['default_junior_group'] = '������������';
$txt['default_full_group'] = '���������';
$txt['default_senior_group'] = '��������';
$txt['default_hero_group'] = '�������';

$txt['default_smiley_smiley'] = '������';
$txt['default_wink_smiley'] = '�������������';
$txt['default_cheesy_smiley'] = '�������';
$txt['default_grin_smiley'] = '���������';
$txt['default_angry_smiley'] = '����';
$txt['default_sad_smiley'] = '��������';
$txt['default_shocked_smiley'] = '���������';
$txt['default_cool_smiley'] = '������';
$txt['default_huh_smiley'] = '������������';
$txt['default_roll_eyes_smiley'] = '������ ������';
$txt['default_tongue_smiley'] = '���������� ����';
$txt['default_embarrassed_smiley'] = '�������������';
$txt['default_lips_sealed_smiley'] = '��� �� �����';
$txt['default_undecided_smiley'] = '� ��������������';
$txt['default_kiss_smiley'] = '��������';
$txt['default_cry_smiley'] = '��������';
$txt['default_evil_smiley'] = '����';
$txt['default_azn_smiley'] = 'Azn';
$txt['default_afro_smiley'] = '����';

$txt['error_message_click'] = '������� ����';
$txt['error_message_try_again'] = '����� ��������� ��������.';
$txt['error_message_bad_try_again'] = '���������� ��������� �� ������ �� �� ��� (�� �������������).';

$txt['install_settings'] = '�������� ���������';
$txt['install_settings_info'] = '��������� ������, ����������� ��� ��������� :).';
$txt['install_settings_name'] = '�������� ������';
$txt['install_settings_name_info'] = '����� ����������� ������ �������� ������ ������, ��������: &quot;�������� �����&quot;.';
$txt['install_settings_name_default'] = '�������� ������ ������';
$txt['install_settings_url'] = '�����(URL) ������';
$txt['install_settings_url_info'] = '�����(URL) ������ ����������� <b>��� \'/\'</b> � �����.<br />��������� ��������� ������������� ���������� �����. ��� ������� �� ������ ��� ��������.';
$txt['install_settings_compress'] = 'Gzip ������';
$txt['install_settings_compress_title'] = '������� ��������� ������ ��� �������� �������.';
// In this string, you can translate the word "PASS" to change what it says when the test passes.
$txt['install_settings_compress_info'] = '��� ������� �������� �� �� ���� ��������.<br />������� <a href="install.php?obgz=1&amp;pass_string=PASS" onclick="return reqWin(this.href, 200, 60);" target="_blank">����</a> ��� �������� ������ �������. (���� ������ �������������� ��������, �� ������� ����� "PASS".)';
$txt['install_settings_dbsession'] = '������ � ���� ������';
$txt['install_settings_dbsession_title'] = '������� ������ � ���� ������';
$txt['install_settings_dbsession_info1'] = '��� ������� �������� ������������������ ������ ��-�� �������� ������� � ������� �������������.';
$txt['install_settings_dbsession_info2'] = '������� ���� ������� ������ � ���� ������, �� �� ����� ������� ��� ����� �� ��������.';
$txt['install_settings_utf8'] = '������������ ��������� UTF-8';
$txt['install_settings_utf8_title'] = '������������ ��������� UTF-8 �� ���������';
$txt['install_settings_utf8_info'] = '��� ����������� ��������� � ���� ������ � ������ ������������ ������������� ���������, UTF-8. ��� ������ ��� ������������� ��������������� ��� ������������� ��������� ���������.';
$txt['install_settings_stats'] = '��������� �������� ����������';
$txt['install_settings_stats_title'] = '��������� Simple Machines �������� ����������� ����������';
$txt['install_settings_stats_info'] = '���� ���������, �� ��� �������� Machines �������� ��� ���� ��� � ����� ��� ����� ������� ����������. ��� ������� ��� ������� ������� �� ����������� ������������ �����������. ��� ��������� ��������� ���������� �������� <a href="http://www.simplemachines.org/about/stats.php">�������������� ��������</a>.';
$txt['install_settings_proceed'] = '����������';

$txt['mysql_settings'] = '��������� MySQL';
$txt['mysql_settings_info'] = '��������� ������ MySQL �������. ���� ��� �� �������� ��� ������, ���������� � ������ �������.';
$txt['mysql_settings_server'] = '������ MySQL';
$txt['mysql_settings_server_info'] = '���� ����� ������������ localhost - ���� �� �� ������ ��� �������, ���������� localhost.';
$txt['mysql_settings_username'] = '������������ MySQL';
$txt['mysql_settings_username_info'] = '������� ��� ������������, ��� ����������� � ���� ������ MySQL.<br />���� �� �� ������ ��� ������������, ���������� ������ ������� ������ FTP ������������. ���� ����� ��� ������ ���������.';
$txt['mysql_settings_password'] = '������ MySQL';
$txt['mysql_settings_password_info'] = '������� ������ ��� ������� � ���� ������ MySQL.<br />���� �� ������ ������, ���������� ������ ������ �� ������� ������ FTP ������������.';
$txt['mysql_settings_database'] = '���� ������ MySQL';
$txt['mysql_settings_database_info'] = '������� �������� ���� ������, ������� �� ������ ������������.<br />���� ���� �����������, ����������� ���������� ������� ��.';
$txt['mysql_settings_prefix'] = '������� ������ MySQL';
$txt['mysql_settings_prefix_info'] = '������� ��� ������ ������� � ���� ������.  <b>�� �������������� ��� ������ � ����� � ��� �� ���������!</b>.';

$txt['user_settings'] = '�������� ����� ������� ������';
$txt['user_settings_info'] = '��������� ��������� ������� ��� ��� ������� ������ ��������������.';
$txt['user_settings_username'] = '��� ������������';
$txt['user_settings_username_info'] = '�������� ���, ������� �� ������ ������������.<br />��������! ��� ��� � ���������� �������� ������! �� ������� �������� ������ ������������ ���.';
$txt['user_settings_password'] = '������';
$txt['user_settings_password_info'] = '������� ������!';
$txt['user_settings_again'] = '����������� ������';
$txt['user_settings_again_info'] = '(������������� ������.)';
$txt['user_settings_email'] = 'Email �����';
$txt['user_settings_email_info'] = '������� ��� email �����.  <b>�� ������ ���� ��������������.</b>';
$txt['user_settings_database'] = '������ � ���� ������ MySQL';
$txt['user_settings_database_info'] = '� ����� ������������, ��� �������� ������� ������ ��������������, ��������� ������ ������ � ���� ������ MySQL.';
$txt['user_settings_proceed'] = '����������';

$txt['ftp_setup'] = '��������� FTP �������';
$txt['ftp_setup_info'] = '��������� ��������� ����� ������������ � ������� �� ��������� FTP, ����� �������� �������� ������(�����), ���� ���������� �������� ����� �� ������ ��� ��������.  ���� �����������, �� �����-�� ��������, �� ���� ����� �������, ������������� ������� ����� �� ����� ����� FTP � �������� �������� ��������� ������ �� ������. ����������, �� ���������, ��� �� ������ ��������� ������, SSL �� ��������������.';
$txt['ftp_server'] = 'FTP ������';
$txt['ftp_server_info'] = '������� �������� FTP ������� � ����.';
$txt['ftp_port'] = '����';
$txt['ftp_username'] = '��� ������������';
$txt['ftp_username_info'] = '��� ������������ ��� ������� � FTP. <i>� ���������� ����� �� ������������ � �� �����������.</i>';
$txt['ftp_password'] = '������';
$txt['ftp_password_info'] = '������ ��� ������� � FTP. <i>� ���������� ����� �� ������������ � �� �����������.</i>';
$txt['ftp_path'] = '���� ���������';
$txt['ftp_path_info'] = '��� ���� FTP �������.';
$txt['ftp_path_found_info'] = '��������� ����.';
$txt['ftp_connect'] = '������������';
$txt['ftp_setup_why'] = '��� ���� ��� �����?';
$txt['ftp_setup_why_info'] = '��������� ����� ������ ����� ����� �� ������. ��������� ��������� ����� ���������� ������� �� ��������, ���� �� �����-�� �������� ����� �� ���������, ����������, ���������� ����� �� ������ ��� ���� ������ �������. CHMOD 777 (�� ��������� �������� 755):';
$txt['ftp_setup_again'] = '��������� ����� �� ������.';

$txt['error_php_too_low'] = '��������! ��� ���������� ������ SMF ������ ��������� ������ PHP ����, ��� ����������� � ���. <br />���� �� ����������� �������� ��������, ���������� � ������� � �������� �������� ������ PHP. ��������� ������ ����� ����������, �� ����� ���������� ��������.<br /><b>��� �� �������������</b>.';
$txt['error_missing_files'] = '��������� ��������� �� ������� ��������� ������!<br /><br />����������, �������������� � ���, ��� �� ��������� �� ������ ��� �����.';
$txt['error_session_save_path'] = '����������, ���������� ������ ������� � ���, ��� <b>session.save_path ��������� � php.ini</b> ��������!  ���� ���������� �������� �� <b>������������</b> ����������, ������� <b>����� ����� �� ������</b>.<br />';
$txt['error_windows_chmod'] = '�� ����������� ������ �� ���� Windows, � ��������� ����� �� ����� ���� �� ������. ����������, ���������� � ������ �������, � �������� ���� ����� �� ������, ������������, ��� ������� �������� PHP. ��������� ����� � ����� ������ ����� ����� �� ������:';
$txt['error_ftp_no_connect'] = '���������� ������������ � FTP ������� � ���������� �������.';
$txt['error_mysql_connect'] = '���������� ������������ � ���� ������ MySQL � ���������� �������.<br /><br />���� �� �� ������, ����� ������ ���������� ������, ����������, ���������� � ������ �������.';
$txt['error_mysql_too_low'] = '������ �������������� MySQL ������� ��������. � �� ������������� ����������� ���������� ������ SMF.<br /><br />����������, ���������� � ������ ������� � �������� �������� ����������� �����������.';
$txt['error_mysql_database'] = '��������� ��������� �� ����� �������� ������ � &quot;<i>%s</i>&quot;. �� ��������� �������� ���������� ������� ������� ���� ������.';
$txt['error_mysql_queries'] = '��������� ������� ���� ����������� ����������. ��� ����� ��������� ��-�� ���������������� ��� ������ ������ MySQL �������.<br /><br />���������� � �������:';
$txt['error_mysql_queries_line'] = '������ #';
$txt['error_mysql_missing'] = '��������� ��������� �� ������ ���������� ��������� MySQL. ����������, �������������� � ���, ��� PHP ����������� � ���������� MySQL.';
$txt['error_session_missing'] = '��������� ��������� �� ������ ���������� ��������� ������ �� ����� ������� ��������������� PHP.  ����������, �������������� � ���, ��� PHP ���������� � ���������� ������.';
$txt['error_user_settings_again_match'] = '������ �� ���������!';
$txt['error_user_settings_taken'] = '��������, ������������ � ����� ������ ��� ���������������.<br /><br />������� ������ �� �������.';
$txt['error_user_settings_query'] = '�� ����� �������� ������� ������ �������������� �������� ��������� ������:';
$txt['error_subs_missing'] = '����������� ���� Sources/Subs.php. ����������, �������������� � ���, ��� �� ��������� �� ������ ��� �����.';
$txt['error_mysql_alter_priv'] = '������� �������� MySQL, ������� �� ������� �� ����� ����� ��������, ��������� � ������� ������� � ���� ������, ��� ���������� ��� ���������� ������ ������ SMF.';
$txt['error_versions_do_not_match'] = '��������� ��������� ���������� ������ ������������� ������ SMF. ���� �� ������ �������� ������������� ������, �������������� ���������� ����������(upgrade.php).<br /><br />';
$txt['error_mod_security'] = '��������� ��������� ���������� ������������� ������ mod_security. ���� ������ ����� ����������� ������������ ������ ������. SMF ����� ����� ���������� ������ ������������, ������� ����������� �� ������������ ���� ������ � �������� ������� �����������.<br /><br /><a href="http://www.simplemachines.org/redirect/mod_security">��� ��������� mod_security</a>';
$txt['error_utf8_mysql_version'] = '������� ������ ���� ������ �� ������������ ��������� UTF-8. �� ������ ��� ������� ���������� SMF, �� ��� ��������� UTF-8. ���� �� ������ �������� ��������� UTF-8 � ������� (�������� ������� MySQL ������ ������ ������ �� ������ >=4.1), �� ������ ������������� ����� � UTF-8 ����� ������ �����������������.';

?>