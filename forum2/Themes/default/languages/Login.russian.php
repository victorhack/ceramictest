<?php
// Version: 1.1.2; Login

$txt[37] = '�� �� ����� ��� ������������.';
$txt[38] = '�� �� ����� ������.';
$txt[39] = '�������� ������';
$txt[98] = '��� ������������';
$txt[155] = '����������� ������������';
$txt[245] = '����������� ������ �������!';
$txt[431] = '�����������! ������ �� ���������������� �� ������.';
// Use numeric entities in the below string.
$txt[492] = '� ��� ������';
$txt[500] = '����������, ������� �������������� email �����, %s.';
$txt[517] = '����������� ����������';
$txt[520] = '������������ ������ ��� ������������� �������.';
$txt[585] = '� ��������';
$txt[586] = '� �� ��������';
$txt[633] = '��������!';
$txt[634] = '������ ������������������ ������������ ����� ������ � ���� ������.';
$txt[635] = '����������, ������� ���';
$txt[636] = '�����������������';
$txt[637] = '�� ' . $context['forum_name'] . '.';
// Use numeric entities in the below two strings.
$txt[701] = '�� ������ �������� ��� ����� ���� ��� ������� � ��������� ������ �������, ��� �������� ��� �������� ����� �����:';
$txt[719] = '��� ������������: ';
$txt[730] = '���� email ����� (%s) ������������ ������ ������������������ �������������. ���� �� �������, ��� ��� ������, ��������� �� �������� ����� � �������������� ������������ ������ ��������� ���� �����.';

$txt['login_hash_error'] = '�������� ������� ���� ��������. ����������, ������� ��� ������.';

$txt['register_age_confirmation'] = '� ������ %d ���';

// Use numeric entities in the below six strings.
$txt['register_subject'] = '����� ���������� �� ' . $context['forum_name'];

// For the below three messages, %1$s is the display name, %2$s is the username, %3$s is the password, %4$s is the activation code, and %5$s is the activation link (the last two are only for activation.)
$txt['register_immediate_message'] = '�� ������������������ �� ' . $context['forum_name'] . ', %1$s!' . "\n\n" . '���� ��� ������������ %2$s � ������ %3$s.' . "\n\n" . '�� ������ �������� ������ ����� ���� ��� ������� � ���������� ������ �������, ��� ������� �� ���� ������ ����� �����:' . "\n\n" . $scripturl . '?action=profile' . "\n\n" . $txt[130];
$txt['register_activate_message'] = '�� ���������������� �� ' . $context['forum_name'] . ', %1$s!' . "\n\n" . '���� ��� ������������ %2$s � ������ %3$s (������� ����� ���� ������� �����.)' . "\n\n" . '����� ��� ��� �����, ��� ����� ������������ ���� ������� ������. ��� ���� ����� ��� �������, ����������, ��������� �� ������:' . "\n\n" . '%5$s' . "\n\n" . '���� � ��� �������� �������� � ����������, ����������, ����������� ���� ��� "%4$s".' . "\n\n" . $txt[130];
$txt['register_pending_message'] = '��� ������ � ����������� �� ' . $context['forum_name'] . ' ��� ������, %1$s.' . "\n\n" . '��� ������������ %2$s � ������ %3$s.' . "\n\n" . '����� ��� ��� �� ������� ����� � ������ ������������ �����, ��� ������ ������ ����������� � ����������� ������������� ������.  ����� �����, �� �������� ������ ������ � ����� ������.' . "\n\n" . $txt[130];

// For the below two messages, %1$s is the user's display name, %2$s is their username, %3$s is the activation code, and %4$s is the activation link (the last two are only for activation.)
$txt['resend_activate_message'] = '�� ���������������� �� ' . $context['forum_name'] . ', %1$s!' . "\n\n" . '���� ��� ������������ "%2$s".' . "\n\n" . '����� ��� ��� �� ������� �����, ������� ����� ������������ ������� ������. ��� ���� ����� ��� ������� ��������� �� ���� ������:' . "\n\n" . '%4$s' . "\n\n" . '���� � ��� �������� �������� � ����������, ����������, ����������� ��� ��� "%3$s".' . "\n\n" . $txt[130];
$txt['resend_pending_message'] = '��� ������ � ����������� �� ' . $context['forum_name'] . ' ��� �������, %1$s.' . "\n\n" . '��� ������������ ��� ������� �� ������������������ %2$s.' . "\n\n" . '����� ��� ��� �� ������� ����� � ������ ������������ �����, ��� ������ ������ ����������� � ����������� ������������� ������.  ����� �����, �� �������� ������ ������ � ����� ������.' . "\n\n" . $txt[130];

$txt['ban_register_prohibited'] = '��������, ��� ��������� ���������������� �� ���� ������.';
$txt['under_age_registration_prohibited'] = '��������, �� �������������, �� ��������� %d ��� �� ��������� ���������������� �� ���� ������.';

$txt['activate_account'] = '��������� ������� ������';
$txt['activate_success'] = '���� ������� ������ ������ ������������. �� ������ ����� �� �����.';
$txt['activate_not_completed1'] = '��� email ����� ������ ���� ��������, ������ ��� �� ������� �����.';
$txt['activate_not_completed2'] = '��������� �������� ������ ��� ���������?';
$txt['activate_after_registration'] = '������� ������� �� �����������. ����� ��������� ����� �� �������� ������ � ������� ��� ��������� ����� ������� ������.';
$txt['invalid_userid'] = '������������ �� ����������';
$txt['invalid_activation_code'] = '������������ ��� ���������';
$txt['invalid_activation_username'] = '��� ������������ ��� email';
$txt['invalid_activation_new'] = '���� �� ������������������ � ������� ������������ email �����, �������� ����� � ������� ��� ������.';
$txt['invalid_activation_new_email'] = '����� email �����';
$txt['invalid_activation_password'] = '������ ������';
$txt['invalid_activation_resend'] = '��������� �������� ��� ���������';
$txt['invalid_activation_known'] = '���� �� ������ ��� ��� ���������, ����������, �������� ��� �����.';
$txt['invalid_activation_retry'] = '��� ���������';
$txt['invalid_activation_submit'] = '������������';

$txt['coppa_not_completed1'] = '������������� ��� �� ������� �������� �� ���� ����������� �� ���������/�������.';
$txt['coppa_not_completed2'] = '����� ��������� ������?';

$txt['awaiting_delete_account'] = '���� ������� ������ ���� �������� �� ��������!<br />���� �� ������ ������������ ���� ������� ������, ����������, ������������� ���� ������� ������ ������� &quot;������������ ��� ������� ������&quot; � ������� �����.';
$txt['undelete_account'] = '������������ ��� ������� ������';

$txt['change_email_success'] = '��� email ����� ��� �������. ��� ���������� ������ ��� ���������.';
$txt['resend_email_success'] = '����� ������ ��������� ���� ������ ����������.';
// Use numeric entities in the below three strings.
$txt['change_password'] = '���������� � ����� ������';
$txt['change_password_1'] = '���������� ��� ����� ��';
$txt['change_password_2'] = '��� ������� � ������ �������. ���� ��������� ���������� ��� �����.';

$txt['maintenance3'] = '����� ��������� �� ����������� ������������.';

// These two are used as a javascript alert; please use international characters directly, not as entities.
$txt['register_agree'] = '����������, ���������� ���������� ������, ��� �� �����������������.';
$txt['register_passwords_differ_js'] = '������ �� ���������!';

$txt['approval_after_registration'] = '������� ������� �� �����������. ������������� ������ ����������� ���� �����������, ������ ��� �� ������� ������������ ���� ������� ������. ����� ������������� �� �������� ������ �� ��������������.';

$txt['admin_settings_desc'] = '����� �� ������ �������� ���������, ��������� � ������������ ����� �������������.';

$txt['admin_setting_registration_method'] = '������ ����������� ����� �������������';
$txt['admin_setting_registration_disabled'] = '����������� ���������';
$txt['admin_setting_registration_standard'] = '���������� �����������';
$txt['admin_setting_registration_activate'] = '��������� ������������';
$txt['admin_setting_registration_approval'] = '��������� ������������';
$txt['admin_setting_notify_new_registration'] = '���������� �������������� ��� ����������� ����� �������������';
$txt['admin_setting_send_welcomeEmail'] = '���������� ����������� ����� �������������';

$txt['admin_setting_password_strength'] = '���������� � ����� ������ �������������';
$txt['admin_setting_password_strength_low'] = '������ - ������� 4 �������';
$txt['admin_setting_password_strength_medium'] = '������� - �� ����� ��������� � ������ ������������';
$txt['admin_setting_password_strength_high'] = '������� - ��������� ��������� ��������';

$txt['admin_setting_image_verification_type'] = '��������� ����������� ���������� ��������';
$txt['admin_setting_image_verification_type_desc'] = '����� ������� ����������� ������� �������� �����';
$txt['admin_setting_image_verification_off'] = '���������';
$txt['admin_setting_image_verification_vsimple'] = '����� ������� - ������� ����� �� �����������';
$txt['admin_setting_image_verification_simple'] = '������� - ����������� ���� �� �������, ��� ����';
$txt['admin_setting_image_verification_medium'] = '������� - ����������� ���� �� �������, � ������';
$txt['admin_setting_image_verification_high'] = '������� - ��������� �������, � ������������� ������';
$txt['admin_setting_image_verification_sample'] = '������';
$txt['admin_setting_image_verification_nogd'] = '<b>�������� ��������:</b> ��� ��� �� ������ ������� �� ����������� GD ���������� ��������� ��������� �������� �� �����.';

$txt['admin_setting_coppaAge'] = '����������� ������� ������������ ��� �������� �����������';
$txt['admin_setting_coppaAge_desc'] = '(0- ��� �����������)';
$txt['admin_setting_coppaType'] = '��������, ����������� ��� ����������� ������������ ������ ���������� ��������';
$txt['admin_setting_coppaType_reject'] = '�������� �����������';
$txt['admin_setting_coppaType_approval'] = '��������� ������������� � ���������/�������';
$txt['admin_setting_coppaPost'] = 'Email �����, �� ������� ������ ���� �������� ��������� � ���������� �����������';
$txt['admin_setting_coppaPost_desc'] = '������ ������������, ���� ������������ ������ �������������� ��������';
$txt['admin_setting_coppaFax'] = '����� �����, �� ������� ������ ���� �������� ��������� � ���������� �����������';
$txt['admin_setting_coppaPhone'] = '��� ���������� ����� ��� ����� � ����������';
$txt['admin_setting_coppa_require_contact'] = '�� ����� ������ ������ �������� ����� ��� ����� �������� ��� ��������� � ����������/���������, ����� �������� ���������.';

$txt['admin_register'] = '����������� ������ ������������';
$txt['admin_register_desc'] = '����� �� ������ ���������������� ����� �������������. ������������� ��������� ������ ������������� �� �� email.';
$txt['admin_register_username'] = '��� ������������';
$txt['admin_register_email'] = 'email �����';
$txt['admin_register_password'] = '������';
$txt['admin_register_username_desc'] = '��� ������ ������������';
$txt['admin_register_email_desc'] = 'Email ����� ������������';
$txt['admin_register_password_desc'] = '������ ������ ������������';
$txt['admin_register_email_detail'] = '��������� ����� ������ ������������';
$txt['admin_register_email_detail_desc'] = '��������� ������ email �����';
$txt['admin_register_email_activate'] = '��������� ��������� ������� ������';
$txt['admin_register_group'] = '�������� ������ ������������';
$txt['admin_register_group_desc'] = '�������� ������, � ������� ����� ������������ ����� ������������';
$txt['admin_register_group_none'] = '(��� �������� ������)';
$txt['admin_register_done'] = '������������ %s ������ ���������������!';

$txt['admin_browse_register_new'] = '���������������� ������ ������������';

// Use numeric entities in the below three strings.
$txt['admin_notify_subject'] = '��������������� ����� ������������';
$txt['admin_notify_profile'] = '�� ����� ������ ��������������� ����� ������������, %s. ������� �� ������, ����� ����������� ��� �������.';
$txt['admin_notify_approval'] = '������ ��� ������������ ������ ���������� ���������, ������� ������ ������ ���� ��������. ������� �� ������ ����� �������� ������������.';

$txt['coppa_title'] = '����� � ������������ �� ��������';
$txt['coppa_after_registration'] = '������� ������� �� ����������� �� ������ ' . $context['forum_name'] . '.<br /><br />��������� �� ������  {MINIMUM_AGE} ���, �� ������ �������� �� ������ �������� ��� ������� ����������, ������ ��� �� ������� ������������ ���� ������� ������. ����� ������������ ������� ������, ����������, ������������ ��� �����:';
$txt['coppa_form_link_popup'] = '������� ����� � ����� ����';
$txt['coppa_form_link_download'] = '��������� ����� ��� ��������� ����';
$txt['coppa_send_to_one_option'] = '��������� ���������/�������� ��������� �����:';
$txt['coppa_send_to_two_options'] = '��������� ���������/�������� ��������� ����������� ����� �� email ��� ����, ��������� ����:';
$txt['coppa_send_by_post'] = '��������� �� ���������� ������:';
$txt['coppa_send_by_fax'] = '��������� ���� �� ���������� ������:';
$txt['coppa_send_by_phone'] = '������������, ������ �������� �������� �������������� ������ �� �������� {PHONE_NUMBER}.';

$txt['coppa_form_title'] = '����� ��������� ��� ����������� �� ' . $context['forum_name'];
$txt['coppa_form_address'] = '�����';
$txt['coppa_form_date'] = '����';
$txt['coppa_form_body'] = '� {PARENT_NAME},<br /><br /> ��������  {CHILD_NAME} (��� �������) ������������������ �� ������: ' . $context['forum_name'] . ', � ������ ������������: {USER_NAME}.<br /><br />� �������, ��� ��������� ������ ���������� {USER_NAME} ����� ���� �������� ������ ������������� ������.<br /><br />�������:<br />{PARENT_NAME} (��������/������).';

$txt['visual_verification_label'] = '���������� ��������';
$txt['visual_verification_description'] = '�������� ������� ������������ �� �����������';
$txt['visual_verification_sound'] = '����������';
$txt['visual_verification_sound_again'] = '��������� �����';
$txt['visual_verification_sound_close'] = '������� ����';
$txt['visual_verification_request_new'] = '��������� ������ �����������';
$txt['visual_verification_sound_direct'] = '�������� � ��������������? ���������� ������ ������ ��� �������������';

?>