<?php
// Version: 1.1.5; ModSettings

$txt['smf3'] = '���� ������ ��������� �������� ��������� ��������� ������� ������ ������.  ����������, ����������� <a href="' . $scripturl . '?action=theme;sa=settings;th=' . $settings['theme_id'] . ';sesc=' . $context['session_id'] . '">��������� ���� ����������</a> ��� ��������� �������������� ����������. �� ������ ����������� ������� �������� ������ �������, ����� �� ������ (?).';

$txt['mods_cat_features'] = '�������� ���������';
$txt['pollMode'] = '�����������';
$txt['smf34'] = '��������� �����������';
$txt['smf32'] = '��������� �����������';
$txt['smf33'] = '���������� ������������ ����������� ��� ����';
$txt['allow_guestAccess'] = '��������� ������ ������������� �����';
$txt['userLanguage'] = '��������� ������������� �������� ���� ������';
$txt['allow_editDisplayName'] = '��������� ������������� �������� ������������ ���';
$txt['allow_hideOnline'] = '��������� ������������� �������� �� online ������?';
$txt['allow_hideEmail'] = '��������� ������������� �������� �� email �����?';
$txt['guest_hideContacts'] = '�� ���������� ���������� ������������� ������ ������';
$txt['titlesEnable'] = '��������� ������� ��� ��������';
$txt['enable_buddylist'] = '��������� ������ ������';
$txt['default_personalText'] = '������� ��� �������� �� ���������';
$txt['max_signatureLength'] = '�������� �������� � �������<div class="smalltext">(0 ��� �����������)</div>';
$txt['number_format'] = '������ ����� �� ���������';
$txt['time_format'] = '������ ������� �� ���������';
$txt['time_offset'] = '������� �� �������<div class="smalltext">(���������� � � �������� �������������.)</div>';
$txt['failed_login_threshold'] = '���������� ��������� ������� �����';
$txt['lastActive'] = '�����, � ������� ��������, ������������ ��������� ��������';
$txt['trackStats'] = '�������� ��������� ����������';
$txt['hitStats'] = '�������� ���������� ����������<div class="smalltext">(���������� ������ ���� ��������.)</div>';
$txt['enableCompressedOutput'] = '������������ ������ �������';
$txt['databaseSession_enable'] = '������� ������ � ���� ������';
$txt['databaseSession_loose'] = '��������� ��������� ������������ �� ������������ ��������';
$txt['databaseSession_lifetime'] = '����������������� ������ � ��������';
$txt['enableErrorLogging'] = '�������� ���������������� ������';
$txt['cookieTime'] = '����� �������� cookies (� �������)';
$txt['localCookies'] = '������������ ��������� �������� cookies<div class="smalltext">(SSI � ���� �������� �������� �� �����)</div>';
$txt['globalCookies'] = '������������ ����������� cookies ��� ����������<div class="smalltext">(������� ��������� ��������� �������� cookies!)</div>';
$txt['securityDisable'] = '��������� �������� ������ ��� ���������������';
$txt['send_validation_onChange'] = '��������� ��������� ������� ������ ����� ����� email ������';
$txt['approveAccountDeletion'] = '����������� ��������� ��������������, ����� ������������ ������� ������� ������';
$txt['autoOptDatabase'] = '�������������� ������� ����� ������ ... ����?<div class="smalltext">(0 ��������.)</div>';
$txt['autoOptMaxOnline'] = '�������� ������������� Online �� ����� �����������<div class="smalltext">(0 ��� �����������.)</div>';
$txt['autoFixDatabase'] = '������������� ��������������� ������������ �������';
$txt['allow_disableAnnounce'] = '��������� ������������� ������������ �� ����������� ������';
$txt['disallow_sendBody'] = '�� ���������� ����� ������ ��������� � ������������?';
$txt['modlog_enabled'] = '���������� �������� �����������';
$txt['queryless_urls'] = '��������� ������������� URL<div class="smalltext"><b>������ ��� Apache!</b></div>';
$txt['max_image_width'] = '������������ ������ ������������� ����������� (0 = ��������)';
$txt['max_image_height'] = '������������ ������ ������������� ����������� (0 = ��������)';
$txt['mail_type'] = '��� Mail �������';
$txt['mail_type_default'] = '(�� ��������� �� PHP)';
$txt['smtp_host'] = 'SMTP ������';
$txt['smtp_port'] = 'SMTP ����';
$txt['smtp_username'] = 'SMTP �����';
$txt['smtp_password'] = 'SMTP ������';
$txt['enableReportPM'] = '��������� �������� � ������ ����������';
$txt['max_pm_recipients'] = '������������ ���������� ����������� ��� �������� ������� ���������.<div class="smalltext">(0 ��� �����������)</div>';
$txt['pm_posts_verification'] = '���������� ���������, ����� �������� ������������ �� ����� ������� ��� ��� ������� ������ ���������.<div class="smalltext">(0- ������ ������� ���, ������������� �� ��������� ������ ���������)</div>';
$txt['pm_posts_per_hour'] = '���������� ������ ��������� ���������� ������������� � ������� ������ ����.<div class="smalltext">(0 ��� �����������, ���������� �� ���������� ������ ���������)</div>';

$txt['mods_cat_layout'] = '��������� �����������';
$txt['compactTopicPagesEnable'] = '���������� ���������� ������������ �������';
$txt['smf235'] = '������ �����������:';
$txt['smf236'] = '��� �����������';
$txt['todayMod'] = '��������� ������� &quot;�������&quot;';
$txt['smf290'] = '��������';
$txt['smf291'] = '������ �������';
$txt['smf292'] = '������� � �����';
$txt['topbottomEnable'] = '���������� ������ �����/����';
$txt['onlineEnable'] = '���������� ������ online/offline � ���������� ������������';
$txt['enableVBStyleLogin'] = '���������� ������� �������� ����� �� ����� �� ������ ��������';
$txt['defaultMaxMembers'] = '���������� ������������� �� �������� (� ������ �������������)';
$txt['timeLoadPageEnable'] = '���������� �����, ����������� �� �������� ��������';
$txt['disableHostnameLookup'] = '�� ���������� �������� ������ �������������?';
$txt['who_enabled'] = '��������� ������ "������������ Online"';

$txt['smf293'] = '�����';
$txt['karmaMode'] = '������� �����';
$txt['smf64'] = '��������|���������� ����� �����|���������� �������� ����/�����';
$txt['karmaMinPosts'] = '����������� ���������� ��������� ��� ��������� ����� �������������';
$txt['karmaWaitTime'] = '����� �������� � �����';
$txt['karmaTimeRestrictAdmins'] = '���������� ��������������� �������� ��������';
$txt['karmaLabel'] = '�������� �����';
$txt['karmaApplaudLabel'] = '����� ��� ����������� �����';
$txt['karmaSmiteLabel'] = '����� ��� ���������� �����';

$txt['caching_information'] = '<div align="center"><b><u>��������! ������ ��� ������������� ��� �������, �������� ��������� ����������.</b></u></div><br />
        SMF ������������ ����������� � ������� �������������. �������������� ��������� ������������:<br />
        <ul>
                <li>APC</li>
                <li>eAccelerator</li>
                <li>Turck MMCache</li>
                <li>Memcached</li>
                <li>Zend Platform/Performance Suite (�� Zend Optimizer!)</li>
        </ul>
        ����������� ����� �������� �����, ���� PHP ������������� � ���������� ������ �� ���������� ���� ������������� ��� �� ������� �������� memcached. <br /><br />
        SMF ������������ ��������� ������� �����������. ��� ���� �������, ��� ������ �������� ���������� �������. ���� ��� ������ ������������ �����������, ������������� ������� ����������� ������ �������.
        <br /><br />
        �������� ��������: ���� �� ����������� memcached, ���� �� ������ ������� ��������� ���������. ������� �� ����� �������, ��� �������� �� �������:<br />
        &quot;������1,������2,������3:����,������4&quot;<br /><br />
        ���� ���� �� ������, SMF ����� ������������ ���� �� ��������� - 11211. SMF ����� �������� ������������� �������������� �������� ����� ���������.
        <br /><br />
        %s
        <hr />';

$txt['detected_no_caching'] = '<b style="color: red;">�� ����� ������� �� ���������� �������������� SMF �������������.</b>';
$txt['detected_APC'] = '<b style="color: green">�� ����� ������� ���������� APC.';
$txt['detected_eAccelerator'] = '<b style="color: green">�� ����� ������� ���������� eAccelerator.';
$txt['detected_MMCache'] = '<b style="color: green">�� ����� ������� ���������� MMCache.';
$txt['detected_Zend'] = '<b style="color: green">�� ����� ������� ���������� Zend.';
$txt['detected_Memcached'] = '<b style="color: green">�� ����� ������� ���������� Memcached.';

$txt['cache_enable'] = '������� �����������';
$txt['cache_off'] = '����������� ���������';
$txt['cache_level1'] = '������� 1';
$txt['cache_level2'] = '������� 2 (�� �������������)';
$txt['cache_level3'] = '������� 3 (�� �������������)';
$txt['cache_memcached'] = '��������� Memcached';

?>