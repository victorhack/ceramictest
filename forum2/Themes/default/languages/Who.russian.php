<?php
// Version: 1.1; Who

$txt['who_hidden'] = '<em>������� ������������. �������� �� ��������.</em>';
$txt['who_unknown'] = '<em>����������� ��������</em>';
$txt['who_user'] = '������������';
$txt['who_time'] = '�����';
$txt['who_action'] = '��������';

$txt['whoall_activate'] = '���������� ���� ������� ������.';
$txt['whoall_help'] = '������� �������� <a href="' . $scripturl . '?action=help">������</a>.';
$txt['whoall_helpadmin'] = '������� ���������.';
$txt['whoall_pm'] = '������������� ������ ���������.';
$txt['whoall_login'] = '������ �� �����.';
$txt['whoall_login2'] = '������ �� �����.';
$txt['whoall_logout'] = '������� � ������.';
$txt['whoall_markasread'] = '�������� ���� ��� ����������� (�������������).';
$txt['whoall_news'] = '������� �������.';
$txt['whoall_notify'] = '�������� ��������� �����������.';
$txt['whoall_notifyboard'] = '�������� ��������� �����������.';
$txt['whoall_recent'] = '������������� ������ <a href="' . $scripturl . '?action=recent">��������� ���������</a>.';
$txt['whoall_register'] = '�������������� �� ������.';
$txt['whoall_register2'] = '�������������� �� ������.';
$txt['whoall_reminder'] = '��������������� ������.';
$txt['whoall_reporttm'] = '������ ������ ����������.';
$txt['whoall_spellcheck'] = '���������� ��������� ����������';
$txt['whoall_unread'] = '������������� ������������� ���� � ���������� ������.';
$txt['whoall_unreadreplies'] = '������������� ������������� ��������� � ���������� ������.';
$txt['whoall_who'] = '������������� <a href="' . $scripturl . '?action=who">��� Online</a>.';
$txt['whoall_.xml'] = '������������� RSS �������.';

$txt['whoall_collapse_collapse'] = '����������� ���������.';
$txt['whoall_collapse_expand'] = '������������� ���������.';
$txt['whoall_pm_removeall'] = '������� ������ ���������.';
$txt['whoall_pm_send'] = '����� ������ ���������.';
$txt['whoall_pm_send2'] = '���������� ������ ���������.';

$txt['whotopic_dlattach'] = '������������� ��������.';
$txt['whotopic_editpoll'] = '����������� ����������� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot;.';
$txt['whotopic_mergetopics'] = '���������� ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot;.';
$txt['whotopic_movetopic'] = '��������� ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot; � ������ ������.';
$txt['whotopic_post'] = '����� ����� � ���� <a href="' . $scripturl . '?topic=%d.0">%s</a>.';
$txt['whotopic_post2'] = '���������� ����� � ���� <a href="' . $scripturl . '?topic=%d.0">%s</a>.';
$txt['whotopic_printpage'] = '������� ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot;.';
$txt['whotopic_sendtopic'] = '���������� ������ �� ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot; ������ �����.';
$txt['whotopic_splittopics'] = '��������� ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot;.';
$txt['whotopic_vote'] = '�������� � ���� <a href="' . $scripturl . '?topic=%d.0">%s</a>.';

$txt['whopost_quotefast'] = '�������� ��������� � ���� &quot;<a href="' . $scripturl . '?topic=%d.0">%s</a>&quot;.';

$txt['whoadmin_detailedversion'] = '���������� ��������� �������� ������ ������.';
$txt['whoadmin_dumpdb'] = '������� ��������� ����� ���� ������.';
$txt['whoadmin_editagreement'] = '����������� ��������������� ����������.';
$txt['whoadmin_featuresettings'] = '�������� ��������� ������.';
$txt['whoadmin_modlog'] = '������������� ������ �������� �����������.';
$txt['whoadmin_serversettings'] = '�������� ��������� ������.';
$txt['whoadmin_packageget'] = '�������� ������.';
$txt['whoadmin_packages'] = '������������� �������� �������.';
$txt['whoadmin_permissions'] = '����������� ����� �������������.';
$txt['whoadmin_pgdownload'] = '��������� �����.';
$txt['whoadmin_theme'] = '�������� ��������� ���� ����������.';
$txt['whoadmin_trackip'] = '��������� IP ����� ������������.';

$txt['whoallow_manageboards'] = '����������� ��������� �������� � ���������.';
$txt['whoallow_admin'] = '<a href="' . $scripturl . '?action=admin">����� �����������������</a>.';
$txt['whoallow_ban'] = '����������� ��� ����.';
$txt['whoallow_boardrecount'] = '������������� ���������� ������.';
$txt['whoallow_calendar'] = '������������� <a href="' . $scripturl . '?action=calendar">���������</a>.';
$txt['whoallow_editnews'] = '����������� �������.';
$txt['whoallow_mailing'] = '���������� email ���������.';
$txt['whoallow_maintain'] = '��������� ������������ ������.';
$txt['whoallow_manageattachments'] = '��������� ����������.';
$txt['whoallow_mlist'] = '������������� <a href="' . $scripturl . '?action=mlist">������ �������������</a>.';
$txt['whoallow_optimizetables'] = '������������ ������� ������.';
$txt['whoallow_repairboards'] = '��������������� ������� ������.';
$txt['whoallow_search'] = '���������� <a href="' . $scripturl . '?action=search">�������</a>.';
$txt['whoallow_search2'] = '������������� ���������� ������.';
$txt['whoallow_setcensor'] = '����������� ������ �����������.';
$txt['whoallow_setreserve'] = '����������� ����������������� �����.';
$txt['whoallow_stats'] = '������������� <a href="' . $scripturl . '?action=stats">���������� ������</a>.';
$txt['whoallow_viewErrorLog'] = '������������� ������ ������.';
$txt['whoallow_viewmembers'] = '������������� ������ �������������.';

$txt['who_topic'] = '������������� ���� <a href="' . $scripturl . '?topic=%d.0">%s</a>.';
$txt['who_board'] = '������������� ������ <a href="' . $scripturl . '?board=%d.0">%s</a>.';
$txt['who_index'] = '������������� ������� �������� <a href="' . $scripturl . '">' . $context['forum_name'] . '</a>.';
$txt['who_viewprofile'] = '������������� ������� ������������ <a href="' . $scripturl . '?action=profile;u=%d">%s</a>.';
$txt['who_profile'] = '����������� ������� <a href="' . $scripturl . '?action=profile;u=%d">%s</a>.';
$txt['who_post'] = '������� ����� ���� � <a href="' . $scripturl . '?board=%d.0">%s</a>.';
$txt['who_poll'] = '������� ����� ����������� � <a href="' . $scripturl . '?board=%d.0">%s</a>.';

?>