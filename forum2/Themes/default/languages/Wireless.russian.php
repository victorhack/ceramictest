<?php
// Version: 1.1; Wireless

$txt['wireless_error_home'] = '������� �������� ������';
$txt['wireless_error_notyet'] = '��������, ���� ������ � ������ ����� ���������� ��� ���������.';

$txt['wireless_options'] = '�������������� ��������';
$txt['wireless_options_login'] = '�����';
$txt['wireless_options_logout'] = '�����';

$txt['wireless_navigation'] = '���������';
$txt['wireless_navigation_up'] = '�� ������� �����';
$txt['wireless_navigation_next'] = '��������� ��������';
$txt['wireless_navigation_prev'] = '���������� ��������';
$txt['wireless_navigation_index'] = '������� �������� ���������';
$txt['wireless_navigation_topic'] = '����� � �����';

$txt['wireless_pm_inbox'] = '�� ��������';
$txt['wireless_pm_inbox_new'] = '�� �������� (<span style="color: red;">%d �����</span>)';
$txt['wireless_pm_by'] = '';
$txt['wireless_pm_add_buddy'] = '�������� �����';
$txt['wireless_pm_select_buddy'] = '������� �����';
$txt['wireless_pm_search_member'] = '����� ������������';
$txt['wireless_pm_search_name'] = '���';
$txt['wireless_pm_no_recipients'] = '��� �����������';
$txt['wireless_pm_reply'] = '��������';
$txt['wireless_pm_reply_to'] = '��������';

$txt['wireless_recent_unread_posts'] = '������������� ���������';
$txt['wireless_recent_unread_replies'] = '������������� ������';

?>