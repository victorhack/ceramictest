<?php
/**********************************************************************************
* Verification.php                                                                *
***********************************************************************************
* Advanced Visual Verification mod                                                *
* =============================================================================== *
* Software Version:           Advanced Visual Verification 1.1 - $Revision:: 10 $ *
* $Date:: 2007-05-10 20:51:14 +1000 (Thu, 10 May 2007)                          $ *
* Software by:                HarzeM and Daniel15                                 *
* Copyright 2007 by:          HarzeM (http://www.harzem.com)                      *
*                             and Daniel15 (http://www.dansoftaustralia.net)      *
*           2006 by:          HarzeM (http://www.harzem.com)                      *
* Support, News, Updates at:  [put modsite url here]                              *
*                                                                                 *
* Forum software by:          Simple Machines (http://www.simplemachines.org)     *
* Copyright 2006-2007 by:     Simple Machines LLC (http://www.simplemachines.org) *
*           2001-2006 by:     Lewis Media (http://www.lewismedia.com)             *
***********************************************************************************
* This program is free software; you may redistribute it and/or modify it under   *
* the terms of the provided license as published by Simple Machines LLC.          *
*                                                                                 *
* This program is distributed in the hope that it is and will be useful, but      *
* WITHOUT ANY WARRANTIES; without even any implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                            *
*                                                                                 *
* See the "license.txt" file for details of the Simple Machines license.          *
* The latest version can always be found at http://www.simplemachines.org.        *
**********************************************************************************/
if (!defined('SMF'))
	die('Hacking attempt...');

/*	This file has a great job of displaying visual verification codes, aka CAPTCHAs.
	This file is distributed within the Advanced Visual Verification Mod package.

	void getVerificationSettings()
		- sets default values for some settings.

	void VerificationCode()
		- directs to either the image or the sound
		- uses URL settings if the users is admin and is testing

	void initVerificationCode()
		- generates a new code and saves to the database
		- if requested, only reads the current saved value
		- sometimes, clears old and unused database records

	binary verifyCode()
		- reads the current code form the database and verifies
		- check whether maximum failure limit exceeded, and throws an error
		- if code is not valid, increases failure count
		- else, deletes the record and returns true
	
	void verificationNotExceed()
		- if user has exceeded maximum failures, throws an error
	
	void showCodeImage()
		- creates the image to be displayed
		- if GD is not installed, redirects to showBitmapImage()
		- reads font files
		- creates a fancy image according to the settings
		- uses three type of backgrounds, and three sizes
	
	void showBitmapImage()
		- creates an image without GD library
		- creates images using white-black bitmap format
		- can create three different backgrounds and sizes
		- image size is only determined by character number
		- outputs the image with bmp headers and "echo" function
	
	void definedimagettftext()
		- returns imagettftext if FreeType exists
		- tries to imitate imagettftext using buil-in character codes
		- uses getCodeChars() or getCodeChars2() to create fonts
		
	void getCodeChars()
		- defines bitmap characters, for use by showBitmapImage()
		- returns an array
	
	void getCodeChars2()
		- defines another set of bitmap characters, for use by showBitmapImage()
		- returns an array

*/


function getVerificationSettings()
{
	global $modSettings;

	if (empty($modSettings['verificationCodeChars']) || $modSettings['verificationCodeChars'] < 3 || $modSettings['verificationCodeChars'] > 12)
		$modSettings['verificationCodeChars'] = 5;
	if (empty($modSettings['verificationMaxTries']) || $modSettings['verificationMaxTries'] < 2)
		$modSettings['verificationMaxTries'] = 5;
	if (empty($modSettings['verificationWaitTime']) || $modSettings['verificationWaitTime'] > 60)
		$modSettings['verificationWaitTime'] = 15;

	// There are three ways to generate a code.
	// Default is the way 1.
	if (!isset($modSettings['verificationImageBackground']))
		$modSettings['verificationImageBackground'] = 1;
	
	// If not defined, use medium size.
	if (!isset($modSettings['verificationImageSize']))
		$modSettings['verificationImageSize'] = 1;

	
}
// Show the verification code or let it hear.
function VerificationCode()
{
	global $sourcedir, $modSettings, $context, $scripturl;
	

	// Initialize some settings if they are not yet set.
	getVerificationSettings();
	
	// Are you an admin doing some testing?
	if (allowedTo('admin_forum'))
	{
		if (isset($_REQUEST['bgtype']))
			$modSettings['verificationImageBackground'] = $_REQUEST['bgtype'];
		if (isset($_REQUEST['size']))
			$modSettings['verificationImageSize'] = $_REQUEST['size'];
		if (isset($_REQUEST['chars']))
			$modSettings['verificationCodeChars'] = $_REQUEST['chars'];
			
	}
	
	// Visual verification needs to be enabled.
	if (!empty($modSettings['disable_visual_verification']))
		header('HTTP/1.1 401 Not Authorised');

	// Show a window that will play the verification code.
	elseif (isset($_REQUEST['sound']))
	{
		loadLanguage('Login');
		loadTemplate('Register');

		$context['verificiation_sound_href'] = $scripturl . '?action=verificationcode;rand=' . md5(rand()) . ';format=.wav';
		$context['sub_template'] = 'verification_sound';
		$context['template_layers'] = array();

		obExit();
	}

	// Will we display an image?
	if (empty($_REQUEST['format']))
	{
		// We are displaying an image, so we should have a code in it!
		$verification_code = initVerificationCode();

		showCodeImage($verification_code);
		// Normally we don't think you are here.
		header('HTTP/1.1 400 Bad Request');
	
	}

	elseif ($_REQUEST['format'] === '.wav')
	{
		// Read the last one.
		$verification_code = initVerificationCode(false);
		
		require_once($sourcedir . '/Subs-Sound.php');

		if (!createWaveFile($verification_code))
			header('HTTP/1.1 400 Bad Request');
	}

	// We all die one day...
	die();
}

// Generate a random code, save it to the database.
// Or just read it.
function initVerificationCode($generate_new = true)
{
	global $db_prefix, $modSettings, $sc;

	// Initialize some settings if they are not yet set.
	getVerificationSettings();
	
	if (!$generate_new)
	{
		$result = db_query("
			SELECT vcode
			FROM {$db_prefix}verification_code
			WHERE session = '$sc'",__FILE__,__LINE__);
		$row = mysql_fetch_assoc($result);
		mysql_free_result($result);
		
		// Somehow it is not here? Then get one.
		if (empty($row['vcode']))
			$row['vcode'] = initVerificationCode();
			
		return $row['vcode'];
	}
	
	$character_range = array_merge(range('A', 'H'), array('K', 'M', 'N', 'P'), range('R', 'Z'));
	$verification_code = '';
	
	for ($i = 0; $i < $modSettings['verificationCodeChars']; $i++)
		$verification_code .= $character_range[array_rand($character_range)];
	
	// If we already have a record for this session, we should keep the "tries" value intact.
	$result = db_query("
		SELECT tries
		FROM {$db_prefix}verification_code
		WHERE session = '$sc'",__FILE__,__LINE__);
	$row = mysql_fetch_assoc($result);
	mysql_free_result($result);
	$row['tries'] = isset($row['tries']) ? $row['tries'] : 0;
	
	// Now save it to the database.
	db_query("
	REPLACE INTO {$db_prefix}verification_code
		(session, vcode, tries, lastTry)
	VALUES
		('$sc', '$verification_code', '{$row['tries']}' , '" . time() . "')",__FILE__, __LINE__);

	// Occasionally, do some maintenance. Delete records older than 2 days.
	// If someone generated a code but not used it for one day, we don't need it any more.
	// Do this with a probability of one tenth, since we don't need to delete them exactly when they are 2 days old.
	// This way saves us many unnecessary database queries.
	if (rand(0,10) == 0)
		db_query("
			DELETE FROM {$db_prefix}verification_code
			WHERE lastTry < ". (time() - 24 * 60 * 60 * 2), __FILE__, __LINE__);
	
	return $verification_code;
}

// Read the random code from the database and verify.
// Also trigger the brute force protection gun.
function verifyCode($vcode, $just_return = false)
{ 
	global $db_prefix, $modSettings, $sc, $txt;

	// Initialize some settings if they are not yet set.
	getVerificationSettings();
	
	$result = db_query("
		SELECT session, vcode , tries , lastTry
		FROM {$db_prefix}verification_code
		WHERE session = '$sc'", __FILE__, __LINE__);
	$row = mysql_fetch_assoc($result);
	mysql_free_result($result);
	
	// Check against brute force protection.
	if ($row['tries'] >= $modSettings['verificationMaxTries'])
	{
		// It seems you failed too much, but let's see whether you have waited long enough.
		if(time() > $row['lastTry'] + $modSettings['verificationWaitTime'] * 60)
		{
			// Reset the trial number.
			db_query("
				UPDATE {$db_prefix}verification_code
				SET tries = 1, lastTry = " . time() . "
				WHERE session = '$sc'",__FILE__,__LINE__);
			$reset = true;
			$row['tries'] = 0;
		}
		// You failed too much, you didn't wait enough... You are fired!
		else
		{
			if(!$just_return)
				fatal_error(sprintf($txt['verification_brute_force'], $modSettings['verificationWaitTime']),true);
			return false;
		}
	}
	
	
	if($vcode != $row['vcode'])
	{
		// Increase the trial number, if needed
		if (empty($reset)){
			db_query("
				UPDATE {$db_prefix}verification_code
				SET tries = tries + 1, lastTry = ".time()."
				WHERE session = '$sc'",__FILE__,__LINE__);}
		
		// You've tried too much.
		if($row['tries'] + 1 >= $modSettings['verificationMaxTries'])
			if(!$just_return)
				fatal_error(sprintf($txt['verification_brute_force'], $modSettings['verificationWaitTime']),true);
			else
				return false;
		
		return false;
	}
	// Correct code, finally!
	else
	{
		// Remove from the database.
		db_query("
			DELETE FROM {$db_prefix}verification_code
			WHERE session = '$sc'",__FILE__,__LINE__);
		
		return true;
	}
}

// Check whether the user exceeded max trials of visual verification
function verificationNotExceed()
{
	global $db_prefix, $modSettings, $sc, $txt;

	// Initialize some settings if they are not yet set.
	getVerificationSettings();
	
	$result = db_query("
		SELECT session, vcode , tries , lastTry
		FROM {$db_prefix}verification_code
		WHERE session = '$sc'",__FILE__,__LINE__);
	$row = mysql_fetch_assoc($result);
	mysql_free_result($result);
	
	// Check against brute force protection.
	if($row['tries'] >= $modSettings['verificationMaxTries'])
	{
		// It seems you failed too much, but let's see whether you have waited long enough.
		if(time() > $row['lastTry'] + $modSettings['verificationWaitTime'] * 60)
		{
			// Remove from the database.
			db_query("
				DELETE FROM {$db_prefix}verification_code
				WHERE session = '$sc'
				",__FILE__,__LINE__);
		}
		// You failed too much, you didn't wait enough... You are fired!
		else
		{
			fatal_error(sprintf($txt['verification_brute_force'], $modSettings['verificationWaitTime']),true);
			return false;
		}
	}
	return true;
}

// Create the image for the visual verification code.
function showCodeImage($code)
{
	global $settings, $modSettings, $freetype;

	// If no GD is installed, we should generate our own bitmap file! We don't need fancy libraries :P
	if (!in_array('gd', get_loaded_extensions()))
		showBitmapImage($code);

	// Else, let's continue.
	
	// Do we have FreeType support?
	$freetype = function_exists('imagettftext') ? true : false;
	
	// Do we have fonts? If not, assume we don't have FreeType.
	if (!is_dir($settings['default_theme_dir'] . '/ttf_fonts'))
		$freetype = false;
	
	getVerificationSettings();
	
	// 0 small, 1 medium, 2 large image.
	if ($modSettings['verificationImageSize'] == 0)
	{
		$image_width = 200;
		$image_height = 75;
	}
	elseif ($modSettings['verificationImageSize'] == 2)
	{
		$image_width = 450;
		$image_height = 150;
	}
	else
	{
		$image_width = 300;
		$image_height = 100;
	}

	$total_chars = strlen($code);
	
	// Do we have FreeType support to display ttf files?
	if ($freetype)
	{
		// Get a list of the available fonts.
		$font_dir = dir($settings['default_theme_dir'] . '/ttf_fonts');
		$font_list = array();

		while ($entry = $font_dir->read())
			//if (preg_match('~^(.+)\.ttf$~', $entry, $matches) === 1)
			// substr() should be faster than preg_match()...
			if (substr($entry, -4) == '.ttf')
			{
				// Kulminoituva doesn't work well with background 2.
				if(!($entry == 'kulminoituva.ttf' && $modSettings['verificationImageBackground'] == 2))
					$font_list[] = $settings['default_theme_dir'] . '/ttf_fonts/' . $entry;
				// We love acidic font! We want more of it! But only with background noise.
				// If we find the acidic font, add it four times.
				if ($entry == 'acidic.ttf' && $modSettings['verificationImageBackground'] == 0)
					$font_list = array_merge($font_list,
						array(
							$settings['default_theme_dir'] . '/ttf_fonts/' . $entry,
							$settings['default_theme_dir'] . '/ttf_fonts/' . $entry,
							$settings['default_theme_dir'] . '/ttf_fonts/' . $entry,
							$settings['default_theme_dir'] . '/ttf_fonts/' . $entry
						)
					);
			}
	}
	else
	{
		// Some font values to be used in definedimagettftext().
		$font_list = array(
			array(0,0),
			array(0,1),
			array(1,0),
			array(1,1),
		);
	}
	
	
	if (empty($font_list))
		return false;
	$char_height = min(2/3 * $image_width / ($total_chars + 1) , $image_height / 2);
	// Create a list of characters to be shown.
	$characters = array();
	for ($i = 0; $i < strlen($code); $i++)
	{
		$characters[$i] = array(
			'id' => $code{$i},
			'font' => $font_list[array_rand($font_list)],
			'height' => $char_height * (1 + rand(0,3) / 10 - 0.1 ),
		);
	}
	
	if (in_array($modSettings['verificationImageBackground'], array(0,1)))
	{
		// Create an image.
		$code_image = imagecreate($image_width, $image_height);

		$background_color = array(234, 237, 243);
		$foreground_color = array(64, 101, 136);
		// Draw the background.
		$bg_color = imagecolorallocate($code_image, $background_color[0], $background_color[1], $background_color[2]);
		imagefilledrectangle($code_image, 0, 0, $image_width - 1, $image_height - 1, $bg_color);
		
		// Fill in the characters.
		$cur_x = - $char_height/3;
		foreach ($characters as $char_index => $character)
		{
			$cur_x += $image_width / ($total_chars+1);
			
			definedimagettftext(
				$code_image,
				$character['height'],
				rand(-200, 200) / 10,
				$cur_x,
				($image_height + $character['height'])* (1/2 + ((rand(0,10)-5)/30)),
				imagecolorallocate($code_image, $foreground_color[0],$foreground_color[1],$foreground_color[2]),
				$character['font'],
				$character['id']);
			
		}
		// Some waves
		$wave = rand(3,5); // wave strength
		$wave_width = rand(8,15); // wave width
		
		for ($i = 0; $i < $image_width; $i += 2)
			imagecopy($code_image, $code_image, $i - 2, sin($i / $wave_width) * $wave, $i, 0, 2, $image_height);

		// Just a flat background and some noise. This way is bad!!!
		if ($modSettings['verificationImageBackground'] == 0)
		{
			// Add some randomness to the background.
			$randomness_color = imagecolorallocate($code_image, $foreground_color[0],$foreground_color[1],$foreground_color[2]);
			for ($i = 0; $i < $image_height; $i++)
				for ($j = rand(0, 6); $j < $image_width; $j += rand(1, 11))
					imagesetpixel($code_image, $j, $i, $randomness_color);
		}
		// Background with random lines. This way is better.
		else
		{
			// Make this transparent.
			imagecolortransparent($code_image, $bg_color);
			
			// Generate a second image
			$code_image2 = imagecreate($image_width, $image_height);
			$bg_color = imagecolorallocate($code_image2, $background_color[0], $background_color[1], $background_color[2]);
			imagefilledrectangle($code_image2, 0, 0, $image_width - 1, $image_height - 1, $bg_color);
			
			// Draw some random lines to the background.
			for ($i = 0; $i < 3; $i ++)
			{
				$y1 = rand(3,$image_height-7);
				$y2 = rand(3,$image_height-7);
				$x1 = rand(0, $image_width/10) + $i*$image_width/3;
				$x2 = $x1 + rand(0, $image_width/10) + $image_width/3;
				if ($x2 > $image_width - 3)
					$x2 = $image_width - 3;
				
				// Add some anti-aliasing above the line.
				if ($freetype)
				{
					$alias_factor = rand(1,10)/10;
					$line_color = imagecolorallocate($code_image2, ($foreground_color[0] + $background_color[0] * (3 + $alias_factor)) / (4 + $alias_factor), ($foreground_color[1] + $background_color[1] * (3 + $alias_factor)) / (4 + $alias_factor), ($foreground_color[2] + $background_color[2] * (3 + $alias_factor)) / (4 + $alias_factor));
					if (rand(0,1))
						imageline($code_image2, $x1, $y1, $x2, $y2, $line_color);
					
					$alias_factor = rand(1,10) / 10 + 1;
					$line_color = imagecolorallocate($code_image2, ($foreground_color[0] + $background_color[0] * $alias_factor) / (1 + $alias_factor), ($foreground_color[1] + $background_color[1] * $alias_factor) / (1 + $alias_factor), ($foreground_color[2] + $background_color[2] * $alias_factor) / (1 + $alias_factor));
					imageline($code_image2, $x1, $y1 + 1, $x2, $y2 + 1, $line_color);
				}
				
				for ($j = 2; $j < $image_width / 60 + 1; $j++) // thickness 3, 5, 7 pixels.
				{
					$line_color = imagecolorallocate($code_image2, rand(max($foreground_color[0] - 2, 0), $foreground_color[0]), rand(max($foreground_color[1] - 2, 0), $foreground_color[1]), rand(max($foreground_color[2] - 2, 0), $foreground_color[2]));
					imageline($code_image2, $x1, $y1 + $j, $x2, $y2 + $j, $line_color);
					
				}
				
				// Add some anti-aliasing below the line.
				if($freetype)
				{
					$alias_factor = rand(1,10) / 10 + 1;
					$line_color = imagecolorallocate($code_image2, ($foreground_color[0] + $background_color[0] * $alias_factor) / (1 + $alias_factor),($foreground_color[1] + $background_color[1] * $alias_factor) / (1 + $alias_factor), ($foreground_color[2] + $background_color[2] * $alias_factor) / (1 + $alias_factor));			
					imageline($code_image2, $x1, $y1 + $j, $x2, $y2 + $j, $line_color);
					
					$alias_factor = rand(1,10) / 10;
					$line_color = imagecolorallocate($code_image2, ($foreground_color[0] + $background_color[0] * (3 + $alias_factor)) / (4 + $alias_factor), ($foreground_color[1] + $background_color[1] * (3 + $alias_factor)) / (4 + $alias_factor), ($foreground_color[2] + $background_color[2] * (3 + $alias_factor)) / (4 + $alias_factor));
					if (rand(0,1))
						imageline($code_image2, $x1, $y1 + $j + 1, $x2, $y2 + $j + 1, $line_color);
				}
			}
			// Add some waves
			$wave = rand(3,7); // wave strength
			$wave_width = rand(5,15); // wave width
			
			for ($i = 0; $i < $image_width; $i += 2)
				imagecopy($code_image2, $code_image2, $i - 2, sin($i / $wave_width) * $wave, $i, 0, 2, $image_height);
			
			imagecopymerge($code_image2, $code_image, 0, 0, 0, 0, $image_width, $image_height, 100);
			$code_image = $code_image2;
		}
	}
	
	// Colored background with coloured letters. Good way, but a bit slow and difficult to read.
	elseif ($modSettings['verificationImageBackground'] == 2)
	{
		// Create a truecolor image.
		$code_image = imagecreatetruecolor($image_width, $image_height);

		// Draw the background.
		$bg_color = imagecolorallocate($code_image, 0, 0, 0);
		imagefilledrectangle($code_image, 0, 0, $image_width - 1, $image_height - 1, $bg_color);
		
		// Generate randomness centers.
		$rand_centers = array();
		for ($i = 0; $i < $image_width / 40; $i++)
			$rand_centers[] = array(rand(0, $image_width), rand(0, $image_height));
		
		$rand_radius = $image_width / 5;
		$multiplier = $modSettings['verificationImageSize'] == 2 ? 3 : 5;
		
		// Gerenate the background with a complex algorithm.
		for ($x = 0; $x < $image_width; $x += 2)
		{
			for ($y = 0; $y < $image_height; $y += 2)
			{
				// Generate a strange pixel color.
				$pixel_color = array(0,0,0);
				foreach ($rand_centers as $value)
				{
					$distancex = hypot($x-$value[0], $y-$value[1]);
					if ($distancex > $rand_radius)
						$distancex = 0;
					
					$distancey = hypot($y-$value[0], $x-$value[1]);
					if ($distancey > $rand_radius)
						$distancey = 0;
					
					
					$pixel_color[0] += $distancex * $multiplier + $distancey;
					$pixel_color[1] += $distancey * $multiplier + $distancex;
					$pixel_color[2] += $distancex * ($multiplier + 1) / 2 + $distancex * ($multiplier + 1) / 2;
				}
				$pixel_color[0] = $pixel_color[0] % 256;
				$pixel_color[1] = $pixel_color[1] % 256;
				$pixel_color[2] = $pixel_color[2] % 256;
				
				$pixel_color = imagecolorallocate($code_image, $pixel_color[0],$pixel_color[1],$pixel_color[2]);
				imageline($code_image, $x, $y, $x, $y + 1, $pixel_color);
				imageline($code_image, $x + 1, $y, $x + 1, $y + 1, $pixel_color);
				
			}
			
		}

		
		// Fill in the characters.
		$cur_x = -$char_height / 3;
		foreach ($characters as $char_index => $character)
		{
			$cur_x += $image_width / ($total_chars + 1);
			
			$letter_color = array();
			$letter_color[0] = rand(120,250);
			$letter_color[1] = rand(120,250);
			$letter_color[2] = (500 - $letter_color[0] - $letter_color[1]) > 120 ? (500 - $letter_color[0] - $letter_color[1]) : (120 + rand(0,20));
			$letter_color[2] = $letter_color[2] < 250 ? $letter_color[2] : (250 - rand(0,20));
	
			$letter_color2 = imagecolorallocate($code_image, $letter_color[0] / (1 + rand(5,10) / 5), $letter_color[1] / (1 + rand(5,10)/5),$letter_color[2]/(1+rand(5,10)/5));	
			$letter_color = imagecolorallocate($code_image, $letter_color[0],$letter_color[1],$letter_color[2]);
			
			
			
			$rotation = rand(-200, 200) / 10;
			$y_pos= ($image_height + $character['height']) * (1 / 2 + ((rand(0, 10) - 5) / 30));
			
			$x_displace = rand(1, 3);
			$y_displace = rand(1, 3);
			
			// One side of shadow.
			definedimagettftext(
				$code_image,
				$character['height'],
				$rotation,
				$cur_x + $x_displace,
				$y_pos + $y_displace,
				$letter_color2,
				$character['font'],
				$character['id']);
			// Other side of the shadow.
			definedimagettftext(
				$code_image,
				$character['height'],
				$rotation,
				$cur_x - $x_displace,
				$y_pos - $y_displace,
				$letter_color2,
				$character['font'],
				$character['id']);
			// Character itself.
			definedimagettftext(
				$code_image,
				$character['height'],
				$rotation,
				$cur_x,
				$y_pos,
				$letter_color,
				$character['font'],
				$character['id']);
			
		}
		// Some waves
		$wave = rand(3, 5); // wave strength
		$wave_width = rand(10, 15); // wave width
		
		for ($i = 0; $i < $image_width; $i += 2)
			imagecopy($code_image, $code_image, $i - 2, sin($i / $wave_width) * $wave, $i, 0, 2, $image_height);		
	}

	// Show the image. We prefer gif, especially if we are using coloured background.
	if (function_exists('imagegif'))
	{
		header('Content-type: image/gif');
		imagegif($code_image);
	}
	else
	{
		header('Content-type: image/png');
		imagepng($code_image);
	}

	// Bail out.
	imagedestroy($code_image);
	die();
}


// Generates a bitmap array for visual verification, then sends it as black-white bitmap.
// Does NOT require GD library!
function showBitmapImage($code)
{
	global $settings, $modSettings;

	// There are three image sizes defined. Medium: 320*100, Large: 448*150, Very Large: 608*150 (all are multiples of 32 in width).
	// Larger sizes are to be used with longer strings.
	
	// File headers.
	$file_header_medium = array(66,77,222,15,0,0,0,0,0,0,62,0,0,0,40,0,0,0,64,1,0,0,100,0,0,0,1,0,1,0,0,0,0,0,160,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0);
	$file_header_large = array(66,77,14,33,0,0,0,0,0,0,62,0,0,0,40,0,0,0,192,1,0,0,150,0,0,0,1,0,1,0,0,0,0,0,208,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0);
	$file_header_verylarge = array(66,77,198,44,0,0,0,0,0,0,62,0,0,0,40,0,0,0,96,2,0,0,150,0,0,0,1,0,1,0,0,0,0,0,136,44,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0);
	
	getVerificationSettings();
	
	// image size.
	if (strlen($code) <= 6)
	{
		$image_width = 320;
		$image_height = 100;
		$code_image = $file_header_medium;
	}	
	elseif (strlen($code) <= 10)
	{
		$image_width = 448;
		$image_height = 150;
		$code_image = $file_header_large;
	}
	else
	{
		$image_width = 608;
		$image_height = 150;
		$code_image = $file_header_verylarge;
	}
	// Create an image array.
	$image = array();

	// Initialize the array so that we have spaces for eveything.
	for ($i = 0; $i < $image_width; $i++)
	{
		$image[$i] = array();
		for ($j = 0; $j < $image_height; $j++)
			$image[$i][$j] = 0;
	}
	
	// We have some offset from left and top. Also some character spacing.
	$x_offset = ($image_width - strlen($code) * 29) / (strlen($code) + 1);
	$x_spacing = $x_offset + 29;
	$y_offset = $image_height / 2 - 24;
	
	$char_data = array();
	// Get the characters to the image.
	for ($char = 0; $char < strlen($code); $char++)
	{
		$char_data[$char] = array();
		
		$use = 256;
		
		// Get a temporary table. We have 2 type of built-in fonts!
		$c_char = rand(0,1) ? getCodeChars($code{$char}) : getCodeChars2($code{$char});
		
		// Sometimes invert the character colour for more security.
		$invert = rand(0,2) == 0 ? 1 : 0;
		
		$char_data[$char]['y_rand'] = rand(0, $image_height) / 3 - $image_height / 6;
		for ($j = 0; $j < 48; $j++)
		{
			for ($i = 0; $i < 29; $i++)
			{
				$use = $use / 2;
				if ($c_char[((int)($j * 29 + $i) / 8)] >= $use)
				{
					$image[$i + $char * $x_spacing + $x_offset][$j + $y_offset + $char_data[$char]['y_rand']] = 1 - $invert;
					$c_char[((int)($j * 29 + $i) / 8)] -= $use;
				}
				else
					$image[$i + $char * $x_spacing + $x_offset][$j + $y_offset + $char_data[$char]['y_rand']] = $invert;
					
				if ($use == 1)
					$use = 256;
			}
		}
		unset($c_char);
	}
	
	// You may not believe but, we are even doing some rotation!
	for ($char = 0; $char < strlen($code); $char ++)
	{
		// Rotation angle.
		$rot = (rand(0,40) - 20) * M_PI / 180;
		
		// Some saving of typing and repetitive calculation.
		$rcenter_x = $char * $x_spacing + $x_offset + 14;
		$rcenter_y = $y_offset + 24 + $char_data[$char]['y_rand'];
		
		$dummy_char = array();
		// We don't use strict character borders, because they will get wider after rotation.
		for ($i = $char * $x_spacing + $x_offset - 9; $i < $char * $x_spacing + $x_offset + 38; $i++)	
		{
			$dummy_char[$i] = array();
			for ($j = $y_offset - 6 + $char_data[$char]['y_rand']; $j < $y_offset + 54 + $char_data[$char]['y_rand']; $j++)
			{
				$angle = atan2(($j-$rcenter_y), ($i-$rcenter_x)) + $rot;
				$hypo = hypot(($j-$rcenter_y), ($i-$rcenter_x));
				
				$dummy_char[$i][$j] = isset($image[cos($angle) * $hypo + $rcenter_x][sin($angle) * $hypo + $rcenter_y]) ? $image[cos($angle) * $hypo + $rcenter_x][sin($angle) * $hypo + $rcenter_y] : 0;
				
			}
		}
		for ($i = $char * $x_spacing + $x_offset - 9; $i < $char * $x_spacing + $x_offset + 38; $i++)	
		{
			for ($j = $y_offset - 6 + $char_data[$char]['y_rand']; $j < $y_offset + 54 + $char_data[$char]['y_rand']; $j++)
				$image[$i][$j] = $dummy_char[$i][$j];
		}
	}
		
	// create a background image, including some distorting lines.
	$bimage = array();
	
	// Assign line pixels,
	// Also create some waves,
	// Also merge with text image,
	// Also wave the text image!
	// Also create some inversion areas!
	// Also create some background noise!
	// Some of these are optional!
	
	// Wave for text
	$wave = rand(3, 5); // Wave strength.
	$wave_width = rand(10, 15); // Wave width.

	// Will we need background lines?
	if (in_array($modSettings['verificationImageBackground'], array(1, 2)))
	{
		$r_lines = array();
		// create some randomness lines.
		for($i = 0; $i < 3; $i ++)
		{
			$r_lines[$i] = array(
				rand(5, $image_height - 10), // y1
				rand(5, $image_height - 10), // y2
				rand(0, $image_width / 10) + $i * $image_width / 3, // x1
				);
			$r_lines[$i][3] = $r_lines[$i][2] + rand(0, $image_width / 10) + $image_width / 3; // x2
			if($r_lines[$i][3] > $image_width - 15) $r_lines[$i][3] = $image_width - 15; // x2 fix
			
			// To calculate the distance, we'll need the formula.
			$r_lines[$i]['a'] = $r_lines[$i][0] - $r_lines[$i][1]; // y1 - y2
			$r_lines[$i]['b'] = $r_lines[$i][3] - $r_lines[$i][2]; // x2 - x1
			$r_lines[$i]['c'] = $r_lines[$i][2] * ($r_lines[$i][1] - $r_lines[$i][0]) - $r_lines[$i][0] * ($r_lines[$i][3] - $r_lines[$i][2]); // don't make me comment in this!
			$r_lines[$i]['h'] = hypot($r_lines[$i]['a'],$r_lines[$i]['b']);
		}
		// Wave for lines
		$wave2 = rand(3, 5); // Wave strength.
		$wave2_width = rand(5, 15); // Wave width.
	}
	
	// If only some noise.
	if ($modSettings['verificationImageBackground'] == 0)
	{
		for ($i = 0; $i < $image_width; $i++)
		{
			$bimage[$i] = array();
			for ($j = 0; $j < $image_height; $j++)
			{
				$bimage[$i][$j] = 0;
				// Merge with text.
				if (isset($image[$i][$j + sin($i / $wave_width) * $wave]) && $image[$i][$j + sin($i / $wave_width) * $wave] == 1)
					$bimage[$i][$j] = 1;
				// Noise.
				if(rand(0,5) == 0)
					$bimage[$i][$j] = 1;
			}
		}
	}
	// Background lines
	elseif ($modSettings['verificationImageBackground'] == 1)
	{
		for ($i = 0; $i < $image_width; $i++)
		{
			$bimage[$i] = array();
			for ($j = 0; $j < $image_height; $j++)
			{
				$bimage[$i][$j] = 0;
				// Check lines.
				foreach ($r_lines as $r_line)
				{
					// Detect whether this pixel is within a line.
					if ((($i > $r_line[2] && $i < $r_line[3])))
						if (abs($r_line['a'] * $i + $r_line['b'] * ($j + sin($i / $wave2_width) * $wave2) + $r_line['c']) / $r_line['h'] < 2)
							$bimage[$i][$j] = 1;
				}
				// Merge with text
				if (isset($image[$i][$j + sin($i / $wave_width) * $wave]) && $image[$i][$j + sin($i / $wave_width) * $wave] == 1)
					$bimage[$i][$j] = 1;
				
			}
		}
	}
	// Background lines + inversion circles.
	else
	{
		$inversion = array();
		for ($i = 0; $i < (int) $image_width / 100; $i++)
			$inversion[] = array(rand(0,$image_width), rand(0,$image_height));

		for ($i = 0; $i < $image_width; $i++)
		{
			$bimage[$i] = array();
			for ($j = 0; $j < $image_height; $j++)
			{
				$bimage[$i][$j] = 0;
				// Check lines.
				foreach ($r_lines as $r_line)
				{
					// Detect whether this pixel is within a line.
					if ((($i > $r_line[2] && $i < $r_line[3])))
						if (abs($r_line['a'] * $i + $r_line['b'] * ($j + sin($i / $wave2_width) * $wave2) + $r_line['c']) / $r_line['h'] < 2)
							$bimage[$i][$j] = 1;
				}
				// Merge with text
				if (isset($image[$i][$j + sin($i / $wave_width) * $wave]) && $image[$i][$j + sin($i / $wave_width) * $wave] == 1)
					$bimage[$i][$j] = 1;
				
				// Add inversion. This is slow.
				foreach ($inversion as $inv)
				{
					if (hypot(($i - $inv[0]),($j - $inv[1])) < $image_height * 2 / 3)
						$bimage[$i][$j] = 1 - $bimage[$i][$j];
				}
			}
		}
	}

	// Convert image array to a printable character array.
	for ($j = $image_height - 1; $j >= 0; $j--)
	{
		for ($i = 0; $i < $image_width; $i += 8)	
		{
			$value = 0;
			$power = 128;
			for($k = 0; $k<8; $k++)
			{
				$value += ($bimage[$i+$k][$j]) * $power;
				$power /= 2;
			}
			$code_image[] = $value;
		}
	}
	
	// Send the image.
	header('Content-type: image/bmp');
	foreach ($code_image as $char)
		echo chr($char);
	
	// Get out of here.
	die();
	
}

// Returns imagettftext if it exists, else simulates it as much as it can.
// imagettftext doesn't exist without FreeType library.
function definedimagettftext($image, $size, $rot, $x, $y, $color, $fontfile, $text)
{
	global $modSettings, $freetype;
	
	// Hopefully we have FreeType!
	if($freetype)
		return imagettftext($image, $size, $rot, $x, $y, $color, $fontfile, $text);
	
	
	// No imagettftext? Go and install FreeType to your server >:(
	
	// At least do you have GD 2 when using background 2?
	$gdinfo = gd_info();
	if(strpos($gdinfo['GD Version'], "2") !== false && $modSettings['verificationImageBackground'] == 2)
		$gd2 = true;
	else
		$gd2 = false;

	// Get the data of the character. Font is randomly sent already.
	$c_char = $fontfile[0] == 0 ? getCodeChars($text) : getCodeChars2($text);
	// Sometimes invert the character color for more security.
	$invert = $fontfile[1];
	
	// Get the characters as a two dimensional array.
	$char = array();	
	
	$use = 256;

	for ($j = 0; $j < 48; $j++)
	{
		for ($i = 0; $i < 29; $i++)
		{
			if (!isset($char[$i]))
				$char[$i] = array();
				
			$use = $use / 2;
			if ($c_char[((int)($j*29+$i) / 8)] >= $use)
			{
				$char[$i][$j] = 1 - $invert;
				$c_char[((int)($j*29+$i) / 8)] -= $use;
			}
			else
				$char[$i][$j] = $invert;
				
			if($use == 1)
				$use = 256;
		}
	}
	
	// Now the important part, paste the char to the image, by also doing the rotation.
	
	// Some calculation fix.
	$y -= 48;
	$size = $size / 32;
	$rot = $rot * M_PI / 180;
	
	// A rotated char can have at most 60 pixels in width or height.
	for ($j = -6 * $size; $j < 54 * $size; $j++)
	{
		for ($i = -15 * $size; $i < 44 * $size; $i++)
		{
			$angle = atan2(($j - 24 * $size),$i - 14 * $size) + $rot;
			$hypo = hypot(($j - 24 * $size),$i - 14 * $size);
			
			$_sin = sin($angle) * $hypo / $size + 24 * $size;
			$_cos = cos($angle) * $hypo / $size + 14 * $size;
			
			if($_sin >= 0 && $_sin < 48 && $_cos >= 0 && $_cos < 29 && $char[$_cos][$_sin] == 1)
				imagesetpixel($image, $i + $x, $j + $y, $color);
		}
	}
}

// Bitmaps for each character.
function getCodeChars($char = false)
{
	$c_chars = array(
'A' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 224, 0, 0, 63, 0, 0, 1, 252, 0, 0, 15, 224, 0, 0, 255, 0, 0, 7, 252, 0, 0, 123, 224, 0, 3, 207, 128, 0, 30, 124, 0, 1, 225, 224, 0, 15, 15, 128, 0, 248, 124, 0, 7, 129, 240, 0, 60, 15, 128, 3, 224, 62, 0, 30, 1, 240, 1, 240, 15, 128, 15, 128, 62, 0, 120, 1, 240, 7, 255, 255, 192, 63, 255, 254, 3, 255, 255, 240, 31, 0, 7, 192, 248, 0, 62, 15, 128, 1, 248, 124, 0, 7, 199, 224, 0, 63, 62, 0, 0, 249, 240, 0, 7, 223, 128, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'B' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 240, 0, 63, 255, 240, 1, 255, 255, 192, 15, 128, 127, 0, 124, 0, 252, 3, 224, 3, 224, 31, 0, 31, 0, 248, 0, 248, 7, 192, 7, 192, 62, 0, 62, 1, 240, 3, 224, 15, 128, 62, 0, 124, 15, 224, 3, 255, 254, 0, 31, 255, 192, 0, 255, 255, 192, 7, 192, 255, 0, 62, 0, 252, 1, 240, 3, 240, 15, 128, 15, 192, 124, 0, 62, 3, 224, 1, 240, 31, 0, 15, 128, 248, 0, 124, 7, 192, 3, 224, 62, 0, 62, 1, 240, 7, 240, 15, 255, 255, 0, 127, 255, 240, 3, 255, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'C' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 248, 0, 7, 255, 248, 0, 255, 255, 192, 15, 224, 62, 1, 252, 0, 48, 15, 128, 0, 0, 248, 0, 0, 15, 128, 0, 0, 124, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 15, 128, 0, 0, 126, 0, 0, 1, 248, 0, 0, 15, 224, 0, 0, 63, 128, 2, 0, 255, 128, 240, 1, 255, 255, 128, 3, 255, 252, 0, 3, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'D' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 224, 0, 63, 255, 224, 1, 255, 255, 192, 15, 128, 127, 0, 124, 0, 252, 3, 224, 3, 240, 31, 0, 15, 128, 248, 0, 62, 7, 192, 1, 240, 62, 0, 15, 129, 240, 0, 62, 15, 128, 1, 240, 124, 0, 15, 131, 224, 0, 124, 31, 0, 3, 224, 248, 0, 31, 7, 192, 0, 248, 62, 0, 7, 193, 240, 0, 62, 15, 128, 1, 224, 124, 0, 31, 3, 224, 0, 248, 31, 0, 7, 128, 248, 0, 124, 7, 192, 7, 192, 62, 0, 124, 1, 240, 31, 224, 15, 255, 252, 0, 127, 255, 192, 3, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'E' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 255, 192, 31, 255, 254, 0, 255, 255, 240, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 255, 255, 128, 15, 255, 252, 0, 127, 255, 224, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 255, 255, 192, 63, 255, 254, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'F' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 255, 224, 31, 255, 255, 0, 255, 255, 248, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 255, 254, 0, 127, 255, 240, 3, 255, 255, 128, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'G' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 248, 0, 7, 255, 248, 0, 255, 255, 192, 15, 224, 62, 1, 252, 0, 48, 15, 128, 0, 0, 248, 0, 0, 15, 128, 0, 0, 124, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 31, 248, 124, 0, 255, 195, 224, 7, 254, 31, 128, 1, 240, 124, 0, 15, 131, 224, 0, 124, 15, 128, 3, 224, 126, 0, 31, 1, 248, 0, 248, 15, 224, 7, 192, 63, 128, 62, 0, 255, 1, 240, 1, 255, 255, 128, 3, 255, 252, 0, 7, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'H' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 192, 3, 224, 62, 0, 31, 1, 240, 0, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 240, 31, 0, 15, 128, 248, 0, 124, 7, 192, 3, 224, 62, 0, 31, 1, 240, 0, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 240, 31, 255, 255, 128, 255, 255, 252, 7, 255, 255, 224, 62, 0, 31, 1, 240, 0, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 240, 31, 0, 15, 128, 248, 0, 124, 7, 192, 3, 224, 62, 0, 31, 1, 240, 0, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'I' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 255, 224, 31, 255, 255, 0, 255, 255, 248, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 7, 255, 255, 192, 63, 255, 254, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'J' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 255, 128, 1, 255, 252, 0, 15, 255, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 31, 0, 0, 0, 248, 0, 32, 7, 192, 1, 224, 252, 0, 15, 255, 192, 0, 127, 252, 0, 0, 127, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'K' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 192, 15, 224, 62, 0, 254, 1, 240, 7, 224, 15, 128, 126, 0, 124, 7, 224, 3, 224, 62, 0, 31, 3, 224, 0, 248, 63, 0, 7, 195, 240, 0, 62, 31, 0, 1, 241, 240, 0, 15, 159, 0, 0, 124, 240, 0, 3, 239, 0, 0, 31, 248, 0, 0, 251, 224, 0, 7, 223, 128, 0, 62, 126, 0, 1, 241, 248, 0, 15, 135, 224, 0, 124, 31, 128, 3, 224, 252, 0, 31, 3, 240, 0, 248, 15, 192, 7, 192, 63, 0, 62, 0, 252, 1, 240, 3, 240, 15, 128, 31, 192, 124, 0, 127, 3, 224, 1, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'L' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 255, 255, 192, 63, 255, 254, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'M' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 192, 1, 248, 127, 0, 15, 195, 248, 0, 254, 31, 192, 7, 240, 255, 0, 63, 135, 248, 3, 252, 61, 224, 29, 225, 239, 0, 239, 15, 120, 15, 120, 121, 224, 115, 195, 207, 3, 158, 30, 120, 60, 240, 241, 225, 199, 135, 143, 30, 60, 60, 120, 241, 225, 225, 231, 15, 15, 15, 120, 120, 120, 123, 195, 195, 193, 252, 30, 30, 15, 224, 240, 240, 127, 7, 135, 129, 240, 60, 60, 15, 129, 225, 224, 124, 15, 15, 0, 0, 120, 120, 0, 3, 195, 192, 0, 30, 30, 0, 0, 240, 240, 0, 7, 135, 128, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'N' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 192, 1, 224, 62, 0, 15, 1, 248, 0, 120, 15, 224, 3, 192, 127, 0, 30, 3, 252, 0, 240, 31, 224, 7, 128, 255, 128, 60, 7, 190, 1, 224, 60, 240, 15, 1, 231, 192, 120, 15, 30, 3, 192, 120, 248, 30, 3, 195, 224, 240, 30, 15, 7, 128, 240, 124, 60, 7, 129, 225, 224, 60, 15, 143, 1, 224, 60, 120, 15, 1, 243, 192, 120, 7, 222, 3, 192, 30, 240, 30, 0, 255, 128, 240, 3, 252, 7, 128, 31, 224, 60, 0, 127, 1, 224, 1, 248, 15, 0, 15, 192, 120, 0, 62, 3, 192, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'O' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 254, 0, 0, 63, 252, 0, 7, 255, 248, 0, 126, 7, 224, 7, 224, 31, 128, 62, 0, 124, 3, 224, 1, 240, 30, 0, 7, 129, 240, 0, 62, 15, 128, 1, 240, 124, 0, 15, 135, 192, 0, 62, 62, 0, 1, 241, 240, 0, 15, 143, 128, 0, 124, 124, 0, 3, 227, 224, 0, 31, 31, 0, 0, 248, 248, 0, 7, 199, 192, 0, 62, 62, 0, 1, 240, 248, 0, 31, 7, 192, 0, 248, 62, 0, 7, 192, 240, 0, 60, 7, 192, 3, 224, 31, 0, 62, 0, 252, 3, 240, 3, 240, 63, 0, 15, 255, 240, 0, 31, 254, 0, 0, 63, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'P' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 252, 0, 31, 255, 248, 0, 255, 255, 240, 7, 192, 31, 192, 62, 0, 126, 1, 240, 1, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 240, 31, 0, 15, 128, 248, 0, 124, 7, 192, 3, 224, 62, 0, 62, 1, 240, 3, 240, 15, 128, 63, 0, 124, 7, 240, 3, 255, 255, 0, 31, 255, 224, 0, 255, 248, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Q' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 254, 0, 0, 63, 252, 0, 7, 255, 248, 0, 126, 7, 224, 7, 224, 31, 128, 62, 0, 124, 3, 224, 1, 240, 30, 0, 7, 129, 240, 0, 62, 15, 128, 1, 240, 124, 0, 15, 135, 192, 0, 62, 62, 0, 1, 241, 240, 0, 15, 143, 128, 0, 124, 124, 0, 3, 227, 224, 0, 31, 31, 0, 0, 248, 248, 0, 7, 199, 192, 0, 62, 62, 0, 1, 240, 248, 0, 31, 7, 192, 0, 248, 62, 0, 7, 192, 240, 0, 60, 7, 192, 3, 224, 31, 0, 62, 0, 124, 3, 240, 3, 240, 63, 0, 15, 255, 240, 0, 31, 254, 0, 0, 63, 240, 0, 0, 7, 224, 0, 0, 31, 192, 0, 0, 63, 192, 0, 0, 255, 128, 0, 1, 248, 0, 0, 7, 128, 0, 0, 12, ),
'R' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 224, 0, 63, 255, 224, 1, 255, 255, 128, 15, 128, 254, 0, 124, 1, 240, 3, 224, 7, 192, 31, 0, 62, 0, 248, 1, 240, 7, 192, 15, 128, 62, 0, 124, 1, 240, 3, 224, 15, 128, 62, 0, 124, 1, 240, 3, 224, 63, 0, 31, 3, 240, 0, 255, 255, 0, 7, 255, 240, 0, 63, 255, 128, 1, 240, 254, 0, 15, 131, 248, 0, 124, 15, 192, 3, 224, 63, 0, 31, 0, 252, 0, 248, 7, 240, 7, 192, 31, 192, 62, 0, 126, 1, 240, 1, 248, 15, 128, 7, 224, 124, 0, 63, 131, 224, 0, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'S' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 255, 128, 0, 63, 255, 128, 7, 255, 252, 0, 62, 3, 224, 3, 192, 1, 0, 60, 0, 0, 1, 224, 0, 0, 15, 0, 0, 0, 120, 0, 0, 3, 192, 0, 0, 31, 0, 0, 0, 252, 0, 0, 3, 252, 0, 0, 15, 248, 0, 0, 63, 248, 0, 0, 255, 240, 0, 0, 255, 224, 0, 1, 255, 128, 0, 1, 254, 0, 0, 3, 240, 0, 0, 7, 192, 0, 0, 30, 0, 0, 0, 240, 0, 0, 7, 128, 0, 0, 60, 0, 0, 1, 224, 16, 0, 30, 0, 224, 1, 240, 7, 224, 63, 0, 63, 255, 240, 1, 255, 254, 0, 0, 255, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'T' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 255, 255, 252, 255, 255, 255, 231, 255, 255, 255, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'U' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 192, 1, 240, 62, 0, 15, 129, 240, 0, 124, 15, 128, 3, 224, 124, 0, 31, 3, 224, 0, 248, 31, 0, 7, 192, 248, 0, 62, 7, 192, 1, 240, 62, 0, 15, 129, 240, 0, 124, 15, 128, 3, 224, 124, 0, 31, 3, 224, 0, 248, 31, 0, 7, 192, 248, 0, 62, 7, 192, 1, 240, 62, 0, 15, 129, 240, 0, 124, 15, 128, 3, 224, 124, 0, 31, 3, 224, 0, 248, 31, 0, 7, 192, 124, 0, 124, 3, 224, 3, 224, 31, 0, 31, 0, 124, 1, 240, 1, 248, 63, 0, 15, 255, 240, 0, 31, 255, 0, 0, 63, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'V' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0, 124, 248, 0, 3, 231, 192, 0, 31, 63, 0, 1, 248, 248, 0, 15, 135, 224, 0, 124, 31, 0, 7, 192, 248, 0, 62, 7, 224, 1, 240, 31, 0, 31, 0, 252, 0, 248, 3, 224, 15, 128, 31, 0, 124, 0, 252, 3, 224, 3, 224, 62, 0, 31, 129, 240, 0, 124, 15, 0, 3, 224, 248, 0, 31, 135, 192, 0, 124, 60, 0, 3, 243, 224, 0, 15, 158, 0, 0, 124, 240, 0, 3, 255, 128, 0, 15, 248, 0, 0, 127, 192, 0, 1, 252, 0, 0, 15, 224, 0, 0, 127, 0, 0, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'W' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 0, 31, 224, 0, 0, 255, 0, 0, 7, 188, 0, 0, 57, 224, 0, 1, 207, 0, 0, 14, 120, 15, 128, 243, 192, 124, 7, 158, 3, 240, 60, 248, 31, 129, 195, 193, 252, 14, 30, 15, 224, 112, 240, 119, 7, 135, 131, 188, 60, 60, 29, 225, 225, 225, 239, 14, 7, 142, 56, 112, 60, 113, 227, 129, 227, 143, 60, 15, 28, 121, 224, 121, 195, 207, 3, 206, 15, 112, 30, 112, 123, 128, 123, 131, 220, 3, 216, 30, 224, 31, 192, 127, 0, 254, 3, 248, 7, 240, 31, 128, 63, 0, 252, 0, 248, 3, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'X' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 192, 0, 124, 126, 0, 3, 193, 248, 0, 60, 7, 224, 3, 192, 63, 128, 62, 0, 252, 3, 224, 3, 240, 30, 0, 31, 193, 224, 0, 126, 30, 0, 1, 249, 240, 0, 7, 255, 0, 0, 63, 240, 0, 0, 255, 0, 0, 3, 248, 0, 0, 15, 192, 0, 0, 254, 0, 0, 7, 248, 0, 0, 127, 224, 0, 7, 255, 128, 0, 124, 252, 0, 3, 195, 240, 0, 60, 15, 192, 3, 224, 127, 0, 62, 1, 248, 3, 224, 7, 224, 30, 0, 31, 129, 224, 0, 252, 31, 0, 3, 241, 240, 0, 15, 223, 0, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Y' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0, 124, 248, 0, 3, 195, 224, 0, 60, 31, 0, 3, 224, 124, 0, 62, 3, 240, 1, 224, 15, 128, 31, 0, 62, 1, 240, 1, 248, 15, 0, 7, 192, 248, 0, 31, 15, 128, 0, 248, 120, 0, 3, 231, 192, 0, 31, 252, 0, 0, 127, 192, 0, 1, 254, 0, 0, 15, 224, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Z' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 255, 240, 63, 255, 255, 129, 255, 255, 252, 0, 0, 7, 224, 0, 0, 62, 0, 0, 3, 224, 0, 0, 62, 0, 0, 3, 224, 0, 0, 63, 0, 0, 1, 240, 0, 0, 31, 0, 0, 1, 240, 0, 0, 31, 128, 0, 0, 248, 0, 0, 15, 128, 0, 0, 248, 0, 0, 15, 128, 0, 0, 252, 0, 0, 7, 192, 0, 0, 124, 0, 0, 7, 192, 0, 0, 124, 0, 0, 3, 224, 0, 0, 62, 0, 0, 3, 224, 0, 0, 62, 0, 0, 3, 240, 0, 0, 31, 255, 255, 224, 255, 255, 255, 7, 255, 255, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'0' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 248, 0, 0, 63, 240, 0, 7, 255, 224, 0, 126, 31, 128, 3, 192, 60, 0, 62, 1, 240, 3, 224, 7, 192, 30, 0, 30, 0, 240, 0, 240, 15, 128, 7, 192, 124, 0, 62, 3, 192, 0, 240, 62, 0, 7, 193, 240, 0, 62, 15, 128, 1, 240, 124, 0, 15, 131, 224, 0, 124, 31, 0, 3, 224, 248, 0, 31, 7, 192, 0, 248, 62, 0, 7, 193, 240, 0, 62, 15, 128, 1, 240, 124, 0, 15, 131, 224, 0, 124, 15, 0, 3, 192, 124, 0, 62, 3, 224, 1, 240, 15, 0, 15, 0, 120, 0, 120, 3, 224, 7, 192, 15, 128, 124, 0, 60, 3, 192, 1, 248, 126, 0, 7, 255, 224, 0, 15, 252, 0, 0, 31, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'1' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 7, 192, 0, 1, 254, 0, 0, 127, 240, 0, 31, 255, 128, 0, 254, 124, 0, 7, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 15, 255, 255, 224, 127, 255, 255, 3, 255, 255, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'2' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 248, 0, 3, 255, 248, 0, 31, 255, 224, 0, 248, 31, 128, 6, 0, 126, 0, 0, 1, 240, 0, 0, 15, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 248, 0, 0, 7, 192, 0, 0, 124, 0, 0, 7, 192, 0, 0, 124, 0, 0, 7, 192, 0, 0, 124, 0, 0, 7, 192, 0, 0, 124, 0, 0, 3, 192, 0, 0, 62, 0, 0, 3, 224, 0, 0, 62, 0, 0, 1, 224, 0, 0, 31, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 252, 0, 0, 7, 255, 255, 128, 63, 255, 252, 1, 255, 255, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'3' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 248, 0, 1, 255, 248, 0, 15, 255, 224, 0, 112, 63, 128, 2, 0, 124, 0, 0, 3, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 128, 0, 0, 124, 0, 0, 3, 192, 0, 0, 62, 0, 0, 7, 192, 0, 15, 252, 0, 0, 127, 192, 0, 3, 255, 192, 0, 0, 127, 0, 0, 0, 252, 0, 0, 3, 240, 0, 0, 15, 128, 0, 0, 126, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 15, 128, 0, 0, 124, 0, 0, 7, 192, 3, 192, 252, 0, 31, 255, 192, 0, 255, 252, 0, 1, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'4' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 0, 3, 224, 0, 0, 63, 0, 0, 1, 248, 0, 0, 31, 192, 0, 0, 254, 0, 0, 15, 240, 0, 0, 247, 128, 0, 7, 188, 0, 0, 121, 224, 0, 7, 143, 0, 0, 60, 120, 0, 3, 195, 192, 0, 62, 30, 0, 1, 224, 240, 0, 30, 7, 128, 0, 240, 60, 0, 15, 1, 224, 0, 248, 15, 0, 7, 128, 120, 0, 120, 3, 192, 7, 192, 30, 0, 60, 0, 240, 1, 255, 255, 252, 15, 255, 255, 224, 127, 255, 255, 0, 0, 15, 0, 0, 0, 120, 0, 0, 3, 192, 0, 0, 30, 0, 0, 0, 240, 0, 0, 7, 128, 0, 0, 60, 0, 0, 1, 224, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'5' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 0, 7, 255, 248, 0, 63, 255, 192, 1, 255, 254, 0, 14, 0, 0, 0, 112, 0, 0, 3, 128, 0, 0, 28, 0, 0, 0, 224, 0, 0, 7, 0, 0, 0, 56, 0, 0, 1, 192, 0, 0, 14, 0, 0, 0, 127, 128, 0, 3, 255, 128, 0, 31, 254, 0, 0, 3, 252, 0, 0, 3, 240, 0, 0, 15, 128, 0, 0, 62, 0, 0, 1, 240, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 252, 0, 0, 15, 192, 1, 192, 252, 0, 15, 255, 192, 0, 127, 248, 0, 0, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'6' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 128, 0, 15, 255, 128, 1, 255, 252, 0, 31, 129, 224, 1, 248, 1, 0, 15, 0, 0, 0, 248, 0, 0, 15, 128, 0, 0, 124, 0, 0, 3, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 15, 0, 0, 0, 248, 0, 0, 7, 193, 248, 0, 62, 63, 240, 1, 247, 255, 192, 15, 184, 63, 0, 127, 128, 252, 3, 248, 3, 240, 31, 128, 15, 128, 252, 0, 126, 7, 224, 1, 240, 63, 0, 15, 129, 248, 0, 124, 15, 192, 3, 224, 62, 0, 31, 1, 240, 0, 248, 15, 128, 7, 192, 60, 0, 124, 1, 240, 3, 224, 15, 128, 30, 0, 62, 1, 240, 0, 252, 63, 0, 3, 255, 240, 0, 15, 255, 0, 0, 31, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'7' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 255, 192, 63, 255, 254, 1, 255, 255, 240, 15, 255, 255, 128, 0, 0, 120, 0, 0, 3, 192, 0, 0, 60, 0, 0, 1, 224, 0, 0, 30, 0, 0, 0, 224, 0, 0, 15, 0, 0, 0, 240, 0, 0, 7, 128, 0, 0, 120, 0, 0, 3, 192, 0, 0, 60, 0, 0, 1, 224, 0, 0, 30, 0, 0, 0, 240, 0, 0, 15, 0, 0, 0, 248, 0, 0, 7, 128, 0, 0, 124, 0, 0, 3, 192, 0, 0, 62, 0, 0, 1, 240, 0, 0, 31, 0, 0, 0, 248, 0, 0, 7, 192, 0, 0, 124, 0, 0, 3, 224, 0, 0, 31, 0, 0, 1, 240, 0, 0, 15, 128, 0, 0, 124, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'8' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 254, 0, 0, 127, 252, 0, 7, 255, 240, 0, 124, 15, 192, 7, 192, 62, 0, 60, 0, 248, 3, 192, 3, 192, 30, 0, 30, 0, 240, 0, 240, 7, 128, 7, 128, 62, 0, 120, 1, 240, 3, 192, 7, 192, 60, 0, 63, 131, 192, 0, 254, 124, 0, 3, 255, 192, 0, 15, 248, 0, 0, 127, 224, 0, 7, 255, 192, 0, 125, 255, 0, 7, 195, 252, 0, 124, 7, 240, 3, 224, 31, 192, 31, 0, 126, 1, 240, 1, 248, 15, 128, 15, 192, 124, 0, 62, 3, 224, 1, 240, 31, 0, 15, 128, 248, 0, 124, 7, 224, 7, 192, 31, 0, 62, 0, 252, 3, 224, 3, 248, 62, 0, 15, 255, 224, 0, 31, 254, 0, 0, 63, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'9' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 252, 0, 0, 127, 248, 0, 7, 255, 224, 0, 126, 31, 128, 7, 192, 62, 0, 60, 0, 248, 3, 224, 7, 192, 31, 0, 30, 1, 240, 0, 248, 15, 128, 7, 192, 124, 0, 62, 3, 224, 1, 248, 31, 0, 15, 192, 248, 0, 126, 7, 192, 3, 240, 63, 0, 31, 128, 248, 0, 252, 7, 224, 15, 224, 31, 128, 255, 0, 126, 14, 248, 1, 255, 247, 192, 7, 254, 62, 0, 15, 193, 240, 0, 0, 15, 128, 0, 0, 120, 0, 0, 7, 192, 0, 0, 62, 0, 0, 1, 224, 0, 0, 31, 0, 0, 0, 248, 0, 0, 15, 128, 0, 0, 248, 0, 64, 15, 192, 3, 192, 252, 0, 31, 255, 192, 0, 255, 248, 0, 0, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
	);
	
	return (empty($char)) ? $c_chars : $c_chars[$char];
}

// Bitmaps for each character. Second type.
function getCodeChars2($char = false)
{

	$c_chars = array(
'A' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 252, 0, 0, 15, 240, 0, 0, 255, 192, 0, 15, 15, 0, 0, 112, 56, 0, 3, 129, 192, 0, 60, 15, 0, 1, 196, 56, 0, 30, 33, 224, 0, 225, 135, 0, 7, 28, 56, 0, 120, 225, 224, 3, 135, 135, 0, 60, 124, 60, 1, 195, 240, 224, 14, 31, 135, 0, 241, 252, 60, 7, 15, 240, 224, 120, 127, 135, 131, 128, 0, 28, 28, 0, 0, 225, 224, 0, 7, 142, 31, 254, 28, 240, 255, 240, 247, 7, 255, 131, 184, 120, 30, 29, 195, 128, 112, 252, 60, 3, 131, 255, 192, 31, 255, 252, 0, 127, 223, 192, 1, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'B' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 255, 128, 7, 255, 255, 0, 63, 255, 252, 1, 192, 1, 240, 14, 0, 3, 192, 112, 0, 15, 3, 135, 240, 56, 28, 63, 225, 192, 225, 255, 14, 7, 14, 120, 112, 56, 127, 195, 129, 195, 252, 60, 14, 31, 131, 192, 112, 0, 60, 3, 128, 7, 224, 28, 0, 15, 128, 225, 248, 30, 7, 15, 240, 112, 56, 127, 195, 193, 195, 159, 14, 14, 28, 56, 112, 112, 225, 195, 131, 135, 254, 28, 28, 63, 224, 224, 225, 254, 15, 7, 0, 0, 112, 56, 0, 15, 129, 192, 1, 248, 15, 255, 255, 128, 127, 255, 240, 3, 255, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'C' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 248, 0, 15, 255, 248, 1, 255, 255, 192, 31, 128, 63, 1, 240, 0, 56, 30, 0, 1, 193, 224, 127, 14, 30, 15, 255, 113, 224, 255, 255, 142, 15, 131, 248, 112, 240, 3, 135, 135, 0, 0, 56, 120, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 224, 0, 7, 135, 0, 0, 28, 60, 0, 0, 224, 252, 31, 7, 131, 255, 252, 30, 15, 255, 240, 120, 15, 227, 129, 224, 0, 28, 15, 192, 0, 224, 63, 128, 63, 0, 255, 255, 240, 1, 255, 255, 128, 3, 255, 252, 0, 3, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'D' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 254, 0, 7, 255, 254, 0, 127, 255, 248, 3, 128, 7, 224, 28, 0, 7, 128, 224, 0, 30, 7, 15, 224, 120, 56, 127, 193, 225, 195, 255, 7, 14, 28, 124, 56, 112, 224, 225, 227, 135, 7, 135, 28, 56, 28, 56, 225, 192, 225, 199, 14, 7, 14, 56, 112, 56, 113, 195, 129, 195, 142, 28, 14, 28, 112, 224, 112, 227, 135, 7, 143, 28, 56, 56, 112, 225, 207, 195, 135, 15, 252, 60, 56, 127, 193, 193, 195, 240, 30, 14, 0, 1, 224, 112, 0, 62, 3, 128, 7, 224, 31, 255, 254, 0, 127, 255, 192, 3, 255, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'E' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 255, 248, 3, 255, 255, 224, 63, 255, 255, 129, 192, 0, 28, 14, 0, 0, 224, 112, 0, 7, 3, 135, 255, 248, 28, 63, 255, 128, 225, 255, 248, 7, 14, 0, 0, 56, 112, 0, 1, 195, 255, 192, 14, 31, 255, 0, 112, 255, 252, 3, 128, 0, 224, 28, 0, 7, 0, 224, 0, 56, 7, 15, 255, 192, 56, 127, 252, 1, 195, 255, 192, 14, 28, 0, 0, 112, 224, 0, 3, 135, 255, 240, 28, 63, 255, 192, 225, 255, 255, 7, 0, 0, 56, 56, 0, 1, 193, 192, 0, 14, 15, 255, 255, 240, 63, 255, 255, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'F' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 255, 254, 3, 255, 255, 248, 31, 255, 255, 224, 224, 0, 7, 7, 0, 0, 56, 56, 0, 1, 193, 195, 255, 254, 14, 31, 255, 224, 112, 255, 254, 3, 135, 0, 0, 28, 56, 0, 0, 225, 255, 240, 7, 15, 255, 192, 56, 127, 255, 1, 192, 0, 56, 14, 0, 1, 192, 112, 0, 14, 3, 135, 255, 240, 28, 63, 255, 0, 225, 255, 240, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 254, 0, 0, 63, 224, 0, 1, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'G' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 248, 0, 15, 255, 248, 1, 255, 255, 192, 31, 128, 63, 1, 240, 0, 56, 30, 0, 1, 193, 224, 127, 14, 30, 15, 255, 113, 224, 255, 255, 142, 15, 131, 248, 112, 240, 3, 135, 135, 0, 0, 56, 120, 255, 225, 195, 143, 255, 142, 28, 255, 254, 112, 231, 0, 115, 135, 56, 3, 156, 57, 192, 28, 225, 239, 240, 231, 135, 63, 135, 28, 60, 252, 56, 224, 252, 225, 199, 131, 255, 14, 30, 15, 248, 112, 120, 15, 195, 129, 224, 0, 28, 15, 192, 0, 224, 63, 128, 63, 0, 255, 255, 240, 1, 255, 255, 128, 3, 255, 252, 0, 7, 254, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'H' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 126, 1, 248, 7, 248, 31, 224, 127, 225, 255, 131, 135, 14, 28, 28, 56, 112, 224, 225, 195, 135, 7, 14, 28, 56, 56, 112, 225, 193, 195, 135, 14, 14, 28, 56, 112, 112, 225, 195, 131, 135, 254, 28, 28, 63, 240, 224, 225, 255, 135, 7, 0, 0, 56, 56, 0, 1, 193, 192, 0, 14, 14, 31, 248, 112, 112, 255, 195, 131, 135, 254, 28, 28, 56, 112, 224, 225, 195, 135, 7, 14, 28, 56, 56, 112, 225, 193, 195, 135, 14, 14, 28, 56, 112, 112, 225, 195, 131, 135, 14, 28, 31, 248, 127, 224, 127, 129, 254, 3, 248, 7, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'I' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 255, 248, 7, 255, 255, 224, 127, 255, 255, 131, 128, 0, 28, 28, 0, 0, 224, 224, 0, 7, 7, 254, 31, 248, 31, 240, 255, 128, 127, 135, 248, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 1, 254, 31, 224, 31, 240, 255, 129, 255, 135, 254, 14, 0, 0, 112, 112, 0, 3, 131, 128, 0, 28, 31, 255, 255, 224, 127, 255, 254, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'J' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 255, 192, 1, 255, 255, 128, 31, 255, 252, 0, 224, 0, 224, 7, 0, 7, 0, 56, 0, 56, 1, 255, 225, 192, 7, 255, 14, 0, 31, 248, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 60, 60, 56, 3, 255, 193, 192, 63, 254, 30, 1, 207, 224, 240, 14, 0, 15, 0, 112, 0, 248, 3, 224, 31, 192, 15, 255, 252, 0, 63, 255, 192, 0, 127, 252, 0, 0, 127, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'K' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 126, 7, 248, 7, 248, 127, 224, 127, 231, 255, 131, 135, 56, 28, 28, 59, 193, 224, 225, 252, 30, 7, 15, 193, 224, 56, 126, 30, 1, 195, 225, 224, 14, 30, 14, 0, 112, 224, 240, 3, 135, 15, 0, 28, 48, 240, 0, 225, 15, 0, 7, 8, 240, 0, 56, 3, 192, 1, 194, 15, 0, 14, 24, 56, 0, 112, 225, 224, 3, 135, 135, 128, 28, 60, 30, 0, 225, 240, 120, 7, 15, 193, 224, 56, 127, 7, 129, 195, 188, 30, 14, 28, 240, 120, 112, 227, 129, 227, 135, 30, 7, 31, 248, 127, 248, 127, 129, 255, 131, 248, 7, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'L' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 3, 252, 0, 0, 63, 240, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 255, 224, 28, 63, 255, 128, 225, 255, 254, 7, 0, 0, 112, 56, 0, 3, 129, 192, 0, 28, 15, 255, 255, 224, 63, 255, 254, 1, 255, 255, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'M' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 254, 1, 252, 15, 248, 31, 248, 255, 225, 255, 199, 7, 14, 14, 56, 60, 112, 113, 192, 231, 131, 142, 7, 56, 28, 112, 61, 192, 227, 136, 254, 71, 28, 71, 226, 56, 226, 63, 17, 199, 24, 249, 142, 56, 199, 140, 113, 198, 60, 99, 142, 56, 231, 28, 113, 198, 56, 227, 142, 49, 199, 28, 120, 158, 56, 227, 192, 241, 199, 30, 7, 142, 56, 248, 124, 113, 199, 195, 227, 142, 62, 31, 28, 113, 255, 248, 227, 143, 255, 199, 28, 119, 238, 56, 227, 128, 113, 199, 28, 3, 142, 63, 224, 31, 240, 254, 0, 127, 135, 224, 1, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'N' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 126, 0, 248, 7, 248, 15, 224, 127, 224, 255, 131, 135, 7, 28, 28, 60, 56, 224, 224, 241, 199, 7, 3, 142, 56, 56, 30, 113, 193, 192, 123, 142, 14, 1, 220, 112, 113, 15, 227, 131, 136, 63, 28, 28, 96, 248, 224, 227, 7, 199, 7, 28, 30, 56, 56, 240, 241, 193, 199, 131, 142, 14, 62, 12, 112, 113, 248, 99, 131, 143, 193, 28, 28, 127, 0, 224, 227, 184, 7, 7, 29, 224, 56, 56, 231, 129, 193, 199, 28, 14, 14, 56, 240, 112, 113, 195, 195, 131, 142, 14, 28, 31, 240, 127, 224, 127, 1, 254, 3, 240, 7, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'O' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 0, 0, 127, 254, 0, 7, 255, 248, 0, 124, 3, 224, 7, 128, 7, 128, 120, 0, 30, 7, 131, 240, 120, 56, 63, 193, 195, 195, 255, 15, 28, 60, 60, 57, 225, 192, 225, 238, 30, 7, 135, 112, 224, 28, 59, 135, 0, 225, 220, 56, 7, 14, 225, 192, 56, 119, 14, 1, 195, 184, 112, 14, 29, 195, 128, 112, 238, 30, 7, 135, 120, 112, 56, 121, 195, 195, 195, 143, 15, 252, 60, 56, 63, 193, 193, 224, 252, 30, 7, 128, 1, 224, 30, 0, 30, 0, 252, 3, 240, 3, 255, 255, 0, 15, 255, 240, 0, 31, 254, 0, 0, 63, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'P' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 255, 128, 3, 255, 255, 0, 63, 255, 254, 1, 192, 1, 248, 14, 0, 3, 224, 112, 0, 7, 3, 135, 248, 60, 28, 63, 224, 224, 225, 255, 135, 7, 14, 28, 56, 56, 112, 225, 193, 195, 135, 14, 14, 28, 248, 112, 112, 255, 135, 131, 135, 248, 56, 28, 63, 3, 192, 224, 0, 60, 7, 0, 3, 192, 56, 0, 252, 1, 195, 255, 192, 14, 31, 252, 0, 112, 255, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 15, 252, 0, 0, 63, 192, 0, 1, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Q' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 0, 0, 127, 254, 0, 7, 255, 248, 0, 124, 3, 224, 7, 128, 7, 128, 120, 0, 30, 7, 131, 240, 120, 56, 63, 193, 195, 195, 255, 15, 28, 60, 60, 57, 225, 192, 225, 238, 30, 7, 135, 112, 224, 28, 59, 135, 0, 225, 220, 56, 7, 14, 225, 192, 56, 119, 14, 1, 195, 184, 112, 14, 29, 195, 128, 112, 238, 30, 7, 135, 120, 112, 56, 121, 195, 195, 195, 143, 15, 252, 60, 56, 63, 193, 193, 224, 252, 30, 7, 128, 1, 224, 30, 0, 30, 0, 124, 0, 254, 3, 255, 131, 248, 15, 254, 7, 224, 31, 248, 7, 0, 63, 224, 120, 0, 7, 195, 128, 0, 31, 188, 0, 0, 127, 192, 0, 0, 255, 128, 0, 1, 248, 0, 0, 7, 128, 0, 0, 12, ),
'R' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 255, 128, 7, 255, 255, 0, 63, 255, 252, 1, 192, 1, 240, 14, 0, 3, 192, 112, 0, 14, 3, 135, 240, 120, 28, 63, 193, 192, 225, 255, 14, 7, 14, 56, 112, 56, 113, 195, 129, 195, 190, 28, 14, 31, 225, 224, 112, 254, 14, 3, 135, 192, 240, 28, 0, 15, 0, 224, 1, 240, 7, 0, 15, 0, 56, 120, 56, 1, 195, 193, 224, 14, 31, 7, 128, 112, 252, 30, 3, 135, 240, 112, 28, 59, 131, 192, 225, 222, 15, 7, 14, 120, 60, 56, 113, 224, 225, 195, 135, 7, 15, 252, 63, 248, 127, 192, 255, 131, 252, 3, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'S' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 128, 0, 127, 255, 128, 7, 255, 254, 0, 124, 1, 248, 7, 128, 1, 192, 120, 0, 14, 7, 131, 248, 112, 56, 127, 255, 129, 195, 255, 248, 14, 30, 31, 128, 112, 254, 0, 3, 131, 254, 0, 30, 7, 254, 0, 112, 7, 248, 3, 224, 7, 224, 15, 192, 7, 128, 63, 128, 30, 0, 127, 128, 120, 0, 255, 129, 192, 1, 254, 14, 7, 129, 248, 112, 127, 131, 195, 135, 255, 254, 28, 57, 255, 225, 225, 193, 252, 14, 14, 0, 0, 240, 112, 0, 31, 3, 248, 3, 240, 15, 255, 255, 0, 63, 255, 240, 1, 255, 254, 0, 0, 255, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'T' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 255, 255, 255, 31, 255, 255, 253, 255, 255, 255, 254, 0, 0, 3, 240, 0, 0, 31, 128, 0, 0, 255, 254, 31, 255, 127, 240, 255, 241, 255, 135, 255, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 63, 240, 0, 0, 255, 0, 0, 3, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'U' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 126, 0, 252, 7, 248, 15, 240, 127, 224, 255, 195, 135, 7, 14, 28, 56, 56, 112, 225, 193, 195, 135, 14, 14, 28, 56, 112, 112, 225, 195, 131, 135, 14, 28, 28, 56, 112, 224, 225, 195, 135, 7, 14, 28, 56, 56, 112, 225, 193, 195, 135, 14, 14, 28, 56, 112, 112, 225, 195, 131, 135, 14, 28, 28, 56, 112, 224, 225, 195, 135, 7, 14, 28, 56, 56, 112, 225, 227, 195, 135, 135, 252, 60, 28, 63, 193, 192, 240, 124, 30, 3, 128, 0, 224, 31, 0, 31, 0, 126, 3, 240, 1, 255, 255, 0, 15, 255, 240, 0, 31, 255, 0, 0, 63, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'V' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 248, 0, 63, 31, 224, 3, 253, 255, 128, 63, 254, 28, 3, 195, 240, 224, 28, 63, 135, 129, 225, 252, 28, 14, 15, 240, 240, 112, 243, 131, 135, 135, 30, 28, 56, 120, 112, 241, 195, 131, 131, 158, 28, 30, 30, 225, 224, 112, 127, 14, 3, 195, 240, 240, 14, 31, 135, 0, 112, 124, 56, 3, 195, 195, 192, 14, 14, 28, 0, 120, 113, 224, 1, 195, 14, 0, 14, 8, 112, 0, 120, 71, 128, 1, 192, 56, 0, 15, 3, 192, 0, 56, 28, 0, 1, 192, 224, 0, 15, 15, 0, 0, 63, 240, 0, 0, 255, 0, 0, 3, 240, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'W' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 224, 0, 31, 63, 128, 1, 255, 254, 0, 31, 252, 120, 0, 227, 225, 192, 7, 31, 14, 126, 120, 252, 119, 251, 143, 227, 255, 252, 119, 31, 135, 227, 184, 252, 63, 29, 199, 224, 248, 238, 30, 7, 199, 112, 240, 60, 123, 199, 129, 227, 142, 60, 7, 28, 113, 198, 56, 227, 142, 49, 199, 28, 49, 142, 56, 225, 140, 99, 199, 140, 225, 28, 28, 71, 136, 224, 226, 60, 71, 7, 17, 226, 56, 56, 159, 1, 193, 192, 252, 30, 14, 7, 224, 224, 120, 63, 7, 1, 195, 248, 56, 15, 253, 255, 192, 63, 199, 252, 0, 252, 31, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'X' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 252, 0, 126, 31, 240, 7, 253, 255, 192, 127, 238, 15, 7, 135, 120, 60, 56, 121, 224, 227, 199, 135, 135, 188, 120, 28, 31, 195, 128, 240, 124, 60, 3, 193, 227, 192, 14, 14, 60, 0, 120, 33, 192, 1, 224, 30, 0, 7, 129, 224, 0, 28, 30, 0, 0, 240, 120, 0, 15, 1, 192, 0, 240, 15, 0, 7, 8, 60, 0, 120, 224, 240, 7, 135, 131, 128, 120, 124, 30, 3, 135, 240, 120, 60, 123, 193, 195, 195, 143, 15, 60, 60, 56, 61, 195, 193, 224, 254, 60, 7, 131, 255, 192, 31, 253, 252, 0, 127, 223, 192, 1, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Y' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 252, 0, 63, 31, 240, 3, 253, 255, 192, 63, 254, 14, 3, 195, 248, 120, 28, 61, 225, 225, 227, 199, 7, 14, 28, 60, 60, 241, 224, 240, 255, 14, 3, 131, 240, 240, 30, 15, 143, 0, 120, 120, 112, 1, 193, 199, 128, 15, 4, 56, 0, 60, 3, 192, 0, 224, 60, 0, 7, 129, 192, 0, 30, 30, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 31, 248, 0, 0, 127, 128, 0, 3, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'Z' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 255, 252, 7, 255, 255, 240, 127, 255, 255, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 255, 252, 28, 31, 255, 193, 224, 127, 254, 30, 0, 1, 225, 224, 0, 30, 30, 0, 1, 225, 224, 0, 30, 14, 0, 0, 224, 240, 0, 15, 15, 0, 0, 240, 240, 0, 15, 15, 0, 0, 240, 112, 0, 7, 7, 128, 0, 120, 120, 0, 7, 135, 128, 0, 120, 120, 0, 7, 135, 255, 240, 120, 63, 255, 195, 131, 255, 255, 28, 0, 0, 56, 224, 0, 1, 199, 0, 0, 14, 63, 255, 255, 240, 255, 255, 255, 7, 255, 255, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'0' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 248, 0, 0, 63, 240, 0, 7, 255, 224, 0, 127, 255, 128, 3, 224, 124, 0, 60, 0, 240, 3, 192, 3, 192, 60, 30, 15, 1, 193, 248, 56, 30, 31, 225, 224, 225, 231, 135, 7, 14, 28, 56, 56, 112, 225, 195, 199, 135, 143, 28, 56, 28, 56, 225, 192, 225, 199, 14, 7, 14, 56, 112, 56, 113, 195, 129, 195, 142, 28, 14, 28, 112, 224, 112, 227, 135, 3, 135, 28, 56, 28, 56, 225, 192, 225, 199, 143, 15, 30, 28, 56, 112, 224, 225, 195, 135, 7, 15, 60, 56, 60, 63, 195, 192, 224, 252, 28, 7, 131, 193, 224, 30, 0, 30, 0, 120, 1, 224, 1, 240, 62, 0, 7, 255, 224, 0, 31, 254, 0, 0, 63, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'1' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 7, 224, 0, 1, 255, 128, 0, 127, 254, 0, 31, 254, 112, 0, 255, 131, 128, 15, 224, 28, 0, 112, 0, 224, 3, 128, 135, 0, 28, 252, 56, 0, 255, 225, 192, 3, 255, 14, 0, 15, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 1, 255, 15, 248, 31, 248, 127, 225, 255, 195, 255, 142, 0, 0, 28, 112, 0, 0, 227, 128, 0, 7, 31, 255, 255, 248, 127, 255, 255, 131, 255, 255, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'2' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 248, 0, 3, 255, 248, 0, 31, 255, 224, 1, 255, 255, 128, 31, 128, 254, 0, 224, 1, 240, 7, 0, 7, 192, 56, 252, 30, 1, 223, 240, 240, 15, 255, 195, 128, 63, 14, 28, 0, 224, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 1, 225, 192, 0, 14, 30, 0, 0, 240, 224, 0, 15, 15, 0, 0, 240, 240, 0, 15, 15, 0, 0, 240, 240, 0, 15, 15, 0, 0, 240, 240, 0, 7, 15, 0, 0, 120, 240, 0, 7, 135, 0, 0, 120, 120, 0, 3, 135, 255, 128, 60, 63, 254, 1, 193, 255, 248, 14, 0, 1, 192, 112, 0, 14, 3, 128, 0, 112, 31, 255, 255, 128, 127, 255, 252, 1, 255, 255, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'3' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 248, 0, 1, 255, 248, 0, 15, 255, 224, 0, 255, 255, 128, 15, 128, 126, 0, 112, 0, 120, 3, 128, 1, 192, 28, 126, 15, 0, 255, 248, 56, 3, 255, 225, 192, 15, 135, 14, 0, 0, 56, 112, 0, 3, 195, 128, 3, 252, 60, 0, 63, 225, 192, 3, 252, 30, 0, 28, 3, 224, 0, 224, 31, 0, 7, 0, 60, 0, 63, 192, 240, 0, 255, 131, 128, 3, 254, 30, 0, 0, 248, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 135, 0, 124, 60, 56, 7, 255, 195, 192, 127, 252, 28, 3, 143, 193, 224, 28, 0, 30, 0, 224, 1, 224, 7, 192, 62, 0, 31, 255, 224, 0, 255, 254, 0, 1, 255, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'4' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 252, 0, 0, 15, 240, 0, 0, 255, 192, 0, 15, 14, 0, 0, 240, 112, 0, 7, 3, 128, 0, 120, 28, 0, 7, 128, 224, 0, 56, 7, 0, 3, 196, 56, 0, 60, 97, 192, 1, 195, 14, 0, 30, 56, 112, 1, 227, 195, 128, 14, 30, 28, 0, 241, 240, 224, 15, 31, 135, 0, 112, 252, 56, 7, 143, 225, 240, 120, 255, 15, 195, 135, 248, 127, 28, 0, 0, 56, 224, 0, 1, 199, 0, 0, 14, 63, 255, 135, 240, 255, 252, 63, 3, 255, 225, 240, 0, 7, 14, 0, 0, 56, 112, 0, 1, 195, 128, 0, 14, 28, 0, 0, 112, 224, 0, 3, 255, 0, 0, 15, 240, 0, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'5' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 0, 15, 255, 248, 0, 255, 255, 224, 7, 0, 7, 0, 56, 0, 56, 1, 192, 1, 192, 14, 63, 254, 0, 113, 255, 224, 3, 143, 254, 0, 28, 112, 0, 0, 227, 224, 0, 7, 31, 192, 0, 56, 255, 128, 1, 192, 126, 0, 14, 0, 248, 0, 112, 1, 224, 3, 254, 7, 128, 15, 252, 28, 0, 63, 240, 224, 0, 7, 135, 128, 0, 30, 28, 0, 0, 112, 224, 0, 3, 135, 0, 0, 28, 56, 0, 0, 225, 192, 0, 15, 14, 0, 120, 112, 112, 7, 255, 135, 128, 127, 248, 56, 3, 159, 131, 192, 28, 0, 60, 0, 224, 3, 192, 7, 192, 124, 0, 31, 255, 192, 0, 127, 252, 0, 0, 255, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'6' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 127, 128, 0, 15, 255, 128, 1, 255, 252, 0, 31, 255, 224, 1, 248, 15, 128, 15, 0, 28, 0, 240, 0, 224, 15, 7, 199, 0, 240, 255, 248, 7, 15, 255, 128, 120, 120, 248, 3, 135, 128, 0, 28, 63, 240, 1, 225, 255, 192, 14, 31, 255, 128, 112, 240, 62, 3, 134, 0, 248, 28, 32, 1, 192, 224, 62, 15, 7, 3, 248, 56, 56, 63, 225, 225, 193, 199, 7, 14, 14, 60, 56, 112, 240, 225, 195, 135, 135, 14, 30, 28, 56, 112, 112, 225, 195, 131, 135, 158, 60, 30, 31, 225, 192, 112, 254, 14, 3, 193, 224, 240, 15, 0, 15, 0, 60, 0, 240, 0, 248, 31, 0, 3, 255, 240, 0, 15, 255, 0, 0, 31, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'7' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 255, 255, 192, 63, 255, 254, 3, 255, 255, 248, 28, 0, 1, 192, 224, 0, 14, 7, 0, 0, 112, 56, 0, 3, 129, 255, 254, 60, 7, 255, 241, 192, 31, 255, 30, 0, 0, 112, 224, 0, 7, 143, 0, 0, 56, 112, 0, 3, 199, 128, 0, 28, 56, 0, 1, 227, 192, 0, 14, 28, 0, 0, 241, 224, 0, 7, 14, 0, 0, 120, 240, 0, 3, 135, 0, 0, 60, 120, 0, 1, 195, 128, 0, 30, 60, 0, 0, 225, 192, 0, 15, 14, 0, 0, 112, 240, 0, 3, 135, 0, 0, 60, 56, 0, 1, 195, 192, 0, 14, 28, 0, 0, 112, 224, 0, 3, 255, 0, 0, 15, 240, 0, 0, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'8' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 254, 0, 0, 127, 252, 0, 7, 255, 240, 0, 127, 255, 192, 7, 224, 31, 0, 60, 0, 60, 3, 192, 0, 224, 60, 31, 7, 129, 193, 252, 28, 14, 31, 240, 224, 112, 227, 135, 3, 135, 156, 56, 28, 31, 225, 192, 224, 254, 30, 7, 131, 225, 224, 30, 6, 30, 0, 120, 1, 224, 1, 224, 31, 0, 30, 0, 124, 1, 225, 0, 240, 30, 30, 3, 192, 225, 248, 14, 15, 15, 240, 120, 112, 247, 193, 195, 135, 30, 14, 28, 56, 120, 112, 225, 193, 195, 135, 15, 30, 28, 56, 63, 240, 225, 224, 255, 15, 7, 3, 224, 240, 60, 0, 15, 0, 248, 0, 240, 3, 240, 31, 0, 15, 255, 240, 0, 31, 255, 0, 0, 63, 224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
'9' => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 252, 0, 0, 127, 248, 0, 7, 255, 224, 0, 127, 255, 128, 7, 224, 126, 0, 60, 0, 248, 3, 192, 1, 192, 60, 30, 15, 1, 193, 252, 56, 14, 31, 225, 224, 241, 231, 135, 7, 14, 28, 56, 56, 112, 225, 225, 195, 135, 135, 14, 28, 60, 56, 112, 241, 193, 195, 131, 142, 14, 30, 31, 240, 112, 112, 127, 3, 131, 193, 240, 28, 15, 0, 16, 224, 60, 1, 135, 0, 240, 60, 56, 3, 255, 225, 192, 15, 254, 30, 0, 63, 240, 224, 0, 7, 135, 0, 124, 120, 120, 7, 255, 195, 128, 127, 252, 60, 3, 143, 131, 192, 28, 0, 60, 0, 224, 3, 192, 7, 192, 124, 0, 31, 255, 192, 0, 255, 252, 0, 0, 255, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ),
	);

	return (empty($char)) ?  $c_chars : $c_chars[$char];
}
?>
