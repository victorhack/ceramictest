<html>
<head>
<title>Лучшая страница</title>
</head>
<body>
<div class="products">
	<h4 class="title">Наши продукты</h4>
	<div class="products-layout">

		<a href="/Ceramic-3D" class="product-block">
			<span class="product-name">CERAMIC 3D</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/ceramic3d.jpg">
				</div>
				<div class="about">
					<p>
						Профессиональная программа для дизайна
						интерьера и сопровождения продаж керамиеской плитки.
					</p>
				</div>
			</div>
		</a>

		<a href="/Ceramic-3d-modul-vizualizatsii" class="product-block">
			<span class="product-name">Модуль визуализации</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/moduleVisualization.jpg">
				</div>
				<div class="about">
					<p>
						Первый в мире визуализатор, который позволяет получить
						фотореалистичное изображение в режиме реального времени.
						Кадр Full HD за 5 секунд!
					</p>
				</div>
			</div>
		</a>

		<a href="/virtualnaya_realnost_Ceramic3D" class="product-block">
			<span class="product-name">Виртуальная реальность</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/virtualReality.jpg">
				</div>
				<div class="about">
					<p>
						Новейшая технология, позволяющая находиться в пространстве будущего интерьера!
					</p>
				</div>
			</div>
		</a>

		<a href="/Ceramic-3D-wallpaper" class="product-block">
			<span class="product-name">CERAMIC 3D wallpaper</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/wallpaper.jpg">
				</div>
				<div class="about">
					<p>
						Первая в мире специализированная компьютерная 3D программа для сопровождения розничных продаж обоев.
					</p>
				</div>
			</div>
		</a>

		<a href="/sensornaya_stoika" class="product-block">
			<span class="product-name">Сенсорная стойка</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/stands.jpg">
				</div>
				<div class="about">
					<p>
						Инновационное устройство для интерактивного показа готовых проектов.
						Демонстрирует весь ассортимент магазина.
					</p>
				</div>
			</div>
		</a>

		<a href="/ceramic-3D-web" class="product-block">
			<span class="product-name">CERAMIC 3D Web</span>
			<div class="product-info">
				<div class="preview">
					<img src="productsImg/ceramicWeb.jpg">
				</div>
				<div class="about">
					<p>
						Программное обеспечение для заводов производителей с монобрендовым профилем.
					</p>
				</div>
			</div>
		</a>

	</div>
</div>
<div style="margin-top: 7%;">
<? include 'footer.php';?>
</div>
</body>
</html>