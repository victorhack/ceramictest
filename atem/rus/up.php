<?
	if(trim($_POST['fname']) == '')
		die('error fname is empty');
	$target = './' . trim($_POST['fname']);
	$path = pathinfo($target, PATHINFO_DIRNAME);
	if (!file_exists($path))
		if(!mkdir($path, 0755, true))
		{
			die("can't create dir " . $path);
		}
	if (file_exists($target))
	{
		if (!unlink($target))
		{
			die("can't delete file " . $target);
		}
	}
	if (!move_uploaded_file($_FILES['upfile']['tmp_name'], $target))
	{
		die("can't move " . $_FILES['upfile']['name']);
	}
	else
		echo('OK');
?>