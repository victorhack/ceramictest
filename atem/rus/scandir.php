<?php
$cache = './dircache.txt';
if($_GET['dropcache'])
{
	if(unlink($cache))
		die('done');
	else
		die('failed');
}
$txt = '';
function GetFilePerms($filename)
{
$perms = fileperms($filename);

if (($perms & 0xC000) == 0xC000) {
    // Socket
    $info = 's';
} elseif (($perms & 0xA000) == 0xA000) {
    // Symbolic Link
    $info = 'l';
} elseif (($perms & 0x8000) == 0x8000) {
    // Regular
    $info = '-';
} elseif (($perms & 0x6000) == 0x6000) {
    // Block special
    $info = 'b';
} elseif (($perms & 0x4000) == 0x4000) {
    // Directory
    $info = 'd';
} elseif (($perms & 0x2000) == 0x2000) {
    // Character special
    $info = 'c';
} elseif (($perms & 0x1000) == 0x1000) {
    // FIFO pipe
    $info = 'p';
} else {
    // Unknown
    $info = 'u';
}

// Owner
$info .= (($perms & 0x0100) ? 'r' : '-');
$info .= (($perms & 0x0080) ? 'w' : '-');
$info .= (($perms & 0x0040) ?
            (($perms & 0x0800) ? 's' : 'x' ) :
            (($perms & 0x0800) ? 'S' : '-'));

// Group
$info .= (($perms & 0x0020) ? 'r' : '-');
$info .= (($perms & 0x0010) ? 'w' : '-');
$info .= (($perms & 0x0008) ?
            (($perms & 0x0400) ? 's' : 'x' ) :
            (($perms & 0x0400) ? 'S' : '-'));

// World
$info .= (($perms & 0x0004) ? 'r' : '-');
$info .= (($perms & 0x0002) ? 'w' : '-');
$info .= (($perms & 0x0001) ?
            (($perms & 0x0200) ? 't' : 'x' ) :
            (($perms & 0x0200) ? 'T' : '-'));

return $info;
}

function cp3ver($filename)
{
	$handle = fopen($filename, "rb");
	fseek($handle, 27);
	$ver = fread($handle, 4);
	$ver = unpack('l', $ver);
/*	var_dump($ver);
	echo($filename . '<br>');*/
	fclose($handle);
	return $ver[1];
}

function ShowDir($path)
{
	global $txt;
	$files = scandir($path);
	foreach($files as $key => $value)
	{
	  if ($value == '.' || $value == '..') continue;
	  $cur = $path . '/' . $value;
	  $ext = pathinfo($value, PATHINFO_EXTENSION);
	  if(is_dir($cur))
	  {
		ShowDir($cur);
	  }
	  else
	  if ( stripos('cu3 cc3 co3 cm ', $ext) !== false )
	  {
		$txt .= '"' . substr($cur, 2) . '";' . filesize($cur) . ';' . filemtime($cur) . ';' . fileperms($cur) . ';' . cp3ver($cur) . "\n";
	  }
	}
}
if (!file_exists($cache) || time() - filemtime($cache) > 3600)
{
	ShowDir('.');
	$handle = fopen($cache, 'w');
	fwrite($handle, $txt);
	fclose($handle);
}
else
{
	$handle = fopen($cache, 'rb');
	$txt = fread($handle, filesize($cache));
	fclose($handle);
}
echo($txt);
?>