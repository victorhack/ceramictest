<script src="/masonry.pkgd.min.js"></script>
<script>
var container;
var msnry;


$(document).ready(function(e) {	
	$('.index_gal1').bind('mousemove', function(z) {
		x = z.clientX;
		out = $(window).width()-2500;
		$('.index_gal').css({ 'margin-left' : Math.round(x*out/$(window).width())+'px' });
	});
	
	<? if ($arr_url[1]=='video') {?>
		initFB('groupV');	
	<? } else {?>
		initFB('group');	
	<? } ?>
	initFB('group2');
	
	container = document.querySelector('#container');
	msnry = new Masonry( container );
});


</script>
<div style="padding:70px 0 70px 0">
<div align="center">
<div style="margin-top:0px; width:960px;" align="left">
<h2 style="font-size:24pt; font-weight:normal; padding:10px 0; text-transform:none;">Последние проекты</h2>
</div>
</div>

<div class="index_gal1" style="width:100%; position:relative; overflow:hidden; height:260px; margin-top:0px;">
<div class="index_gal">
<?
$arr_type[0] = '';
$arr_type[1] = 'video/';
$arr_type[2] = 'panorama/';
$more_photo_id = 0;
$q = $pdo->query("SELECT * FROM qf_gal ORDER BY qf_id DESC LIMIT 5");
while ($res = $q->fetch()) {
?>
<a rel="group2" href="<? if ($res['qf_type']==0) {?>http://<?=$_SERVER['HTTP_HOST']?>/gallery/<?=$res['qf_id']?>.jpg<? } else { ?>http://<?=$_SERVER['HTTP_HOST']?>/get_folio/<?=$res['qf_id']?>/<? } ?>" title="<?=htmlspecialchars($res['qf_name'])?>" style="background-image:url(<? if ($res['qf_youtube']) { ?>http://img.youtube.com/vi/<? $aa1 = explode("/",$res['qf_youtube']); $aa1 = explode("?",$aa1[4]); echo $aa1[0];?>/mqdefault.jpg<? } else {?>/gallery/prev/<?=$res['qf_id']?>.jpg<? } ?>)"><div class="desc"><b><?=$res['qf_name']?></b><br><?=$res['qf_desc']?></div></a>
<? } ?>
</div>
</div>


<div align="center">
<div style="width:960px;" align="left">
<a href="/portfolio/" class="ah1<? if ($arr_url[1]!="video" && $arr_url[1]!='panorama') echo '_a';?>">Изображения</a>
<a href="/portfolio/video/" class="ah1<? if ($arr_url[1]=='video') echo '_a';?>">Видео</a>
<a href="/portfolio/panorama/" class="ah1<? if ($arr_url[1]=='panorama') echo '_a';?>">Панорамы</a>

<div id="container" class="js-masonry" >
	<?
	$t = 0;
	if ($arr_url[1]=="video") $t = 1;
	if ($arr_url[1]=="panorama") $t = 2;
	if ($t > 0) {
		$q = $pdo->prepare("SELECT * FROM qf_gal WHERE qf_type = ?  and qf_id >= ".$more_photo_id ." ORDER BY qf_id DESC LIMIT 9");
		$q->execute(array($t));
		while ($res = $q->fetch()) {    
		?>    
		<div class="item" style="height:<?=rand(200,300)?>px; width:310px; float:left; padding:5px;">
			<a rel="<?=$t==1?'groupV':'group'?>" href="<? if ($res['qf_type']==0) {?>http://<?=$_SERVER['HTTP_HOST']?>/gallery/<?=$res['qf_id']?>.jpg<? } else { ?>http://<?=$_SERVER['HTTP_HOST']?>/get_folio/<?=$res['qf_id']?>/<? } ?>" title="<?=htmlspecialchars($res['qf_name'])?>" class="prev">
			<div class="bg" style="background-image:url(<? if ($res['qf_youtube']) { ?>http://img.youtube.com/vi/<? $aa1 = explode("/",$res['qf_youtube']); $aa1 = explode("?",$aa1[4]); echo $aa1[0];?>/mqdefault.jpg<? } else {?>/gallery/prev/<?=$res['qf_id']?>.jpg<? } ?>); height:100%; width:100%;"></div>
			</a>
		</div>
		<? } ?>
		
        <? } else { //photo
			$total_rows = ceil($pdo->query('SELECT qf_id FROM qf_gal WHERE qf_type = 0 and qf_id >= '.$more_photo_id )->rowCount()/9);
			$r_page = $arr_url[1];
			if ($r_page!="") $r_page=$r_page; else $r_page = 1;
			$our_page=$r_page*9-9;
			
			$q = $pdo->prepare("SELECT * FROM qf_gal WHERE qf_type = ?  and qf_id >= ".$more_photo_id." ORDER BY qf_id DESC LIMIT ".$our_page.",9");
			$q->execute(array($t));
			while ($res = $q->fetch()) {    
			?>    

                
<a itemscope itemtype="http://schema.org/ImageObject" rel="group" data-img="1" href="http://<?=$_SERVER['HTTP_HOST']?>/gallery/<?=$res['qf_id']?>.jpg" title="<?=htmlspecialchars($res['qf_name'])?>" class="prev item" style="float:left; padding:5px; height:<?=rand(200,300)?>px; width:310px; position:absolute; overflow:hidden;">
	<img itemprop="contentUrl" src="/gallery/prev/<?=$res['qf_id']?>.jpg?v<?=$res['qf_id']?>" title="<?=htmlspecialchars($res['qf_name'])?>" alt="<?=htmlspecialchars($res['qf_name'])?>" style="position:absolute; left:5px; top:10px; width:310px;">
<meta itemprop="name" content="Ceramic 3D, программное обеспечение для дизайна интерьера">
<meta itemprop="description" content="<?=htmlspecialchars($res['qf_name'])?>">
</a>			
			<? } ?>
		<? } //END PHOTO?>
        <div style="clear:both; float:none" id="temp"></div>
</div>

<? if ($t > 0) {?><div align="center" style="padding-top:30px;" id="donew"><div class="but_2">РАЗВЕРНУТЬ ГАЛЕРЕЮ</div></div><? } else { //Pagination 4 4to?>
<div style="padding:30px 0;" align="center">
<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100" align="left"><? if ($r_page>1) {?><a alt="Галерея проектов | Страница <?=$r_page-1?>" title="Галерея проектов | Страница <?=$r_page-1?>" rel="prev" class="arrow_l" href="/portfolio/<?=$r_page-1?>">Страница <?=$r_page-1?></a><? } ?>&nbsp;</td>    
    <td align="center">Страница <?=$r_page?> из <?=$total_rows?></td>
    <td width="100" align="right"><? if ($r_page<$total_rows) {?><a alt="Галерея проектов | Страница <?=$r_page+1?>" title="Галерея проектов | Страница <?=$r_page+1?>" rel="next" class="arrow_r" href="/portfolio/<?=$r_page+1?>">Страница <?=$r_page+1?></a><? } ?></td>
  </tr>
</table>
</div>
<? } ?>
</div>
</div>
</div>
<div id="myFull">
	<div class="exit" onClick="exitFullScreen()"><img src="/fullscreen_buttons/exit.png"></div>
	<div class="full_content"></div>
    <div class="array left"><img src="/fullscreen_buttons/left.png"></div>
    <div class="array right"><img src="/fullscreen_buttons/right.png"></div>
</div>
<? include 'footer.php';?>
<script>
	$('#donew').on('click', function(e) {
		$('#donew').html('<img src="/img/loader.gif" height="58">');
		$.post('http://<?=$_SERVER['HTTP_HOST']?>/all_g.php',{type:'<?=$arr_url[1]?>'}, function(data) {
			h = $(window).scrollTop();
			h2 = 350 + $('#container').height();
			$('#container').append(data);
			$('#temp').detach().appendTo('#container');
			$('#donew').detach();			
			$(container).masonry('destroy');
			msnry = new Masonry( container );
			$(window).scrollTop(h);
			$("body").animate({"scrollTop":h2},"slow");
			initFB('group');
		});
	});
</script>