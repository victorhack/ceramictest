<?
include 'config.php';

$userName = "ceramic3d-api"; 
$password = "kFRLcTtrq3";
$curl_url = 'https://securepayments.sberbank.ru/payment/rest/';
$pay_id = $_GET['mdOrder'];
if ($pay_id) {		
		$q = $pdo->prepare("SELECT qf_order.*, qf_money.qf_id AS money_id FROM qf_order
							LEFT JOIN qf_money ON qf_money.qf_order = qf_order.qf_id
							WHERE qf_sbrf_id = ? LIMIT 1");
		$q->execute(array($pay_id));
		while ($res = $q->fetch()) {			
			//Нашли, проверим что скажет сбер
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $curl_url.'getOrderStatusExtended.do');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, 'userName='.$userName.'&password='.$password.'&orderId='.$res['qf_sbrf_id']);
			$out = curl_exec($curl);
			//echo $out;
			curl_close($curl);
			$out = json_decode($out);
			
			if ($out->orderStatus) { //Обновим статус
				$s = $pdo->prepare("UPDATE qf_order SET qf_status = ?, qf_description = ?, qf_errorCode = ?, qf_error = ?, qf_obn_date = ? WHERE qf_id = ? LIMIT 1");
				$s ->execute(array(
								$out->orderStatus,
								$arr_Status[$out->orderStatus],
								$out->ErrorCode,
								$out->ErrorMessage,								
								date("Y-m-d H:i:s"),
								$res['qf_id']								
							));
				echo $out->orderStatus;
				
				if ($out->orderStatus == 4) { //Возврат
					if (!$res['money_id']) { //Записи не было, добавим
						$s = $pdo->prepare("INSERT INTO qf_money (qf_user, qf_summa, qf_comment, qf_date, qf_system, qf_order, qf_pay, qf_refunded) VALUES (?,?,?,?,?,?,?)");
						$s->execute(array(
								$res['qf_user'],
								$out->paymentAmountInfo->depositedAmount / 100,
								'Пополнение с банковской карты '.$out->Pan,
								date('Y-m-d H:i:s'),
								1,
								$res['qf_id'],
								$out->paymentAmountInfo->approvedAmount / 100,
								$out->paymentAmountInfo->refundedAmount / 100
						));
					} else { //Обновим
						$s = $pdo->prepare("UPDATE qf_money SET qf_summa = ?, qf_pay = ?, qf_refunded = ? WHERE qf_id = ? LIMIT 1");
						$s->execute(array(
								$out->paymentAmountInfo->depositedAmount / 100,
								$out->paymentAmountInfo->approvedAmount / 100,
								$out->paymentAmountInfo->refundedAmount / 100,
								$res['money_id']
						));																							
					}		
					//Обновим баланс, пересчитаем все подряд			
					//Посчитаем сумму
					$s = $pdo->query("SELECT SUM(qf_summa) AS s FROM qf_money WHERE qf_user = ".$res['qf_user']);
					$ss = $s->fetch();
					$summa = $ss['s'];					
					//Обновим
					$s = $pdo->query("UPDATE qf_users SET qf_summa = ".$summa." WHERE qf_id = ".$res['qf_user']." LIMIT 1");
				}
			}
		}
	}
?>