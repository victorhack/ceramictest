<? 
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
include 'config.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$r_title?></title>
<META NAME="Keywords" CONTENT="<?=$r_key?>"/>
<META NAME="Description" CONTENT="<?=$r_desc?>"/>
<meta name='yandex-verification' content='49157eaf4040d663' />
<link rel="icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="http://<?=$_SERVER['HTTP_HOST']?>/ah.css?rnd=<?=rand(100,999)?>">
<link rel="stylesheet" type="text/css" href="http://<?=$_SERVER['HTTP_HOST']?>/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link href="http://<?=$_SERVER['HTTP_HOST']?>/carusel/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="http://<?=$_SERVER['HTTP_HOST']?>/chosen/chosen.css">
<script src="http://<?=$_SERVER['HTTP_HOST']?>/jquery.min.js"></script>
<script type="text/javascript" src="http://<?=$_SERVER['HTTP_HOST']?>/c3dpanoNew.js?v1"></script>

<script type="text/javascript" src="http://<?=$_SERVER['HTTP_HOST']?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="http://<?=$_SERVER['HTTP_HOST']?>/carusel/jquery.flexisel.js"></script>

<link rel="stylesheet" href="/phoneBuilder/css/intlTelInput.css">
<script src="/phoneBuilder/js/intlTelInput.js"></script>
<script type="text/javascript" src="http://<?=$_SERVER['HTTP_HOST']?>/main.js?<?=rand(1000,9999)?>"></script>
</head>
<body>
<? include 'top.php'; ?>
<? if ($arr_url['0']=='') include '1.php';?>
<? if ($arr_url['0']=='contacts') include 'contact.php';?>
<? if ($arr_url['0']=='mention') include 'mention.php';?>
<? if ($arr_url['0']=='blog') include 'news.php';?>
<? if ($arr_url['0']=='portfolio') include 'portfolio.php';?>
<? if ($arr_url['0']=='portfolio1') include 'portfolio1.php';?>
<? if ($arr_url['0']=='ourteam') include 'ourteam.php';?>
<? if ($arr_url['0']=='search') include 'search.php';?>
<? if ($arr_url['0']=='products') include 'products.php';?>
<? if ($arr_url['0']=='updates') include 'updates.php';?>
<? if ($arr_url['0']=='addcollection') include 'addcollection.php';?>
<? if ($arr_url['0']=='helpme') include 'help.php';?>
<? if ($arr_url['0']=='test') include 'test.php';?>
<? if ($thiispage == 1) include 'p.php';?>
<? include 'code/privacyPolicy.php'; ?>
<noindex>
<div style="display:none;">
    <div id="top_q" style="padding:30px;">
        <input class="input" id="top_q_name" placeholder="Введите имя" autocomplete="off" value="" style="background-image:url(/img/input_top_q.png); width:412px;">
        <input class="input" type="tel" id="top_q_tel" placeholder="Введите номер телефона с кодом города" autocomplete="off" value=""style="margin-top:10px;background-image:url(/img/input_top_q.png); width:412px;">
        <textarea class="text" placeholder="Введите вопрос" id="top_q_txt" style="margin-top:10px;"></textarea>        
        <div class="but_2" style="margin-top:10px; padding:10px 20px;" onclick="send_top_q()">Задать вопрос</div>
    </div>
    
    <div id="index_alert" style="padding:30px;">
        <img src="/img/index_alert.jpg" width="1024">
    </div>
    
    <div id="top_key" style="padding:30px;">
        <input class="input required" id="top_key_name" placeholder="Введите ваше имя" autocomplete="off" value="" style="background-image:url(/img/input_top_q.png); width:412px;">
        <input class="input required" type="email" id="top_key_email" placeholder="Введите ваш email" autocomplete="off" value="" style="background-image:url(/img/input_top_q.png); width:412px; margin-top:10px">
        <span class="top_key_list_countries" id="top_key_codesOfCountries">
        <select id="top_key_countries" class="my_select_box" style="display: none;">
            <? include 'codesCountries.php' ;?>
            <option value="+ " selected disabled> </option>
        </select>
        </span>
        
        <input class="input required" type="tel" id="top_key_tel" placeholder="Введите номер телефона и код города" autocomplete="off" value="" style="margin-top:10px;background-image:url(/img/input_top_q.png); width: 333px; background-size: 373px 55px;">
        <input class="input required" type="text" id="top_key_id" placeholder="Введите ID ключа" autocomplete="off" value="" style="margin-top:10px;background-image:url(/img/input_top_q.png); width:412px;">
        <div class="top_key_verification">
            <span class="block_verification"></span>
            <span class="text_verification">Нажимая на кнопку "Отправить", я даю согласие на обработку персональных данных и соглашаюсь c условиями <b class="privacy_button_open">политики конфиденциальности</b>.</span>
        </div>
        <div class="but_2" style="margin-top:10px; padding:10px 20px;" onclick="send_top_key()">Отправить заявку</div>
    </div>
    
    <div id="index_demo" style="padding:15px;">
    <h4 style="display:block; text-align:center; margin-bottom:20px;">ПОЛУЧИТЬ ПРОГРАММУ НА 30 ДНЕЙ</h4>
        <input type="text" id="demo_org" class="demo" placeholder="Организация" value="" autocomplete="off">
        <input type="text" id="demo_fio" class="demo" placeholder="ФИО" value="" autocomplete="off">
        <input type="text" id="demo_email" class="demo" placeholder="E-mail" value="" autocomplete="off">
        <input type="text" id="demo_tel" class="demo" placeholder="Номер телефона с кодом города" value="" autocomplete="off">
        <div align="center" style="color:#0082dd;"><?=str_replace("\n",'<br>',str_replace("\r\n",'<br>',$arr_g['cont']['demotxt']))?></div>
        <div align="center" style="margin-top:15px;">
            <div class="but_2" style="padding:10px 20px;" onclick="send_demo()">Отправить заявку</div>
        </div>
    </div>    

        <div id="support-form" style="width:960px; overflow:hidden"><? include $_SERVER['DOCUMENT_ROOT'].'code/support.php'?></div>
        <a href="#support-form" class="support-form">Тыц</a>

    <a href="#index_alert" class="index_alert">Тыц</a>
    <a href="#top_q" class="top_q">Тыц</a>
    <a href="#top_key" class="top_key">Тыц</a>
</div>
<? 
if ( time() < 1514332799 && time() > 1514160001){
?>
<script>
    $(document).ready(function(e) {
        if (getCookie('baner')!=1) {
            $('a.index_alert').click();
            setCookie('baner','1');
        }
    });    
</script>
<? } ?>
</noindex>
<script src="http://<?=$_SERVER['HTTP_HOST']?>/chosen/chosen.jquery.js" type="text/javascript"></script>
</body>
</html>

<!-- <!DOCTYPE html>
<html>
<head>
    <title>iste</title>
</head>
<body>
    <h1>it work</h1>
</body>
</html> -->