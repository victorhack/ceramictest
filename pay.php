<?
if (!$fromFile) include 'config.php';

$userName = "ceramic3d-api"; 
$password = "kFRLcTtrq3";
$curl_url = 'https://securepayments.sberbank.ru/payment/rest/';

if ($_POST['s']) { //Регистрация нового заказа	
	$amount = $_POST['s']*100;
	if ($amount < 1) {
		echo '{"errorCode":"-1","errorMessage":"Некорректная сумма"}';
		//echo '{"formUrl":"http://yandex.ru"}';
		exit();
	}
	$q = $pdo->prepare("INSERT INTO qf_order (qf_summa,qf_user,qf_status) VALUES (?,?,?)");
	$q->execute(array( $amount, $user['qf_id'],-2));
	$merchantOrderNumber = $pdo->lastInsertId();
	//$merchantOrderNumber = 1;
	$returnUrl = "http://www.ceramic3d.ru/pay";
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $curl_url.'register.do');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, 'amount='.$amount.'&language=ru&orderNumber='.$merchantOrderNumber.'&returnUrl='.$returnUrl.'&userName='.$userName.'&password='.$password);
	$out = curl_exec($curl);
	echo $out;
	curl_close($curl);
	$out = json_decode($out);
	if ($out->formUrl) {
		$q = $pdo->prepare("UPDATE qf_order SET qf_sbrf_id = ?, qf_formUrl = ?, qf_status = ? WHERE qf_id = ? LIMIT 1");
		$q->execute(array(
			$out->orderId,
			$out->formUrl,
			-1,
			$merchantOrderNumber
		));
	} else {		
		$q = $pdo->prepare("UPDATE qf_order SET qf_errorCode = ?, qf_error = ? WHERE qf_id = ? LIMIT 1");
		$q->execute(array(
			$out->errorCode,
			$out->errorMessage,
			$merchantOrderNumber
		));		
	}
} else { //Проверка статуса заказа
	if (!$fromFile) $pay_id = $_POST['id'];
	
	$arr_Status = [
		'Заказ зарегистрирован, но не оплачен',
		'Предавторизованная сумма захолдирована (для двухстадийных платежей)',
		'Проведена полная авторизация суммы заказа',
		'Авторизация отменена',
		'По транзакции была проведена операция возврата',
		'Инициирована авторизация через ACS банка-эмитента',
		'Авторизация отклонена'
	];

	if ($pay_id) {
		$q = $pdo->prepare("SELECT qf_order.*, qf_money.qf_id AS money_id FROM qf_order
							LEFT JOIN qf_money ON qf_money.qf_order = qf_order.qf_id
							WHERE qf_sbrf_id = ? LIMIT 1");
		$q->execute(array($pay_id));
		while ($res = $q->fetch()) {
			//Нашли, проверим что скажет сбер
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $curl_url.'getOrderStatusExtended.do');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, 'userName='.$userName.'&password='.$password.'&orderId='.$res['qf_sbrf_id']);
			$out = curl_exec($curl);			
			curl_close($curl);
			$out = json_decode($out);

			
			if ($out->orderStatus) { //Обновим статус
				$s = $pdo->prepare("UPDATE qf_order SET qf_status = ?, qf_description = ?, qf_errorCode = ?, qf_error = ?, qf_obn_date = ? WHERE qf_id = ? LIMIT 1");
				$s ->execute(array(
								$out->orderStatus,
								$arr_Status[$out->orderStatus],
								$out->ErrorCode,
								$out->ErrorMessage,								
								date("Y-m-d H:i:s"),
								$res['qf_id']								
							));
				if (!$res['money_id'] && $out->orderStatus == 2) { //Добавим в историю и обновим баланс
					$s = $pdo->prepare("INSERT INTO qf_money (qf_user, qf_summa, qf_comment, qf_date, qf_system, qf_order, qf_pay) VALUES (?,?,?,?,?,?,?)");
					$s->execute(array(
								$res['qf_user'],
								$res['qf_summa']/100,
								'Пополнение с банковской карты '.$out->Pan,
								date('Y-m-d H:i:s'),
								1,
								$res['qf_id'],
								$res['qf_summa']/100
					));
					$s = $pdo->query("UPDATE qf_users SET qf_summa = qf_summa+".($res['qf_summa']/100)." WHERE qf_id = ".$res['qf_user']." LIMIT 1");
				}
			}
		}
	}
	
}
?>