<?
include 'config.php';

$userName = "ceramic3d-api"; 
$password = "kFRLcTtrq3";
$curl_url = 'https://securepayments.sberbank.ru/payment/rest/';

//Найдем сформированные заказы, но без ответа от банка
$arr_Status = [
	'Заказ зарегистрирован, но не оплачен',
	'Предавторизованная сумма захолдирована (для двухстадийных платежей)',
	'Проведена полная авторизация суммы заказа',
	'Авторизация отменена',
	'По транзакции была проведена операция возврата',
	'Инициирована авторизация через ACS банка-эмитента',
	'Авторизация отклонена'
];

$q = $pdo->query("SELECT qf_order.*, qf_money.qf_id AS money_id 
					FROM qf_order
					LEFT JOIN qf_money ON qf_money.qf_order = qf_order.qf_id
					WHERE qf_order.qf_status = -1
					OR (qf_order.qf_status = 2 AND qf_money.qf_id IS NULL)");				
while ($res = $q->fetch()) {
	$pay_id = $res['qf_sbrf_id'];
	
	//Нашли, проверим что скажет сбер
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $curl_url.'getOrderStatusExtended.do');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, 'userName='.$userName.'&password='.$password.'&orderId='.$pay_id);
	$out = curl_exec($curl);			
	curl_close($curl);
	$out = json_decode($out);
	
	if ($out->orderStatus == 6) { //Эти заказы отменены, уберем их из истории
		$s = $pdo->prepare("UPDATE qf_order SET qf_status = ?, qf_description = ?, qf_errorCode = ?, qf_error = ?, qf_obn_date = ? WHERE qf_id = ? LIMIT 1");
		$s ->execute(array(
						$out->orderStatus,
						$arr_Status[$out->orderStatus],
						$out->ErrorCode,
						$out->ErrorMessage,								
						date("Y-m-d H:i:s"),
						$res['qf_id']								
					));
	} elseif ($out->orderStatus == 2) { //Добавим запись в историю 
		$d = date('Y-m-d H:i:s',substr($out->authDateTime,0,-3));		
		
		$s = $pdo->prepare("UPDATE qf_order SET qf_status = ?, qf_description = ?, qf_errorCode = ?, qf_error = ?, qf_obn_date = ? WHERE qf_id = ? LIMIT 1");
		$s ->execute(array(
						$out->orderStatus,
						$arr_Status[$out->orderStatus],
						$out->ErrorCode,
						$out->ErrorMessage,								
						date("Y-m-d H:i:s"),
						$res['qf_id']								
					));
		//Добавим запись в моней
		$s = $pdo->prepare("INSERT INTO qf_money (qf_user, qf_summa, qf_comment, qf_date, qf_system, qf_order, qf_pay) VALUES (?,?,?,?,?,?,?)");
		$s->execute(array(
					$res['qf_user'],
					$res['qf_summa']/100,
					'Пополнение с банковской карты '.$out->Pan,
					$d,
					1,
					$res['qf_id'],
					$res['qf_summa']/100
		));
		$s = $pdo->query("UPDATE qf_users SET qf_summa = qf_summa+".($res['qf_summa']/100)." WHERE qf_id = ".$res['qf_user']." LIMIT 1");
		
		
		
	}
}

?>