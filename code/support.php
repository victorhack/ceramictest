<? 
    
    if (isset($_POST['send-support-ajax'])) {   
    
    include '../functions.php';
    
    $arrField = [
        ['fio','ФИО'],
        ['telefone','Телефон'],
        ['email','Email'],
        ['company','Название компании'],
        ['key','ID ключа'],
        ['deffect','Описание обнаруженного дефекта'],
        ['what','Обстоятельства приведшие к возникновению ошибки (какое действие производилось и с какими объектами)'],
    ];
    $error = [];
    foreach ($arrField as $a) {
        $_POST[$a[0]] = trim($_POST[$a[0]]);
        if ($_POST[$a[0]]=='') {
            $error[] = $a[0];           
        }       
    }   
    if (count($error) < 1) {
        $f = false;
        $path = $_SERVER['DOCUMENT_ROOT'].'support_files/';
        $arr_files = ['project','screen'];
        $folder = '';
        foreach ($arr_files as $a) {
            if (!empty($_FILES[$a]['tmp_name'])) { 
                // Закачиваем файл
                error_reporting(E_ALL);
                if (!is_dir($path.date('Y-m-d'))) mkdir($path.date('Y-m-d'));
                $fPath = $path.date('Y-m-d').'/'.str2url($_POST['company']);
                if ( !is_dir($fPath)) mkdir($fPath);
                $folder = $fPath;
                $fPath = $fPath.'/'.$a."_".time().'_'.str2url($_FILES[$a]['name']);
                
                if (copy($_FILES[$a]['tmp_name'], $fPath)) $picture = $fPath;
                $f[] = $picture;
            }
        }       
        //Запишем в базу
        $q = $pdo->prepare("INSERT INTO support (`fio`, `tel`, `email`,`company`,`key`,`deffect`,`what`,`folder`,`files`) VALUES (?,?,?,?,?,?,?,?,?)");
        $q->execute(array($_POST['fio'], $_POST['telefone'], $_POST['email'], $_POST['company'], $_POST['key'], $_POST['deffect'], $_POST['what'], $folder, $f?implode(';',$f):''));
        $id = $pdo->lastInsertId();     
        $txt = "<p>Обращение #$id от ".date("d.m.Y H:i:s")."(<a href='http://ceramic3d.ru/ahcms/?m=20&edit=$id'>открыть на портале</a>)</p>";
        $txt .= "<p><b>".$_POST['company']."</b> (KEY ID ".$_POST['key'].")<br>".$_POST['fio']." (".$_POST['email']." | ".$_POST['telefone'].")</p>";
        $txt .= "<p><i>Описание обнаруженного дефекта</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['deffect']))."</p>";
        $txt .= "<p><i>Обстоятельства приведшие к возникновению ошибки</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['what']))."</p>";
        //Отправим письмо
        sendmail('Обращение #'.$id,$txt, $arr_g['cont']['support_email'],$f,false);                 
        $send = true;
    } else {        
        $send = false;
    }
}

if (!$send && !$thisIsAjax) {?>
<form enctype='multipart/form-data' name="send-support" id="send-support" method="post" action="/">

    <div style="padding:25px 15px; font-size:18px; text-align:center; font-weight:bold; margin-bottom:30px; border:2px solid #222;">Внимание! Если у вас ошибка в программе  обязательно прикрепите снимок экрана с ошибкой</div>
    <input name="send-support-ajax" value="1" style="display:none;">    
    <ul id="support">
        <li class="one">            
            <label>         
                <span>ФИО</span>
                <input type="text" name="fio">
            </label>            
        </li>       
       
        <li class="two">
            <? if ($_SERVER['REMOTE_ADDR']=='82.193.135.180') {?>
            <label>                         
                <input type="tel" id="supportPhone" name="telefone">
            </label>  
            
            <? } else { ?>
            <label>         
                <span>Телефон</span>
                <input type="tel" name="telefone">                
            </label>            
            <? } ?>
            <label>         
                <span>Email</span>
                <input type="email" name="email" required>
            </label>        
        </li>
        
        <li class="two">
            <label>         
                <span>Название компании</span>
                <input type="text" name="company">
            </label>            
            <label>         
                <span>ID ключа</span>
                <input type="text" name="key">
            </label>        
        </li>
        
        <li class="one">
            <label>         
                <span>Описание обнаруженного дефекта</span>
                <textarea name="deffect"></textarea>
            </label>            
        </li>
        
        <li class="one">
            <label>         
                <span>Обстоятельства приведшие к возникновению ошибки (какое действие производилось и с какими объектами)</span>
                <textarea name="what"></textarea>
            </label>            
        </li>
        
        <li class="one file" style="text-align:right">
            <label>         
                <span>Проект с обнаруженным дефектом (архив)</span>
                <input type="file" name="project" style="display:none">
                <div>Выбрать файл</div>
            </label>            
        </li>
        
        <li class="one file" style="text-align:right;">
            <label>         
                <span>Снимок экрана с ошибкой</span>
                <input type="file" name="screen" style="display:none">
                <div>Выбрать файл</div>
            </label>            
        </li>
        
        <li class="desc">
            <p>После получения заявки вы получите письмо, в котором будет указано время, в которое специалист технической поддержки с вами свяжется.</p>
            <p>Уважаемый клиент, если время, указанное в заявке Вам не подходит, свяжитесь с администратором по номеру +7(495)215-24-47 чтобы назначить другое время.</p>            
            <p><input type="checkbox" name="policy" disabled checked> Нажимая на кнопку "Отправить", я даю согласие на обработку персональных данных и соглашаюсь c условиями <b class="privacy_button_open">политики конфиденциальности</b>.</p>
            <input type="submit" value="Отправить" name="send-support-b">
        </li>        
    </ul>
</form>


<? } elseif ($send && $thisIsAjax) {?>
<div style="padding:70px 0;" id="support">
    <h1>Ваше обращение принято</h1>
    <p>После получения заявки вы получите письмо, в котором будет указано время, в которое специалист технической поддержки с вами свяжется.</p>
    <p>Уважаемый клиент, если время, указанное в заявке Вам не подходит, свяжитесь с администратором по номеру +7(495)215-24-47 чтобы назначить другое время.</p>
</div>
<? } else {
    echo 'error;'.implode(';',$error); exit();
}?>