<div align="center">
<?
if ($user['qf_id']) { //Авторизован
	echo $arr_g['pay']['pay_after'];?>
    
    <? // проверим результат оплаты
				$pay_id = explode('?',$_SERVER['REQUEST_URI']);
				if ($pay_id['1']) $pay_id = explode('=',$pay_id[1]);
				$pay_id = $pay_id[1];
				$fromFile = true;
//				$pay_id = '30d8666a-3316-4d02-9e2b-c8c8e11f78d2';
				include 'pay.php';				
				
		// проверим нет ли платежей без оплаты
		$arr_Status = [
						'Заказ зарегистрирован, но не оплачен',
						'Предавторизованная сумма захолдирована (для двухстадийных платежей)',
						'Проведена полная авторизация суммы заказа',
						'Авторизация отменена',
						'По транзакции была проведена операция возврата',
						'Инициирована авторизация через ACS банка-эмитента',
						'Авторизация отклонена'
					];
		$q2 = $pdo->query("SELECT * FROM qf_order WHERE qf_user = ".$user['qf_id']." AND qf_status = 2 AND qf_description = '' AND qf_date > '".date('Y-m-d 00:00:01',time()-86400)."'");
		while ($res2 = $q2->fetch()) {
			$pay_id = $res2['qf_sbrf_id'];
					if ($pay_id) {
						$q = $pdo->prepare("SELECT qf_order.*, qf_money.qf_id AS money_id FROM qf_order
											LEFT JOIN qf_money ON qf_money.qf_order = qf_order.qf_id
											WHERE qf_sbrf_id = ? LIMIT 1");
						$q->execute(array($pay_id));
						while ($res = $q->fetch()) {
							//Нашли, проверим что скажет сбер
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_URL, $curl_url.'getOrderStatusExtended.do');
							curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
							curl_setopt($curl, CURLOPT_POST, true);
							curl_setopt($curl, CURLOPT_POSTFIELDS, 'userName='.$userName.'&password='.$password.'&orderId='.$res['qf_sbrf_id']);
							$out = curl_exec($curl);			
							curl_close($curl);
							$out = json_decode($out);
				
							if ($out->orderStatus) { //Обновим статус
								$s = $pdo->prepare("UPDATE qf_order SET qf_status = ?, qf_description = ?, qf_errorCode = ?, qf_error = ?, qf_obn_date = ? WHERE qf_id = ? LIMIT 1");
								$s ->execute(array(
												$out->orderStatus,
												$arr_Status[$out->orderStatus],
												$out->ErrorCode,
												$out->ErrorMessage,								
												date("Y-m-d H:i:s"),
												$res['qf_id']								
											));
								if (!$res['money_id'] && $out->orderStatus == 2) { //Добавим в историю и обновим баланс
									$s = $pdo->prepare("INSERT INTO qf_money (qf_user, qf_summa, qf_comment, qf_date, qf_system, qf_order, qf_pay) VALUES (?,?,?,?,?,?,?)");
									$s->execute(array(
												$res['qf_user'],
												$res['qf_summa']/100,
												'Пополнение с банковской карты '.$out->Pan,
												date('Y-m-d H:i:s'),
												1,
												$res['qf_id'],
												$res['qf_summa']/100
									));
									$s = $pdo->query("UPDATE qf_users SET qf_summa = qf_summa+".($res['qf_summa']/100)." WHERE qf_id = ".$res['qf_user']." LIMIT 1");
								}
							}
						}
					}
			
		}
		
		
		
			?>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td width="27%" align="left" valign="top">
          	<p style="font-size:18px; font-weight:bold;">
            	<?=$user['qf_name']?>                
            </p>
            <p>            
            	Договор: <b><?=$user['qf_dog']?></b><br>
				Баланс: <b <?=$user['qf_summa']<0?' style="color:red" ':'';?> title="на <?=date("d.m.Y H:i", strtotime($user['qf_date']))?> (MSK)"><?=numberFormat($user['qf_summa'])?></b><br>
                <?=$user['qf_dop']?'<br>'.str_replace("\r\n",'<br>',$user['qf_dop']):''?>
            </p>
            
            <p><a href="/<?=implode('/',$arr_url)?>">Оплата</a></p>
            <p><a class="oferta" href="#oferta">Прайс-лист</a></p>            
            <p><a href="/sposobi_oplati">Процесс оплаты</a></p>
            <p><a href="/logout">Выход</a></p>             
            
          </td>
          <td width="3%">&nbsp;</td>
          <td align="left" valign="top"><?=$arr_g['pay']['pay_text']?>			
			<label for="pay-amount">Сумма пополнения: </label>
            <input type="text" size="6" id="pay-amount" value="<?=$user['qf_summa']<0?$user['qf_summa']*-1:'0'?>"/> 
            <input type="button" value="Пополнить" id="pay" /> 
            <img src='/img/loader.gif' style="display: none; height:33px;  vertical-align: top;" id="pay-spinner" alt='' />
            <div id="pay-result" style="color:red; margin-top:10px;"></div>
            <script>
				$(document).ready(function(e) {
                    $('#pay').on('click', function(e) {
						$('#pay-result').html('');
						$('#pay-spinner').show();
						$('#pay').hide();
						$.post('/pay.php',{s:$('#pay-amount').val()}, function(data) {
							console.log(data);							
							data = JSON.parse(data);
							if (data.errorCode) {
								$('#pay-result').html(data.errorMessage+' (Код ошибки: '+data.errorCode+')');
							} else {
								window.location.href=data.formUrl;		
							}
							$('#pay-spinner').hide();
							$('#pay').show();							
						});
					});
                });
			</script>
            
            
            
          
          	<h3 style="margin-top:30px;">История платежей</h3>
            <ul class="history">
			<?
            	$q = $pdo->prepare("SELECT * FROM qf_money WHERE qf_user = ? ORDER BY qf_date DESC");
				$q->execute(array($user['qf_id']));
				while ($res = $q->fetch()) {				
			?>
              <li class="<?=$res['qf_summa']>0?'plus':'minus'?>">
              	<span class="date"><?=date("d.m.Y H:i", strtotime($res[qf_date]))?></span>
                <span class="summa"><?=numberFormat($res['qf_summa'],2)?></span>
                
                <?=$res['qf_comment']?>
              </li>
              <? if ($res['qf_refunded'] > 0) {?>
           		<li class="minus" style="padding-left:227px;">
                    Возврат платежа на сумму: <?=numberFormat($res['qf_refunded'],2)?>
                </li>              	
              <? } ?>
            <? } ?>
            </ul>
          
          </td>
        </tr>
      </tbody>
    </table>
    

<? } else { //Покажем форму
	echo $arr_g['pay']['pay_before'];?>
    <form enctype="multipart/form-data" method="POST" name="auth" action="" style="margin-bottom:50px;">
    	<span class="input-placeholder" style="margin-top:15px;">
        	
            <input autocomplete="off" name="qf_login" id="qf_login" type="text" style="width: 293px;" data-placeholder="Введите номер договора" class="input-inline" required="">
        </span><br>
        <span class="input-placeholder" style="margin-top:15px;">
            <input autocomplete="off" name="qf_password" id="qf_password" type="password" style="width: 293px;" data-placeholder="Введите пароль" class="input-inline" required="">
        </span><br>
            <input type="submit" name="payEnter" class="button-inline" style="width:325px; margin-top:15px; font-size:16px;" value="Войти">
            <p style="font-size:14px;">Нажимая кнопку "Войти", я принимаю условия <a href="#oferta" class="oferta">оферты</a></p>
    </form>
    <style>
		.input-placeholder {display:inline-block; position:relative;}
		.input-placeholder .input-inline {display:inline-block; padding:15px; border:1px solid #ccc; background:#FFF; font-size:14px; font-fanily:'fontello';}
		.input-placeholder .input-inline:focus {border:1px solid #61b0e9;}
		.input-placeholder .placeholder {position:absolute; left:17px; color:#8C8C8C; -webkit-transition: All 0.5s ease; -moz-transition: All 0.5s ease; -o-transition: All 0.5s ease; -ms-transition: All 0.5s ease; transition: All 0.5s ease; font-size:16px; top:15px;}
		.button-inline {display:inline-block; padding:15px; font-size:14px; border:1px solid #389ae2; background:#389ae2; color:#FFF; cursor:pointer; min-width: 200px; text-align: center; margin-bottom:5px;}
		.button-inline:hover {display:inline-block; padding:15px; font-size:14px; border:1px solid #389ae2; background:#6bbef6; color:#FFF; cursor:pointer; text-decoration: none;}	
	</style>    
<? } ?>
</div>
<div style="display:none">
    	<div style="width:680px;" id="oferta" align="left">
        	<?=$user['qf_id']?$arr_g['pay']['oferta_full']:$arr_g['pay']['pay_oferta']?>
        </div>
        
        <div style="width:800px;" id="oplata" align="left">
        	<?=$arr_g['pay']['oplata']?>
        </div>
    </div>