<?
if((isset($_POST['name']) && $_POST['name']!="") &&
(isset($_POST['phone']) && $_POST['phone']!="") &&
(isset($_POST['email']) && $_POST['email']!="")) {
    $arrField = [
        ['name','ФИО'],
        ['email','Email'],
        ['phone','Телефон'],
        ['collection','Названия коллекций'],
        ['comment-col','Комментарий']
    ];

    $error = [];
    foreach ($arrField as $a) {
        if (!is_array($_POST[$a[0]])) {
            $_POST[$a[0]] = trim($_POST[$a[0]]);
            if ($_POST[$a[0]]=='') {
                $error[] = $a[0];
            }
        }
    }

    echo count($error);

    if (count($error) < 1) {
        $arr_files = ['filescollect'];
        $total = count($_FILES['filescollect']['name']);
        $path = $_SERVER['DOCUMENT_ROOT'].'collection_files/';
        $folder = '';
        $f = false;
        for( $i=0 ; $i < $total ; $i++ ) {
            $tmpFilePath = $_FILES['filescollect']['tmp_name'][$i];
            echo $_FILES['filescollect']['tmp_name'][$i];
            if (!empty($_FILES['filescollect']['tmp_name'][$i])) {
                error_reporting(E_ALL);
                if (!is_dir($path.date('Y-m-d'))) mkdir($path.date('Y-m-d'));
                $fPath = $path.date('Y-m-d').'/'.str2url($_POST['email']);
                if ( !is_dir($fPath)) mkdir($fPath);
                $folder = $fPath;
                $fPath = $fPath.'/filescollect_'.time().'_'.str2url($_FILES['filescollect']['name'][$i]);
                if (copy($_FILES['filescollect']['tmp_name'][$i], $fPath)) $picture = $fPath;
                $f[] = $picture;
            }
        }

        $q = $pdo->prepare("INSERT INTO qf_collections (`name`, `email`, `phone`,`name_collections`,`folder`,`files`,`comment`) VALUES (?,?,?,?,?,?,?)");
        $q->execute(array($_POST['name'], $_POST['email'], $_POST['phone'], join(';',$_POST['collection']), $folder, $f?implode(';',$f):'', $_POST['comment-col']));
        $id = $pdo->lastInsertId();
        $email = 'content@ceramic3d.ru';
        $name = $_POST['name'];
        $arr_collection = $_POST['collection'];
        $message = 
        '
        <table cellpadding="5">
            <thead>
                <tr>
                    <th width="500px" colspan="2"><h1 style="font-size: 30px; margin: 0;">Привет!<br>Клиент желает добавить новую коллекцию</h1></th>
                    <th width="50px"><img src="http://www.ceramic3d.ru/img/logo.png"></th>
                </tr>
            </thead>
            <tbody style="font-size: 25px;">
                <tr>
                    <td width="200px">Имя:</td>
                    <td colspan="2" width="300px">'.$_POST['name'].'</td>
                </tr>
                <tr>
                    <td width="200px">Почта:</td>
                    <td colspan="2">'.$_POST['email'].'</td>
                </tr>
                <tr>
                    <td width="200px">Телефон:</td>
                    <td colspan="2">'.$_POST['phone'].'</td>
                </tr> ';
        foreach ($arr_collection as $collection) {
          $message = $message
                .'<tr>
                    <td width="200px">Коллекция:</td>
                    <td colspan="2">'.$collection.'</td>
                </tr>';
        }
        $message = $message
                .'<tr>
                    <td width="200px">Комментарий:</td>
                    <td colspan="2">'.$_POST['comment-col'].'</td>
                </tr>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
        <p><a href="http://www.ceramic3d.ru/ahcms/?m=26&edit='.$id.'">Посмотреть заявку</a></p>
        ';
        $to = $email;
        $from = "Info@ceramic3d.ru";
        $subject = "Пользователь хочет добавить коллекцию";
        $subject = "=?utf-8?B?".base64_encode($subject)."?=";
        $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/html; charset=utf-8\r\n";
        mail ($to, $subject, $message, $headers);
    }
}
?>

<div class="subheader">
    <div class="title-item">
        <div class="title-page">
            <h1>Добавить коллекцию</h1>
        </div>
    </div>
</div>
<section class="addcollection" id="page-addcollection">
    <article class="block-form">
        <form name="addcollection-form" id="addcollection-form" method="post" action="" class="collection-form">
            <div class="input-item">
                <p class="input-block">
                    <input id="name-user" type="text" name="name" placeholder=" " required>
                    <label class="label" for="name-user">ФИО</label>
                </p>
            </div>
            <div class="input-item">
                <p  class="input-block">
                    <input id="email-user" type="email" name="email" placeholder=" " required>
                    <label class="label" for="email-user">E-mail</label>
                </p class="input-block">
            </div>
            <div class="input-item">
                <p class="input-block">
                    <input id="tel-user" type="tel" name="phone" placeholder=" " required  >
                    <label class="label" for="tel-user">Телефон</label>
                </p>
            </div>
            <div class="input-item" id="name-collection">
                <p class="input-block">
                    <input id="name-collect" type="text" name="collection[]" placeholder=" " required>
                    <span class="label">Название коллекции или объекта</span>
                </p>
            </div>
            <div class="input-item">
                <p class="input-block">
                    <textarea id="comment" type="textarea" name="comment-col" placeholder=" " required></textarea>
                    <label class="label" for="comment">Комментарий</label>
                </p>
            </div>
            <div class="item-upload collect">
                <div class="block-upload title-block">
                    <span class="name-item">Дополнительные файлы</span>
                </div>
                <div class="block-upload">
                    <span class="check-file">Файлы не выбраны</span>
                    <label class="file-upload">
                        <span>Выберите файлы</span>
                        <input type="file" id="upload-collect" name="filescollect[]" multiple>
                    </label>
                </div>
            </div>
            <p class="action">* В комментариях Вы можете оставить ссылку на сайт производителя</p>
            <p>Перед отправкой заявки внимательно ознакомьтесь с 
                <label for="regulations-col" class="open-col-dialog">регламентом</label>
            </p>
            
            <button class="send-button" type="submit" id="send-collection">Отправить заявку</button>
            <p id="sended-request">Заявка отправлена<br>С Вами свяжутся в ближайшее время</p>
        </form>
    </article>
    <input type="checkbox" name="regulations" id="regulations-col">
    <article class="col-dialog">
        <label for="regulations-col" class="close-col-dialog"> &#10006;</label>
        <div class="col-dialog-content">
            <?
                $dialog = mysql_query("SELECT *
                FROM qf_pages
                WHERE qf_name = 'regulations-collect' LIMIT 1");
                while ($res = mysql_fetch_array($dialog)) {
                    echo $res['qf_znach'];
                }
            ?>
        </div>
    </article>
</section>
<div style="margin-top: 7%;">
<? include 'footer.php';?>
</div>

<script type="text/javascript">
    function addPlus(itemInput) {
        const plusEl = '<span id="add-collect">+</span>';
        const isPlusEl = itemInput.querySelectorAll('#add-collect');
        const isMinusEl = itemInput.querySelectorAll('#del-collect');
        if (isPlusEl.length === 0) {
            itemInput.insertAdjacentHTML('beforeEnd', plusEl);

            return itemInput.querySelector('#del-collect');
        }
    }

    function addCollectItem(itemsNameCollect, addcollectPage) {
        if (itemsNameCollect.length === 0) {
            console.log('Not found items with collect');
            return;
        } else if (itemsNameCollect.length <= 20) {
            const lastIndex = itemsNameCollect.length - 1;
            const newItem = itemsNameCollect[lastIndex].cloneNode(true);
            newItem.querySelector('input').value = '';
            if (lastIndex === 0) {
                changePlusOnMinus(newItem);
            }
            addClickRemove(newItem);
            itemsNameCollect[lastIndex].insertAdjacentElement('afterEnd', newItem);
        }
    }

    function changePlusOnMinus(item) {
        const minusEl = '<span id="del-collect">-</span>';
        const isPlusEl = item.querySelectorAll('#add-collect');
        isPlusEl[0].remove();
        item.insertAdjacentHTML('beforeEnd', minusEl);
    }

    function addClickRemove(item) {
        const btnDel = item.querySelector('#del-collect');
        btnDel.onclick = function() {
            item.remove();
        }
    }

    const addcollectPage = document.getElementById('page-addcollection');
    let itemsNameCollects = addcollectPage.querySelectorAll('#name-collection')
    addPlus(itemsNameCollects[0]);
    let plusCollectEl = addcollectPage.querySelector('#add-collect');

    plusCollectEl.onclick = function() {
        addCollectItem(itemsNameCollects, addcollectPage);
        itemsNameCollects = addcollectPage.querySelectorAll('#name-collection');
    };

    const uploadCollect = document.getElementById('upload-collect');
    uploadCollect.addEventListener('change', checkUploadFile, false);
</script>

<script type="text/javascript">
$(document).ready(function(){
    $("#addcollection-form").submit(function(e) {
      e.preventDefault();
      var form_data = new FormData();
      $('#addcollection-form input, #addcollection-form textarea').each(function(index, element) {
            if ( $(this).attr('type')!='file') {
                form_data.append($(this).attr('name'), $(this).val());
            } else if ($(this).attr('type')==='file') {
                const files = $(this).prop('files');
                addFiles(files, form_data, $(this).prop('name'));
            }
        });
      $.ajax({
      type: "POST",
      url: "addcollection",
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      context: document.body,
      success: function() {
        sendMail();
      },
      error: function() {
        console.log("Error send mail");
      }
    });
    });

    function sendMail() {
        var button = $("#send-collection");
        button.prop( "disabled", true );
        button.hide();
        $("#sended-request").show();
    }

    function addFiles(files, formData, name) {
        for (const fileId in files) {
            if (files.hasOwnProperty(fileId)) {
                formData.append(name, files[fileId]);
            }
        }
    }
});
</script>

<style type="text/css">
    .addcollection
    {
        width: 960px;
        margin: 0 auto;
    }
    .addcollection .block-form
    {
        margin: 0 auto;
        text-align: center;
        margin-top: 5%;
    }
    #name-collection
    {
        position: relative;
    }

    #name-collection #add-collect,
    #name-collection #del-collect
    {
        position: absolute;
        right: -11px;
        top: 21px;
        font-size: 30px;
        line-height: 30px;
        cursor: pointer;
        transition: 100ms;
        width: 30px;
        height: 30px;
        color: #fff;
        background: #008dd2;
        border-radius: 20px;
    }

    #name-collection #del-collect
    {
        font-size: 35px;
        line-height: 25px;
        background: #e31e24;
    }

    #name-collection #add-collect:hover,
    #name-collection #del-collect:hover
    {
        transform: scale(1.1);
    }

    .block-form .collection-form 
    {
        width: 45%;
        margin: 0 auto;
    }

    .block-form .input-item
    {
        padding: 5px;
        margin-bottom: 7%;
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
        border-radius: 50px;
    }

    .input-block
    {
        position: relative;
        padding: 20px 25px 10px 25px;
        border-radius: 45px;
        background: #fff;
        margin: 0;
        z-index: 0;
    }

    .input-block .label
    {
        position: absolute;
        top: 20px;
        left: 25px;
        font-size: 1.2em;
        transition: 200ms;
        white-space: nowrap;
        user-select: none;
        z-index: -1;
    }

    .input-block input
    {
        width: 100%;
        min-height: 30px;
        border: none;
        font-size: 20px;
        background: transparent;
        z-index: 1;
    }

    .input-block input:focus+.label,
    .input-block input:not(:placeholder-shown)+.label
    {
        font-size: 22px;
        top: -12px;
        left: 50%;
        transform: translate(-50%, 0);
        background: #fff;
        transition: 200ms;
        color: #008DD2;
        padding: 0 10px;
        white-space: nowrap;
    }

    .input-block #comment
    {
        width: 100% !important;
        font-size: 20px;
        border: none;
        background: transparent;
    }

    .input-block #comment:not(:placeholder-shown)+label,
    .input-block #comment:focus+label
    {
        font-size: 22px;
        top: -12px;
        left: 50%;
        transform: translate(-50%, 0);
        background: #fff;
        transition: 200ms;
        color: #008DD2;
        padding: 0 10px;
        white-space: nowrap;
    }

    .block-form .send-button {
        text-transform: uppercase;
        width: 100%;
        min-height: 40px;
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
        height: 50px;
        border-radius: 50px;
        font-size: 25px;
        color: #fff;
        border: none;
        cursor: pointer;
        transition: 200ms;
        box-shadow: none;

    }

    .block-upload.title-block
    {
        height: 30px;
        min-height: 30px;
    }

    .block-form .send-button:hover,
    .item-upload.collect .file-upload:hover
     {
        background: -webkit-linear-gradient(#1beabd, #008dd2);
        background: -o-linear-gradient(#1beabd, #008dd2);
        background: linear-gradient(#1beabd, #008dd2);
        transition: 200ms;
    }

    .block-form .send-button:active,
    .item-upload.collect .file-upload:active {
        background: -webkit-linear-gradient(#008dd2, #1beabd);
        background: -o-linear-gradient(#008dd2, #1beabd);
        background: linear-gradient(#008dd2, #1beabd);
    }

    .block-form .send-button:focus,
    .item-upload.collect .file-upload:focus {
        outline:0;
    }
    
    #sended-request {
        font-size: 22px;
        display: none;
    }

    .action {
        color: #008dd2;
        font-size: 22px;
        margin: 20px 0;
    }

    .item-upload.collect .block-upload {
        width: 100%;
        position: relative;
    }

    .item-upload.collect .block-upload:last-child {
        text-align: center;
    }

    .item-upload.collect .file-upload {
        top: 50%;
        transform: translateY(-50%);
        position: absolute;
        right: 0;
    }

    .item-upload.collect .check-file {
        position: absolute;
        left: 0;
        max-width: 50%;
        top: 25%;
        display: inline-block;
    }

    .open-col-dialog {
        font-weight: bold;
        text-decoration: underline;
        cursor: pointer;
    }

    #regulations-col {
        display: none;
    }

    .col-dialog {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        min-height: 100vh;
        background: rgba(0, 0, 0, .6);
        z-index: 110;
        display: none;
    }

    .col-dialog-content {
        width: 80%;
        min-height: 100px;
        margin: 0 auto;
        background: rgba(255, 255, 255, .9);
        margin-top: 10vh;
        color: #000;
        position: relative;
        max-height: 80vh;
        overflow-y: auto;
        overflow-x: hidden;
        max-width: 1080px;
    }

    .close-col-dialog {
        position: absolute;
        right: calc(50% - 560px);
        top: calc(10vh - 20px);
        cursor: pointer;
        color: #fff;
    }

    #regulations-col:checked ~ .col-dialog {
        display: block;
    }

    @supports (-ms-ime-align:auto) {    
        .input-block input +.label,
        .input-block #comment + label
        {
            font-size: 22px;
            top: -12px;
            left: 50%;
            transform: translate(-50%, 0);
            background: #fff;
            transition: 200ms;
            color: #008DD2;
            padding: 0 10px;
            white-space: nowrap;
        }
    }
</style>