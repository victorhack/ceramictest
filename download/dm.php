<?php
echo('FILE_LIST_0' . "\n");
require_once('config.php');
$db = new PDO('mysql:host=' . CONFIG_DBHOST . ';dbname=' . CONFIG_DBNAME, CONFIG_DBUSER, CONFIG_DBPASS);
$db->exec('SET NAMES utf8');

$id = $db->quote($_GET['id']);
$sql = 'select *, UNIX_TIMESTAMP(modified) as time from pack_content where pack_id = ' . $id . ' order by sort';
$r = $db->query($sql);
if($r === false) die(var_dump($db->errorInfo()));
$n = $r->fetchAll(PDO::FETCH_ASSOC);
for($i = 0; $i < count($n); $i++)
{
	$row = $n[$i];
	echo('"' . $row['action'] . '";"' . $row['srcpath'] . '";"' . $row['filename'] . '";"' . $row['filesize'] . '";"' . $row['time'] . '";"' . $row['filehash'] . '";"' . $row['url'] . '";"' . $row['dstpath'] . '";"' . $row['sparam'] . '";"' . $row['iparam'] . '"' . "\n");
}
$db = null;
?>