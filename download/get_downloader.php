<?php
function getRemoteFilename($url)
{
	$content = get_headers($url, 1);
	$content = array_change_key_case($content, CASE_LOWER);
	$realfilename = '';
	if ($content['content-disposition']) 
	{
			$tmp_name = explode('=', $content['content-disposition']);
			if ($tmp_name[1]) $realfilename = trim($tmp_name[1],'";\'');
	} 
	if( $realfilename == '' )
	{
		$stripped_url = preg_replace('/\\?.*/', '', $url);
		$realfilename = basename($stripped_url);
	} 
	return $realfilename;
}
function downloadFile ($url, $filename) 
{
  $src = fopen ($url, "rb");
  if ($src) 
	{
    $dst = fopen ($filename, "wb");
    if ($dst)
		{
			while( !feof($src) )
				fwrite( $dst, fread( $src, 1024 * 8 ), 1024 * 8 );
			fclose( $dst );
		}
    fclose($src);
  }
}
function getFileObject($filename)
{
	$f = './' . $filename;
	if(file_exists($f))
	{
		return array(
			'name' => $filename,
			'time' => filemtime($f),
			'size' => filesize($f),
			'hash' => md5_file($f),
		);
	}
	else
		return null;
}

//var_dump($_POST);
require_once('config.php');
$db = new PDO('mysql:host=' . CONFIG_DBHOST . ';dbname=' . CONFIG_DBNAME, CONFIG_DBUSER, CONFIG_DBPASS);
$db->exec('SET NAMES utf8');

$name = $db->quote($_POST['cap']);
if($name == '')
	$name = 'dman';

$files = array();
foreach ( $_POST as $key => $value )
{
	if( substr($key, 0, 2) == 'ch' )
		$files[] = getFileObject($value);
}
function cmp($a, $b)
{
    if ($a['name'] == $b['name']) {
        return 0;
    }
    return ($a['name'] < $b['name']) ? -1 : 1;
}
uasort($files, 'cmp');
$hash = '';
foreach($files as $v)
	$hash .= json_encode($v);
$hash = md5($hash);
//var_dump($files);die();
$sql = 'select * from packs where hash = "' . $hash . '" limit 1';
$r = $db->query($sql);
if($r === false) die($db->errorInfo());
$n = $r->fetchAll(PDO::FETCH_ASSOC);
if(count($n))
{
	$v = $n[0];
	$id = $v['id'];
	$name = $v['name'];
}
else
{
	$sql = 'insert into packs (name, hash) values ("' . $name . '", "' . $hash . '");';
	$q = $db->exec($sql);
	if($q === false) die($db->errorInfo());
	$id = $db->lastInsertId();
	$sql = '';
	$sort = 0;
	foreach($files as $v)
	{
		$file = pathinfo($v['name'], PATHINFO_BASENAME);
		$path = pathinfo($v['name'], PATHINFO_DIRNAME);
		$sql .= ', (' . $id . ', ' . $sort++ . ', "' . date('Y-m-d, H:i:s', $v['time']) . '", ' . $v['size'] . ', "' . $v['hash'] . '", "' . $file . '", "' .  $path . '", NULL, 0, "http://distrib.ceramic3d.ru/' . 
		$v['name'] . '", NULL, NULL)';
	}
	$sql = 'insert into pack_content (pack_id, sort, modified, filesize, filehash, filename, srcpath, dstpath, action, url, sparam, iparam) values ' . substr($sql, 1) . ';';
	$q = $db->exec($sql);
	if($q === false) die('error executing query <br>' . $sql . '<br>' . var_dump($db->errorInfo()));
}
$path = 'dman/' . $id . '/';
if( !is_dir($path) ) mkdir( $path, 0777, true );
$url = 'http://code.ceramic3d.ru:4455/get?id=' . $id . '&name=' . urlencode(substr($name, 1, -1));
$fname = getRemoteFilename($url);
$dst = $path . $fname;
if( !file_exists( $dst ) )
	downloadFile( $url, $dst );
if( file_exists( $dst ) )
{
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($dst) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($dst));
		readfile( $dst );
}
else
		header('HTTP/1.0 404 Not Found');
$db = null;
?>