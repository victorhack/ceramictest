<?
    if ((isset($_POST['name']) && $_POST['name']!="") &&
    (isset($_POST['phone']) && $_POST['phone']!="") &&
    (isset($_POST['email']) && $_POST['email']!="") &&
    (isset($_POST['check-radio']) && $_POST['check-radio']!="stand")) {
    
    $arrField = [
        ['name','ФИО'],
        ['phone','Телефон'],
        ['email','Email'],
        ['company','Название компании'],
        ['key','ID ключа'],
        ['description','Описание обнаруженного дефекта'],
        ['cause','Обстоятельства приведшие к возникновению ошибки (какое действие производилось и с какими объектами)'],
    ];
    $error = [];
    foreach ($arrField as $a) {
        $_POST[$a[0]] = trim($_POST[$a[0]]);
        if ($_POST[$a[0]]=='') {
            $error[] = $a[0];
        }
    }
    if (count($error) < 1) {
        $arr_files = ['project','screen'];
        $path = $_SERVER['DOCUMENT_ROOT'].'support_files/';
        $folder = '';
        $f = false;
        foreach ($arr_files as $a) {
            if (!empty($_FILES[$a]['tmp_name'])) {
                // Закачиваем файл
                error_reporting(E_ALL);
                if (!is_dir($path.date('Y-m-d'))) mkdir($path.date('Y-m-d'));
                $fPath = $path.date('Y-m-d').'/'.str2url($_POST['company']);
                if ( !is_dir($fPath)) mkdir($fPath);
                $folder = $fPath;
                $fPath = $fPath.'/'.$a."_".time().'_'.str2url($_FILES[$a]['name']);
                
                if (copy($_FILES[$a]['tmp_name'], $fPath)) $picture = $fPath;
                $f[] = $picture;
            }
        }
        //Запишем в базу
        $q = $pdo->prepare("INSERT INTO support (`fio`, `tel`, `email`,`company`,`key`,`deffect`,`what`,`folder`,`files`) VALUES (?,?,?,?,?,?,?,?,?)");
        $q->execute(array($_POST['name'], $_POST['phone'], $_POST['email'], $_POST['company'], $_POST['key'], $_POST['description'], $_POST['cause'], $folder, $f?implode(';',$f):''));
        $id = $pdo->lastInsertId();
        $txt = "<p>Обращение #$id от ".date("d.m.Y H:i:s")."(<a href='http://ceramic3d.ru/ahcms/?m=20&edit=$id'>открыть на портале</a>)</p>";
        $txt .= "<p><b>".$_POST['company']."</b> (KEY ID ".$_POST['key'].")<br>".$_POST['name']." (".$_POST['email']." | ".$_POST['phone'].")</p>";
        $txt .= "<p><i>Описание обнаруженного дефекта</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['description']))."</p>";
        $txt .= "<p><i>Обстоятельства приведшие к возникновению ошибки</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['cause']))."</p>";
        //Отправим письмо
        sendmail('Обращение #'.$id,$txt, $arr_g['cont']['support_email'],$f,false);
        $send = true;
    }
} else if ((isset($_POST['name']) && $_POST['name']!="") &&
    (isset($_POST['phone']) && $_POST['phone']!="") &&
    (isset($_POST['company']) && $_POST['company']!="")) {
    $arrField = [
        ['name','ФИО'],
        ['date-mount', '11-11-11'],
        ['phone','Телефон'],
        ['address','Адрес стойки'],
        ['company','Название компании'],
        ['inventory-number','Инвентарный номер'],
        ['description','Описание обнаруженного дефекта'],
        ['cause','Обстоятельства приведшие к возникновению ошибки (какое действие производилось и с какими объектами)'],
        ['serial','Серийный номер'],
        ['parallel', 'Ключ параллель'],
        ['date-shipment', '11-01-01'],
        ['city', 'Город отправки']
    ];
    $error = [];
    foreach ($arrField as $a) {
        $_POST[$a[0]] = trim($_POST[$a[0]]);
        if ($_POST[$a[0]]=='') {
            $error[] = $a[0];
            echo $a[0];
        }
    }
    if (count($error) < 1) {
        $arr_files = ['project','screen','photo-stand'];
        $path = $_SERVER['DOCUMENT_ROOT'].'support_files/';
        $folder = '';
        $f = false;
        foreach ($arr_files as $a) {
            if (!empty($_FILES[$a]['tmp_name'])) {
                // Закачиваем файл
                error_reporting(E_ALL);
                if (!is_dir($path.date('Y-m-d'))) mkdir($path.date('Y-m-d'));
                $fPath = $path.date('Y-m-d').'/'.str2url($_POST['company']);
                if ( !is_dir($fPath)) mkdir($fPath);
                $folder = $fPath;
                $fPath = $fPath.'/'.$a."_".time().'_'.str2url($_FILES[$a]['name']);
                
                if (copy($_FILES[$a]['tmp_name'], $fPath)) $picture = $fPath;
                $f[] = $picture;
            }
        }

        $q = $pdo->prepare("INSERT INTO `support_stand`(`fio`, `date_install`, `tel`, `address`, `company`, `inventory_number`, `deffect`, `cause`, `folder`, `files`, `serial_key`, `parallel`, `date_shipment`, `city`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $q->execute(array($_POST['name'], $_POST['date-mount'], $_POST['phone'], $_POST['address'], $_POST['company'], $_POST['inventory-number'], $_POST['description'], $_POST['cause'], $folder, $f?implode(';',$f):'', $_POST['serial'], $_POST['parallel'], $_POST['date-shipment'], $_POST['city']));
            $id = $pdo->lastInsertId();
        }
        $txt = "<p>Обращение по стойке #$id от ".date("d.m.Y H:i:s")."(<a href='http://ceramic3d.ru/ahcms/?m=25&edit=$id'>открыть на портале</a>)</p>";
        $txt .= "<p><b>".$_POST['company']."</b> (Инвентарный номер ".$_POST['inventory-number'].")<br>".$_POST['name']." (".$_POST['phone'].")</p>";
        $txt .= "<p><i>Описание обнаруженного дефекта</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['description']))."</p>";
        $txt .= "<p><i>Обстоятельства приведшие к возникновению ошибки</i><br>".str_replace("\r\n","<br>",htmlspecialchars($_POST['cause']))."</p>";
        //Отправим письмо
        sendmail('Обращение #'.$id,$txt, $arr_g['cont']['support_email'],$f,false);
        $send = true;
}
?>
<div class="subheader">
    <div class="title-item">
        <div class="title-page">
            <h1>Техническая поддержка</h1>
        </div>
    </div>
</div>
<section class="help-page">
    <article class="block-form-help">
        <div class="attention-block">
            <p class="attention">
                <b>Внимание!</b> В случае обнаружения ошибки в программе обязательно прикрепите снимок экрана
            </p>
        </div>
        <form enctype="multipart/form-data" id="help-form" method="post" class="help-form" name="send-support" action="">
            <input type="radio" name="check-radio" value="program" id="check-program" class="input-radio">
            <label for="check-program" class="btn-problem"><span>Программа<span></label>
            <input type="radio" name="check-radio" value="stand" id="check-stand" class="input-radio">
            <label for="check-stand" class="btn-problem"><span>Сенсорная стойка</span></label>
            <input type="radio" name="check-radio" value="virtual" id="check-virtual" class="input-radio">
            <label for="check-virtual" class="btn-problem"><span>Виртуальная реальность</span></label>
            <fieldset class="block-btn-radio">
                <article class="block-stand">
                    <table>
                        <tr>
                            <th>Серийный № (Ceramic3d), он же номер гарантийного талона</th>
                            <th>Инвентарный № (Параллель)</th>
                            <th>Дата отгрузки</th>
                            <th>Фото стойки при отгрузке</th>
                            <th>Город отправки (направление)</th>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="serial" id="key-serial">
                            </td>
                            <td>
                                <input type="text" name="parallel" id="key-parallel">
                            </td>
                            <td>
                                <input type="date" name="date-shipment" id="date-shipment">
                            </td>
                            <td>
                                <input type="file" name="photo-stand">
                            </td>
                            <td>
                                <input type="text" name="city" id="city">
                            </td>
                        </tr>
                    </table>
                </article>
            </fieldset>
            <fieldset class="input-layout">
                <section class="item-input flex-input">
                    <label for="name-help" class="block-input block-name">
                        <input class="input-help" type="text" placeholder="&nbsp;" name="name" id="name-help" required>
                        <span class="label-help">ФИО</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="date-mount" class="block-input block-date">
                        <input class="input-help" type="date" placeholder="&nbsp;" name="date-mount" id="date-mount-help">
                        <span class="label-help">Дата установки</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="phone-help" class="block-input">
                        <input
                            class="input-help"
                            type="tel"
                            placeholder="&nbsp;"
                            name="phone"
                            id="phone-help"
                            pattern="^\+?[0-9]{1,5}[\- ]?[0-9]{3}[\- ]?[0-9]{3}[\- ]?[0-9]{2}[\- ]?[0-9]{2}$"
                            required
                        >
                        <span class="label-help">Телефон</span>
                        <span class="border-help"></span>
                        <span class="pattern-phone"></span>
                    </label>
                    <label for="address-help" class="block-input block-address">
                        <input class="input-help" type="text" placeholder="&nbsp;" name="address" id="address-help">
                        <span class="label-help">Адрес местонахождения стойки</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="email-help" class="block-input block-email">
                        <input
                            class="input-help"
                            type="text"
                            placeholder="&nbsp;"
                            name="email"
                            id="email-help"
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                            required>
                        <span class="label-help">E-mail</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="company-help" class="block-input">
                        <input class="input-help" type="text" placeholder="&nbsp;" name="company" id="company-help" required>
                        <span class="label-help">Название компании</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="inventory-number-help" class="block-input block-inventory">
                        <input class="input-help" type="text" placeholder="&nbsp;" name="inventory-number"
                        id="inventory-number-help">
                        <span class="label-help">Инвентарный номер</span>
                        <span class="border-help"></span>
                    </label>
                    <label for="key-help" class="block-input block-key">
                        <input class="input-help" type="text" placeholder="&nbsp;" name="key" id="key-help" required>
                        <span class="label-help">ID ключа</span>
                        <span class="border-help"></span>
                    </label>
                </section>
            </fieldset>
            <fieldset>
                <p class="item-input">
                    <label for="name-help" class="block-input block-textarea">
                        <textarea class="input-help" placeholder="&nbsp;" name="description" id="description-help" required></textarea>
                        <span class="label-help">Описание обнаруженного дефекта</span>
                        <span class="border-help"></span>
                    </label>
                </p>
                <p class="item-input">
                    <label for="name-help" class="block-input block-textarea">
                        <textarea class="input-help" placeholder="&nbsp;" name="cause" id="cause-help"></textarea>
                        <span class="label-help">Что привело к возникновению ошибки?</span>
                        <span class="border-help"></span>
                    </label>
                </p>
            </fieldset>
            <fieldset>
                <div class="item-upload">
                    <div class="block-upload">
                        <span class="name-item">Проект с обнаруженным дефектом (архив)</span>
                    </div>
                    <div class="block-upload">
                        <span class="check-file">Файл не выбран</span>
                        <label class="file-upload">
                            <span class="button">Выберите файл</span>
                            <input type="file" id="upload-defect" name="project">
                        </label>
                    </div>
                </div>
                <div class="item-upload">
                    <div class="block-upload">
                        <span>Снимок экрана с ошибкой</span>
                    </div>
                    <div class="block-upload">
                        <span class="check-file">Файл не выбран</span>
                        <label class="file-upload">
                            <span class="button">Выберите файл</span>
                            <input type="file" id="upload-photo-window" name="screen">
                        </label>
                    </div>
                </div>
            </fieldset>
            <p class="info">
                После получения заявки мы отправим Вам письмо с указанием времени,
                в которое специалист технической поддержки свяжется с Вами.<br>
                Уважаемый клиент, если время, указанное в заявке, Вам не подходит,
                свяжитесь с нами по номеру +7 (495) 215-24-47, чтобы назначить другое время.
            </p>
            <p><input type="checkbox" name="policy" disabled checked>Нажимая на кнопку "Отправить", я даю <label for="privacy-dialog-check" class="privacy-button-open">согласие на обработку персональных данных</label>.</p>
            <p class="warranty-msg">Срок бесплатного гарантийного обслуживания истек. Стоимость ремонтных работ вам сообщит специалист, после оценки обнаруженного дефекта.</p>
            <p class="succsess-msg">Ваша заявка принята в работу, в течение дня с вами свяжется специалист!</p>
            <input type="submit" value="Отправить" name="send-support-b" class="btn-submit" id="help-bnt-submit">
        </form>
    </article>
    <? include 'code/privacyPolicy.php' ?>
    <div style="margin-top: 7%;">
        <? include 'footer.php';?>
    </div>
</section>

<script type="text/javascript">
    function setRequired() {
        for (const elNameId of arguments) {
            const el = document.getElementById(elNameId);
            if (el) {
                el.required = true;
            }
        }
    }

    function delRequired() {
        for (const elNameId of arguments) {
            const el = document.getElementById(elNameId);
            if (el) {
                el.required = false;
            }
        }
    }

    const uploadDefect = document.getElementById('upload-defect');
    const uploadWindow = document.getElementById('upload-photo-window');
    uploadDefect.addEventListener('change', checkUploadFile, false);
    uploadWindow.addEventListener('change', checkUploadFile, false);

    const radioStand = document.getElementById('check-stand');
    const radioProgram = document.getElementById('check-program');
    const radioVirtual = document.getElementById('check-virtual');
    radioStand.onclick = function() {
        setRequired(
            'key-serial',
            'key-parallel',
            'date-shipment',
            'city',
            'date-mount-help',
            'address-help',
            'inventory-number-help'
            );
        delRequired(
            'email-help',
            'key-help'
            );
    };
    radioProgram.onclick = function() {
        delRequired(
            'key-serial',
            'key-parallel',
            'date-shipment',
            'city',
            'date-mount-help',
            'address-help',
            'inventory-number-help'
            );
        selRequired(
            'email-help',
            'key-help'
            );
    };
    radioVirtual.onclick = radioProgram.onclick;
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#help-form").submit(function(e) {
            e.preventDefault();
            var form_data = new FormData();
            var radioName = 'check-radio';
            var radioChecked = $('input[name="' + radioName + '"]:checked').val();
            $('#help-form input, #help-form textarea').each(function(index, element) {
                if ( $(this).attr('type')!='file' && $(this).attr('type')!='radio' ) {
                    form_data.append($(this).attr('name'), $(this).val());
                } else if ($(this).attr('type')==='file') {
                    form_data.append($(this).attr('name'), $(this).prop('files')[0]);
                }
            });
            if (radioChecked) {
                form_data.append(radioName, radioChecked);
            } else {
                form_data.append(radioName, '');
            }

        $.ajax({
            url: 'helpme',
            dataType: 'html',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            context: document.body,
            type: 'POST',
            success: function() {
                sendMail(radioChecked);
            },
            error: function() {
                 console.log("Error send mail");
            }
        });
    });

      function sendMail(radioChecked) {
        var btn = $("#help-bnt-submit");
        var succsessMsg = $('.succsess-msg');
        btn.prop( "disabled", true );
        btn.val('✔');
        btn.addClass('succsess');
        succsessMsg.show();
        if (radioChecked === 'stand') {
            var dateShipment = $('#date-shipment').val().split('-');
            var timeShipment = new Date(dateShipment[0], dateShipment[1], dateShipment[2]);
            var timeNow = new Date();
            timeNow.setHours(0, 0, 1, 0)
            var timeWarranty = (timeNow - timeShipment) / 1000 / 60 / 60 / 24 / 365;
            if (timeWarranty > 2) {
                $('.warranty-msg').show();
            }
        }
      }
  });    
</script>

<style type="text/css">
    .help-page
    {
        width: 100%;
        height: 500px;
    }

    .help-form
    {
        margin-top: 20px;
    }

    .help-form fieldset
    {
        border: none;
        margin: 0;
    }

    .block-form-help
    {
        max-width: 960px;
        margin: 0 auto;
    }

    .attention-block
    {
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
        border-radius: 50px;
        padding: 5px;
        width: calc(100% - 10px);
        margin-top: 20px;
    }

    .attention
    {
        padding: 20px;
        margin: 0 auto;
        text-align: center;
        width: 95.9%;
        border-radius: 50px;
        font-size: 19px;
        background: #fff;
    }

    .attention b
    {
        font-size: 1.4em;
    }

    .flex-input
    {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }

    .block-input
    {
        position: relative;
        flex-basis: 49%;
        display: block;
        padding: 5px 0;
    }

    .block-name
    {
        flex-basis: 100%;
    }

    .block-date,
    .block-address,
    .block-inventory
    {
        display: none;
    }

    .block-input .input-help
    {
        width: calc(100% - 20px);
        border: 0;
        padding: 15px 10px 5px 10px;
        font-size: 18px;
        border-bottom: 2px solid #C8CCD4;
        height: 25px;
    }

    .block-input .label-help
    {
        position: absolute;
        left: 10px;
        top: 20px;
        font-size: 22px;
        transition: 200ms;
    }

    .block-textarea .input-help
    {
        margin-top: 20px;
        min-height: 70px;
        padding-top: 0;
        resize: vertical;
    }

    .block-input .border-help
    {
        position: absolute;
        left: 0;
        bottom: 5px;
        width: 100%;
        height: 3px;
        border-radius: 1px;
        transition: 200ms;
        transform: scaleX(0);
        transform-origin: 0 0;
        transition: all 300ms ease-in;
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
    }

    .block-textarea .border-help
    {
        bottom: 7px;
    }

    .input-help:focus + .label-help,
    .input-help:not(:placeholder-shown) + .label-help
    {
        top: 0;
        font-size: 16px;
        color: #008dd2;
        transition: 200ms;
    }

    .input-help:focus ~ .border-help,
    .input-help:not(:placeholder-shown) ~ .border-help
    {
        transform: scaleX(1);
        transition: 300ms ease-out;
    }

    .input-help:not(:valid) ~ .label-help {
        color: #e31e24;
    }

    .input-help:not(:valid) ~ .border-help {
        background: -webkit-linear-gradient(291deg, #e31e24, #F666FF);
        background: -o-linear-gradient(291deg, #e31e24, #F666FF);
        background: linear-gradient(21deg, #e31e24, #F666FF);
    }

    /*#phone-help:not(:valid) ~ .border-help::after {
        content: 'в формате +1-123-123-1234';
        color: #e31e24;
        position: absolute;
        display: block;
        top: -49px;
        left: 70px;
    }*/

    .input-help:not(:focus).input-help:placeholder-shown + .label-help {
        color: #000;
    }

    .input-help:not(:placeholder-shown).input-help:valid + .label-help {
        color: #008dd2;
    }

    .couple-block {
        position: relative;
    }

    .couple-item
    {
        width: 49%;
        display: inline-block;
    }

    .couple-item:last-child
    {
        position: absolute;
        right: 0;
    }

    .block-btn-radio
    {
        position: relative;
        text-align: center;
    }

    .btn-problem
    {
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
        padding: 10px 20px;
        display: inline-block;
        width: 200px;
        min-height: 50px;
        vertical-align: middle;
        color: #fff;
        font-size: 22px;
        position: relative;
        border-radius: 50px;
        cursor: pointer;
        text-align: center;
    }

    .btn-problem:nth-child(4)
    {
        margin: 0 114px;
    }

    .btn-problem:hover
    {
        opacity: .8;
    }

    .btn-problem span
    {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin-left: auto;
        margin-right: auto;
    }

    .block-btn-radio .btn-problem:nth-child(2)
    {
        position: absolute;
        left: 0;
        top: 10px;
    }

    .block-btn-radio .btn-problem:nth-child(4)
    {
        margin-left: auto;
        margin-right: auto;
        margin-top: 5px;
    }

    .block-btn-radio .btn-problem:nth-child(6)
    {
        position: absolute;
        right: 0;
        top: 10px;
    }

    .input-radio:checked + .btn-problem {
        background: #008dd2;
    }

    .input-radio
    {
        display: none;
    }

    .block-stand
    {
        margin-top: 25px;
        transition: 200ms;
        transform: scaleY(0) all ease;
        transition-delay: 2ms;
        display: none;
    }

    #check-stand:checked ~ .block-btn-radio .block-stand
    {
        display: block;
        transition: 200ms all ease;
        transition-delay: 2s;
        transform: scaleY(1);
    }

    #check-stand:checked ~ .input-layout .block-name
    {
        flex-basis: 49%;
    }

    #check-stand:checked ~ .input-layout .block-email,
    #check-stand:checked ~ .input-layout .block-key
    {
        display: none;
    }

    #check-stand:checked ~ .input-layout .block-date,
    #check-stand:checked ~ .input-layout .block-address,
    #check-stand:checked ~ .input-layout .block-inventory
    {
        display: block;
    }

    .help-form .btn-submit
    {
        width: 300px;
        height: 50px;
        background: -webkit-linear-gradient(291deg, #008dd2, #1beabd);
        background: -o-linear-gradient(291deg, #008dd2, #1beabd);
        background: linear-gradient(21deg, #008dd2, #1beabd);
        color: #fff;
        font-size: 30px;
        display: block;
        border: none;
        margin-left: auto;
        margin-right: auto;
        border-radius: 50px;
        margin-top: 20px;
        cursor: pointer;
    }
    .help-form .btn-submit:hover {
        opacity: .9;
    }
    .help-form .btn-submit:active {
        background: #008dd2;
    }
    .help-form .succsess {
        width: 50px;
        transition: 200ms;
        cursor: default;
    }
    .help-form .succsess:hover {
        opacity: 1;
    }
    .succsess-msg,
    .warranty-msg {
        margin-top: 20px;
        text-align: center;
        font-size: 19px;
        display: none;
    }
    .warranty-msg {
        color: #e31e24;
    }
    @supports (-ms-ime-align:auto) {
        .input-help ~ .label-help {
            top: 0;
            font-size: 16px;
            color: #008dd2;
        }
    }
</style>