<?
//error_reporting(E_ALL);
if ($_POST['add-page']) { //Пишем новую страницу в базу и перекинем на страницу редактирования сразу. 
	$arr_post[] = trim($_POST['w_user']);
	$arr_post[] = trim($_POST['w_summa']);
	$arr_post[] = trim($_POST['w_comment']);
	$arr_post[] = date("Y-m-d H:i:s");
	$arr_post[] = 0;
	print_r($arr_post);
	$s = $pdo->prepare("INSERT INTO `qf_money` (
						`qf_user`,
						`qf_summa`,
						`qf_comment`,
						`qf_date`,
						`qf_system`
						) VALUES (?,?,?,?,?)");
		$s->execute($arr_post);
		$newid = $pdo->lastInsertId();
	$s = $pdo->prepare("UPDATE qf_users SET qf_summa = qf_summa + ?, qf_date = ? WHERE qf_id = ? LIMIT 1");
	$s->execute(array($arr_post[1],$arr_post[3],$arr_post[0]));
	?>
    <script>
	window.location.href = 'http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&edit=<?=$newid?>';
	</script>
<? exit();
}//Закончили добавление ?>

<?
if ($_GET['del']) {//Delete from 
	$s = $pdo->query("SELECT * FROM `qf_money` WHERE qf_id='".$_GET['del']."' LIMIT 1");
	while ($res = $s->fetch()) {
		$q = $pdo->prepare("UPDATE qf_users SET qf_summa = qf_summa - ?, qf_date = ? WHERE qf_id = ? LIMIT 1");
		$q->execute(array($res['qf_summa'],date("Y-m-d H:i:s"),$res['qf_user']));
	}
	$s = $pdo->query("DELETE FROM `qf_money` WHERE qf_id='".$_GET['del']."' LIMIT 1");
?>
<script>
	window.location.href = 'http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>';
</script>
<? exit(); } ?>

<?
if ($_POST['edit-page']) { //Сохраним все что изменили
	$arr_post[] = trim($_POST['w_user']);
	$arr_post[] = trim($_POST['w_summa']);
	$arr_post[] = trim($_POST['w_comment']);
	$arr_post[] = $_POST['w_date']." ".$_POST['w_time'];
	$arr_post[] = 0;
	$arr_post[] = $_GET['edit'];	
	$s = $pdo->prepare("UPDATE qf_money SET
		`qf_user` = ?,
		`qf_summa` = ?,
		`qf_comment` = ?,
		`qf_date` = ?,
		`qf_system` = ?
		 WHERE qf_id = ? LIMIT 1");
	$s->execute($arr_post);	
	
	$q = $pdo->prepare("UPDATE qf_users SET qf_summa = qf_summa - ? + ?, qf_date = ? WHERE qf_id = ? LIMIT 1");
	$q->execute(array($_POST['w_summa_old'], $arr_post[1],date("Y-m-d H:i:s"),$arr_post[0]));	
		
}//Закончили сохранение ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="middle"><h1>История</h1></td>
    <td align="right" valign="middle"><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&add=1" class="button">+ Добавить</a></td>
  </tr>
</table>
<hr>
<?
if ($_GET['add']==1) {//Добавляем страницу
?>
<form enctype='multipart/form-data' name="form1" id="edit123" method="post" action="?m=<?=$_GET['m']?>">
<h2>Новый элемент</h2>
<div class="block">
<div class="name">Опции</div>
<div class="znach">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td width="150">Клиент</td>
    		<td width="20">&nbsp;</td>
    		<td>
            	<select id="w_user" name="w_user" class="input">
                	<?
                    $q2 = $pdo->query("SELECT * FROM qf_users ORDER BY qf_name");
					while ($r2 = $q2->fetch()) {
					?>
                    	<option value="<?=$r2['qf_id']?>"><?=$r2['qf_name']?></option>
                    <? } ?>
                </select>            
            </td>
		</tr>
  		<tr>
        	<td>Сумма</td>
            <td>&nbsp;</td>
            <td><input type="number" class="input" id="w_summa" name="w_summa" step="0.01" value=""></td>
        </tr>
        <tr>
        	<td>Комментарий</td>
            <td>&nbsp;</td>
            <td><textarea class="input" id="w_comment" name="w_comment" rows="1">Начисление за </textarea></td>
        </tr>
  </table>
</div>
</div>
<p align="right"><input name="add-page" type="submit" id="add-page" value="Добавить" class="button-inline"></p>
</form>


<? exit();} //Закончили ?>

<? if ($_GET['edit']) {//Редактируем страницу?>

<form enctype='multipart/form-data' name="form1" id="edit123" method="post" action="?m=<?=$_GET['m']?>&edit=<?=$_GET['edit']?>">
<h2>Изменить</h2>
<?
$s = $pdo->query("SELECT * FROM qf_money WHERE qf_id = '".$_GET['edit']."' AND qf_system = 0 LIMIT 1");
while ($res = $s->fetch()) {
?>
<div class="block">
<div class="name">Опции</div>
<div class="znach">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td width="150">Клиент</td>
    		<td width="20">&nbsp;</td>
    		<td>
            	<select id="w_user" name="w_user" class="input">
                	<?
                    $q2 = $pdo->query("SELECT * FROM qf_users ORDER BY qf_name");
					while ($r2 = $q2->fetch()) {
					?>
                    	<option value="<?=$r2['qf_id']?>" <?=$res['qf_user']==$r2['qf_id']?' selected':''?>><?=$r2['qf_name']?></option>
                    <? } ?>
                </select>            
            </td>
		</tr>
  		<tr>
        	<td>Сумма</td>
            <td>&nbsp;</td>
            <td><input type="number" class="input" id="w_summa" name="w_summa" step="0.01" value="<?=$res['qf_summa']?>"></td>
        </tr>
        <tr style="display:none;">
        	<td>Сумма старая</td>
            <td>&nbsp;</td>
            <td><input type="number" class="input" id="w_summa_old" name="w_summa_old" step="0.01" value="<?=$res['qf_summa']?>"></td>
        </tr>
        <tr>
        	<td>Комментарий</td>
            <td>&nbsp;</td>
            <td><textarea class="input" id="w_comment" name="w_comment" rows="1"><?=$res['qf_comment']?></textarea></td>
        </tr>
        <tr>
        	<td>Дата</td>
            <td>&nbsp;</td>
            <td><input type="date" class="input" id="w_date" name="w_date" value="<?=date("Y-m-d",strtotime($res['qf_date']))?>"></td>
        </tr>
        <tr>
        	<td>Время</td>
            <td>&nbsp;</td>
            <td><input type="time" class="input" id="w_time" name="w_time" value="<?=date("H:i",strtotime($res['qf_date']))?>"></td>
        </tr>
  </table>
</div>
</div>


<p align="right"><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>">Отменить</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="edit-page" type="submit" id="edit-page" value="Сохранить изменения" class="button-inline"></p>
</form>
<? } ?>
<? exit(); } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr>
    <th height="30" align="left" valign="middle">Дата</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th height="30" align="left" valign="middle">Номер документа</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th height="30" align="left" valign="middle">Сумма</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th height="30" align="left" valign="middle">Клиент</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th width="120" align="center" valign="middle">Редактировать</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th width="120" align="center" valign="middle">Удалить</th>
    <tr>
  <tbody>
<? //Выводим список страниц
$s = mysql_query("SELECT qf_money.*, qf_users.qf_name AS user_name FROM qf_money 
					LEFT JOIN qf_users ON qf_users.qf_id = qf_money.qf_user
					ORDER BY qf_money.qf_date DESC LIMIT 100");
while($res = mysql_fetch_array($s)) {
?>
 <tr>
    <td align="left" valign="middle"><?=date("d.m.Y H:i",strtotime($res['qf_date']))?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=date("Ymd",strtotime($res['qf_date']))?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle" <?=$res['qf_summa']<0?' style="color:red"':''?>><?=$res['qf_summa']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$res['user_name']?></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle"><? if ($res['qf_system']==0) {?><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&edit=<?=$res['qf_id']?>">Редактировать</a><? } else {?>недоступно<? } ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td align="center" valign="middle"><? if ($res['qf_system']==0) {?><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&del=<?=$res['qf_id']?>" class="del">Удалить</a><? } else {?>недоступно<? } ?></td>
  </tr>
<? } ?>
</tbody>
</table>