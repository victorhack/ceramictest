<? 
function crypt_apr1_md5($plainpasswd) { 
    $salt = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 8); 
    $len = strlen($plainpasswd); 
    $text = $plainpasswd.'$apr1$'.$salt; 
    $bin = pack("H32", md5($plainpasswd.$salt.$plainpasswd)); 
    for($i = $len; $i > 0; $i -= 16) { $text .= substr($bin, 0, min(16, $i)); } 
    for($i = $len; $i > 0; $i >>= 1) { $text .= ($i & 1) ? chr(0) : $plainpasswd{0}; } 
    $bin = pack("H32", md5($text)); 
    for($i = 0; $i < 1000; $i++) { 
        $new = ($i & 1) ? $plainpasswd : $bin; 
        if ($i % 3) $new .= $salt; 
        if ($i % 7) $new .= $plainpasswd; 
        $new .= ($i & 1) ? $bin : $plainpasswd; 
        $bin = pack("H32", md5($new)); 
    } 
    for ($i = 0; $i < 5; $i++) { 
        $k = $i + 6; 
        $j = $i + 12; 
        if ($j == 16) $j = 5; 
        $tmp = $bin[$i].$bin[$k].$bin[$j].$tmp; 
    } 
    $tmp = chr(0).chr(0).$bin[11].$tmp; 
    $tmp = strtr(strrev(substr(base64_encode($tmp), 2)), 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", 
    "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"); 
    return "$"."apr1"."$".$salt."$".$tmp; 
}  
?>
<h1>Изменить пароль</h1><p style="font-style:italic">Допустимо использование любых символов. Рекомендуем задавать пароль не менее 6 символов. Используйте буквы и цифры для обеспечения безопасности.</p><hr>
<? if ($_POST['save-pass']) { 
	$alert=0;
	if ($_POST['w_pass']=="") {$alert = 1;}
	if ($alert == 0) {//Меняем пароль
		$line = '';
		$fh = fopen("../incognito/.htpasswd", "r");
		while (!feof($fh)) $line = $line.fgets($fh);
		fclose($fh);
		//Вычленяем логин
		$line = explode(":",$line);
		//Пишем
		$fh = fopen("../incognito/.htpasswd", "w");
		fwrite($fh, $line[0].":".crypt_apr1_md5($_POST['w_pass']));
		fclose($fh);
		//Сообщаем об успешном завершении				
		echo '<p style="color:#4da74d;">Пароль успешно изменен.</p>';	
	} else { //Сообщаем об ошибке
		echo '<p style="color:#cb4b4b;">Укажите новый пароль!</p>';	
	}
}
?>
<form enctype='multipart/form-data' name="form1" id="edit123" method="post" action="?m=<?=$_GET['m']?>">
  <input class="input-inline" type="text" style="width:180px;" id="w_pass" name="w_pass" autocomplete="off" placeholder="Введите новый пароль" value="">
    <input name="save-pass" type="submit" id="save-pass" value="Изменить пароль" class="button-inline">
</form>

<h1>Изменить логин</h1><hr>
<? if ($_POST['save-login']) { 
	$alert=0;
	if ($_POST['w_login']=="") {$alert = 1;}
	if ($alert == 0) {//Меняем
		$line = '';
		$fh = fopen("../incognito/.htpasswd", "r");
		while (!feof($fh)) $line = $line.fgets($fh);
		fclose($fh);
		//Вычленяем пароль
		$line = explode(":",$line);
		//Пишем
		$fh = fopen("../incognito/.htpasswd", "w");
		fwrite($fh, $_POST['w_login'].":".$line[1]);
		fclose($fh);
		//Сообщаем об успешном завершении				
		echo '<p style="color:#4da74d;">Логин успешно изменен.</p>';	
	} else { //Сообщаем об ошибке
		echo '<p style="color:#cb4b4b;">Укажите новый логин!</p>';	
	}
}
?>
<form enctype='multipart/form-data' name="form2" method="post" action="?m=<?=$_GET['m']?>">
  <input class="input-inline" type="text" style="width:180px;" id="w_login" name="w_login" autocomplete="off" placeholder="Введите новый логин" value="">
    <input name="save-login" type="submit" id="save-login" value="Изменить логин" class="button-inline">
</form>
<div style="color:#FFF; margin-top:25px;"><?=$_SERVER['DOCUMENT_ROOT']?></div>