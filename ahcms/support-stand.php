<? // Сохраняем
if ($_POST['edit']) {
    $q = $pdo->prepare("UPDATE support_stand SET 
                        `responsible` = ?,
                        `status` = ?,
                        `expiration_date` = ?
                        WHERE id = ? LIMIT 1
                        ");
    $q->execute(array($_POST['responsible'],$_POST['status'],$_POST['date-expiration'],$_GET['edit']));
}
?>

<? if ($_GET['edit']) { 
    $s = mysql_query("SELECT * FROM support_stand WHERE id = '".$_GET['edit']."' LIMIT 1");
    while ($res = mysql_fetch_array($s)) {
        $a = explode(';',$res['files']);
                $project = '/'.str_replace($_SERVER['DOCUMENT_ROOT'],"",$a[0]);
                $screen = '/'.str_replace($_SERVER['DOCUMENT_ROOT'],"",$a[1]);
                $img_stand = '/'.str_replace($_SERVER['DOCUMENT_ROOT'],"",$a[2]);
        ?>
    <h1>Информация о заявке</h1>
    <div>
        <p>Время поступления заявки: <?=date("H:i d.m.Y",strtotime($res['date']))?></p>
    </div>
    <div>
        <h2>Основная информация</h2>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
        <thead>
            <tr>
                <th align="left" valign="middle">ID</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Имя клиента</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Дата установки</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Телефон</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Адрес</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Компания</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Инвентарный номер</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Описание обнаруженного дефекта</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
                <th align="center" valign="middle">Обстоятельства приведшие к возникновению ошибки</th>
                <th width="20" align="center" valign="middle">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="left" valign="middle"><?=$res['id']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['fio']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=date("d.m.Y",strtotime($res['date_install']))?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['tel']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['address']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['company']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['inventory_number']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['deffect']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle"><?=$res['cause']?></td>
                <td width="20" align="center" valign="middle">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <p>Проект с обнаруженным дефектом (архив): <? if ($project!='/') {?><a href="<?=$project?>" download target="_blank">Скачать</a>  <? } else { echo 'нет';}?></p>
    <p>Снимок экрана с ошибкой: <? if ($screen!='/') {?><a href="<?=$screen?>" download target="_blank">Скачать</a>  <? } else { echo 'нет';}?></p>
    <!-- <div class="container">
        <div class="item item-1">
            <p>Имя клиента: <?=$res['fio']?></p>
        </div>
        <div class="item item-2">
            <p>Дата установки: <?=date("d.m.Y",strtotime($res['date_install']))?></p>
        </div>
        <div class="item item-3"><p>Телефон: <?=$res['tel']?></p></div>
        <div class="item item-4"><p>Адрес: <?=$res['address']?></p></div>
        <div class="item item-5"><p>Компания: <?=$res['company']?></p></div>
        <div class="item item-6"><p>Инвентарный номер: <?=$res['inventory_number']?></p></div>
        <div class="item item-7">Описание обнаруженного дефекта:<p><?=$res['deffect']?></p></div>
        <div class="item item-8">Обстоятельства приведшие к возникновению ошибки:<p><?=$res['cause']?></p></div>
    </div> -->
    <div>
        <h2>Информация о стойке</h2>
    </div>
    <div>
        <div>
            <p>Серийный № (Ceramic3d), он же номер гарантийного талона: <?=$res['serial_key']?></p>
        </div>
        <div>
            <p>Инвентарный № (Параллель): <?=$res['parallel']?></p>
        </div>
        <div>
            <p>Дата отгрузки: <?=date("d.m.Y",strtotime($res['date_shipment']))?></p>
        </div>
        <div>
            <p>Город отправки (направление): <?=$res['city']?></p>
        </div>
        <div>
            <p>Фото стойки при отгрузке: <? if ($img_stand!='/') {?><a href="<?=$img_stand?>" download target="_blank">Скачать</a>  <? } else { echo 'нет';}?></p>
        </div>
    </div>
    <div>
        <h2>Редактировать заявку</h2>
    </div>
    <div>
        <form method="post" name="support-stand" action="?m=<?=$_GET['m']?>&edit=<?=$_GET['edit']?>">
            <div class="input-block">
                <label for="responsible">Ответсвенный от Ceramic3D</label>
                <input
                    type="text"
                    name="responsible"
                    id="responsible"
                    value="<?=$res['responsible']!='0' ? $res['responsible'] : 'Не назначен' ?>"
                >
            </div>
            <div class="input-block">
                <label for="date-expiration">Срок исправления</label>
                <input
                    type="date"
                    name="date-expiration"
                    id="date-expiration"
                    value="<?=date("Y-m-d",strtotime($res['expiration_date']))?>"
                >
            </div>
            <div class="input-block">
                <label for="">Статус заявки</label>
                <select name="status" id="">
                    <option value="1" <?=$res['status']=='1'?' selected':''?>>Новая</option>
                    <option value="2" <?=$res['status']=='2'?' selected':''?>>Закрыто</option>
                    <option value="3" <?=$res['status']=='3'?' selected':''?>>В работе</option>
                    <option value="4" <?=$res['status']=='4'?' selected':''?>>Передал в технический отдел</option>
                </select>
            </div>
            <input type="submit" name="edit" id="submit-btn" class="button-inline" value="Сохранить изменения">
        </form>
    </div>
    <style type="text/css">
        .container {
            display: grid;
            grid-template-columns: 400px 50%;
            grid-template-rows: auto;
        }
        .item-7, .item-8 {
            grid-column: 1 / 3;
        }

        .input-block {
            margin: 10px 0;
            width: 400px;
            display: grid;
            grid-template-columns: 250px 200px;
        }

        #submit-btn {
            margin-top: 10px;
        }
    </style>
<? } exit(); } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="middle"><h1>Заявки в техподдержку (стойки)</h1></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
    <tr>
        <th height="30" align="left" valign="middle">ID</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th  align="center" valign="middle">Клиент</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th  align="center" valign="middle">Номер гарантийного талона</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th width="95" align="left" valign="middle">Дата заявки</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th  align="center" valign="middle">Ответственный от Ceramic3d</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th  align="center" valign="middle">Срок исправления</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th  align="center" valign="middle">Статус</th>
        <th width="20" align="center" valign="middle">&nbsp;</th>
        <th width="200" align="center" valign="middle">Редактировать</th>
    </tr>
    <tbody>
        <?
            $q = mysql_query("SELECT
                support_stand.*,
                support_status.name as status_name
             FROM support_stand
             LEFT JOIN support_status ON support_status.qf_id = support_stand.status
             ORDER BY date");
            while ($res = mysql_fetch_array($q)) {
        ?>
        <tr>
            <td align="left" valign="middle"><?=$res['id']?></td>
            <td>&nbsp;</td>
            <td align="center" valign="middle"><?=$res['fio']?></td>
            <td>&nbsp;</td>
            <td align="center" valign="middle"><?=$res['serial_key']?></td>
            <td>&nbsp;</td>
            <td align="left" valign="middle"><?=date("d.m.Y H:i",strtotime($res['date']))?></td>
            <td>&nbsp;</td>
            <td align="center" valign="middle">
                <? if($res['responsible'] == 0) { ?>
                    Не назначен
                <? } else { 
                    echo $res['responsible'];
                } ?>
            </td>
            <td>&nbsp;</td>
            <td align="center" valign="middle">
                <? if($res['expiration_date'] === '0000-00-00 00:00:00.000000') { ?>
                    Не назначен 
                <? } else {
                   echo date("d.m.Y",strtotime($res['expiration_date']));
                } ?>
            </td>
            <td>&nbsp;</td>
            <td align="center" valign="middle">
                <?=$res['status_name']?>
            </td>
            <td>&nbsp;</td>
            <td align="center" valign="middle"><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&edit=<?=$res['id']?>">Открыть заявку</a></td>
        </tr>
    <? } ?>
    </tbody>
</table>