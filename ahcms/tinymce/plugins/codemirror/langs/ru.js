tinymce.addI18n('ru',{
	'HTML source code': 'Редактор исходного кода',
	'Start search': 'Начать поиск',
	'Find next': 'Вперед',
	'Find previous': 'Назад',
	'Replace': 'Заменить',
	'Replace all': 'Заменить все'
});
