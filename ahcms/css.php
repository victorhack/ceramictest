<script language="Javascript" type="text/javascript" src="./edit_area/edit_area_full.js"></script>
<script>
$(document).ready(function(e) {
    editAreaLoader.init({
			id: "w_css"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,min_height:400
			,language: "ru"
			,syntax: "css"
			,font_size: 10
            ,toolbar: "search, go_to_line, |, undo, redo, |, select_font, |, change_smooth_selection, highlight, reset_highlight, |, fullscreen"
		});
});
</script>
<h1>Редактор стилей CSS</h1><p>Будтье внимательны при редактировании стилей!</p><hr>
<?
error_reporting(E_ALL);
if (isset($_POST['otkat'])) {
	copy('../ah_original.css','../ah.css');
}
if (isset($_POST['save'])) {
	$fp = fopen("../ah.css", "a");
	ftruncate($fp, 0);
	$test = fwrite($fp, stripslashes($_POST['w_css']));
	if ($test) echo '<p style="color:#4da74d;">Изменения сохранены</p>';
	else echo '<p style="color:#cb4b4b;">Ошибка при записи в файл.</p>';
	fclose($fp);
}			
?>

<form enctype='multipart/form-data' name="form1" id="edit123" method="post" action="?m=5" onsubmit="$('#edit_area_toggle_checkbox_w_css').click()">
<textarea name="w_css" id="w_css" class="input" style="width:100%; border:none; background:#fff; min-height:300px; height:300px;" rows="30"><?
    $fp=fopen("../ah.css","r");
	if ($fp) { 
	 $str=fread($fp,filesize("../ah.css")); 
	  echo $str; 
	}?></textarea><br><br>
<input name="save" type="submit" id="save" value="Сохранить" class="button-inline">
<input name="otkat" type="submit" id="otkat" value="Восстановить оригинал" class="button-inline">
</form>