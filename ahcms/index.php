<? include 'config.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//RU" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache">
<title>Alex Hit | CMS</title>
<link rel="stylesheet" type="text/css" href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/ah.css?<?=rand(1000,9999)?>">
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="./tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/chosen/chosen.css">
<script src="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/chosen/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="main.js?<?=rand(1000,9999)?>"></script>
</head>
<body>
<div class="top">
	<div align="left">Alex Hit | CMS v3.06</div>
    <div align="right"><a href="http://<?=$_SERVER['HTTP_HOST']?>" target="_blank"><?=$_SERVER['SERVER_NAME']?></a></div>
</div>

<div class="left">
<form enctype='multipart/form-data' name="forms" id="forms" method="post" action="?m=search">
	<input id="poisk" name="w_poisk" placeholder="Поиск..." autocomplete="off">
</form>
	<ul class="menu">
    	<li><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/" <? if ($_GET['m']==0) {?>class="active"<? } ?>>Старт</a></li>
        <li><a href="?m=7" <? if ($_GET['m']==7) {?>class="active"<? } ?>>Меню</a></li>
        <li><a href="?m=1" <? if ($_GET['m']==1) {?>class="active"<? } ?>>Страницы</a></li>
        <li><a href="?m=2" <? if ($_GET['m']==2) {?>class="active"<? } ?>>Статьи</a></li>
        <li><a href="?m=8" <? if ($_GET['m']==8) {?>class="active"<? } ?>>Отзывы</a></li>
        <li><a href="?m=9" <? if ($_GET['m']==9) {?>class="active"<? } ?>>О компании</a></li>
        <li><a href="?m=10" <? if ($_GET['m']==10) {?>class="active"<? } ?>>Галерея</a></li>
        <li><a href="?m=11" <? if ($_GET['m']==11) {?>class="active"<? } ?>>Модуль Плитка</a></li>
        <li><a href="?m=12" <? if ($_GET['m']==12) {?>class="active"<? } ?>>Партнеры</a></li>
        <li><a href="?m=23" <? if ($_GET['m']==23) {?>class="active"<? } ?>>Обновления</a></li>
        <hr>
        <li><a href="?m=13" <? if ($_GET['m']==13) {?>class="active"<? } ?>>Почта</a></li>
        <li><a href="?m=20" <? if ($_GET['m']==20) {?>class="active"<? } ?>>Заявки</a></li>
        <li><a href="?m=25" <? if ($_GET['m']==25) {?>class="active"<? } ?>>Заявки (стойки)</a></li>
        <li><a href="?m=26" <? if ($_GET['m']==26) {?>class="active"<? } ?>>Коллекции</a></li>
        <li><a href="?m=3" <? if ($_GET['m']==3) {?>class="active"<? } ?>>Файлы</a></li>
        <li><a href="?m=5" <? if ($_GET['m']==5) {?>class="active"<? } ?>>CSS</a></li>
        <li><a href="?m=6" <? if ($_GET['m']==6) {?>class="active"<? } ?>>Доступ</a></li>
        <hr>
        <li><a href="?m=14" <? if ($_GET['m']==14) {?>class="active"<? } ?>>Клиенты</a></li>
        <li><a href="?m=15" <? if ($_GET['m']==15) {?>class="active"<? } ?>>История</a></li>
              
	</ul>
</div>

<div class="content">
<? if ($_GET['m']=='search') include 'search.php'; else {?>
<? if ($_GET['m']==0) include 'start.php';?>
<? if ($_GET['m']==1) include 'pages.php';?>
<? if ($_GET['m']==2) include 'news.php';?>
<? if ($_GET['m']==3) include 'file.php';?>
<? if ($_GET['m']==5) include 'css.php';?>
<? if ($_GET['m']==6) include 'pass.php';?>
<? if ($_GET['m']==7) include 'menu.php';?>
<? if ($_GET['m']==8) include 'mention.php';?>
<? if ($_GET['m']==9) include 'about.php';?>
<? if ($_GET['m']==10) include 'galery.php';?>
<? if ($_GET['m']==11) include 'functions.php';?>
<? if ($_GET['m']==12) include 'logos.php';?>
<? if ($_GET['m']==13) include 'mail.php';?>
<? if ($_GET['m']==14) include 'users.php';?>
<? if ($_GET['m']==15) include 'money.php';?>

<? if ($_GET['m']==20) include 'support.php';?>
<? if ($_GET['m']==21) include 'support-class.php';?>
<? if ($_GET['m']==22) include 'support-status.php';?>
<? if ($_GET['m']==25) include 'support-stand.php';?>
<? if ($_GET['m']==26) include 'collections.php';?>

<? if ($_GET['m']==23) include 'sectionUpdate.php';?>
<? if ($_GET['m']==24) include 'update.php';?>
<? } ?>
</div>
</body>
</html>