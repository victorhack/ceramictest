<?
if ($_POST['edit-page']) { //Сохраним все что изменили
	$q = $pdo->prepare("UPDATE support SET 
						`manual` = ?,
						`class` = ?,
						`status` = ?,
						`comment` = ? WHERE id = ? LIMIT 1
						");
	$q->execute(array($_POST['manual'],$_POST['class'],$_POST['status'],$_POST['comment'],$_GET['edit']));	
}//Закончили сохранение ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="middle"><h1>Заявки в техподдержку</h1></td>
    <td align="right" valign="middle"><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=21">Редактировать классификатор</a><br><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=22">Редактировать статусы</a></td>
  </tr>
</table>
<? if (!$_GET['edit']) {
	$from = $_POST['from']?$_POST['from']:'2017-01-01';
	$to = $_POST['to']?$_POST['to']:date("Y-m-d");
	?>
<form method="post" name="s-search">
	<ul class="filter">	
    	<li>
        	<span>Начиная с</span>
            <input type="date" name="from" value="<?=$from?>">
        </li>
        <li>
        	<span>Заканчивая</span>
            <input type="date" name="to" value="<?=$to?>">
        </li>
        <li>
        	<span>Инструкция</span>
            <select name="manual">
            	<option value="" <?=$_POST['manual']==''?' selected':''?>>Все</option>
                <option value="1" <?=$_POST['manual']=='1'?' selected':''?>>Есть</option>
                <option value="2" <?=$_POST['manual']=='2'?' selected':''?>>В работе</option>
                <option value="3" <?=$_POST['manual']=='3'?' selected':''?>>Нет</option>                
            </select>
        </li>
        <?
        	$arrManual = ['&mdash;','Есть', "В работе", "Нет"];
		?>
        
        <li>
        	<span>Открыта</span>
            <select name="read">
            	<option value="0">Нет</option>
                <option value="1" <?=$_POST['read']=='1'?' selected':''?>>Все</option>           
            </select>
        </li>
        
        <li>
        	<span>Со статусом</span>
            <select name="status">
            	<option value="0">Любой</option>
                <?
                	$q = $pdo->query("SELECT * FROM support_status WHERE active = 1 ORDER BY qf_sort");
					while ($res = $q->fetch()) {
				?>
                <option value="<?=$res['qf_id']?>" <?=$res['qf_id']==$_POST['status']?' selected':''?>><?=$res['name']?></option>
                <? } ?>
            </select>
        </li>
        <li>
        	<span>Классификатор</span>
            <select name="class">
            	<option value="0">Любой</option>                
                <?
                	$q = $pdo->query("SELECT * FROM support_class WHERE active = 1 ORDER BY qf_sort");
					while ($res = $q->fetch()) {
				?>
                <option value="<?=$res['qf_id']?>" <?=$res['qf_id']==$_POST['class']?' selected':''?>><?=$res['name']?></option>
                <? } ?>
            </select>
        </li>
        
        <li>
			<input type="submit" value="Применить фильтр" name="do-filter">
        </li>        
    </ul>
</form>
<? } ?>
<hr>


<? if ($_GET['edit']) {//Редактируем страницу?>    
<form enctype='multipart/form-data' name="form1" id="edit123" method="post" action="?m=<?=$_GET['m']?>&edit=<?=$_GET['edit']?>">
<h2>Заявка #<?=$_GET['edit']?></h2>
<?
$s = mysql_query("UPDATE support SET `read` = 1 WHERE id = '".$_GET['edit']."' LIMIT 1");
$s = mysql_query("SELECT * FROM support WHERE id = '".$_GET['edit']."' LIMIT 1");
while ($res = mysql_fetch_array($s)) {
?>
<div class="block">
<div class="name">Опции</div>
<div class="znach">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td width="150">Дата создания</td>
    		<td width="20">&nbsp;</td>
    		<td><?=date("d.m.Y H:i:s",strtotime($res['date']))?></td>
		</tr> 
        <tr>
    		<td width="150">&nbsp;</td>
    		<td width="20">&nbsp;</td>
    		<td>&nbsp;</td>
		</tr>  		
        <tr>
    		<td width="150">Инструкция</td>
    		<td width="20">&nbsp;</td>
    		<td>
            	<select name="manual" class="input">
                	<option value="">Не выбрано</option>
                    <option value="1" <?=$res['manual']=='1'?' selected':''?>>Есть</option>
		            <option value="2" <?=$res['manual']=='2'?' selected':''?>>В работе</option>
        		    <option value="3" <?=$res['manual']=='3'?' selected':''?>>Нет</option>                    
                </select>            
            </td>
		</tr>  		
        <tr>
    		<td width="150">Классификатор</td>
    		<td width="20">&nbsp;</td>
    		<td>
            	<select name="class" class="input">
                	<option value="">Не выбрано</option>
                    <?
                    	$q2 = $pdo->query("SELECT * FROM support_class WHERE active = 1 ORDER BY qf_sort");
						while ($res2 = $q2->fetch()) {
					?>
        		    	<option value="<?=$res2['qf_id']?>" <?=$res['class']==$res2['qf_id']?' selected':''?>><?=$res2['name']?></option>                 
                    <? } ?>
                </select>            
            </td>
		</tr>
        
        <tr>
    		<td width="150">Статус</td>
    		<td width="20">&nbsp;</td>
    		<td>
            	<select name="status" class="input">                	
                    <?
                    	$q2 = $pdo->query("SELECT * FROM support_status WHERE active = 1 ORDER BY qf_sort");
						while ($res2 = $q2->fetch()) {
					?>
        		    	<option value="<?=$res2['qf_id']?>" <?=$res['status']==$res2['qf_id']?' selected':''?>><?=$res2['name']?></option>                 
                    <? } ?>
                </select>            
            </td>
		</tr>
        <tr>
    		<td width="150">&nbsp;</td>
    		<td width="20">&nbsp;</td>
    		<td>&nbsp;</td>
		</tr> 
        <tr>
        	<td align="left" valign="top">Клиент</td>
            <td>&nbsp;</td>
            <td><b><?=$res['company']?> KEY ID <?=$res['key']?></b><br><?=$res['fio']?> (<?=$res['tel']?> | <?=$res['email']?>)</td>
        </tr>
        <tr>
    		<td width="150">&nbsp;</td>
    		<td width="20">&nbsp;</td>
    		<td>&nbsp;</td>
		</tr> 
        <tr>
        	<td align="left" valign="top">Путь к файлам</td>
            <td>&nbsp;</td>
            <?
            	$a = explode(';',$res['files']);
				$project = '/'.str_replace($_SERVER['DOCUMENT_ROOT'],"",$a[0]);
				$screen = '/'.str_replace($_SERVER['DOCUMENT_ROOT'],"",$a[1]);				
			?>
            <td>Путь: <?=$res['folder']?><br>Файл 1: <? if ($project!='/') {?><a href="<?=$project?>" download target="_blank">Скачать</a>  <? } else { echo 'нет';}?>| Файл 2: <? if ($screen!='/') {?><a href="<?=$screen?>" download target="_blank">Скачать</a><? } else { echo 'нет';}?></td>
        </tr>
        <tr>
    		<td width="150">&nbsp;</td>
    		<td width="20">&nbsp;</td>
    		<td>&nbsp;</td>
		</tr> 
        <tr>
        	<td align="left" valign="top">Описание</td>
            <td>&nbsp;</td>
            <td><?=str_replace("\r\n","<br>",$res['deffect'])?><hr><?=str_replace("\r\n","<br>",$res['what'])?></td>
        </tr> 
        <tr>
    		<td width="150">&nbsp;</td>
    		<td width="20">&nbsp;</td>
    		<td>&nbsp;</td>
		</tr>       
        <tr>
        	<td align="left" valign="top">Комментарий</td>
            <td>&nbsp;</td>
            <td><textarea class="input" rows="5" name="comment"><?=$res['comment']?></textarea></td>
        </tr>       
  </table>
</div>
</div>
<p align="right"><a href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>">Список заявок</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="edit-page" type="submit" id="edit-page" value="Сохранить изменения" class="button-inline"></p>
</form>
<? } ?>

<? exit(); } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr>
    <th height="30" align="left" valign="middle">id</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th width="95" align="left" valign="middle">Дата</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Клиент</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Ключ</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Классификация</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Описание</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Инструкция</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th  align="center" valign="middle">Статус</th>
    <th width="20" align="center" valign="middle">&nbsp;</th>
    <th width="120" align="center" valign="middle">Редактировать</th>
    <tr>
  <tbody>
<? //Выводим список 
	if ($_POST['manual'] > 0) $where .= " AND support.manual = ".$_POST['manual'];
	if ($_POST['read'] == 0) $where .= " AND support.read = 0";
	if ($_POST['status'] > 0) $where .= " AND support.status = ".$_POST['status'];
	if ($_POST['class'] > 0) $where .= " AND support.class = ".$_POST['class'];
	
	$query = "SELECT 
				support.*,
				support_class.name as class_name,
				support_status.name as status_name
			FROM support
			LEFT JOIN support_class ON support_class.qf_id = support.class
			LEFT JOIN support_status ON support_status.qf_id = support.status
			WHERE support.date BETWEEN ? AND ?
			$where
			ORDER BY support.date			
	";
	$q = $pdo->prepare($query);
	$q->execute(array($from." 00:00:00", $to." 23:59:59"));
	while ($res = $q->fetch()) {
	
?>
 <tr znach="<?=$res['qf_id']?>" style=" <?=$res['read']?'':'font-weight:bold;'?> ">
    <td align="left" valign="middle"><?=$res['id']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=date("d.m.Y H:i",strtotime($res['date']))?></td>
    <td>&nbsp;</td>
    <td align="left" valign="middle"><?=$res['company']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$res['key']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$res['class_name']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$res['deffect']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$arrManual[$res['manual']]?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><?=$res['status_name']?></td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="center" valign="middle"><a target="_blank" href="http://<?=$_SERVER['HTTP_HOST']?>/ahcms/?m=<?=$_GET['m']?>&edit=<?=$res['id']?>">Редактировать</a></td>
  </tr>
<? } ?>
</tbody>
</table>