
function get_edit(sel) {
	tinymce.init({
    selector: sel,
	content_css : "../../ah.css",
	plugins: [
        "advlist autolink lists link image charmap preview hr anchor",
        "searchreplace wordcount visualblocks visualchars fullscreen codemirror",
        "media nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image preview media | forecolor backcolor | attribs",
    toolbar2: "styleselect fontselect fontsizeselect | div100 | div960 | div720 | Demo | col3",
	formats : {
          div_bg_color : {selector : 'div', attributes : {title : "Фон Блока"}, styles : {background: '#000'}}
        },
	menu : { // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        insert : {title : 'Insert', items : 'link media | template hr'},
        view   : {title : 'View'  , items : 'visualaid'},
        format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
        tools  : {title : 'Tools' , items : 'spellchecker code'}
    },
	visualblocks_default_state: true,
	fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 32pt 36pt 42pt 46pt 54pt",
	relative_urls: false,
	element_format : "html",
	remove_script_host: false,
	cleanup : false,
	verify_html : false,
	cleanup_on_startup : false,
	cleanup_on_save : false,
    browser_spellcheck : true ,
    filemanager_title:"Файловый менеджер",
	link_list : "./link_list.php",
    external_filemanager_path:"./filemanager/",
    external_plugins: { "filemanager" : "../filemanager/plugin.min.js"},
    image_advtab: true,
	tabfocus_elements: ":prev,:next",
	table_advtab: true,
	table_cell_advtab: true,
	table_row_advtab: true,	
	link_class_list: [
        {title: 'Нет', value: ''},
        {title: 'Увеличить фото', value: 'bigPhoto'}
    ],
	rel_list: [
        {title: 'Нет', value: ''},
        {title: 'Галерея 1', value: 'group1'},
		{title: 'Галерея 2', value: 'group1'},
		{title: 'Галерея 3', value: 'group1'}
    ],
	contextmenu: "link image code | inserttable cell row column deletetable",
    codemirror: {
		indentOnInit: true, // Whether or not to indent code on init. 
		path: 'CodeMirror'},
	language : 'ru',
	plugin_preview_width : '1200',
//    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	setup: function(editor) {
        editor.addButton('div100', {
            text: 'Блок 100%',
            icon: false,
            onclick: function() {
                editor.insertContent('<div style="width:100%;">Текст здесь</div><p></p>');
            }
        });
		
		editor.addButton('div960', {
            text: 'Блок 960px',
            icon: false,
            onclick: function() {
                editor.insertContent('<div style="width:100%;" align="center"><div style="width:960px;" align="left">Текст здесь</div></div>');
            }
        });
		
		editor.addButton('div720', {
            text: 'Блок 720px',
            icon: false,
            onclick: function() {
                editor.insertContent('<div style="width:100%;" align="center"><div style="width:720px; padding-left:240px;" align="left">Текст здесь</div></div>');
            }
        });
		
		editor.addButton('Demo', {
            text: 'Демо-версия',
            icon: false,
            onclick: function() {
                editor.insertContent('<a href="#index_demo" class="but_2 index_demo">Заказать бесплатную демо-версию</a>');
            }
        });
		
		editor.addButton('col3', {
            text: '3 колонки',
            icon: false,
            onclick: function() {
                editor.insertContent('<table cellpadding="0" cellspacing="0" width="100%"><tr><td width="30%">&nbsp;</td><td width="5%">&nbsp;</td><td width="30%">&nbsp;</td><td width="5%">&nbsp;</td><td width="30%">&nbsp;</td></tr></table>');
            }
        });
		

    }

});
}

function JSfunc()
{
	/* Making transliteration! */
	this.strTranslit = function(el)
	{
		new_el = document.getElementById('w_url');
		new_t = document.getElementById('w_title');
		new_t.value = el.value;
		A = new Array();
		A["Ё"]="yo";A["Й"]="i";A["Ц"]="ts";A["У"]="u";A["К"]="k";A["Е"]="e";A["Н"]="n";A["Г"]="g";A["Ш"]="sh";A["Щ"]="sch";A["З"]="z";A["Х"]="h";A["Ъ"]="";
		A["ё"]="yo";A["й"]="i";A["ц"]="ts";A["у"]="u";A["к"]="k";A["е"]="e";A["н"]="n";A["г"]="g";A["ш"]="sh";A["щ"]="sch";A["з"]="z";A["х"]="h";A["ъ"]="";
		A["Ф"]="f";A["Ы"]="i";A["В"]="v";A["А"]="a";A["П"]="p";A["Р"]="r";A["О"]="o";A["Л"]="l";A["Д"]="d";A["Ж"]="zh";A["Э"]="e";
		A["ф"]="f";A["ы"]="i";A["в"]="v";A["а"]="a";A["п"]="p";A["р"]="r";A["о"]="o";A["л"]="l";A["д"]="d";A["ж"]="zh";A["э"]="e";
		A["Я"]="ya";A["Ч"]="ch";A["С"]="s";A["М"]="m";A["И"]="i";A["Т"]="t";A["Ь"]="";A["Б"]="b";A["Ю"]="yu";
		A["я"]="ya";A["ч"]="ch";A["с"]="s";A["м"]="m";A["и"]="i";A["т"]="t";A["ь"]="";A["б"]="b";A["ю"]="yu";
		A[" "]="_";A[":"]="_";A[";"]="_";A["/"]="_";A["|"]="_";A["+"]="_";A["%"]="_";A["!"]="_";A["@"]="_";A["#"]="_";A["$"]="_";A["&"]="_";A["("]="_";A[")"]="_";A["="]="_";A["*"]="_";A["^"]="_";A["?"]="_";
		new_el.value = el.value.replace(/([\u0020\u0410-\u0451 :;|+%!@?#$&()=*^/])/g,
			function (str,p1,offset,s) {
				if (A[str] != 'undefined'){return A[str];}
			}
		);
	}
	/* Normalizes a string, eю => eyu */
	this.strNormalize = function(el)
	{
		if (!el) { return; }
		this.strTranslit(el);
	}
}
var oJS = new JSfunc();

$(document).ready(function(e) {
    
	$("a.del").bind('click',function(e) {
		if ($(this).html()=="Удалить") {
			$(this).html('Уверены?');
			return false;			
		} else {
			return true;
		}
	});
	
	var configChosen = {
		'#w_user':{}
	}
    for (var selector in configChosen) {
      $(selector).chosen(configChosen[selector]);
    }
	
	$(".block .name").bind('click', function(e) {
		el = $(this).next();
		if (el.css('display')=='none') el.slideDown('fast'); else el.slideUp('fast');
	});
	
	$('.drag').draggable();
	
});

function getlink() {
	$("#w_link").val('/'+$("#w_page :selected").attr("link"));
}

$.fn.draggable = function(){
	function disableSelection(){
		return false;
	}
    $(this).mousedown(function(e){
		var drag = $(this);
		var posParentTop = drag.parent().offset().top;
		var posParentBottom = posParentTop + drag.parent().height();
		var posOld = drag.offset().top;
		var posOldCorrection = e.pageY - posOld;
        drag.addClass('dragActive');
		var mouseMove = function(e){
			var posNew = e.pageY - posOldCorrection;
			if (posNew < posParentTop){
				if (drag.prev().length > 0 ) {
					drag.insertBefore(drag.prev().css({'top':-drag.height()}).animate({'top':0}, 100));
				}
				drag.offset({'top': posParentTop});
            } else if (posNew + drag.height() > posParentBottom){
				if (drag.next().length > 0 ) {
					drag.insertAfter(drag.next().css({'top':drag.height()}).animate({'top':0}, 100));
                }
				drag.offset({'top': posParentBottom - drag.height()});
            } else {
				drag.offset({'top': posNew});
				if (posOld - posNew > drag.height() - 1){
					drag.insertBefore(drag.prev().css({'top':-drag.height()}).animate({'top':0}, 100));
					drag.css({'top':0});
					posOld = drag.offset().top;
					posOldCorrection = e.pageY - posOld;
				} else if (posNew - posOld > drag.height() - 1){
					drag.insertAfter(drag.next().css({'top':drag.height()}).animate({'top':0}, 100));
					drag.css({'top':0});
					posOld = drag.offset().top;
					posOldCorrection = e.pageY - posOld;
				}
			}
		};
		var mouseUp = function(){
			$(document).off('mousemove', mouseMove).off('mouseup', mouseUp);
			$(document).off('mousedown', disableSelection);
            drag.animate({'top':0}, 100, function(){
				drag.removeClass('dragActive');
				//Запишем
				arr_el = $("#sortmenu .drag");
				spisok = '';
				for(var i=0; i<arr_el.length; i++) spisok=spisok+$(arr_el[i]).attr('znach')+";";
				$.post("savesort.php",{s:spisok,t:$("#sortmenu").attr('znach')}, function(data) {
					
					});
	        });
			
		
        
        };
		$(document).on('mousemove', mouseMove).on('mouseup', mouseUp).on('contextmenu', mouseUp);
		$(document).on('mousedown', disableSelection);
        $(window).on('blur', mouseUp);
    });
}