<script>
$(document).ready(function(){
    $('.view-source .update-info').hide();
    $('.view-source .update-section').toggle(
      function(){
        $(this).siblings('.update-info').stop(false, true).slideDown(500);
        $(this).siblings('.update-icon').css({"transform": "rotate(135deg)", "top": "25px"});
        $(this).css({"border-radius": "10px 10px 0 0"});
        $(this).siblings('.update-info').css("border-top", "1px solid #000000");
      },
     function(){
        var v = $(this);
        function bor() {v.css("border-radius", "10px")};
        $(this).siblings('.update-info').stop(false, true).slideUp(500);
        $(this).siblings('.update-icon').css({"transform": "rotate(-45deg)", "top": "5px"});
        setTimeout(bor, 500);
     }
   );
});
</script>

<div style="min-height: 68vh;">
<div style="margin-top:79px; margin-bottom:10px; background:#008DD2;" align="center">
<div align="left" style="width:960px; height:69px; line-height:69px;"><h1 style="color:#FFF; text-align:left; padding:0px; margin:0px; font-size:18pt;">ОБНОВЛЕНИЯ</div>
</div>
  <div class="updates">
    <? 
    $q = $pdo->query("SELECT * FROM qf_sections_updates");
    while ($res = $q->fetch()) {
    ?>
      <div class="view-source">
        <div class="update-section"><?=$res['qf_name']?></div>
        <span class="update-icon"></span>
        <div class="update-info">
          <?
          $upd = $pdo->query("SELECT * FROM qf_updates");
          while ($resUpd = $upd->fetch()) {
            if($resUpd['qf_section'] == $res['qf_id']) {
          ?>
            <div class="update-block">
              <div class="item-top">
                <div class="item-name">
                  <span><a href="<?=$resUpd['qf_link']?>"><?=$resUpd['qf_name']?></a></span>
                </div>
                <div class="item-size">
                  <span>
                    <? if (($resUpd['qf_size'] <> "Нерабочая ссылка") && ($resUpd['qf_size'] <> "Нет соединения") && ($resUpd['qf_size'] <> "Не для скачивания")) {?>
                      ( <?=$resUpd['qf_size']?> )
                    <? } ?>
                  </span>
                </div>
                <div class="item-date">
                  <span><?=$resUpd['qf_date']?></span>
                </div>
              </div>
              <div class="item-description">
                <span class="head">Описание:</span>
                <div class="block-description"><?=$resUpd['qf_description']?></div>
              </div>
            </div>
          <? } 
          }
          ?>
        </div>
      </div>
    <? } ?>
    
  </div>
</div>
<? include 'footer.php';?>