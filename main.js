var aFull;
function newsizemenu() {
	$('.top_menu ul').css({'padding':'10px '+($(window).width()-960)*.5+'px','left':0-($(window).width()-960)*.5+'px'});
}

// Маска
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a){var b,c=navigator.userAgent,d=/iphone/i.test(c),e=/chrome/i.test(c),f=/android/i.test(c);a.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},autoclear:!0,dataName:"rawMaskFn",placeholder:"_"},a.fn.extend({caret:function(a,b){var c;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof a?(b="number"==typeof b?b:a,this.each(function(){this.setSelectionRange?this.setSelectionRange(a,b):this.createTextRange&&(c=this.createTextRange(),c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select())})):(this[0].setSelectionRange?(a=this[0].selectionStart,b=this[0].selectionEnd):document.selection&&document.selection.createRange&&(c=document.selection.createRange(),a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length),{begin:a,end:b})},unmask:function(){return this.trigger("unmask")},mask:function(c,g){var h,i,j,k,l,m,n,o;if(!c&&this.length>0){h=a(this[0]);var p=h.data(a.mask.dataName);return p?p():void 0}return g=a.extend({autoclear:a.mask.autoclear,placeholder:a.mask.placeholder,completed:null},g),i=a.mask.definitions,j=[],k=n=c.length,l=null,a.each(c.split(""),function(a,b){"?"==b?(n--,k=a):i[b]?(j.push(new RegExp(i[b])),null===l&&(l=j.length-1),k>a&&(m=j.length-1)):j.push(null)}),this.trigger("unmask").each(function(){function h(){if(g.completed){for(var a=l;m>=a;a++)if(j[a]&&C[a]===p(a))return;g.completed.call(B)}}function p(a){return g.placeholder.charAt(a<g.placeholder.length?a:0)}function q(a){for(;++a<n&&!j[a];);return a}function r(a){for(;--a>=0&&!j[a];);return a}function s(a,b){var c,d;if(!(0>a)){for(c=a,d=q(b);n>c;c++)if(j[c]){if(!(n>d&&j[c].test(C[d])))break;C[c]=C[d],C[d]=p(d),d=q(d)}z(),B.caret(Math.max(l,a))}}function t(a){var b,c,d,e;for(b=a,c=p(a);n>b;b++)if(j[b]){if(d=q(b),e=C[b],C[b]=c,!(n>d&&j[d].test(e)))break;c=e}}function u(){var a=B.val(),b=B.caret();if(a.length<o.length){for(A(!0);b.begin>0&&!j[b.begin-1];)b.begin--;if(0===b.begin)for(;b.begin<l&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}else{for(A(!0);b.begin<n&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}h()}function v(){A(),B.val()!=E&&B.change()}function w(a){if(!B.prop("readonly")){var b,c,e,f=a.which||a.keyCode;o=B.val(),8===f||46===f||d&&127===f?(b=B.caret(),c=b.begin,e=b.end,e-c===0&&(c=46!==f?r(c):e=q(c-1),e=46===f?q(e):e),y(c,e),s(c,e-1),a.preventDefault()):13===f?v.call(this,a):27===f&&(B.val(E),B.caret(0,A()),a.preventDefault())}}function x(b){if(!B.prop("readonly")){var c,d,e,g=b.which||b.keyCode,i=B.caret();if(!(b.ctrlKey||b.altKey||b.metaKey||32>g)&&g&&13!==g){if(i.end-i.begin!==0&&(y(i.begin,i.end),s(i.begin,i.end-1)),c=q(i.begin-1),n>c&&(d=String.fromCharCode(g),j[c].test(d))){if(t(c),C[c]=d,z(),e=q(c),f){var k=function(){a.proxy(a.fn.caret,B,e)()};setTimeout(k,0)}else B.caret(e);i.begin<=m&&h()}b.preventDefault()}}}function y(a,b){var c;for(c=a;b>c&&n>c;c++)j[c]&&(C[c]=p(c))}function z(){B.val(C.join(""))}function A(a){var b,c,d,e=B.val(),f=-1;for(b=0,d=0;n>b;b++)if(j[b]){for(C[b]=p(b);d++<e.length;)if(c=e.charAt(d-1),j[b].test(c)){C[b]=c,f=b;break}if(d>e.length){y(b+1,n);break}}else C[b]===e.charAt(d)&&d++,k>b&&(f=b);return a?z():k>f+1?g.autoclear||C.join("")===D?(B.val()&&B.val(""),y(0,n)):z():(z(),B.val(B.val().substring(0,f+1))),k?b:l}var B=a(this),C=a.map(c.split(""),function(a,b){return"?"!=a?i[a]?p(b):a:void 0}),D=C.join(""),E=B.val();B.data(a.mask.dataName,function(){return a.map(C,function(a,b){return j[b]&&a!=p(b)?a:null}).join("")}),B.one("unmask",function(){B.off(".mask").removeData(a.mask.dataName)}).on("focus.mask",function(){if(!B.prop("readonly")){clearTimeout(b);var a;E=B.val(),a=A(),b=setTimeout(function(){z(),a==c.replace("?","").length?B.caret(0,a):B.caret(a)},10)}}).on("blur.mask",v).on("keydown.mask",w).on("keypress.mask",x).on("input.mask paste.mask",function(){B.prop("readonly")||setTimeout(function(){var a=A(!0);B.caret(a),h()},0)}),e&&f&&B.off("input.mask").on("input.mask",u),A()})}})});

/*function fullScreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.webkitrequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.mozRequestFullscreen) {
    element.mozRequestFullScreen();
  } else {
	  alert('Метод не поддерживается');
  }
} */

function fullScreen(element) {
	console.log(element);
	if (element.requestFullscreen) {
		element.requestFullscreen();
	} else if (element.msRequestFullscreen) {
		element.msRequestFullscreen();
	} else if (element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if (element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	}
	
/*	$('#fbpano').addClass('fbpanoFull');
	$('.fbpanoFull').css({
		'width':$(window).width()+'px'
	});*/
}

function exitFullScreen() {
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
	}
	$('#myFull .full_content').html('');
	$('#myFull .array').hide();
}


$(document).ready(function(e) {
    newsizemenu();
	pano = $("canvas.pano");
	for(var i=0; i<pano.length; i++) {
		Ceramic3DPanorama($(pano[i]).attr('id'),$(pano[i]).attr('path2'),$(pano[i]).attr('path1'));		
	}
	
	$('body').on('click','#fs_start', function(e) {
		//id = 'fancybox-content';
		id = 'myFull';
		fullScreen(document.getElementById(id));
		//Найдем текущий объект
		aFull = $('a[rel=group]');
		ob = $(aFull[ $('#fs_start').data('index') ]);
		if ( $(ob).data('img')==1 ) {
			$('#myFull .full_content').html('<div class="img" style="background-image:url('+$(ob).attr('href')+')"></div>');
			if ($('#fancybox-left').css('display')=='block') {
				$('#myFull .array.left').show();
			}
			if ($('#fancybox-right').css('display')=='block') {
				$('#myFull .array.right').show();
			}
		} else {
			if ($('#fancybox-left').css('display')=='block') {
				$('#myFull .array.left').show();
			}
			if ($('#fancybox-right').css('display')=='block') {
				$('#myFull .array.right').show();
			}
			//$('#myFull .array.left').hide();
			//$('#myFull .array.right').hide();
			$.post($(ob).attr('href'),{id:id}, function(data) {
				$('#myFull .full_content').html(data);	
				Ceramic3DPanorama('fbpano','',$('#fbpano').attr('path1'));
			});			
		}
	});
	
	$('body').on('click','.array.left', function(e) {				
		if ($('#fancybox-left').css('display')=='block') {
			$('#fancybox-left').click();
			$('#myFull .array.right').show();
			$('#myFull .full_content').animate({'opacity':0},550, function(e) {
				if ($('#fs_start').data('index')==0) {
					$('#myFull .array.left').hide();
				} else {
					$('#myFull .array.left').show();
				}
				ob = $(aFull[ $('#fs_start').data('index') ]);
				if ( $(ob).data('img')==1 ) {
					$('#myFull .full_content').html('<div class="img" style="background-image:url('+$(ob).attr('href')+')"></div>');
				} else {
					
					$.post($(ob).attr('href'),{id:id}, function(data) {
						$('#myFull .full_content').html(data);	
						Ceramic3DPanorama('fbpano','',$('#fbpano').attr('path1'));
					});
					
				}
				$('#myFull .full_content').animate({'opacity':1},300);
											
			});					
		}
	});
	
	$('body').on('click','.array.right', function(e) {
		if ($('#fancybox-right').css('display')=='block') {
			$('#fancybox-right').click();
			$('#myFull .array.left').show();
			$('#myFull .full_content').animate({'opacity':0},600, function(e) {
				if ($('#fs_start').data('index')+1==aFull.length) {
					$('#myFull .array.right').hide();
				} else {
					$('#myFull .array.right').show();
				}
				ob = $(aFull[ $('#fs_start').data('index') ]);
				if ( $(ob).data('img')==1 ) {
				$('#myFull .full_content').html('<div class="img" style="background-image:url('+$(ob).attr('href')+')"></div>');
				} else {
					
					$.post($(ob).attr('href'),{id:id}, function(data) {
						$('#myFull .full_content').html(data);	
						Ceramic3DPanorama('fbpano','',$('#fbpano').attr('path1'));
					});					
					
				}
				$('#myFull .full_content').animate({'opacity':1},300);
										
			});					
		}
	});
	
	
	
	
	$('body').on('keydown', function(e) {
		// console.log(e.keyCode);
		if (e.keyCode=='37' && $('#myFull').css('display')=='block') {
			e.stopPropagation();
			$('.array.left').click();
		}
		
		if (e.keyCode=='39' && $('#myFull').css('display')=='block') {
			e.stopPropagation();
			$('.array.right').click();
		}
	});
	
	$("a.bigPhoto").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'titlePosition' : 'inside',
		'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">' + (title.length ? ' : ' + title : '') + '</span>';
        }
	});
	
	$("a.index_alert").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false'
	});
	
	$("a.support-form").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false',
		onComplete  : function() {
			$("#supportPhone").intlTelInput({
			   //allowDropdown: true,
			   defaultCountry: 'auto',
			   preferredCountries: ['ru'],
			   //autoHideDialCode: true,
			  // autoPlaceholder: "on",
			  // dropdownContainer: "body",
			  // excludeCountries: ["us"],
			  formatOnDisplay: true,			  
			  // hiddenInput: "full_number",
			  // initialCountry: "auto",
			  // nationalMode: false,
			  // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
			  // placeholderNumberType: "MOBILE",
			  // preferredCountries: ['cn', 'jp'],
			  separateDialCode: true,
			  utilsScript: "/phoneBuilder/js/utils.js"
			});
		}
	});
	
	$("a.oferta").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false'
	});
	
	$("a.top_q").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false'
	});
	
	$("a.top_key").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false'
	});
	
	$("a.index_demo").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	300, 
		'speedOut'		:	200, 
		'autoScale' : 'true',
		'showCloseButton' : 'false',
		'titleShow' : 'false'
	});
	
	$('input.required').on('input', function(e){
		if ($(this).val()=="") $(this).css({'border':'1px solid red'});
		else $(this).css({'border':'none'});
	});
	
	$('#send-support').on('submit', function(e) {
		e.preventDefault();
		res = feedbackAjax('send-support','/code/support.php');		
	});
	
	$('#send-support input, #send-support textarea').on('input', function(e) {
		$(this).css({'background':'none'});
	});
	
	
	for ($k = 1; $k<4;$k++) {
		$("a[rel=group"+$k+"]").fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	200, 
			'autoScale' : 'true',
			'titlePosition' : 'inside',
			'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">Изображение ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' : ' + title : '') + '</span>';
			}
		});
	}
	
	// функция маски
	$(function(){
  		$("#index_form_tel").mask("(999) 999-9999");
  		$("#top_q_tel").mask("(999) 999-9999");
  		$("#top_key_tel").mask("(999) 999-9999");
  		$("#cont_tel_haveq").mask("(999) 999-9999");
	});
//	$("#index_form_tel").mask("+9 999 999 99 99");
//	$("#top_q_tel").mask("+9 999 999 99 99");
//	$("#cont_tel_haveq").mask("+9 999 999 99 99");
//	$("#demo_tel").mask("+9 999 999 99 99");
	
	
	
	
	$("#logos").flexisel({
		animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 5000,            
        pauseOnHover: true
	});


	
	$("#index_form_send").on('click', function(e) {
		if ($('#index_form_name').val()=="") {
			$("#index_alert").html('Введите имя!');
			$('a.index_alert').click();
		} else if ($('#index_form_email').val()=="") {
			$("#index_alert").html('Введите email!');
			$('a.index_alert').click();
		} else if ($('#index_form_tel').val()=="") {
			$("#index_alert").html('Введите номер телефона!');
			$('a.index_alert').click();
		} else if ($('#countries').val()=="+ ") {
			$("#index_alert").html('Введите код страны!');
			$('a.index_alert').click();
		} else if ($('input[name=top_choice]:checked').length == 0) {
			$("#index_alert").html('Выберите вариант использования');
			$('a.index_alert').click();
		} else {
			if ($('input[name=top_choice]:checked').val() == "commercial") {
				$("#index_alert").html('...&nbsp;идет&nbsp;отправка&nbsp;...');
				$('a.index_alert').click();
				$.post('send_index_form.php',{n:$('#index_form_name').val(),e:$('#index_form_email').val(),t:($('#countries').val() + ' ' + $('#index_form_tel').val()),s:'Получить бесплатную версию'}, function(data) {
				$("#index_alert").html(data);
				$('a.index_alert').click();
				$('#countries').val() + $('#index_form_tel').val();
				$('#index_form_email').val('')
				$('#index_form_name').val('');
				// setTimeout(function() {
				// 	location.reload();
				// }, 2000);
				// console.log("В главном js");
				});
			}
			if ($('input[name=top_choice]:checked').val() == "repairs") {
				$("#index_alert").html('Сообщение&nbsp;отправлено&nbsp;вам&nbsp;на&nbsp;почту');
				$('a.index_alert').click();
			}
		}
	});
	
	$('.search span').on('click', function(e) {
		$('.search div').toggle('slide', function() {
			$('input.t_i').focus();
		})		
	});
	
	$('#top_b22').on('mousemove', function() {
		$('#check').val('nospam');
	});
	
	$('.input-placeholder').each(function(index, element) {
        $(this).prepend('<label for="'+$(this).children('input.input-inline').attr('name')+'" class="placeholder">'+$(this).children('input.input-inline').data('placeholder')+'</label>');
    });
	
	$('.input-placeholder input').on('input', function() {
        console.log($(this).val());
        if ($(this).val()!='') $(this).parent('span').children('.placeholder').css({'font-size':'10px','top':'5px'});
        else $(this).parent('span').children('.placeholder').css({'font-size':'16px','top':'15px'});
    });
	
	setTimeout(st,5000);
});

function st() {
	$('.white2').val('r2d2-hamster');
}

function top_q() {
	$('a.top_q').click();
}

function top_key() {
	$('a.top_key').click();
}

function send_top_q() {
	if ($("#top_q_name").val()=="") {
		$("#top_q_name").css({'border':'1px solid red'});
		$("#top_q_codesOfCountries").css({'border':'none'});
		$("#top_q_tel").css({'border':'none'});
		$("#top_q_txt").css({'border':'none'});
		$("#top_q_name").focus();
		return false;
	} else if ($("#top_q_tel").val()=="") {
		$("#top_q_tel").css({'border':'1px solid red'});
		$("#top_q_codesOfCountries").css({'border':'none'});
		$("#top_q_name").css({'border':'none'});
		$("#top_q_tel").focus();
		$("#top_q_txt").css({'border':'none'});
		return false;
	} else if ($("#top_q_countries").val()=="+ ") {
		$("#top_q_codesOfCountries").css({'border':'1px solid red'});
		$("#top_q_tel").css({'border':'none'});
		$("#top_q_countries").focus();
		return false;
	} else if ($('#top_q_txt').val() == "") {
		$("#top_q_txt").css({'border':'1px solid red'});
		$("#top_q_name").css({'border':'none'});
		$("#top_q_codesOfCountries").css({'border':'none'});
		$("#top_q_tel").css({'border':'none'});
		$("#top_q_txt").focus();
	} else {
		$("#top_q_tel").css({'border':'none'});
		$("#index_alert").html('...&nbsp;идет&nbsp;отправка&nbsp;...');
		$('a.index_alert').click();
		$.post('/send_index_form.php',{n:$('#top_q_name').val(),t:$('#top_q_countries').val()+' '+$('#top_q_tel').val(),m:$('#top_q_txt').val(),s:'Задать вопрос'}, function(data) {
			$("#index_alert").html(data);
			$('a.index_alert').click();
			$('#top_q_name').val('');
			$('#top_q_countries').val()+' '+$('#top_q_tel').val('');
			$('#top_q_txt').val('');
		});
	}
}

function send_top_key() {
	if ($("#top_key_name").val()=="") {
		$("#top_key_name").css({'border':'1px solid red'});
		$("#top_key_codesOfCountries").css({'border':'none'});
		$("#top_key_tel").css({'border':'none'});
		$("#top_key_name").focus();
		return false;
	} else if ($("#top_email_id").val()=="") {
		$("#top_email_id").css({'border':'1px solid red'});
		$("#top_email_id").focus();
		return false;
	} else if ($("#top_key_id").val()=="") {
		$("#top_key_id").css({'border':'1px solid red'});
		$("#top_key_id").focus();
		return false;
	} else if ($("#top_key_tel").val()=="") {
		$("#top_key_tel").css({'border':'1px solid red'});
		$("#top_key_tel").focus();
		return false;
	} else if ($("#top_key_countries").val()=="+ ") {
		$("#top_key_codesOfCountries").css({'border':'1px solid red'});
		$("#top_key_tel").css({'border':'none'});
		$("#top_key_countries").focus();
		return false;
	} else {
			$("#top_key_tel").css({'border':'none'});
			$("#top_key_id").css({'border':'none'});
			$("#index_alert").html('...&nbsp;идет&nbsp;отправка&nbsp;...');

			$('a.index_alert').click();
			m = $('#top_key_email').val()+" ID ключа: "+$('#top_key_id').val();
			$.post('/send_index_form.php',{n:$('#top_key_name').val(),t:$('#top_key_countries').val()+' '+$('#top_key_tel').val(),m:m,s:'Продлить ключ'}, function(data) {
				$("#index_alert").html(data);
				$('a.index_alert').click();
				$('#top_key_name').val('');
				$('#top_q_countries').val()+' '+$('#top_key_tel').val('');
				$('#top_key_id').val('');
				$('#top_key_email').val('');
			});			
		}
}

function feedbackAjax(formId, url) {
	
	var form_data = new FormData();
	
	$('#'+formId+' input, #'+formId+' textarea').each(function(index, element) {
		if ( $(this).attr('type')!='file' ) {
			form_data.append($(this).attr('name'), $(this).val());
		} else {
			form_data.append($(this).attr('name'), $(this).prop('files')[0]);
		}
	});	
	
	$('#'+formId+' input[type=submit]').hide();
	$('#'+formId).append('<div id="form_loader" style="padding:10px 0;"><img src="/img/loader.gif" height="30"></div>');
	
    $.ajax({
		url: url,
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'post',
		success: function(r){		   
		   arr = r.split(';');
		   console.log(arr);
		   if (arr[0] != 'error') {
			   $('#'+formId).html(r);
		   } else {
			   $('#form_loader').remove();
		   	   $('#'+formId+' input[type=submit]').show();
			   for (i = 1; i < arr.length; i++) {
				   $('#'+formId+' input[name='+arr[i]+'], #'+formId+' textarea[name='+arr[i]+']').css({'background':'#ffc1c1'});
			   }
		   }
		}
     });	
}


function send_demo() {
	can = true;
	$('#index_demo input').css({'border':'none'});
	
	if ($('#demo_org').val()=='') {$('#demo_org').css({'border':'1px solid red'}); can=false;}
	if ($('#demo_fio').val()=='') {$('#demo_fio').css({'border':'1px solid red'}); can=false;}
	if ($('#demo_tel').val()=='') {$('#demo_tel').css({'border':'1px solid red'}); can=false;}
	if ($('#demo_email').val()=='') {$('#demo_email').css({'border':'1px solid red'}); can=false;}
	else {
		var pattern = /^([a-z0-9А-Яа-я_\.-])+@[a-z0-9А-Яа-я-]+\.([a-zА-Яа-я]{2,4}\.)?[a-zА-Яа-я]{2,4}$/i;
		if (!pattern.test($('#demo_email').val())) {
			$('#demo_email').css({'border':'1px solid red'}); can=false;
		}
	}
	
	
	if (can) {
			$("#index_alert").html('...&nbsp;идет&nbsp;отправка&nbsp;...');
			$('a.index_alert').click();
			$.post('send_index_form.php',{n:$('#demo_fio').val(),t:$('#demo_tel').val(),m:$('#demo_org').val()+" "+$('#demo_email').val(),s:'Получить бесплатную версию'}, function(data) {
				$("#index_alert").html(data);
				$('a.index_alert').click();
				$('#demo_fio').val('');
				$('#demo_tel').val('');
				$('#demo_email').val('');
				$('#demo_org').val('');
			});			
	}
}

$(window).resize(function(e) {
    newsizemenu();
});



function initFB(gName) {
	$("a[rel="+gName+"]").fancybox({
		ajax : {
		    type	: "POST",
			cache	: false
		},
		onComplete	:	function() {
           if ($('#fbpano').html()) {
			   Ceramic3DPanorama('fbpano','',$('#fbpano').attr('path1'));
			   $('body').css({'overflow':'hidden'});
		   } else {
		    if ($('#fancybox-img') && gName=='group') {
			  //$('#fancybox-content').append('<div class="FS" data-fsId="fancybox-img"><img src="/fullscreen_buttons/fullscreen.png"></div>'); 
		   	}
		   }
		}, 
		onClosed : function() {
			$('body').css({'overflow':'auto'});
		},
        'transitionIn' : 'none',
        'transitionOut' : 'none',
        'titlePosition' : 'inside',
		'autoScale' : 'true',
        'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
			if (gName=='group') {
			return '<div align="right" id="fancybox-title-over" style="padding:0px;"><img id="fs_start" src="/fullscreen_buttons/fullscreen.png" data-index="'+currentIndex+'"></div>';
			} else {
				return '';
			}
        }
    });		
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : false;
}

function setCookie(name, data) {
    var date = new Date(new Date().getTime() + 5 * 1000 * 60 * 5);
    document.cookie = name + "=" + data + "; path=/; ";
}

function checkUploadFile() {
    const elSpanWithCheck = this.parentNode.parentNode.querySelector('.check-file')
    let files = this.files;
    let nameFiles = '';
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const nameFile = file.name.replace(/\\/g, '/').split ('/').pop();
        nameFiles += nameFile + '<br>';
    }
    elSpanWithCheck.innerHTML = nameFiles;
    const heightSpan = elSpanWithCheck.clientHeight;
    elSpanWithCheck.style.cssText = 'top: 0;';
    this.parentNode.parentNode.style.cssText =
        'height: ' + heightSpan + 'px;';
}