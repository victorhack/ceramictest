<link href="movingboxes.css" rel="stylesheet">
<script src="jquery.movingboxes.js"></script>
<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 	var device="mobile";
}else{
	var device="desktop";
}
$(function(){

		$('#slider').movingBoxes({
			/* width and panelWidth options deprecated, but still work to keep the plugin backwards compatible
			width: 500,
			panelWidth: 0.5,
			*/
			startPanel   : 2,      // start with this panel
			wrap         : true,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
			buildNav     : false,   // if true, navigation links will be added
			navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
		});

	});
	
$(document).ready(function(e) {	
    $('.c_team').bind('mouseover', function(e) {
		
		<?
		$start = '';
		$start2 = '';
		$q = $pdo->query("SELECT * FROM qf_about WHERE qf_type=1 ORDER BY qf_id");
		while ($res = $q->fetch()) {
			if ($res['qf_id']==1) {$start = $res['qf_z2'];$start2 = $res['qf_z1'];}
		?>
		if ($(this).attr('data')==<?=$res['qf_id']?>) {
			$('#t1').html('<?=$res['qf_z2']?>');
			$('#t2').html('<?=$res['qf_z1']?>');
		}		
		<? } ?>
	});
});
</script>
</head>
<body>
<? include 'top.php'; ?>
<h2 style="font-size:24pt; text-align:center; padding:99px 0 30px 0;">Наша команда</h2>

<div align="center">
<div id="c_text" style="padding:0 0 15px 0; width:800px; min-height:150px;">
<p id="t1" style="font-size:15pt;"><?=$start?></p>
<p id="t2" style="text-align:right; color:#008DD2; font-size:14pt;"><?=$start2?></p>
</div>

<div style="width:960px;">
<img src="about/1_1.jpg" class="c_team" data=1>
<img src="about/2_1.jpg" class="c_team" data=2>
<img src="about/3_1.jpg" class="c_team" data=3>
<div style="clear:both; float:none"></div>
</div>
</div>

<div style="background:#ededed; padding:30px 0;" align="center"><div style="width:960px;" align="left">
<?=$arr_g['company']['txt']?>
</div></div>


<div style="background:#008DD2; padding:30px 0;" align="center" id="sliderteam">
<h1 style="font-size:24pt; color:#FFF; text-align:center; padding:0 0 15px 0; margin:0;">Компания в лицах</h1>

<div style="width:960px;">
<!-- MovingBoxes Slider -->
	<ul id="slider">
    	<?
        $q = $pdo->query("SELECT * FROM qf_about WHERE qf_type=2 ORDER BY qf_sort");
		while($res = $q->fetch()) {
		?>
        <li itemscope itemtype="http://schema.org/ImageObject">
        	<img itemprop="contentUrl" src="http://<?=$_SERVER['HTTP_HOST']?>/about/<?=$res['qf_id']?>.jpg" alt="<?=$res['qf_z1']?>" title="<?=$res['qf_z1']?>">
            <h2 itemprop="name"><?=$res['qf_z1']?></h2>
            <p itemprop="description"><?=$res['qf_z2']?></p>
        </li>        
        <? } ?>     
	</ul>
<!-- end Slider #1 -->
</div>
</div>

<div class="bg" style="background-image:url(img/mention.jpg); padding:0px;" align="center">
<div style="background:rgba(0,0,0,.7)">
<h1 style="color:#FFF; font-size:24pt; text-align:center;"><br>
НАЧНИТЕ ПОЛЬЗОВАТЬСЯ ПРОГРАММОЙ УЖЕ СЕЙЧАС!</h1>
<h1 style="font-size:24pt; padding:0; text-align:center" align="center">402 665 активных пользователей!</h1>
<a class="but_2 index_demo" href="#index_demo" style="margin-top:30px; padding:10px 60px; color:#FFF; border-color:#FFF; font-size:17pt; text-decoration:none">ЗАКАЗАТЬ ДЕМО-ВЕРСИЮ ПРОГРАММЫ</a>


<div style="margin:30px 0; width:500px; padding:15px; background:rgba(255,255,255,.7); -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;" id="faveaq">
<div style="text-align:center; text-transform:uppercase; font-size:18pt;">Написать письмо</div>
<? if (isset($_POST['mail-send'])) {
	$send = true;
	if ($_POST['cont_name']=="") {$send = false; $mes = '- Введите ваше имя!<br>';}
	if (!filter_var(trim($_POST['cont_mail']), FILTER_VALIDATE_EMAIL)) {$send = false; $mes .= '- Введите корректный e-mail';}
	if ($send) {
		sendmail('Сообщение с сайта','<b>Отправитель</b> '.htmlspecialchars(trim($_POST['cont_name'])).' ('.trim($_POST['cont_mail']).')<br><b>Сообщение</b><p>'.htmlspecialchars(trim($_POST['cont_txt'])).'</p>',$arr_g['cont']['email']);
		echo '<h4 style="color:green;">Ваше сообщение отправлено!</h4>';
	} else {
		'<h4 style="color:red;">Ошибка!</h4><p style="color:red">'.$mes.'</p>';
	}
}?>
<? if (!$send) {?>
<form enctype='multipart/form-data' name="send-contacts" id="send-contacts" method="post" action="<?="http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>#faveaq">
<input class="black" name="cont_name" placeholder="ФИО" autocomplete="off" required value="<?=htmlspecialchars(trim($_POST['cont_name']))?>">
<input class="black" name="cont_mail" placeholder="E-mail" autocomplete="off" type="email" required  value="<?=htmlspecialchars(trim($_POST['cont_mail']))?>">
<textarea class="black" placeholder="Сообщение" name="cont_txt"><?=htmlspecialchars(trim($_POST['cont_txt']))?></textarea>
<input type="submit" class="but_2" style="margin-top:15px; padding:5px; color:#FFF; border-color:#FFF; font-size:14pt; background:#333;" value="Отправить" name="mail-send"></form>
<? } ?>
</div>




<? include 'footer.php';?>
</div>



</body>
</html>